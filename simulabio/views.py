#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015  Étienne Loks  <etienne.loks@proxience.com>

import os
import datetime
import ho.pisa as pisa
import json
import StringIO
import tempfile

from ooopy.OOoPy import OOoPy
from ooopy.Transformer import Transformer
import ooopy.Transforms as Transforms

from django import forms as django_forms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.signals import user_logged_out
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.db import IntegrityError, transaction
from django.db.models import Q
from django.forms.models import inlineformset_factory, modelformset_factory
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import RequestContext
from django.template.defaultfilters import slugify
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.utils.functional import curry
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.views.generic import ListView, TemplateView, RedirectView, View
from django.views.generic.edit import CreateView, UpdateView, DeleteView, \
    FormView
from django.test.client import RequestFactory

from extra_views import InlineFormSetView

from simulabio import forms
from simulabio import models
from simulabio import widgets
from tasks import preload_study, compute
from compute import Compute


def clear_session(sender, request, user, **kwargs):
    if not user or not hasattr(user, 'simulabio_user') \
       or not user.simulabio_user.count():
        return
    simulabio_user = user.simulabio_user.all()[0]
    remote_addr = '0.0.0.0'
    if "REMOTE_ADDR" in request.META:
        remote_addr = request.META['REMOTE_ADDR']
    user_agent = 'empty-user-agent'
    if "HTTP_USER_AGENT" in request.META:
        # 4ko is enough
        user_agent = request.META['HTTP_USER_AGENT'][:4096]
    if simulabio_user.current_ip == remote_addr and \
       simulabio_user.current_agent == user_agent:
        # reinit
        simulabio_user.current_ip = None
        simulabio_user.current_agent = None
        if simulabio_user.current_session:
            simulabio_user.current_session.delete()
            simulabio_user.current_session = None
        simulabio_user.save()

user_logged_out.connect(clear_session)

# utilities


class SwitchItem(object):
    def __init__(self, value, label):
        self.value, self.label = value, label


def tuple_to_switchlist(tupl):
    return [SwitchItem(value, label) for value, label in tupl]


def get_or_create(cls, fltr):
    # TODO: check why it is necessary
    q = cls.objects.filter(**fltr).order_by('pk')
    if q.count() > 1:
        for obj in q.all()[1:]:
            obj.delete()
    obj, created = cls.objects.get_or_create(**fltr)
    return obj


class CSVFile(View):
    template = 'template.csv'
    filename = 'filename.csv'
    data_class = ''

    def get(self, request):
        dct = {'data': '', 'separator': settings.CSV_DELIMITER}
        if self.data_class:
            # TODO: catch more precisely the error
            try:
                dct['data'] = getattr(models, self.data_class).objects\
                    .filter(available=True).all()
            except:
                dct['data'] = getattr(models, self.data_class).objects.all()
        response = HttpResponse(
            render_to_string(self.template, dct).encode(settings.ENCODING),
            mimetype='text/csv; charset=%s' % settings.ENCODING)
        response['Content-Disposition'] = \
            'attachment; filename=%s' % self.filename
        return response

# generic views


class ExtraYearFormConf:
    def __init__(self, year_model, year_form, study_key='study',
                 parent_model=None, can_delete=True, extra_filter={},
                 extra_exclude={}, parent_study_key='study'):
        self.year_model = year_model
        self.year_form = year_form
        self.study_key = study_key
        self.parent_study_key = parent_study_key
        self.parent_model = parent_model
        self.related_parent_name = None
        self.can_delete = can_delete
        self.extra_filter = extra_filter.copy()
        self.extra_exclude = extra_exclude.copy()
        if self.parent_model:
            for field in self.year_model._meta.fields:
                if hasattr(field, 'related') \
                   and field.related.parent_model == self.parent_model:
                    self.related_parent_name = field.name


class ExtraYearFormsView(TemplateView):
    """
    Manage many "model forms" for each year
    """
    # list of ExtraYearFormConf
    year_models = []

    def get_years(self, **kwargs):
        self.years = ['D'] + \
            [unicode(y + 1) for y in xrange(self.study.transition_years)] + \
            ['P']

    def get_context_data(self, **kwargs):
        posted_year_forms = None
        if 'year_forms' in kwargs:
            posted_year_forms = kwargs.pop('year_forms')
        context_data = super(ExtraYearFormsView, self).get_context_data(
            **kwargs)
        if not self.year_models:
            raise NotImplementedError(u"You should specify year_models")
        if not hasattr(self, 'study'):
            raise NotImplementedError(u"This class should have a study arg")
        if not hasattr(self, 'years'):
            self.get_years()
        """
        if year_forms:
            context_data['year_forms'] = year_forms
            for idx, year in enumerate(self.years):
                context_data['year_forms'][idx].verbose_name = \
                                                   dict(models.YEARS)[year]
            return context_data"""
        year_forms_list = []

        for ym in self.year_models:
            if not ym:
                continue
            if not ym.parent_model:
                c_year_forms = []
                if posted_year_forms:
                    c_year_forms = posted_year_forms
                    for idx, year in enumerate(self.years):
                        c_year_forms[idx].verbose_name = \
                            dict(models.YEARS)[year]
                else:
                    for year in self.years:
                        keys = {'study': self.study, 'year': year}
                        form_keys = {'prefix': 'year-' + year}
                        form_keys['instance'], created = \
                            ym.year_model.objects.get_or_create(**keys)
                        form = ym.year_form(**form_keys)
                        form.verbose_name = dict(models.YEARS)[year]
                        c_year_forms.append(form)
                year_forms_list.append(c_year_forms)
            else:
                # inherited model such as WorkshopExpenses doesn't give the
                # right name so force it
                parent_class_name = ym.parent_model.__name__ \
                    if hasattr(ym.parent_model, '__classname__') else \
                    ym.parent_model.__class__.__name__
                parent_name = parent_class_name + 'Form'
                parent_lbl = ym.parent_model.__name__
                q = ym.parent_model.objects
                if ym.parent_study_key:
                    q = ym.parent_model.objects.filter(
                        **{ym.parent_study_key:
                           getattr(self, ym.parent_study_key)})
                if ym.extra_filter:
                    q = q.filter(**ym.extra_filter)
                if ym.extra_exclude:
                    q = q.exclude(**ym.extra_exclude)
                for idx_obj, obj in enumerate(q.all()):
                    c_year_forms = []
                    # dynamicaly generate a readonly model form
                    meta = type('Meta', (object,), {'model': ym.parent_model})
                    hidden_fields = []
                    if ym.parent_study_key:
                        hidden_fields.append(ym.parent_study_key)
                    attrs = {
                        'Meta': meta,
                        '_hidden_fields': hidden_fields,
                        'DELETE': django_forms.BooleanField(required=False)
                    }
                    if hasattr(ym.parent_model, '_lbl_field'):
                        lbl_field = ym.parent_model._lbl_field
                        choices = None
                        field = ym.parent_model._meta.get_field_by_name(
                            lbl_field)[0]
                        if hasattr(field, 'related'):
                            q = field.related.parent_model.objects
                            choices = q.values_list('pk', 'name')
                        attrs['_readonly_fields'] = [(lbl_field, choices)]
                    parent_form = type(parent_name, (forms.BaseForm,), attrs)
                    form = parent_form(
                        instance=obj,
                        prefix="%s-%d" % (parent_lbl, obj.pk))
                    c_year_forms.append(form)
                    if posted_year_forms:
                        for idx, year in enumerate(self.years):
                            form = posted_year_forms[idx + idx_obj *
                                                     len(self.years)]
                            form.verbose_name = dict(models.YEARS)[year]
                            c_year_forms.append(form)
                    else:
                        for year in self.years:
                            keys = {'year': year,
                                    ym.related_parent_name: obj}
                            # assume that if the link with the study is not on
                            # the parent this should be on the childs
                            # very fragile...
                            if "study" not in ym.parent_study_key:
                                keys['study'] = self.study
                            form_keys = {'prefix':
                                         '%d-year-%s' % (obj.pk, year)}
                            form_keys['instance'], created = \
                                ym.year_model.objects.get_or_create(**keys)
                            form = ym.year_form(**form_keys)
                            form.verbose_name = dict(models.YEARS)[year]
                            c_year_forms.append(form)
                    year_forms_list.append(c_year_forms)
        context_data['year_forms'] = year_forms_list
        if len(self.year_models) == 1 and year_forms_list:
            context_data['year_forms'] = year_forms_list[0]
        return context_data

    def construct_forms(self):
        if not hasattr(self, 'years'):
            self.get_years()
        forms = []
        request = self.request
        for ym in self.year_models:
            if not ym:
                continue
            if not ym.parent_model:
                for year in self.years:
                    keys = {ym.study_key: self.study, 'year': year}
                    m, created = ym.year_model.objects.get_or_create(**keys)
                    form = ym.year_form(request.POST, instance=m,
                                        prefix='year-' + year)
                    forms.append(form)
            else:
                # parent_name = ym.parent_model.__class__.__name__ + 'Form'
                parent_lbl = ym.parent_model.__name__

                q = ym.parent_model.objects
                if ym.parent_study_key:
                    q = ym.parent_model.objects.filter(
                        **{ym.parent_study_key:
                           getattr(self, ym.parent_study_key)})
                if ym.extra_filter:
                    q = q.filter(**ym.extra_filter)
                if ym.extra_exclude:
                    q = q.exclude(**ym.extra_exclude)
                for obj in q.all():
                    if ym.can_delete and \
                       request.POST.get("%s-%d-DELETE" % (parent_lbl, obj.pk)):
                        obj.delete()
                        continue
                    for year in self.years:
                        keys = {'year': year,
                                ym.related_parent_name: obj}
                        form_keys = {'prefix': '%d-year-%s' % (obj.pk, year)}
                        form_keys['instance'], created = \
                            ym.year_model.objects.get_or_create(**keys)
                        form = ym.year_form(request.POST, **form_keys)
                        forms.append(form)
        return forms

    def post(self, request, *args, **kwargs):
        forms = self.construct_forms()
        # TODO manage form input errors
        ln, s_frm = 0, []
        for f in forms:
            if f.is_valid():
                ln += 1
            s_frm.append(f)
        if len(forms) == ln:
            for f in forms:
                f.save()
        else:
            return self.render_to_response(
                self.get_context_data(year_forms=s_frm))
        return HttpResponseRedirect('.')


class ExtraFormsMixin(object):
    extra_forms = {}

    def get_context_data(self, **kwargs):
        context_data = self.get_extra_forms(self.request)
        context_data.update(super(ExtraFormsMixin, self).get_context_data(
            **kwargs))
        return context_data

    def get_extra_forms(self, request, posted=False):
        extra_forms_data = {}
        for k in self.extra_forms.keys():
            if request.method == 'POST' or posted:
                extra_forms_data[k] = self.extra_forms[k](
                    request.POST, **self.get_extra_forms_kwargs())
            else:
                extra_forms_data[k] = self.extra_forms[k](
                    **self.get_extra_forms_kwargs())
        return extra_forms_data

    def get_extra_forms_kwargs(self, posted=False):
        kwargs = {'study': self.study}
        if self.request.method in ('POST', 'PUT') or posted:
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def construct_extra_forms(self, posted=False):
        return [self.extra_forms[k](**self.get_extra_forms_kwargs(posted))
                for k in self.extra_forms]


class ExtraYearsExtraFormsMixin(ExtraFormsMixin,
                                ExtraYearFormsView):

    def post(self, request, *args, **kwargs):
        forms = self.construct_forms()
        extra_forms = self.construct_extra_forms(posted=True)
        # TODO: extrayears form not checked...
        for f in forms:
            if f and f.is_valid():
                f.save()
        # valid = len(extra_forms) == len([f for f in extra_forms
        #                                  if f.is_valid()])
        for f in extra_forms:
            if f and f.is_valid():
                f.save()
        # TODO manage form input errors
        return HttpResponseRedirect('.')


class ExtraInlineFormSetView(ExtraFormsMixin, InlineFormSetView):
    """
    Manage several extra form in an InlineFormSetView
    """
    can_delete = True
    extra = 1

    def get_factory_kwargs(self, *args, **kwargs):
        kwargs = super(ExtraInlineFormSetView, self).get_factory_kwargs(
            *args, **kwargs)
        kwargs["can_delete"] = self.can_delete
        return kwargs

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        formset = self.construct_formset()
        extra_forms = self.construct_extra_forms()
        # valid = formset.is_valid() and \
        #         len(extra_forms) == len([f for f in extra_forms
        #                                  if f.is_valid()])
        valid = True
        if formset.is_valid():
            self.object_list = formset.save()
        else:
            valid = False
        for f in extra_forms:
            if f.is_valid():
                f.save()
            else:
                valid = False
        if valid:
            return self.formset_valid(formset)
        else:
            return self.formset_invalid(formset)


class InlineStudyFormSetView(InlineFormSetView):

    def construct_formset(self):
        return self.get_formset()(instance=self.object,
                                  **self.get_formset_kwargs())

    def get_formset(self):
        """
        Add current study to forms
        """
        formset = inlineformset_factory(self.model, self.inline_model,
                                        **self.get_factory_kwargs())
        formset.form = staticmethod(curry(self.get_form_class(),
                                          study=self.study))
        return formset

# print mixins


class CommentMixin(object):
    _comments = True  # comments can be put on that form
    comment_form = forms.StudyCommentForm
    _comment_form_name = ""

    def get_comment_context(self):
        return ""

    def get_current_comment(self, context=None):
        if not hasattr(self, 'study') or not self.study:
            return
        form_name = self.__class__.__name__
        if self._comment_form_name:
            form_name = self._comment_form_name
        if not context:
            context = self.get_comment_context()
        fltr = {'study': self.study, 'form': form_name,
                'context': context}
        try:
            with transaction.commit_on_success():
                comment = models.StudyComment.objects.create(**fltr)
        except IntegrityError:
            q = models.StudyComment.objects.filter(**fltr)
            comment = q.all()[0]
        return comment

    def get_context_data(self, **kwargs):
        context = super(CommentMixin, self).get_context_data(**kwargs)
        if self._comments:
            comment = self.get_current_comment()
            if comment:
                context['comment_form'] = self.comment_form(instance=comment)
        return context

    def comment_submission(self, request):
        comment = self.get_current_comment()
        if not comment:
            return
        form = self.comment_form(request.POST, request.FILES,
                                 instance=comment)
        if 'comment' in request.POST:
            if form.is_valid():
                form.save()
            else:
                self.errors = form.errors
            return redirect('.')

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(CommentMixin, self).post(
            request, *args, **kwargs)


class PrintableMixin(object):
    """
    To be used with a form view
    """
    def readonlyfy(self, form):
        # change fields of a form or a formset to readonly
        single_form = hasattr(form, 'fields')
        frms = [form]
        if not single_form:
            try:
                frms = form.forms
            except AttributeError:
                frms = form
        for frm in frms:
            visible_fields = [field.name for field in frm.visible_fields()]
            for field_key in frm.fields:
                if field_key not in visible_fields:
                    continue
                kwargs = {}
                widget = frm.fields[field_key].widget
                if hasattr(widget, 'choices') and widget.choices:
                    kwargs['choices'] = widget.choices
                frm.fields[field_key].widget = widgets.ReadOnlyWidget(**kwargs)
        if not single_form:
            return form
        return frms[0]

    def printing_kwargs(self, request, context, study):
        kwargs = {}
        if hasattr(self, 'get_queryset') and self.get_queryset:
            kwargs['object_list'] = self.get_queryset()
        if hasattr(self, 'get_form'):
            form_class = self.get_form_class()
            form = self.readonlyfy(self.get_form(form_class))
            kwargs['form'] = form
        elif hasattr(self, 'get_formset'):
            self.extra = 0
            formset = self.readonlyfy(self.construct_formset())
            kwargs['form'] = formset
            kwargs['formset'] = formset
        context.update(self.get_context_data(**kwargs))
        if 'year_forms' in context:
            context['year_forms'] = [self.readonlyfy(frm)
                                     for frm in context['year_forms']]
        if hasattr(self, 'extra_forms'):
            for k in self.extra_forms.keys():
                context[k] = self.readonlyfy(self.extra_forms[k](study=study))
        return kwargs

    def printing(self, request, context, study):
        self.dispatch(request, pk=study.pk)
        # form = None
        # kwargs = self.printing_kwargs(request, context, study)
        self.printing_kwargs(request, context, study)
        tpl = 'simulabio/print/base.html'
        if hasattr(self, 'print_template_name') and self.print_template_name:
            tpl = self.print_template_name
        elif hasattr(self, 'template_name') and self.template_name:
            tpl = self.template_name
        context['media_root'] = settings.MEDIA_ROOT
        return render_to_string(tpl, context)


class YearPrintableMixin(PrintableMixin):
    """Generate content by iterating through available year"""

    def printing(self, request, context, study):
        self.study = study
        content = render_to_string(self.head_print_template_name)
        kwargs = {'study_pk': study.pk}
        tpl = self.print_template_name
        for year_idx, year_label in self.get_steps():
            kwargs['step'] = year_idx
            context['year_label'] = year_label
            self.dispatch(request, **kwargs)
            self.step = year_idx
            context_kwargs = {}
            if hasattr(self, 'get_queryset'):
                context_kwargs['object_list'] = self.get_queryset()
            if not self.get_queryset().count() or \
               (hasattr(self, 'object') and not self.object.has_values()):
                continue
            if hasattr(self, 'get_form_class'):
                form_class = self.get_form_class()
                context_kwargs['form'] = self.readonlyfy(
                    self.get_form(form_class))
            context.update(self.get_context_data(**context_kwargs))
            content += render_to_string(tpl, context)
        return content


class AnimalYearPrintableMixin(PrintableMixin):
    """Generate content by iterating through available year and animals"""
    second_level_keys = ('animal', 'animal_label')

    def get_second_level_list(self, study, year):
        return models.ANIMALS

    def get_extra_print_context(self):
        return {}

    def printing(self, request, context, study):
        self.study = study
        content = ''
        kwargs = {'study_pk': study.pk}
        tpl = self.print_template_name
        self.is_readonly = True

        steps = self.get_steps()
        steps = [('P', _(u"Project"))]
        for year_idx, year_label in steps:
            kwargs['step'] = year_idx
            second_level_list = self.get_second_level_list(study, year_idx)
            for idx, label in second_level_list:
                kwargs[self.second_level_keys[0]] = idx
                context[self.second_level_keys[1]] = label
                self.dispatch(request, **kwargs)
                self.request = request
                context_kwargs = {}
                if hasattr(self, 'get_forms'):
                    context_kwargs['forms'] = self.get_forms()
                else:
                    form_class = self.get_form_class()
                    form = self.readonlyfy(self.get_form(form_class))
                    if not self.get_queryset().count() or \
                       not self.object.has_values():
                        continue
                    context_kwargs['form'] = form
                context.update(self.get_context_data(**context_kwargs))
                context.update(self.get_extra_print_context())
                if not content:
                    content = '['
                else:
                    content += ','
                content += render_to_string(tpl, context)
        if content:
            content += ']'
        return content


class HarvestYearPrintableMixin(AnimalYearPrintableMixin):
    """Generate content by iterating through available year and harvest"""
    second_level_keys = ('harvest_category', 'harvest_label')

    def get_second_level_list(self, study, year):
        return self.get_harvest_category_list(study=study, year=year)


class WelcomeView(TemplateView):
    template_name = "simulabio/welcome.html"


class NotRestrictedMixin(object):
    @classmethod
    def as_view(cls, *args, **kwargs):
        view = super(NotRestrictedMixin, cls).as_view(*args, **kwargs)
        view.restricted_page = False
        return view


class HasActiveSession(NotRestrictedMixin, TemplateView):
    template_name = "has_active_session.html"


class ReinitializeSession(NotRestrictedMixin, RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        request = self.request
        user = request.user
        if user and hasattr(user, 'simulabio_user') \
           and user.simulabio_user.count():
            simulabio_user = user.simulabio_user.all()[0]
            if simulabio_user.current_session:
                simulabio_user.current_session.delete()
                try:
                    with transaction.commit_on_success():
                        simulabio_user.current_session_id = \
                            request.session.session_key
                        simulabio_user.save()
                except IntegrityError:
                    pass
            remote_addr = '0.0.0.0'
            if "REMOTE_ADDR" in request.META:
                remote_addr = request.META['REMOTE_ADDR']
            simulabio_user.current_ip = remote_addr
            user_agent = 'empty-user-agent'
            if "HTTP_USER_AGENT" in request.META:
                # 4ko is enough
                user_agent = request.META['HTTP_USER_AGENT'][:4096]
            simulabio_user.current_agent = user_agent
            simulabio_user.save()
        return reverse('simulabio:welcome')

# farms


class FarmCheckMixin(CommentMixin):
    farm_pk_name = 'farm_pk'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        # check if the farm exists
        try:
            assert(self.farm_pk_name in kwargs)
            self.farm = models.Farm.objects.get(pk=kwargs[self.farm_pk_name])
        except (AssertionError, ObjectDoesNotExist):
            return redirect('simulabio:index')
        #  check if the farm is owned by the user: currently disabled
        # user = request.user
        # if not user.simulabio_user.count() or \
        #    not self.farm.simulabio_user.filter(user=user).count():
        #     return redirect('simulabio:index')
        self.studies = self.farm.study.all()
        return super(FarmCheckMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FarmCheckMixin, self).get_context_data(**kwargs)
        context['farm'] = self.farm
        context['studies'] = self.studies
        return context


class FarmList(ListView):
    context_object_name = "farms"
    template_name = "simulabio/farm_list.html"

    def get_queryset(self):
        return models.Farm.objects.filter(available=True)


class FarmCreate(CreateView):
    model = models.Farm
    success_url = reverse_lazy('simulabio:farm-list')
    form_class = forms.FarmForm

    def form_valid(self, form):
        returned = super(FarmCreate, self).form_valid(form)
        user = self.request.user
        if not user.simulabio_user.count():
            models.SimulabioUser.objects.create(user=user)
        form.instance.simulabio_user.add(user.simulabio_user.all()[0])
        return returned


class FarmUpdate(FarmCheckMixin, CommentMixin, UpdateView):
    model = models.Farm
    farm_pk_name = 'pk'
    success_url = reverse_lazy('simulabio:farm-list')


class FarmDelete(DeleteView):
    model = models.Farm
    success_url = reverse_lazy('simulabio:farm-list')

# studies


class StudyCreate(FarmCheckMixin, CreateView):
    model = models.Study
    form_class = forms.StudyForm

    def get_initial(self):
        initial = super(StudyCreate, self).get_initial()
        initial['farm'] = self.farm
        initial['is_diagnostic'] = True
        initial['year'] = datetime.date.today().year
        return initial

    def get_form_kwargs(self):
        kwargs = super(StudyCreate, self).get_form_kwargs()
        kwargs['farm'] = self.farm
        return kwargs


class StudyUpdate(FarmCheckMixin, PrintableMixin, UpdateView):
    model = models.Study
    form_class = forms.StudyFormUpdate
    print_template_name = 'simulabio/print/study_form.json'

    def get_current_comment(self):
        if not hasattr(self, 'object') or not self.object:
            self.object = self.get_object()
            if not self.object:
                return
        form_name = self.__class__.__name__
        if self._comment_form_name:
            form_name = self._comment_form_name
        comment, created = models.StudyComment.objects.get_or_create(
            study=self.object, form=form_name,
            context=self.get_comment_context())
        return comment

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        returned = super(StudyUpdate, self).dispatch(request, *args, **kwargs)
        preload_study(self.object.pk)
        return returned

    def printing(self, request, context, study):
        self.dispatch(request, pk=study.pk, farm_pk=study.farm.pk)
        form_class = self.get_form_class()
        form = self.readonlyfy(self.get_form(form_class))
        context.update(self.get_context_data(form=form))
        tpl = self.print_template_name
        return render_to_string(tpl, context)

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(StudyUpdate, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(StudyUpdate, self).get_context_data(**kwargs)
        context['gen_forms'] = list(models.GenericForm.objects.filter(
            available=True).order_by('order').all())
        return context


class SimpleStudyCheckMixin(CommentMixin):
    def check_and_init(self, request, *args, **kwargs):
        # check if the study exists
        try:
            assert('study_pk' in kwargs or 'pk' in kwargs)
            study_key = 'study_pk' if 'study_pk' in kwargs else 'pk'
            self.study = models.Study.objects.get(pk=kwargs[study_key])
        except (AssertionError, ObjectDoesNotExist):
            return redirect('simulabio:index')
        #  check if the farm is owned by the user: currently disabled
        # user = request.user
        # if not user.simulabio_user.count() or \
        #    not self.study.farm.simulabio_user.filter(user=user).count():
        #     return redirect('simulabio:index')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        redir = self.check_and_init(request, *args, **kwargs)
        if redir:
            return redir
        return super(SimpleStudyCheckMixin, self).dispatch(request, *args,
                                                           **kwargs)


class StudyCheckMixin(SimpleStudyCheckMixin):
    LOCKS = []

    def get_steps(self):
        return [('D', _(u"Diagnostic"))] + \
            [(unicode(yr), _(u'n+%(year)d') % {"year": yr})
             for yr in xrange(1, self.study.transition_years + 1)] + \
            [('P', _(u"Project"))]

    def check_and_init(self, request, *args, **kwargs):
        redir = super(StudyCheckMixin, self).check_and_init(request, *args,
                                                            **kwargs)
        if redir:
            return redir
        self.studies = self.study.farm.study.all()
        self.step_list = tuple_to_switchlist(self.get_steps())
        self.step = kwargs.get('step')
        # get current step from session
        cstep = request.session.get('current_step')
        if not self.step and cstep \
           and cstep in [step for step, lbl in self.get_steps()]:
            self.step = cstep
        request.session['current_step'] = self.step
        # fallback to diag step (fallback is not saved)
        if not self.step:
            self.step = 'D'
        preload_study(self.study.pk)

    def get_context_data(self, **kwargs):
        context = super(StudyCheckMixin, self).get_context_data(**kwargs)
        context['farm'] = self.study.farm
        context['study'] = self.study
        context['locked'] = models.StudyLock.has_lock(self.study.pk,
                                                      self.LOCKS)
        context['locklist'] = '-'.join(self.LOCKS)
        context['studies'] = self.studies
        context['step'] = self.step
        context['step_list'] = self.step_list
        return context

    def get_queryset(self):
        return models.Study.objects.filter(pk=self.study.pk)


class HasLockView(SimpleStudyCheckMixin, View):
    def check_and_init(self, request, *args, **kwargs):
        redir = super(HasLockView, self).check_and_init(request, *args,
                                                        **kwargs)
        if redir:
            return redir
        self.lock = kwargs.get('lock')

    def get(self, request, *args, **kwargs):
        # dct = {'data': '', 'separator': settings.CSV_DELIMITER}
        locked = 'true' if models.StudyLock.has_lock(self.study.pk,
                                                     self.lock) else 'false'
        response = HttpResponse(locked, content_type="text/plain")
        return response

# generic form


class GenericFormView(StudyCheckMixin, PrintableMixin, UpdateView):
    template_name = 'simulabio/generic_form_view.html'
    print_template_name = 'simulabio/print/generic_form_view.json'
    form_key = None

    def check_and_init(self, request, *args, **kwargs):
        redir = super(GenericFormView, self).check_and_init(request, *args,
                                                            **kwargs)
        if redir:
            return redir
        form_key = kwargs.get('form') or self.form_key
        try:
            self.generic_form = models.GenericForm.objects.get(slug=form_key,
                                                               available=True)
        except models.GenericForm.DoesNotExist:
            return redirect('simulabio:index')

    def get_form_class(self, *args, **kwargs):
        return forms.get_generic_form(self.generic_form)

    def get_success_url(self):
        return unicode(reverse_lazy('simulabio:generic-form',
                       args=[self.study.pk, self.generic_form.slug]))

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(GenericFormView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(GenericFormView, self).get_context_data(**kwargs)
        context['generic_form'] = self.generic_form
        context['gen_forms'] = list(models.GenericForm.objects.filter(
            available=True).order_by('order').all())
        return context


# harvests


class HarvestModificationMixin(StudyCheckMixin):
    def get_success_url(self):
        url = self.request.get_full_path()
        return url + "?success=1"

    def get_context_data(self, **kwargs):
        context = super(HarvestModificationMixin, self).get_context_data(
            **kwargs)
        if self.request.GET and 'success' in self.request.GET:
            context['MESSAGE'] = _(
                u"Once your modifications are finished check your fodder "
                u"balance. If you have modified Project/Diagnostic "
                u"availability of a parcel, theses changes are <b>not</b> "
                u"reported to rotation dominant affectation.")
        return context


class HarvestSettingsView(HarvestModificationMixin, PrintableMixin,
                          ExtraInlineFormSetView):
    LOCKS = ['SIT']
    template_name = 'simulabio/harvest_settings.html'
    print_template_name = 'simulabio/print/harvest_settings.json'
    model = models.Study
    inline_model = models.HarvestSettings
    form_class = forms.HarvestSettingsForm
    extra_forms = {'transition_form': forms.TransitionYearForm}

    transition_form_class = forms.TransitionYearForm
    extra = 0
    can_delete = False

    def get_formset_kwargs(self, *args, **kwargs):
        kwargs = super(HarvestSettingsView, self).get_formset_kwargs(*args,
                                                                     **kwargs)
        available_harvest_list = [
            harvest_plot.harvest.harvest.pk
            for harvest_plot in self.study.harvest_plots.all()]
        for rotation_dominant in self.study.rotation_dominant.all():
            available_harvest_list += [
                rdh.harvest.pk
                for rdh in rotation_dominant.rotation_dominant_harvest.all()]

        available_harvest_list = set(available_harvest_list)
        kwargs['queryset'] = self.inline_model.objects\
            .filter(study=self.study)\
            .filter(Q(harvest__pk__in=available_harvest_list) |
                    Q(harvest_settings__plot__study=self.study)).distinct()
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(HarvestSettingsView, self).post(request, *args, **kwargs)

    def formset_valid(self, *args, **kwargs):
        returned = super(HarvestSettingsView, self).formset_valid(*args,
                                                                  **kwargs)
        tasks = [['calculate_db_harvest', []], ]
        tasks.append(('calculate_db_pastureavailable', []))
        years = ['D'] + \
            [unicode(yr) for yr in xrange(1, self.study.transition_years + 1)] \
            + ['P']
        for year in years:
            tasks.append(['update_db_feedneeds', [year]])
        tasks.append(('calculate_db_economic_synthesis', []))
        compute(self.study.pk, tasks, ['HHA', 'HPA', 'AFN', 'ESY'])
        return returned


class HarvestPlotsDeleteView(HarvestModificationMixin, TemplateView):
    template_name = "simulabio/harvest_plots_delete.html"
    pks = 'pks'
    back_url = ''
    success_url = ''

    def post(self, request, *args, **kwargs):
        '''
        Manualy mix create and list views
        '''
        if request.POST.get('delete'):
            for obj in self.objects:
                obj.delete()
            tasks = [['calculate_db_harvest', []], ]
            tasks.append(('calculate_db_economic_synthesis', []))
            compute(self.study.pk, tasks, ['HHA', 'AFN', 'ESY'])
            return redirect(self.success_url)
        return redirect(self.back_url)

    def check_and_init(self, request, *args, **kwargs):
        redir = super(HarvestPlotsDeleteView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.back_url = reverse_lazy('simulabio:harvest-plots',
                                     args=[self.study.pk])
        self.success_url = reverse_lazy('simulabio:harvest-plots',
                                        args=[self.study.pk])
        object_ids = kwargs.get('pks') or ''
        try:
            self.object_ids = [int(pk) for pk in object_ids.split('_')]
            self.objects = list(self.get_queryset().all())
            assert self.objects
        except (ValueError, AssertionError):
            return redirect('simulabio:index')

    def get_queryset(self):
        return models.HarvestPlots.objects.filter(
            study=self.study, pk__in=self.object_ids)

    def get_context_data(self, **kwargs):
        context = super(HarvestPlotsDeleteView, self).get_context_data(
            **kwargs)
        context['object_list'] = self.objects
        return context


class HarvestPlotsView(HarvestModificationMixin, PrintableMixin, CreateView,
                       ListView):
    _comment_form_name = "HarvestPlotsView"
    model = models.HarvestPlots
    form_class = forms.HarvestPlotsForm
    template_name = 'simulabio/harvest_plots.html'
    print_template_name = 'simulabio/print/harvest_plots.json'
    file_form_class = forms.HarvestPlotsImportForm

    def get(self, request, *args, **kwargs):
        '''
        Manualy mix create and list views
        '''
        context = self.get_context_data(**kwargs)
        if self.object and self.object.study != self.study:
            return redirect('simulabio:harvest-plots', self.study.pk)
        if self.object:
            context['dialog_opened'] = True
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context['file_form'] = self.file_form_class()
        context.update(super(CreateView, self).get_context_data(form=form))
        return self.render_to_response(context)

    def success_url(self):
        url = reverse('simulabio:harvest-plots', args=[self.study.pk])
        return url + "?success=1"

    def post(self, request, *args, **kwargs):
        '''
        Manualy mix create and list views
        '''
        context = self.get_context_data(**kwargs)
        if self.object and self.object.study != self.study:
            return redirect(self.success_url())
        redir = self.comment_submission(request)
        if redir:
            return redir
        tasks = [('calculate_db_harvest', []), ]
        tasks.append(('calculate_db_pastureavailable', []))
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if request.FILES:
            file_form = self.file_form_class(request.POST, request.FILES)
            if file_form.is_valid():
                context['number_imported'], context['import_errors'], \
                    context['import_messages'] = file_form.save(self.study)
        else:
            if form.is_valid():
                self.object = form.save()
                tasks.append(('calculate_db_economic_synthesis', []))
                compute(self.study.pk, tasks, ['HHA', 'ESY'])
                return redirect(self.success_url())
            else:
                context['dialog_opened'] = True
        context.update(super(CreateView, self).get_context_data(form=form))
        tasks.append(('calculate_db_economic_synthesis', []))
        compute(self.study.pk, tasks, ['HHA', 'HPA', 'ESY'])
        return self.render_to_response(context)

    def get_queryset(self):
        return models.HarvestPlots.objects.filter(study=self.study)

    def get_context_data(self, **kwargs):
        if kwargs.get('pk'):
            self.object = self.get_object()
        else:
            self.object = None
        context = super(HarvestPlotsView, self).get_context_data(**kwargs)
        self.object_list = self.get_queryset()
        context.update(super(ListView, self).get_context_data(
            object_list=self.object_list))
        year = datetime.date.today().year
        context['year_2'] = year - 2
        context['year_1'] = year - 1
        context['year'] = year
        context['harvests'] = models.Harvest.objects.filter(available=True)
        context['CSV_DELIMITER'] = settings.CSV_DELIMITER
        context['CSV_ENCODING'] = settings.ENCODING
        context['plot_col'] = mark_safe(unicode(_(u'Name')))
        context['numeric_cols'] = [
            mark_safe(unicode(col).replace(' ', '_'))
            for col in (_(u'Area'), _(u'Plot group number'))]
        return context

    def get_initial(self):
        initial = super(HarvestPlotsView, self).get_initial()
        initial['study'] = self.study
        return initial

    def get_form_kwargs(self):
        kwargs = super(HarvestPlotsView, self).get_form_kwargs()
        kwargs['study'] = self.study
        return kwargs

    def printing(self, request, context, study):
        self.study = study
        self.dispatch(request, study_pk=study.pk)
        context.update(self.get_context_data())
        context['media_root'] = settings.MEDIA_ROOT
        tpl = self.print_template_name
        return render_to_string(tpl, context)


class HarvestRotationDominantMixin(HarvestModificationMixin):
    def get_steps(self):
        return [('D', _(u"Diagnostic")),
                ('P', _(u"Project"))]

    def check_and_init(self, request, *args, **kwargs):
        redir = super(HarvestRotationDominantMixin, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        # only diag and project are available
        self.is_diag = self.step == 'D'
        self.rotations = models.RotationDominant\
                               .objects.filter(study=self.study,
                                               diagnostic=self.is_diag).all()
        rotation_pk = kwargs.get('rotation_pk')
        if not rotation_pk:
            self.objects, self.rotation = [], None
            return
        try:
            self.rotation = models.RotationDominant.objects.get(
                pk=int(rotation_pk))
            assert self.rotation.study == self.study
        except (ValueError, AssertionError, ObjectDoesNotExist):
            return redirect('simulabio:index')


class HarvestRotationDominantView(HarvestRotationDominantMixin, PrintableMixin,
                                  ListView):
    LOCKS = ['HHA']
    context_object_name = "rotationdominant"
    template_name = 'simulabio/harvest_rotationdominant.html'
    print_template_name = 'simulabio/print/harvest_rotationdominant.json'
    head_print_template_name = \
        'simulabio/print/harvest_rotationdominant_head.html'

    def check_and_init(self, request, *args, **kwargs):
        redir = super(HarvestRotationDominantView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.objects = list(self.get_queryset().all())

    def get_comment_context(self):
        if self.rotation:
            return self.rotation.pk
        return self.is_diag and 'D' or 'P'

    def get_context_data(self, **kwargs):
        self.object_list = self.get_queryset()
        kwargs['object_list'] = self.objects
        context = super(HarvestRotationDominantView, self).get_context_data(
            **kwargs)
        year = datetime.date.today().year
        context['year_2'] = year - 2
        context['year_1'] = year - 1
        context['year'] = year
        context['is_diag'] = self.is_diag
        context['rotation'] = self.rotation
        context['rotations'] = self.rotations
        context['base_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:harvest-rotationdominant', args=[self.study.pk, 'D']
            )).split('/')[:-2]) + u"/"
        context['delete_url'] = ''
        if self.rotation:
            context['delete_url'] = reverse_lazy(
                'simulabio:harvest-rotationdominant-delete',
                args=(self.study.pk, self.step, self.rotation.pk))
        context['categories'] = models.HarvestCategory.get_cached(
            available=True)
        context['plot_col'] = mark_safe(unicode(_(u'Name')))
        context['numeric_cols'] = [
            mark_safe(unicode(col).replace(' ', '_'))
            for col in (_(u'Area'), _(u'Plot group number'))]
        return context

    def get_queryset(self):
        if not self.rotation:
            return models.HarvestPlots.objects.filter(pk=0)
        rotation_key = 'rotation_dominant_diag' if self.is_diag \
                       else 'rotation_dominant_project'
        return models.HarvestPlots.objects.filter(
            **{rotation_key: self.rotation})

    def get_context_form(self):
        context = {}
        context['template_form'] = forms.HarvestRotationTemplateAddForm(
            study=self.study, diagnostic=self.is_diag,
            geographical_zone=self.study.farm.geographical_zone)
        if self.rotation:
            data = {
                'name': self.rotation.name,
                'harvests': [
                    rd.harvest.pk
                    for rd in models.RotationDominantHarvest
                                    .objects.filter(
                                        rotation_dominant=self.rotation)
                                    .order_by('order').all()]
            }
            context['harvestrotation_form'] = forms.HarvestRotationModifyForm(
                data=data, rotation=self.rotation)
        context['form'] = forms.HarvestRotationAddForm(
            study=self.study, diagnostic=self.is_diag)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        context.update(self.get_context_form())
        return self.render_to_response(context)

    def success_url(self, rd):
        url = reverse(
            'simulabio:harvest-rotationdominant-detail',
            kwargs={'study_pk': self.study.pk,
                    'step': 'D' if self.is_diag else 'P',
                    'rotation_pk': rd.pk})
        return url + "?success=1"

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        redir = self.comment_submission(request)
        if redir:
            return redir
        template_form = forms.HarvestRotationTemplateAddForm(
            request.POST, study=self.study, diagnostic=self.is_diag,
            geographical_zone=self.study.farm.geographical_zone)
        if template_form.is_valid():
            rd = template_form.save(self.study, self.is_diag)
            return redirect(self.success_url(rd))
        form = forms.HarvestRotationAddForm(
            request.POST, study=self.study, diagnostic=self.is_diag)
        if form.is_valid():
            form.save(self.rotation, self.is_diag)
            return redirect(self.success_url(self.rotation))
            return redirect(
                'simulabio:harvest-rotationdominant-detail',
                study_pk=self.study.pk,
                step='D' if self.is_diag else 'P',
                rotation_pk=self.rotation.pk)
        if self.rotation:
            harvestrotation_form = forms.HarvestRotationModifyForm(
                request.POST, rotation=self.rotation)
            if harvestrotation_form.is_valid():
                harvestrotation_form.save(self.rotation)
                return redirect(self.success_url(self.rotation))
                return redirect(
                    'simulabio:harvest-rotationdominant-detail',
                    study_pk=self.study.pk, step=self.step,
                    rotation_pk=self.rotation.pk)
        context.update(self.get_context_form())
        return self.render_to_response(context)

    def printing(self, request, context, study):
        self.study = study
        kwargs = {'study_pk': study.pk, 'step': 'P'}
        tpl = self.print_template_name
        content = "["
        for idx, rotation in enumerate(
                models.RotationDominant
                      .objects.filter(study=study, diagnostic=False).all()):
            kwargs['rotation_pk'] = rotation.pk
            self.dispatch(request, **kwargs)
            context.update(self.get_context_data(**kwargs))
            context['media_root'] = settings.MEDIA_ROOT
            if idx:
                content += ", "
            content += render_to_string(tpl, context)
        content += "]"
        return content


class HarvestRotationDominantDeleteView(HarvestRotationDominantMixin,
                                        RedirectView):
    permanent = False

    def get_redirect_url(self, study_pk, step, rotation_pk):
        self.rotation.delete()
        return reverse_lazy(
            'simulabio:harvest-rotationdominant', args=(int(study_pk), step))


class HarvestRotationDominantDeleteAttachedView(HarvestRotationDominantMixin,
                                                RedirectView):
    permanent = False

    def get_redirect_url(self, study_pk, step, rotation_pk, pks):
        url = reverse_lazy(
            'simulabio:harvest-rotationdominant-detail',
            args=(study_pk, step, rotation_pk))
        try:
            hps = [models.HarvestPlots.objects.get(pk=pk)
                   for pk in pks.split('_')]
        except ObjectDoesNotExist:
            return url
        for hp in hps:
            if hp.study == self.study:
                if step == 'D':
                    hp.rotation_dominant_diag = None
                else:
                    hp.rotation_dominant_project = None
                hp.save()
        tasks = [['calculate_db_harvest', []], ]
        if step == 'P':
            tasks.append(('calculate_db_pastureavailable', [step]))
        tasks.append(('calculate_db_economic_synthesis', []))
        compute(self.study.pk, tasks, ['HHA', 'HPA', 'AFN', 'ESY'])
        return url


class HarvestFullPlotsView(HarvestModificationMixin, PrintableMixin,
                           InlineFormSetView):
    _comment_form_name = "HarvestPlotsView"
    template_name = 'simulabio/harvest_full_plots.html'
    model = models.Study
    inline_model = models.HarvestPlots
    form_class = forms.HarvestPlotsFullForm
    formset_class = forms.HarvestPlotsFormSet
    extra = 5

    def get_context_data(self, **kwargs):
        context = super(HarvestFullPlotsView, self).get_context_data(**kwargs)
        # formset = context.get('formset')
        context['plot_col'] = mark_safe(unicode(_(u'Name')))
        context['numeric_cols'] = [
            mark_safe(unicode(col).replace(' ', '_'))
            for col in (_(u'Area'), _(u'Plot group number'),
                        _(u'Plot caracteristic'))]
        return context

    def get_factory_kwargs(self, *args, **kwargs):
        kwargs = super(HarvestFullPlotsView, self).get_factory_kwargs(
            *args, **kwargs)
        kwargs["can_delete"] = False
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(HarvestFullPlotsView, self).post(request, *args,
                                                        **kwargs)
        tasks = [('calculate_db_harvest', []), ]
        tasks.append(('calculate_db_pastureavailable', []))
        tasks.append(('calculate_db_economic_synthesis', []))
        compute(self.study.pk, tasks, ['HHA', 'HPA', 'ESY'])
        return posted


class HarvestProductionView(StudyCheckMixin, PrintableMixin, ListView):
    LOCKS = ['HHA']
    context_object_name = "harvest_production"
    template_name = "simulabio/harvest_prod_list.html"
    print_template_name = "simulabio/print/harvest_prod_list.json"

    def get_queryset(self):
        return models.HarvestSettings.objects.filter(
            study=self.study,
            year_details__surface_total__gt=0,
            year_details__year__in=['D', 'P']
            + list(range(1, self.study.transition_years + 1))).distinct()

    def get_context_data(self, **kwargs):
        context_data = super(HarvestProductionView, self).get_context_data(
            **kwargs)
        context_data['object'] = self.study
        context_data['categories'] = models.HarvestCategory.get_cached(
            available=True)
        return context_data

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(HarvestProductionView, self).post(request, *args,
                                                       **kwargs)

    def printing(self, request, context, study):
        self.study = study
        content = ''
        kwargs = {'study_pk': study.pk}
        tpl = self.print_template_name
        self.is_readonly = True

        years = [('D', _(u"Diagnostic"))] \
            + [(unicode(i + 1), u'n + %d' % (i + 1))
               for i in range(self.study.transition_years)] \
            + [('P', _(u'Project'))]
        # eg: year_tuples = [('D', 1), (2, 3), (4, 'P')]
        year_tuples = []
        for idx in range(len(years) / 2):
            year_tuples.append((years[2 * idx], years[2 * idx + 1]))
        if len(years) % 2:
            # eg: year_tuples = [('D', 1), (2, 3), (3, 'P')]
            year_tuples.append((years[-2], years[-1]))

        for year_tuple in year_tuples:
            self.dispatch(request, **kwargs)
            self.request = request
            context_kwargs = {}
            context_kwargs['object_list'] = self.get_queryset()
            context.update(self.get_context_data(**context_kwargs))
            context['year'], context['year_label'] = year_tuple[0]
            context['year_1'], context['year_1_label'] = year_tuple[1]
            if not content:
                content = '['
            else:
                content += ','
            content += render_to_string(tpl, context)
        if content:
            content += ']'
        return content


class HarvestIndicatorsView(StudyCheckMixin, PrintableMixin, TemplateView):
    LOCKS = ['HHA']
    template_name = 'simulabio/harvest_indicators.html'

    def check_and_init(self, request, *args, **kwargs):
        redir = super(HarvestIndicatorsView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.diag_rotations = models.RotationDominant.objects.filter(
            study=self.study, diagnostic=True).all()
        self.project_rotations = models.RotationDominant.objects.filter(
            study=self.study, diagnostic=False).all()

    def get_context_data(self, **kwargs):
        context_data = super(HarvestIndicatorsView, self).get_context_data(
            **kwargs)
        context_data['object'] = self.study
        context_data['diag_rotations'] = self.diag_rotations
        context_data['project_rotations'] = self.project_rotations
        context_data['categories'] = models.HarvestCategory.get_cached(
            available=True)
        context_data['base_url'] = unicode(reverse_lazy(
            'simulabio:harvest-indicators', args=[self.study.pk]))

        return context_data

# animals


class AnimalsHerdView(StudyCheckMixin, PrintableMixin, ExtraYearFormsView):
    template_name = 'simulabio/animals_herd_detail.html'
    head_print_template_name = 'simulabio/print/animals_herd_head.html'
    print_template_name = 'simulabio/print/animals_herd.json'
    year_models = (
        ExtraYearFormConf(models.HerdDetail, forms.AnimalsHerdForm,
                          study_key='herd_detail__study',
                          parent_model=models.Herd, can_delete=False),)

    def check_and_init(self, request, *args, **kwargs):
        redir = super(AnimalsHerdView, self).check_and_init(request,
                                                            *args, **kwargs)
        self.herd = get_or_create(models.Herd, {'study': self.study})
        if redir:
            return redir

    def printing_kwargs(self, request, context, study):
        kwargs = super(AnimalsHerdView, self).printing_kwargs(
            request, context, study)
        if context.get('year_forms'):
            context['year_forms'] = context['year_forms'][1:]
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(AnimalsHerdView, self).post(request, *args,
                                                   **kwargs)
        c = Compute(self.study.pk)
        tasks = [('calculate_db_milkproduction', [])]
        tasks.append(('calculate_db_pastureavailable', []))
        for hd in models.HerdDetail.objects.filter(herd__study=self.study)\
                                           .all():
            c.calculate_db_herddetail(hd)
            tasks.append(('update_db_feedneeds', [hd.year]))
        tasks.append(('calculate_db_economic_synthesis', []))
        compute(self.study.pk, tasks, ['AMK', 'AFN', 'HPA', 'ESY'])
        return posted


class AnimalsCustomFeedView(StudyCheckMixin, UpdateView):
    model = models.Feed
    template_name = 'simulabio/animals_feed.html'
    form_class = forms.AnimalsFeedCustomFeedTemplateForm

    def check_and_init(self, request, *args, **kwargs):
        redir = super(AnimalsCustomFeedView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.animal = kwargs.get('animal')
        self.feed = models.Feed.objects.get(study=self.study,
                                            year=self.step,
                                            animal=self.animal)

    def post(self, request, *args, **kwargs):
        customfeed_form = self.form_class(self.request.POST,
                                          current_feed=self.feed,
                                          user=request.user)
        if customfeed_form.is_valid():
            customfeed_form.save()
        return redirect('simulabio:animals-feed',
                        study_pk=self.study.pk, animal=self.animal,
                        step=self.step)


class AnimalsFeedView(StudyCheckMixin, AnimalYearPrintableMixin, UpdateView):
    LOCKS = ['HPA']
    model = models.Feed
    template_name = 'simulabio/animals_feed.html'
    form_class = forms.AnimalsFeedForm
    print_template_name = 'simulabio/print/animals_feed.json'
    head_print_template_name = 'simulabio/print/animals_feed_head.html'

    def check_and_init(self, request, *args, **kwargs):
        redir = super(AnimalsFeedView, self).check_and_init(request,
                                                            *args, **kwargs)
        if redir:
            return redir
        self.animal = kwargs.get('animal')
        self.feed_template = None
        # get_or_create seems to generate duplicate...
        try:
            self.feed = models.Feed.objects.get(study=self.study,
                                                year=self.step,
                                                animal=self.animal)
        except:
            self.feed = models.Feed.objects.create(study=self.study,
                                                   year=self.step,
                                                   animal=self.animal)
        template_pk = kwargs.get('template_pk')
        if template_pk:
            try:
                self.feed_template = models.FeedTemplate.objects.get(
                    pk=template_pk)
            except ObjectDoesNotExist:
                return
        season = request.session.get('season') and self.step == 'D'
        self.herd = get_or_create(models.Herd, {'study': self.study})
        self.herd_detail = get_or_create(
            models.HerdDetail, {'herd': self.herd, 'year': self.step})

        if season and not template_pk:
            return redirect('simulabio:animals-feed-season',
                            study_pk=self.study.pk, animal=self.animal,
                            step=self.step)

    def get_comment_context(self):
        return self.step

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        if self.request.POST and self.request.POST.get('season_edit') == "1":
            request.session['season'] = True
            return redirect('simulabio:animals-feed-season',
                            study_pk=self.study.pk, animal=self.animal,
                            step=self.step)
        posted = super(AnimalsFeedView, self).post(request, *args, **kwargs)
        return posted

    def get_success_url(self):
        return unicode(reverse_lazy('simulabio:animals-feed',
                       args=[self.study.pk, self.animal, self.step]))

    def get_context_data(self, **kwargs):
        if not hasattr(self, 'object') or not self.object:
            self.object = self.get_object()
        context = super(AnimalsFeedView, self).get_context_data(**kwargs)
        context['content_class'] = 'feed_view'
        context['base_url'] = u"/".join(
            unicode(reverse_lazy('simulabio:animals-feed',
                                 args=[self.study.pk, 'dc', 'P'])
                    ).split('/')[:-3]) + u"/"
        context['form_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:animals-feed-update',
                args=[self.study.pk, self.animal, self.step, 1])
            ).split('/')[:-2]) + u"/"
        context['step_url'] = context['base_url'] + self.animal + u"/"
        context['animal'] = self.animal
        if self.animal == 'dc':
            q = models.PastureAvailable.objects.filter(
                study=self.study, year=self.step)
            if q.count():
                context['pasture_available'] = q.all()[0]
        context['animal_url'] = context['base_url']
        context['animal_end_url'] = self.step + u"/"
        context['animal_list'] = tuple_to_switchlist(
            [(k, lbl) for k, lbl in models.ANIMALS
             if getattr(self.herd_detail, models.ANIMALS_KEY[k])])
        context['current_url'] = context['step_url'] + self.step + u"/"
        context['feed_template'] = self.feed_template
        if self.feed_template:
            context['template_graph_series'], context['template_colors'], \
                context['feed_labels'] = self.feed_template.graph_series()
        if self.object and self.object.feed_template:
            context['graph_series'], context['colors'], \
                context['labels'] = self.object.graph_series()

        custom_keys = {}
        if self.object and self.object.feed_template and \
           self.object.feed_template.associated_user == self.request.user:
            custom_keys['instance'] = self.object.feed_template
        else:
            milk_production = 0
            if self.object and self.object.feed_template:
                milk_production = self.object.feed_template.milk_production\
                    or 0
            custom_keys['initial'] = {'milk_production': milk_production}
        custom_keys['current_feed'] = self.object.pk
        context['custom_feed_form'] = AnimalsCustomFeedView.form_class(
            **custom_keys)
        return context

    def get_object(self, queryset=None):
        query = self.get_queryset()
        return query.get()

    def get_forms(self):
        return []

    def get_queryset(self):
        return models.Feed.objects.filter(pk=self.feed.pk)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(AnimalsFeedView, self).get_form_kwargs(*args, **kwargs)
        kwargs['study'] = self.study
        return kwargs

    def get_extra_print_context(self):
        if not self.object or not self.object.feed_template:
            return {}
        self.object.numpy_graph()
        return {'animal_image': settings.MEDIA_ROOT + self.object.image_path}


class AnimalsFeedSeasonView(StudyCheckMixin, UpdateView):
    model = models.Feed
    template_name = 'simulabio/animals_feed_season.html'
    form_class = forms.AnimalsFeedForm

    def check_and_init(self, request, *args, **kwargs):
        redir = super(AnimalsFeedSeasonView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.animal = kwargs.get('animal')
        if 'season' not in request.session.keys():
            request.session['season'] = True
        # get_or_create seems to generate duplicate...
        try:
            self.feed = models.Feed.objects.get(study=self.study,
                                                year=self.step,
                                                animal=self.animal)
        except:
            self.feed = models.Feed.objects.create(study=self.study,
                                                   year=self.step,
                                                   animal=self.animal)
        self.herd = get_or_create(models.Herd, {'study': self.study})
        self.herd_detail = get_or_create(
            models.HerdDetail, {'herd': self.herd, 'year': self.step})

        if not request.session['season']:
            return redirect('simulabio:animals-feed',
                            study_pk=self.study.pk, animal=self.animal,
                            step=self.step)

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        if self.request.POST and self.request.POST.get('monthly_edit') == "1":
            request.session['season'] = False
            return redirect('simulabio:animals-feed',
                            study_pk=self.study.pk, animal=self.animal,
                            step=self.step)
        posted = super(AnimalsFeedSeasonView, self).post(request, *args,
                                                         **kwargs)
        return posted

    def get_success_url(self):
        return unicode(reverse_lazy('simulabio:animals-feed-season',
                       args=[self.study.pk, self.animal, self.step]))

    def get_context_data(self, **kwargs):
        context = super(AnimalsFeedSeasonView, self).get_context_data(
            **kwargs)
        context['content_class'] = 'feed_view'
        context['base_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:animals-feed-season',
                args=[self.study.pk, 'dc', 'D'])).split('/')[:-3]) + u"/"
        context['form_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:animals-feed-update',
                args=[self.study.pk, self.animal, self.step, 1])
            ).split('/')[:-2]) + u"/"
        context['animal'] = self.animal
        context['step_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:animals-feed',
                args=[self.study.pk, self.animal, 'P'])
            ).split('/')[:-2]) + u"/"
        context['animal_url'] = context['base_url']
        context['animal_end_url'] = self.step + u"/"
        context['animal_list'] = tuple_to_switchlist(
            [(k, lbl) for k, lbl in models.ANIMALS
             if getattr(self.herd_detail, models.ANIMALS_KEY[k])])
        context['current_url'] = context['step_url'] + self.step + u"/"
        if self.object:
            context['graph_series'], context['colors'], \
                context['labels'] = self.object.graph_series(season=True)
        return context

    def get_object(self, queryset=None):
        query = self.get_queryset()
        return query.get()

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(AnimalsFeedSeasonView, self).get_form_kwargs(*args,
                                                                    **kwargs)
        kwargs['study'] = self.study
        return kwargs

    def get_queryset(self):
        return models.Feed.objects.filter(pk=self.feed.pk)


class AnimalsFeedUpdateView(StudyCheckMixin, FormView):
    template_name = 'simulabio/animals_feed_update.html'
    form_class = forms.AnimalsFeedUpdateForm

    def check_and_init(self, request, *args, **kwargs):
        redir = super(AnimalsFeedUpdateView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.animal = kwargs.get('animal')
        self.month = kwargs.get('month')
        try:
            self.feed = models.Feed.objects.get(year=self.step,
                                                animal=self.animal,
                                                study=self.study)
        except ObjectDoesNotExist:
            return redirect('simulabio:index')
        self.season = request.session.get('season') and self.step == 'D'
        season = '-season' if self.season else ''
        self.success_url = reverse_lazy(
            'simulabio:animals-feed' + season,
            args=[self.study.pk, self.animal, self.step])

    def get_form_kwargs(self):
        kwargs = super(AnimalsFeedUpdateView, self).get_form_kwargs()
        kwargs['feed'] = self.feed
        kwargs['month'] = self.month
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(AnimalsFeedUpdateView, self).get_context_data(**kwargs)
        context['url'] = reverse_lazy(
            'simulabio:animals-feed-update',
            args=[self.study.pk, self.animal, self.step, self.month])
        return context

    def form_valid(self, form):
        form.save(season=self.season)
        return HttpResponseRedirect(self.get_success_url())


class AnimalsLitterView(StudyCheckMixin, AnimalYearPrintableMixin, UpdateView):
    model = models.StrawUsage
    template_name = 'simulabio/animals_litter.html'
    print_template_name = 'simulabio/print/animals_litter.html'
    head_print_template_name = 'simulabio/print/animals_litter_head.html'
    form_class = forms.AnimalsLitterForm

    def check_and_init(self, request, *args, **kwargs):
        redir = super(AnimalsLitterView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.animal = kwargs.get('animal')
        self.herd = get_or_create(models.Herd, {'study': self.study})
        self.herd_detail = get_or_create(
            models.HerdDetail, {'herd': self.herd, 'year': self.step})

    def get_success_url(self):
        return unicode(
            reverse_lazy('simulabio:animals-litter',
                         args=[self.study.pk, self.animal, self.step]))

    def get_context_data(self, **kwargs):
        context = super(AnimalsLitterView, self).get_context_data(**kwargs)
        context['base_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:animals-litter',
                args=[self.study.pk, 'dc', 'D'])).split('/')[:-3]) + u"/"
        context['step_url'] = context['base_url'] + self.animal + u"/"
        context['animal'] = self.animal
        context['animal_url'] = context['base_url']
        context['animal_end_url'] = self.step + u"/"
        context['animal_list'] = tuple_to_switchlist(
            [(k, lbl) for k, lbl in models.ANIMALS
             if getattr(self.herd_detail, models.ANIMALS_KEY[k])])
        context['current_url'] = context['step_url'] + self.step + u"/"
        return context

    def get_object(self, queryset=None):
        query = self.get_queryset()
        return query.get()

    def get_queryset(self):
        straw_usage = get_or_create(
            self.model, {'herd_detail': self.herd_detail,
                         'animal': self.animal})
        return self.model.objects.filter(pk=straw_usage.pk)

    def get_comment_context(self):
        return self.step

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(AnimalsLitterView, self).post(request, *args, **kwargs)


class MilkProductionView(StudyCheckMixin, PrintableMixin, ExtraYearFormsView):
    LOCKS = ['AMK']
    template_name = 'simulabio/animals_milk_production.html'
    print_template_name = 'simulabio/print/animals_milk_production.json'
    year_models = (ExtraYearFormConf(models.MilkProduction,
                                     forms.MilkProductionForm),)

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(MilkProductionView, self).post(request, *args, **kwargs)

    def printing_kwargs(self, request, context, study):
        kwargs = super(MilkProductionView, self).printing_kwargs(
            request, context, study)
        if context.get('year_forms'):
            context['year_forms'] = context['year_forms']
        return kwargs

# balances


class AnimalsPrimaryBalanceView(StudyCheckMixin, YearPrintableMixin, ListView):
    LOCKS = ['HHA', 'AFN']
    context_object_name = "harvest_category_study"
    template_name = "simulabio/animals_primary_balance_list.html"
    print_template_name = "simulabio/print/animals_primary_balance_list.html"
    head_print_template_name = \
        "simulabio/print/animals_primary_balance_list_head.html"

    def get_queryset(self):
        return models.HarvestCategoryStudy.objects.filter(
            study=self.study, year=self.step).distinct()

    def get_context_data(self, **kwargs):
        context_data = super(AnimalsPrimaryBalanceView, self).get_context_data(
            **kwargs)
        context_data['object'] = self.study
        context_data['step'] = self.step
        context_data['step_list'] = self.step_list
        context_data['step_url'] = u"/".join(
            unicode(reverse_lazy(
                'simulabio:animals-primary-balance',
                args=[self.study.pk, 'D'])).split('/')[:-2]) + u"/"
        context_data['categories'] = models.HarvestCategory.get_cached(
            available=True)
        return context_data

    def get_comment_context(self):
        return self.step

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(AnimalsPrimaryBalanceView, self).post(request, *args,
                                                           **kwargs)


class BalanceFodderView(StudyCheckMixin, HarvestYearPrintableMixin, FormView):
    template_name = 'simulabio/balance_fodder.html'
    print_template_name = 'simulabio/print/balance_fodder.json'
    head_print_template_name = 'simulabio/print/balance_fodder_head.html'
    model = models.FeedNeed
    formset_models = [models.FeedNeedSupply, models.FeedNeedExtraSupply]
    formset_classes = [forms.FeedNeedFormSet, forms.FeedNeedExtraFormSet]

    def get_harvest_category_list(self, study=None, year=None):
        # study and year can be manualy set to permit the use of this method
        # before initialization of attributes
        hc_list = set([fn.harvest_category_usage.harvest_category
                       for fn in self.all_feed_need_query(study=study,
                                                          year=year).all()])
        hc_list = sorted(list(hc_list), key=lambda x: x.order)
        return [(hc.txt_idx, unicode(hc)) for hc in hc_list]

    def all_feed_need_query(self, extra_filter={}, study=None, year=None):
        if not study:
            study = self.study
        if not year:
            year = self.step
        ordering = ('harvest_category_usage__harvest_category__order',
                    'harvest_category_usage__usage_type__order')
        q = models.FeedNeed.objects.filter(
            study=study, year=year, amount__gt=0)
        if extra_filter:
            q = q.filter(**extra_filter)
        return q.distinct().order_by(*ordering)

    def check_and_init(self, request, *args, **kwargs):
        redir = super(BalanceFodderView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.harvest_category = None
        self.feed_needs = []

        harvest_category = kwargs.get('harvest_category')
        if harvest_category:
            try:
                self.harvest_category = models.HarvestCategory.objects.get(
                    txt_idx=harvest_category)
            except ObjectDoesNotExist:
                pass
        self.harvest_category_list = tuple_to_switchlist(
            self.get_harvest_category_list())
        if not self.harvest_category and self.harvest_category_list:
            # get first
            fn = self.all_feed_need_query().all()[0]
            self.harvest_category = fn.harvest_category_usage.harvest_category
        if self.harvest_category:
            self.feed_needs = list(self.all_feed_need_query(
                extra_filter={
                    'harvest_category_usage__harvest_category':
                    self.harvest_category}).all())

    def get(self, request, *args, **kwargs):
        extra_row = 4
        if 'extra_row' in kwargs:
            extra_row = kwargs.pop('extra_row')
        self.forms = self.get_forms(extra_row=extra_row)
        return self.render_to_response(self.get_context_data())

    def get_comment_context(self):
        return self.step

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        extra_row = 4
        if 'extra_row' in kwargs:
            extra_row = kwargs.pop('extra_row')
        self.forms = self.get_forms(self.request.POST, extra_row=extra_row)
        c = Compute(self.study.pk)
        c.calculate_db_harvest()
        tasks = []
        tasks.append(('calculate_db_economic_synthesis', []))
        compute(self.study.pk, tasks, ['ESY'])
        return redirect(
            'simulabio:balance-fodder', study_pk=self.study.pk, step=self.step,
            harvest_category=self.harvest_category.txt_idx)

    def get_forms(self, post=None, extra_row=4):
        readonlyfy = False if not hasattr(self, 'is_readonly') \
            else self.is_readonly
        current_hc, current_lst, forms, form_idx = None, [], [], 0

        def lst_append(feed_need, current_hc, current_lst, forms, form_idx):
            if not feed_need or current_hc != \
                    feed_need.harvest_category_usage.harvest_category:
                if current_lst:
                    number = sum(
                        [number for usage_type, feed, frmset, number
                         in current_lst])
                    forms.append((current_hc, current_lst, number))
                    current_lst = []
                if feed_need:
                    current_hc = \
                        feed_need.harvest_category_usage.harvest_category
            if feed_need:
                formsets = []
                form_sum = 0
                for idx, formset_class in enumerate(self.formset_classes):
                    model = self.formset_models[idx]
                    factory_keys = {'formset': formset_class,
                                    'extra': 1 if not readonlyfy else 0,
                                    'can_delete': False}
                    formset_keys = {'feed_need': feed_need,
                                    'prefix': "%d-%d" % (form_idx, idx),
                                    'queryset': model.objects.filter(
                                        feed_need=feed_need)}
                    if readonlyfy:
                        formset_keys['round'] = 0
                    if post:
                        formset_keys['data'] = post
                    formset_cls = modelformset_factory(model, **factory_keys)
                    formset = formset_cls(**formset_keys)
                    if post and formset.is_valid() and \
                       len([data for data in formset.cleaned_data if data]):
                        formset.save()
                        formset_keys.pop('data')
                        formset = formset_cls(**formset_keys)
                    if readonlyfy:
                        formset = self.readonlyfy(formset)
                    formsets.append(formset)
                    form_sum += formset.total_form_count()
                current_lst.append(
                    (feed_need.harvest_category_usage.usage_type,
                     feed_need, formsets, extra_row + form_sum))
            form_idx += 1
            return current_hc, current_lst, forms, form_idx
        for feed_need in self.feed_needs:
            if not feed_need.amount:
                continue
            current_hc, current_lst, forms, form_idx = lst_append(
                feed_need, current_hc, current_lst, forms, form_idx)
        current_hc, current_lst, forms, form_idx = lst_append(
            None, current_hc, current_lst, forms, form_idx)
        return forms

    def get_context_data(self, **kwargs):
        context = super(BalanceFodderView, self).get_context_data(**kwargs)
        context['forms'] = self.forms
        context['object'] = self.study
        context['harvest_category_list'] = self.harvest_category_list
        context['harvest_category'] = self.harvest_category.txt_idx \
            if self.harvest_category else ''
        context['harvest_category_url'] = unicode(
            reverse_lazy('simulabio:balance-fodder',
                         args=[self.study.pk, self.step]))
        context['categories'] = models.HarvestCategory.get_cached(
            available=True)
        context['step_url'] = u"/".join(
            context['harvest_category_url'].split('/')[:-2]) + u"/"
        c = Compute(self.study.pk)
        for feed_need in self.feed_needs:
            feed_need.calculate_db()
        c.save()
        return context

    def printing(self, request, context, study):
        self.study = study
        self.is_readonly = True
        content = ''
        kwargs = {'study_pk': study.pk, 'step': 'P', 'extra_row': 2}
        tpl = self.print_template_name

        harvest_list = self.get_harvest_category_list(study=self.study,
                                                      year='P')
        for harvest_cat_txt_idx, harvest_cat in harvest_list:
            kwargs['harvest_category'] = harvest_cat_txt_idx
            self.dispatch(request, **kwargs)
            self.request = request
            context_kwargs = {}
            data = self.get_context_data(**context_kwargs)
            data['harvest_category'] = models.HarvestCategory.objects.get(
                txt_idx=data['harvest_category'])
            context.update(data)
            if not content:
                content = '['
            else:
                content += ','
            content += render_to_string(tpl, context)
        if content:
            content += ']'
        return content

# economy


class EconomySettingsView(StudyCheckMixin, PrintableMixin,
                          InlineStudyFormSetView):
    template_name = 'simulabio/economy_settings.html'
    print_template_name = 'simulabio/print/economy_settings.html'
    model = models.HarvestSettings
    inline_model = models.HarvestSettingsSold
    form_class = forms.CropPriceForm
    formset_class = forms.CropPriceFormSet

    def get_factory_kwargs(self, *args, **kwargs):
        kwargs = super(EconomySettingsView, self).get_factory_kwargs(*args,
                                                                     **kwargs)
        kwargs["can_delete"] = False
        kwargs["extra"] = 1
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        return super(EconomySettingsView, self).post(request, *args, **kwargs)


class SimpleEcoMixin(StudyCheckMixin):
    def get_context_data(self, **kwargs):
        context = super(SimpleEcoMixin, self).get_context_data(**kwargs)
        context['action_list'] = tuple_to_switchlist(
            (('initial-state', _(u"Initial state")),
             ('revenues', _(u"Revenues")),
             ('expenses', _(u"Expenses")),
             ('investments', _(u"Investments")),
             ('synthesis', _(u"Economic synthesis")),
             ))
        context['base_url'] = u"/".join(
            unicode(reverse_lazy('simulabio:economy-simple-initial',
                                 args=[self.study.pk])).split('/')[:-2]) + u"/"
        return context


class SimpleEcoInitialView(SimpleEcoMixin, PrintableMixin,
                           ExtraInlineFormSetView):
    template_name = 'simulabio/economy_simple_initial.html'
    print_template_name = 'simulabio/print/economy_simple_initial.json'
    model = models.Study
    inline_model = models.SimpleEcoInitialExpenseDetail
    form_class = forms.InitialExpenseDetailForm
    formset_class = forms.InitialExpenseDetailFormSet
    extra_forms = {'initial_form': forms.SimpleEcoInitialForm}
    extra = 0
    can_delete = False

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(SimpleEcoInitialView, self).post(request, *args,
                                                        **kwargs)
        c = Compute(self.study.pk)
        c.calculate_db_annuities()
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        return posted


class SimpleEcoRevenuesView(SimpleEcoMixin, PrintableMixin,
                            ExtraYearsExtraFormsMixin):
    LOCKS = ['HHA', 'AFN']
    template_name = 'simulabio/economy_simple_revenues.html'
    print_template_name = 'simulabio/print/economy_simple_revenues.json'
    year_models = (ExtraYearFormConf(models.Revenues, forms.RevenuesForm),
                   ExtraYearFormConf(
                       models.ExtraRevenueYear, forms.ExtraRevenueYearForm,
                       study_key='extra_revenue__study',
                       parent_model=models.ExtraRevenue,
                       extra_exclude={'full_eco': True}))
    extra_forms = {'extra_revenue': forms.SimpleEcoExtraRevenueForm}

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(SimpleEcoRevenuesView, self).post(request, *args,
                                                         **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ERV', 'ESY'])
        return posted

    def printing_kwargs(self, request, context, study):
        super(SimpleEcoRevenuesView, self).printing_kwargs(
            request, context, study)
        context['revenues_col'] = 1 + 2 + study.transition_years
        context['extrarevenues_col'] = 1 + (2 + study.transition_years) * 3
        return context


class SimpleEcoExpensesView(SimpleEcoMixin, PrintableMixin,
                            ExtraYearsExtraFormsMixin):
    LOCKS = ['ESY']
    template_name = 'simulabio/economy_simple_expenses.html'
    print_template_name = 'simulabio/print/economy_simple_expenses.json'
    year_models = (ExtraYearFormConf(models.Expenses, forms.ExpensesForm),
                   ExtraYearFormConf(
                       models.ExtraSupplyPriceYear,
                       forms.ExtraSupplyPriceYearSimpleForm,
                       study_key='extra_supply_prices__study',
                       parent_model=models.ExtraSupplyPrice),
                   ExtraYearFormConf(
                       models.ExtraExpenseYear, forms.ExtraExpenseYearForm,
                       study_key='extra_expenses__study',
                       extra_exclude={'full_eco': True},
                       parent_model=models.ExtraExpenses))
    extra_forms = {'extra_expense': forms.SimpleEcoExtraExpenseForm}

    def get_context_data(self, **kwargs):
        # init extrasupply
        c = Compute(self.study.pk)
        c.initialize_extrasupplyprices()
        c.save()
        context_data = super(SimpleEcoExpensesView, self).get_context_data(
            **kwargs)
        return context_data

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(SimpleEcoExpensesView, self).post(request, *args,
                                                         **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['EEX', 'ESY'])
        return posted

    def printing_kwargs(self, request, context, study):
        super(SimpleEcoExpensesView, self).printing_kwargs(
            request, context, study)
        context['expenses_col'] = 1 + 2 + study.transition_years
        return context


class SimpleEcoInvestmentsView(SimpleEcoMixin, PrintableMixin,
                               InlineFormSetView):
    template_name = 'simulabio/economy_simple_investments.html'
    print_template_name = 'simulabio/print/economy_simple_investments.json'
    model = models.Study
    inline_model = models.Investment
    form_class = forms.InvestmentForm
    formset_class = forms.InvestmentFormSet
    extra = 1

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(SimpleEcoInvestmentsView, self).post(request, *args,
                                                            **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        return posted


class SimpleEcoSynthesisView(SimpleEcoMixin, PrintableMixin,
                             ExtraYearFormsView):
    LOCKS = ['HHA', 'AFN', 'ESY', 'ERV', 'EEX']
    template_name = 'simulabio/economy_simple_synthesis.html'
    print_template_name = 'simulabio/print/economy_simple_synthesis.json'
    year_models = (ExtraYearFormConf(models.EconomicSynthesis,
                                     forms.EconomicSynthesisForm),)

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(SimpleEcoSynthesisView, self).post(request, *args,
                                                          **kwargs)
        c = Compute(self.study.pk)
        c.calculate_db_economic_synthesis()
        c.save()
        return posted


class FullEcoMixin(StudyCheckMixin):
    LOCKS = ['HHA', 'AFN']

    def get_context_data(self, **kwargs):
        context = super(FullEcoMixin, self).get_context_data(**kwargs)
        context['action_list'] = tuple_to_switchlist(
            (('workshop', _(u"Workshops")),
             ('extra-revenues', _(u"Extra revenues")),
             ('expenses', _(u"Fixed expenses")),
             ('investments', _(u"Investments")),
             ('synthesis', _(u"Economic synthesis")),))
        context['base_url'] = u"/".join(
            unicode(reverse_lazy('simulabio:economy-full-revenues',
                                 args=[self.study.pk])).split('/')[:-2]) + u"/"
        return context


class WorkshopMixin(FullEcoMixin):
    def get_workshop_list(self, study):
        workshop_list = [
            (workshop.txt_idx, workshop.name)
            for workshop in models.Workshop.get_cached(available=True)]
        workshop_list += [
            (workshop.txt_idx, workshop.name)
            for workshop in models.Workshop.objects.filter(
                available=False, study=study)]
        return workshop_list

    def check_and_init(self, request, *args, **kwargs):
        redir = super(WorkshopMixin, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        self.workshop_list = self.get_workshop_list(self.study)
        self.workshop_list += [('new', _(u"New workshop"))]
        workshop = kwargs.get('workshop')
        self.workshop = None
        if workshop in [txt_idx for txt_idx, name in self.workshop_list]:
            self.workshop = models.Workshop.get_cached(
                txt_idx=workshop, study__pk=self.study.pk)
            if not self.workshop:
                self.workshop = models.Workshop.get_cached(txt_idx=workshop)
            self.workshop = self.workshop[0]
        self.state, self.extra = "", ""
        states = (kwargs.get('state') or '').split('-')
        if states:
            self.state = states[0]
            if len(states) > 1:
                self.extra = "-".join(states[1:])

    def get_context_data(self, **kwargs):
        context = super(WorkshopMixin, self).get_context_data(**kwargs)
        context['step_url'] = unicode(
            reverse_lazy('simulabio:economy-full-newworkshop',
                         args=[self.study.pk]))
        context['step_url'] = context['step_url'].split('new')[0]
        if context['step_list']:
            context['step_list'].pop(0)
        context['workshop_list'] = tuple_to_switchlist(self.workshop_list)
        context['workshop'] = self.workshop
        context['state'], context['extra'] = self.state, self.extra
        return context


class FullEcoHarvestWorkshopView(WorkshopMixin, PrintableMixin,
                                 InlineStudyFormSetView):
    template_name = 'simulabio/economy_full_harvest_workshop.html'
    print_template_name = 'simulabio/print/economy_full_harvest_workshop.json'
    model = models.HarvestSettings
    inline_model = models.HarvestSettingsSold
    form_class = forms.CropSellingForm
    formset_class = forms.CropSellingFormSet

    def get_context_data(self, **kwargs):
        context = super(FullEcoHarvestWorkshopView, self).get_context_data(
            **kwargs)
        if context['step_list']:
            context['step_list'].pop(0)
        context['base_url'] = u"/".join(
            context['step_url'].split(u'/')[0:-2]) \
            + u"/"
        return context

    def get_factory_kwargs(self, *args, **kwargs):
        kwargs = super(FullEcoHarvestWorkshopView, self).get_factory_kwargs(
            *args, **kwargs)
        kwargs["can_delete"] = False
        kwargs["extra"] = 0
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(FullEcoHarvestWorkshopView, self).post(
            request, *args, **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        return posted

    def printing_kwargs(self, request, context, study):
        kwargs = super(FullEcoHarvestWorkshopView, self).printing_kwargs(
            request, context, study)
        context['col_number'] = 2 + 3 + 3 * (study.transition_years)
        context['harvestexpenses_col_number'] = 1 + 2 + \
            2 * (study.transition_years) + 2
        context['harvest_tables'] = []
        harvest_list = FullEcoHarvestSettingsExpenseView.get_harvest_list(
            self.study)
        for harvest in harvest_list:
            c_context = {}
            v = FullEcoHarvestSettingsExpenseView()
            v.dispatch(request, pk=self.study.pk,
                       harvest=harvest.harvest.txt_idx)
            v.request = request
            context_kwargs = {}
            context_kwargs['formset'] = v.get_formset()(
                instance=self.study, harvest=harvest.harvest)
            c_context.update(v.get_context_data(**context_kwargs))
            context['harvest_tables'].append({'harvest': harvest.harvest,
                                              'formset': c_context['formset']})
        return kwargs


class FullEcoHarvestSettingsExpenseView(WorkshopMixin, PrintableMixin,
                                        InlineStudyFormSetView):
    template_name = 'simulabio/economy_full_simple_harvestexpense.html'
    print_template_name = 'simulabio/print/economy_full_harvestexpense.html'
    model = models.HarvestSettings
    inline_model = models.HarvestSettingsExpense
    form_class = forms.HarvestExpenseForm
    formset_class = forms.HarvestExpenseFormSet

    @classmethod
    def get_harvest_list(cls, study):
        return models.HarvestSettings\
                     .objects.filter(
                         study=study, year_details__surface_total__gt=0)\
                     .distinct().order_by('harvest__order')

    def check_and_init(self, request, *args, **kwargs):
        redir = super(FullEcoHarvestSettingsExpenseView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        prefix = 'expenses-'
        self.harvest_list = [
            (prefix + harvest.harvest.txt_idx, unicode(harvest.harvest))
            for harvest in self.get_harvest_list(self.study)]
        harvest = kwargs.get('extra') or kwargs.get('harvest')
        self.harvest = None
        if harvest in [txt_idx[len(prefix):]
                       for txt_idx, name in self.harvest_list]:
            self.harvest = models.Harvest.get_cached(txt_idx=harvest)[0]
        if not self.harvest and self.harvest_list:
            return redirect(
                'simulabio:economy-full-harvest-expenses', pk=self.study.pk,
                harvest=self.harvest_list[0][0][len(prefix):])

    def get_context_data(self, **kwargs):
        context = super(FullEcoHarvestSettingsExpenseView, self
                        ).get_context_data(**kwargs)
        if context['step_list']:
            context['step_list'].pop(0)
        context['harvest_list'] = tuple_to_switchlist(self.harvest_list)
        context['harvest'] = self.harvest.txt_idx if self.harvest else ''
        return context

    def get_formset_kwargs(self, *args, **kwargs):
        kwargs = super(FullEcoHarvestSettingsExpenseView, self)\
            .get_formset_kwargs(*args, **kwargs)
        kwargs['harvest'] = self.harvest
        return kwargs

    def get_factory_kwargs(self, *args, **kwargs):
        kwargs = super(FullEcoHarvestSettingsExpenseView, self
                       ).get_factory_kwargs(*args, **kwargs)
        kwargs["can_delete"] = False
        kwargs["extra"] = 0
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        super(FullEcoHarvestSettingsExpenseView, self).post(request,
                                                            *args, **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        state = 'expenses' + ('-' + self.harvest.txt_idx) \
                if self.harvest else ''
        return redirect('simulabio:economy-full-harvest-workshop',
                        pk=self.study.pk, state=state)


class FullEcoNewWorkshopView(WorkshopMixin, CreateView):
    template_name = 'simulabio/economy_full_newworkshop.html'
    model = models.Workshop
    form_class = forms.NewWorkshopForm

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(FullEcoNewWorkshopView, self).get_form_kwargs(*args,
                                                                     **kwargs)
        kwargs['study'] = self.study
        return kwargs


class FullEcoWorkshopView(WorkshopMixin, PrintableMixin,
                          ExtraYearsExtraFormsMixin):
    template_name = 'simulabio/economy_full_workshop.html'
    print_template_name = 'simulabio/print/economy_full_workshop.json'
    year_models = [
        [],
        ExtraYearFormConf(
            models.WorkshopExpensesYear, forms.WorkshopExpensesYearForm,
            parent_study_key='workshop',
            parent_model=models.WorkshopExpenses,),
        ExtraYearFormConf(
            models.WorkshopRevenuesYear, forms.WorkshopRevenuesYearForm,
            parent_study_key='workshop',
            parent_model=models.WorkshopRevenues,), ]
    extra_forms = {'extra_expense': forms.WorkshopExpensesForm,
                   'extra_revenue': forms.WorkshopRevenuesForm}

    def check_and_init(self, request, *args, **kwargs):
        redir = super(FullEcoWorkshopView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        if self.workshop and self.workshop.txt_idx == 'cultures':
            kwargs = {'pk': self.study.pk}
            if self.state:
                kwargs['state'] = self.state
            if self.extra:
                kwargs['extra'] = self.extra
            return redirect('simulabio:economy-full-harvest-workshop',
                            **kwargs)
        if not self.workshop and self.workshop_list:
            return redirect(
                'simulabio:economy-full-workshop', pk=self.study.pk,
                workshop=self.workshop_list[0][0])

    def get_extra_forms_kwargs(self, posted=False):
        kwargs = {'workshop': self.workshop}
        if self.request.method in ('POST', 'PUT') or posted:
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(FullEcoWorkshopView, self).post(request, *args,
                                                       **kwargs)
        Compute(self.study.pk)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        return posted

    def printing(self, request, context, study):
        content = ''
        kwargs = {'study_pk': study.pk}
        context['col_number'] = 1 + 3 + 3 * (study.transition_years) + 3
        for wk_slug, lbl in self.get_workshop_list(study):
            if wk_slug in ('cultures', 'bovin-lait'):
                continue
            self.workshop = models.Workshop.get_cached(txt_idx=wk_slug)[0]
            kwargs['workshop'] = wk_slug
            self.dispatch(request, **kwargs)
            self.request = request
            context_kwargs = {}
            context.update(self.get_context_data(**context_kwargs))
            if not content:
                content = '['
            else:
                content += ','
            content += render_to_string(self.print_template_name, context)
        if content:
            content += ']'
        return content


class FullEcoMilkWorkshopView(
        WorkshopMixin, PrintableMixin, InlineStudyFormSetView,
        ExtraYearsExtraFormsMixin):
    template_name = 'simulabio/economy_full_milkworkshop.html'
    print_template_name = 'simulabio/print/economy_full_milkworkshop.json'
    model = models.Workshop
    inline_model = models.WorkshopExpenses
    form_class = forms.WorkshopForm
    formset_class = forms.WorkshopFormSet
    year_models = (
        ExtraYearFormConf(models.Revenues, forms.RevenuesFullEcoForm),
        ExtraYearFormConf(models.Expenses, forms.ExpensesFullEcoForm),
        ExtraYearFormConf(
            models.ExtraSupplyPriceYear, forms.ExtraSupplyPriceYearForm,
            study_key='extra_supply_prices__study',
            parent_model=models.ExtraSupplyPrice),)
    extra_forms = {'extra_revenue': forms.ExtraRevenueForm}

    def check_and_init(self, request, *args, **kwargs):
        redir = super(FullEcoMilkWorkshopView, self).check_and_init(
            request, *args, **kwargs)
        if redir:
            return redir
        # init extrasupply
        c = Compute(self.study.pk)
        c.initialize_extrasupplyprices()
        c.save()
        self.workshop = models.Workshop.get_cached(txt_idx='bovin-lait')[0]

    def get_context_data(self, **kwargs):
        formset = self.construct_formset()
        kwargs['formset'] = formset
        return super(FullEcoMilkWorkshopView, self).get_context_data(**kwargs)

    def get_formset_kwargs(self, *args, **kwargs):
        kwargs = super(FullEcoMilkWorkshopView, self)\
            .get_formset_kwargs(*args, **kwargs)
        kwargs['workshop'] = self.workshop
        return kwargs

    def get_factory_kwargs(self, *args, **kwargs):
        kwargs = super(FullEcoMilkWorkshopView, self
                       ).get_factory_kwargs(*args, **kwargs)
        kwargs["can_delete"] = False
        kwargs["extra"] = 0
        return kwargs

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(FullEcoMilkWorkshopView, self).get(
            request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        self.object = self.get_object()
        forms = self.construct_forms()
        for f in forms:
            if f.is_valid():
                f.save()
        formset = self.construct_formset()
        if formset.is_valid():
            self.object_list = formset.save()
        posted = super(FullEcoMilkWorkshopView, self).post(
            request, *args, **kwargs)
        c = Compute(self.study.pk)
        c.calculate_db_expenses()
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        return posted

    def printing_kwargs(self, request, context, study):
        kwargs = super(FullEcoMilkWorkshopView, self).printing_kwargs(
            request, context, study)
        context['col_number'] = 1 + 2 + 2 + 2 * (study.transition_years)
        context['expenses_col_number'] = \
            1 + 3 + 3 + 3 * (study.transition_years)
        return kwargs


class FullEcoRevenuesView(FullEcoMixin, PrintableMixin,
                          ExtraYearsExtraFormsMixin):
    template_name = 'simulabio/economy_full_revenues.html'
    print_template_name = 'simulabio/print/economy_full_revenues.json'
    year_models = (
        [],
        ExtraYearFormConf(
            models.ExtraRevenueYear, forms.ExtraRevenueYearForm,
            study_key='extra_revenue__study', parent_model=models.ExtraRevenue,
            extra_exclude={'full_eco': False}),)
    extra_forms = {'extra_revenue': forms.ExtraRevenueForm}

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(FullEcoRevenuesView, self).post(request, *args,
                                                       **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ERV', 'ESY'])
        return posted

    def printing_kwargs(self, request, context, study):
        kwargs = super(FullEcoRevenuesView, self).printing_kwargs(
            request, context, study)
        context['col_number'] = 1 + 3 + 3 + 3 * (study.transition_years)
        return kwargs


class FullEcoExpensesView(FullEcoMixin, PrintableMixin,
                          ExtraYearsExtraFormsMixin):
    template_name = 'simulabio/economy_full_expenses.html'
    print_template_name = 'simulabio/print/economy_full_expenses.json'
    year_models = (
        [],
        ExtraYearFormConf(
            models.ExtraExpenseYear, forms.ExtraExpenseYearForm,
            study_key='extra_expenses__study',
            parent_model=models.ExtraExpenses,
            extra_exclude={'full_eco': False},))
    extra_forms = {'extra_expense': forms.ExtraExpenseForm}

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(FullEcoExpensesView, self).post(request, *args,
                                                       **kwargs)
        c = Compute(self.study.pk)
        c.calculate_db_expenses()
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['EEX', 'ESY'])
        return posted

    def printing_kwargs(self, request, context, study):
        kwargs = super(FullEcoExpensesView, self).printing_kwargs(
            request, context, study)
        context['col_number'] = 3 + study.transition_years
        return kwargs


class FullEcoInvestmentsView(FullEcoMixin, SimpleEcoInvestmentsView):
    template_name = 'simulabio/economy_full_investments.html'

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(FullEcoInvestmentsView, self).post(request, *args,
                                                          **kwargs)
        tasks = (('calculate_db_economic_synthesis', []),)
        compute(self.study.pk, tasks, ['ESY'])
        return posted


class FullEcoSynthesisView(FullEcoMixin, PrintableMixin,
                           ExtraYearFormsView):
    LOCKS = ['HHA', 'AFN', 'ESY', 'ERV', 'EEX']
    template_name = 'simulabio/economy_full_synthesis.html'
    print_template_name = 'simulabio/print/economy_full_synthesis.json'
    year_models = (ExtraYearFormConf(models.EconomicSynthesis,
                                     forms.FullEconomicSynthesisForm),)

    def post(self, request, *args, **kwargs):
        redir = self.comment_submission(request)
        if redir:
            return redir
        posted = super(FullEcoSynthesisView, self).post(request, *args,
                                                        **kwargs)
        c = Compute(self.study.pk)
        c.calculate_db_economic_synthesis()
        c.save()
        return posted

    def printing_kwargs(self, request, context, study):
        kwargs = super(FullEcoSynthesisView, self).printing_kwargs(
            request, context, study)
        q = models.Workshop.objects.filter(study=study)
        wks_nb = q.count()
        context['grey_rows'] = [0, 1] + [4 + n for n in range(wks_nb)] + \
            [5 + wks_nb] + [7 + wks_nb + n for n in range(wks_nb)] + \
            [8 + 2 * wks_nb, 9 + 2 * wks_nb, 12 + 2 * wks_nb] + \
            [16 + 2 * wks_nb, 17 + 2 * wks_nb, 18 + 2 * wks_nb]
        context['green_rows'] = [2, 6 + wks_nb]
        context['brown_rows'] = [3, 7 + wks_nb]
        return kwargs


class ReportGenerationView(StudyCheckMixin, TemplateView):
    views = [
        StudyUpdate, HarvestSettingsView, HarvestPlotsView,
        HarvestRotationDominantView, AnimalsHerdView, AnimalsFeedView,
        AnimalsLitterView, MilkProductionView, HarvestProductionView,
        AnimalsPrimaryBalanceView, BalanceFodderView, EconomySettingsView,
        SimpleEcoInitialView, SimpleEcoRevenuesView, SimpleEcoExpensesView,
        SimpleEcoInvestmentsView, SimpleEcoSynthesisView,
        FullEcoHarvestWorkshopView, FullEcoMilkWorkshopView,
        FullEcoWorkshopView, FullEcoRevenuesView, FullEcoExpensesView,
        FullEcoInvestmentsView, FullEcoSynthesisView]
    view_dct = dict([(v.__name__, v) for v in views])
    template_name = 'simulabio/study_report_gen.html'
    _comments = False
    form_class = forms.ReportGenerationForm

    def get_context_data(self, form, **kwargs):
        context = super(ReportGenerationView, self).get_context_data(**kwargs)
        context['gen_forms'] = list(models.GenericForm.objects.filter(
            available=True).order_by('order').all())
        context['form'] = form
        return context

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data(
            form=self.form_class()))

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if not form.is_valid():
            return self.render_to_response(self.get_context_data(form=form))
        submited = form.save()
        base_context = {}
        base_context['farm'] = self.study.farm
        base_context['study'] = self.study
        # get a fake request
        self.factory = RequestFactory()
        fake_request = self.factory.get('.')
        fake_request.user = request.user
        fake_request.session = {}
        docs = []
        tempdir = tempfile.mkdtemp("-simulabio-reports")
        for report_target, report_template in submited:
            if report_target.generic_form:
                view = GenericFormView
                view.form_key = report_target.generic_form.slug
            elif report_target.class_name in self.view_dct:
                view = self.view_dct[report_target.class_name]
            else:
                continue
            json_val = view().printing(fake_request, base_context, self.study)
            if not json_val:
                continue
            # print(json_val)
            contexts = json.loads(json_val)
            for idx, context in enumerate(contexts):
                docs.append(
                    report_target.generate_doc(
                        report_template.template,
                        context.copy(), tempdir=tempdir,
                        extra_file_name=str(idx)))
            if report_target.generic_form:
                view.form_key = None

        doc = None
        name = '%s_%s_%s.odt' % (slugify(unicode(self.study.farm)),
                                 slugify(self.study.name),
                                 datetime.datetime.now().strftime('%Y-%m-%d'))
        if not docs:
            return redirect('.')
        elif len(docs) == 1:
            doc = docs[0]
        else:
            doc = tempdir + os.sep + name
            o = OOoPy(infile=docs[0], outfile=doc)
            if len(docs) > 1:
                t = Transformer(o.mimetype, Transforms.get_meta(o.mimetype),
                                Transforms.Concatenate(*(docs[1:])),
                                Transforms.renumber_all(o.mimetype),
                                Transforms.set_meta(o.mimetype),
                                Transforms.Fix_OOo_Tag(),
                                Transforms.Manifest_Append())
                t.transform(o)
            o.close()

        response = HttpResponse(
            open(doc), mimetype='application/vnd.oasis.opendocument.text')
        response['Content-Disposition'] = 'attachment; filename=%s' % name
        return response


class ReportView(StudyCheckMixin, TemplateView):
    views = [StudyUpdate, HarvestSettingsView, HarvestPlotsView,
             HarvestRotationDominantView, AnimalsHerdView, AnimalsFeedView,
             AnimalsLitterView, MilkProductionView, HarvestProductionView,
             AnimalsPrimaryBalanceView, BalanceFodderView, EconomySettingsView,
             SimpleEcoInitialView, SimpleEcoRevenuesView,
             SimpleEcoExpensesView, SimpleEcoInvestmentsView,
             SimpleEcoSynthesisView, FullEcoHarvestWorkshopView]
    template_name = 'simulabio/print/main.html'

    def get_context_data(self, **kwargs):
        context_data = super(ReportView, self).get_context_data(**kwargs)
        context_data['farm'] = self.study.farm
        context_data['study'] = self.study
        return context_data

    @classmethod
    def fetch_resources(cls, uri, rel):
        if settings.MEDIA_URL in uri:
            path = settings.MEDIA_ROOT + uri.replace(settings.MEDIA_URL, "")
            path = path.replace('//', '/')
            return path
        return uri

    def get(self, request, *args, **kwargs):
        self.object = self.study
        base_context = self.get_context_data(**kwargs)

        content = ""
        for view in self.views:
            content += view().printing(request, base_context, self.object)

        base_context['content'] = content
        content = render_to_string(self.template_name, base_context,
                                   context_instance=RequestContext(request))
        # return HttpResponse(content)
        result = StringIO.StringIO()
        html = content.encode('utf-8')
        html = html.replace("<table", "<table repeat='1'")
        pdf = pisa.pisaDocument(
            StringIO.StringIO(html), result, encoding='utf-8',
            link_callback=self.fetch_resources)
        response = HttpResponse(result.getvalue(),
                                mimetype='application/pdf')

        filename = slugify(u"%s - %s - %s" % (
            datetime.datetime.now().strftime('%Y%m%d'),
            unicode(self.study.farm), unicode(self.study)))
        response['Content-Disposition'] = 'attachment; filename=%s.pdf' % \
            filename
        if not pdf.err:
            return response
        return HttpResponse(content, content_type="application/xhtml")
