#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

"""
Import feed from a CSV file
"""

import codecs

from django.conf import settings
from django.template.loader import render_to_string
from django.template.defaultfilters import slugify

from simulabio.models import HarvestCategory, FeedItem, FeedTemplate, \
    FeedTemplateDetail, UsageType
from simulabio.unicode_csv_reader import unicode_csv_reader


def import_feed(filename, farming_type='BIO'):
    try:
        values = unicode_csv_reader(codecs.open(filename, 'rb', "utf-8"),
                                    delimiter=settings.CSV_DELIMITER,
                                    quotechar=settings.CSV_QUOTECHAR)
    except (IOError):
        return [u"Incorrect CSV file."]
    # check CSV header
    feed_sample = render_to_string('simulabio/files/feed_sample.csv')
    sample_values = unicode_csv_reader(
        feed_sample.split('\n'), delimiter=settings.CSV_DELIMITER,
        quotechar=settings.CSV_QUOTECHAR)
    try:
        header = sample_values.next()
        assert(header == values.next())
    except:
        return [u"CSV file doesn't match the reference header: %s" %
                (feed_sample.split('\n')[0])]
    ref_feeds = {}
    for items in values:
        feed_name, feed = items[0:2]
        if feed_name not in ref_feeds:
            ref_feeds[feed_name] = {}
        ref_feeds[feed_name][feed] = items[2:]
    feeds = {}
    for feed_name in ref_feeds:
        txt_idx = slugify(feed_name)
        feed_template, created = FeedTemplate.objects.get_or_create(
            txt_idx=txt_idx, farming_type=farming_type,
            defaults={'name': feed_name})
        for feed in ref_feeds[feed_name]:
            items = ref_feeds[feed_name][feed]
            hc_name, usage_name = items[0:2]
            months = items[2:]
            if feed not in feeds:
                txt_idx = slugify(feed)
                feeds[feed], created = FeedItem.objects.get_or_create(
                    txt_idx=txt_idx, defaults={'name': feed})
                txt_idx = slugify(hc_name)
                hc, created = HarvestCategory.objects.get_or_create(
                    txt_idx=txt_idx, defaults={'name': hc_name})
                if feeds[feed].harvest_category != hc:
                    feeds[feed].harvest_category = hc
                    feeds[feed].save()
                txt_idx = slugify(usage_name)
                usage, created = UsageType.objects.get_or_create(
                    txt_idx=txt_idx, defaults={'name': usage_name})
                if feeds[feed].usage_type != usage:
                    feeds[feed].usage_type = usage
                    feeds[feed].save()
            ftd, created = FeedTemplateDetail.objects.get_or_create(
                feed_template=feed_template, feed_item=feeds[feed])
            quantities = [float(mt.replace(',', '.'))
                          if mt else 0 for mt in months]
            for idx, quantity in enumerate(quantities):
                month_nb = idx + 1
                setattr(ftd, 'amount_%d' % month_nb, quantity)
            ftd.save()
