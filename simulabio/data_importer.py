#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

"""
# Usage exemple (extracted from simulabio application)

class HarvestPlotImporter(Importer):
    LINE_FORMAT = [
        ImportFormater('name', Importer.get_unicode_formater(100)),
        ImportFormater('plot_group_number',
                       Importer.get_unicode_formater(3), required=False),
        ImportFormater('geographical_area', unicode, required=False),
        ImportFormater('soil_type', Importer.choices_check(SOIL_TYPE),
                                                     required=False),
        ImportFormater('cow_access', Importer.boolean_formater),
        ImportFormater('area', Importer.float_formater),
        ImportFormater('remark', unicode, required=False),
        ImportFormater('diagnostic', Importer.boolean_formater),
        ImportFormater('project', Importer.boolean_formater),
        ImportFormater('harvest_n2', 'harvest_formater', required=False),
        ImportFormater('harvest_n1', 'harvest_formater', required=False),
        ImportFormater('harvest', 'harvest_formater'),
        ImportFormater('harvest_setting', 'harvest_formater',
                              through=HarvestTransition,
                              through_key='plot',
                              through_dict={'year':1},
                              through_unicity_keys=['plot', 'year'],
                              required=False),
               ImportFormater('harvest_setting', 'harvest_formater',
                              through=HarvestTransition,
                              through_key='plot',
                              through_dict={'year':2},
                              through_unicity_keys=['plot', 'year'],
                              required=False),
               ImportFormater('harvest_setting', 'harvest_formater',
                              through=HarvestTransition,
                              through_key='plot',
                              through_dict={'year':3},
                              through_unicity_keys=['plot', 'year'],
                              required=False),
               ImportFormater('harvest_setting', 'harvest_formater',
                              through=HarvestTransition,
                              through_key='plot',
                              through_dict={'year':4},
                              through_unicity_keys=['plot', 'year'],
                              required=False),
               ImportFormater('harvest_setting', 'harvest_formater',
                              through=HarvestTransition,
                              through_key='plot',
                              through_dict={'year':5},
                              through_unicity_keys=['plot', 'year'],
                              required=False),
               ImportFormater('harvest_setting', 'harvest_formater',
                              through=HarvestTransition,
                              through_key='plot',
                              through_dict={'year':6},
                              through_unicity_keys=['plot', 'year'],
                              required=False),
               ]
    OBJECT_CLS = HarvestPlots
    UNICITY_KEYS = []

    def __init__(self, study, skip_first_line=None):
        # get the reference header
        dct = {'separator':settings.CSV_DELIMITER}
        dct['data'] = Harvest.objects.filter(available=True).all()
        reference_file = render_to_string('simulabio/files/parcelles_ref.csv',
                                          dct)
        reference_header = unicode_csv_reader(
                                [reference_file.split('\n')[0]]).next()
        super(HarvestPlotImporter, self).__init__(
                                           skip_first_line=skip_first_line,
                                           reference_header=reference_header)
        self.study = study
        self.default_vals = {'study':self.study}

    def harvest_formater(self, value):
        value = value.strip()
        if not value:
            return
        try:
            harvest = Harvest.objects.get(name__iexact=value)
        except ObjectDoesNotExist:
            raise ValueError(_(u"\"%(value)s\" not in %(values)s") % {
                'value':value,
                'values':u", ".join([val.name
                        for val in Harvest.objects.filter(available=True)])
                })
        hs, created = HarvestSettings.objects.get_or_create(study=self.study,
                                                            harvest=harvest)
        if created:
            self.message = _(u"\"%(harvest)s\" has been added in your settings"
                             u"don't forget to fill yields for this harvest.")\
                             % {'harvest':harvest.name}
        return hs

class HarvestPlotsImportForm(forms.Form):
    csv_file = forms.FileField(label=_(u"Plot list file (CSV)"))

    def save(self, study):
        csv_file = self.cleaned_data['csv_file']
        importer = models.HarvestPlotImporter(study, skip_first_line=True)
        # some softwares (at least Gnumeric) convert CSV file to utf-8 no
        # matter what the CSV source encoding is
        encodings = [settings.ENCODING, 'utf-8']
        for encoding in encodings:
            try:
                importer.importation(unicode_csv_reader(
                                [line.decode(encoding)
                                 for line in csv_file.readlines()]))
            except ImporterError, e:
                if e.type == ImporterError.HEADER and \
                        encoding != encodings[-1]:
                    csv_file.seek(0)
                    continue
                return 0, [[0, 0, e.msg]], []
            except UnicodeDecodeError, e:
                return 0, [[0, 0, Importer.ERRORS['header_check']]], []
            break
        return importer.number_imported, importer.errors, importer.messages
"""

import datetime
import logging

from django.utils.translation import ugettext_lazy as _


class ImportFormater(object):
    def __init__(self, field_name, formater, required=True, through=None,
                 through_key=None, through_dict=None,
                 through_unicity_keys=None):
        self.field_name = field_name
        self.format = formater
        self.required = required
        self.through = through
        self.through_key = through_key
        self.through_dict = through_dict
        self.through_unicity_keys = through_unicity_keys

    def __unicode__(self):
        return self.field_name


class ImporterError(Exception):
    STANDARD = 'S'
    HEADER = 'H'

    def __init__(self, message, type='S'):
        self.msg = message
        self.type = type

    def __str__(self):
        return self.msg

logger = logging.getLogger(__name__)


class Importer(object):
    LINE_FORMAT = []
    OBJECT_CLS = None
    UNICITY_KEYS = []
    ERRORS = {
        'header_check': _(
            u"The given file is not correct. Check the file "
            u"format. If you use a CSV file: check that column separator "
            u"and encoding are similar to the ones used by the reference "
            u"file."),
        'too_many_cols': _(u"Too many cols (%(user_col)d) when "
                           u"maximum is %(ref_col)d"),
        'no_data': _(u"No data provided"),
        'value_required': _(u"Value is required"),
        'not_enough_cols': _(u"At least %d columns must be filled")
    }

    def __init__(self, skip_first_line=False, reference_header=None):
        """
         * skip_first_line must be set to True if the data provided has got
           an header.
         * a reference_header can be provided to perform a data compliance
           check. It can be useful to warn about bad parsing.
        """
        self.message = ''
        self.default_vals = {}
        self.skip_first_line = skip_first_line
        self.reference_header = reference_header
        self.errors = []  # list of (line, col, message)
        self.messages = []  # list of (line, col, message)
        self.number_imported = 0

    def importation(self, table):
        table = list(table)
        if not table or not table[0]:
            raise ImporterError(self.ERRORS['no_data'], ImporterError.HEADER)
        if len(table[0]) > len(self.LINE_FORMAT):
            raise ImporterError(self.ERRORS['too_many_cols'] % {
                'user_col': len(table[0]), 'ref_col': len(self.LINE_FORMAT)})
        self.errors = []
        self.messages = []
        self.number_imported = 0
        # index of the last required column
        for idx_last_col, formater in enumerate(reversed(self.LINE_FORMAT)):
            if formater.required:
                break
        else:
            idx_last_col += 1
        # min col number to be filled
        min_col_number = len(self.LINE_FORMAT) - idx_last_col
        # check the conformity with the reference header
        if self.reference_header and \
           self.skip_first_line and \
           self.reference_header != table[0]:
            raise ImporterError(self.ERRORS['header_check'],
                                type=ImporterError.HEADER)
        now = datetime.datetime.now()
        for idx_line, line in enumerate(table):
            if (self.skip_first_line and not idx_line)\
               or not line:
                continue
            throughs = []  # list of (formater, value)
            data = self.default_vals.copy()
            n = datetime.datetime.now()
            logger.debug('%s - Processing line %d' %
                         (unicode(n - now), idx_line))
            now = n
            n2 = n
            c_errors = False
            for idx_col, val in enumerate(line):
                formater = self.LINE_FORMAT[idx_col]
                self.message = ''
                func = formater.format
                if not callable(func) and type(func) in (unicode, str):
                    func = getattr(self, formater.format)
                try:
                    value = func(val)
                except ValueError, e:
                    c_errors = True
                    self.errors.append((idx_line + 1, idx_col + 1, e.message))
                    continue
                if self.message:
                    self.messages.append(self.message)
                if value is None:
                    if formater.required:
                        c_errors = True
                        self.errors.append((idx_line + 1, idx_col + 1,
                                           self.ERRORS['value_required']))
                    continue
                if not formater.through:
                    data[formater.field_name] = value
                else:
                    throughs.append((formater, value))
            if not c_errors and (idx_col + 1) < min_col_number:
                c_errors = True
                self.errors.append(
                    (idx_line + 1, idx_col + 1,
                     self.ERRORS['not_enough_cols'] % min_col_number))
            if c_errors:
                continue
            n = datetime.datetime.now()
            logger.debug('* %s - Cols read' % (unicode(n - n2)))
            n2 = n
            # manage unicity of items (mainly for updates)
            self.number_imported += 1
            if self.UNICITY_KEYS:
                data['defaults'] = {}
                for k in data.keys():
                    if k not in self.UNICITY_KEYS \
                       and k != 'defaults':
                        data['defaults'][k] = data.pop(k)
            obj, created = self.OBJECT_CLS.objects.get_or_create(**data)

            if not created and 'defaults' in data:
                for k in data['defaults']:
                    setattr(obj, k, data['defaults'][k])
                obj.save()
            n = datetime.datetime.now()
            logger.debug('* %s - Item saved' % (unicode(n - n2)))
            n2 = n
            for formater, value in throughs:
                n = datetime.datetime.now()
                logger.debug('* %s - Processing formater %s' %
                             (unicode(n - n2), formater.field_name))
                n2 = n
                data = {}
                if formater.through_dict:
                    data = formater.through_dict.copy()
                if formater.through_key:
                    data[formater.through_key] = obj
                data[formater.field_name] = value
                through_cls = formater.through
                if formater.through_unicity_keys:
                    data['defaults'] = {}
                    for k in data.keys():
                        if k not in formater.through_unicity_keys \
                           and k != 'defaults':
                            data['defaults'][k] = data.pop(k)
                t_obj, created = through_cls.objects.get_or_create(**data)
                if not created and 'defaults' in data:
                    for k in data['defaults']:
                        setattr(t_obj, k, data['defaults'][k])
                    t_obj.save()

    @classmethod
    def choices_check(cls, choices):
        def function(value):
            choices_dct = dict(choices)
            value = value.strip()
            if not value:
                return
            if value not in choices_dct.values():
                raise ValueError(_(u"\"%(value)s\" not in %(values)s") % {
                    'value': value,
                    'values': u", ".join([val for val in choices_dct.values()])
                })
            return value
        return function

    @classmethod
    def get_unicode_formater(cls, max_length):
        def formater(value):
            try:
                value = unicode(value)
            except UnicodeDecodeError:
                return
            if len(value) > max_length:
                raise ValueError(_(u"\"%(value)s\" is too long. "
                                   u"The max length is %(length)d characters."
                                   ) % {'value': value, 'length': max_length})
            return value
        return formater

    @classmethod
    def boolean_formater(cls, value):
        value = value.strip().upper()
        if value in ('1', 'OUI', 'VRAI', 'YES', 'TRUE'):
            return True
        if value in ('', '0', 'NON', 'FAUX', 'NO', 'FALSE'):
            return False
        raise ValueError(_(u"\"%(value)s\" not equal to yes or no") % {
            'value': value})

    @classmethod
    def float_formater(cls, value):
        value = value.strip().replace(',', '.')
        if not value:
            return
        try:
            return float(value)
        except ValueError:
            raise ValueError(_(u"\"%(value)s\" is not a float") % {
                'value': value})
