
def clean_duplicate(orm, model_name, unique_together):
    ordering = list(unique_together) + ['-pk']
    items = orm[model_name].objects.order_by(*ordering)
    c_tuple, to_delete = None, []
    for item in items:
        n_tuple = []
        for keys in unique_together:
            v = item
            for k in keys.split('__'):
                v = getattr(v, k)
            if hasattr(v, 'pk'):
                v = v.pk
            n_tuple.append(v)
        if c_tuple == n_tuple:
            to_delete.append(item.pk)
        c_tuple = n_tuple[:]

    if to_delete:
        orm[model_name].objects.filter(pk__in=to_delete).delete()
