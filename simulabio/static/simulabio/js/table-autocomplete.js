
function table_input_changed(item, val, jump){
   if (!jump) jump = 0;
   if ($(item).val() == '' || $(item).val() == null){
        $(item).val(val);
        $(item).parent().nextAll().eq(jump).children('input[type="text"]').each(
            function(){ table_input_changed(this, val, jump) }
        );
   }
}

function table_autocomplete(table_id, jump, skip_first){
    if (!jump) jump = 0;
    $('#' + table_id + " td input[type='text']").change(
        function(){
            if (skip_first &&
                ($(this).parents('tr').children("td"
                                     ).children("input[type='text']").attr('id'))
                    == $(this).attr('id')){
                return;
            }

            var val = $(this).val();
            $(this).parent().nextAll().eq(jump).children('input[type="text"]'
                ).each(
                function(){ table_input_changed(this, val, jump); }
            );
        }
    );
}

