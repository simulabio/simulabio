$(document).ready(function(){
    $("#steps").buttonset();
    $('#steps input[type="radio"]').click(function(){
        window.location.replace(base_url + $(this).val() + "/");
    });
    $("#rotation_template_add_form").dialog({modal:true,
                            autoOpen:false,
                            width:500
                           });
    $('#rotation_template_add').click(function(){
        $("#rotation_template_add_form").dialog('open');
    });
    $("#rotation_modify_form").dialog({modal:true,
                            autoOpen:false,
                            width:500
                           });
    $('#rotation_modify').click(function(){
        $("#rotation_modify_form").dialog('open');
    });
    $('#rotation_template_remove').click(function(){
        $("#rotation_template_remove_confirm").dialog({autoOpen:false});
        $("#rotation_template_remove_confirm").dialog('option', 'buttons',[
            {
                text:confirm_label,
                click : function() {
                    window.location.href = delete_url;
                }
            },
            {
                text:cancel_label,
                click : function() {
                    $(this).dialog("close");
                }
            }
          ]);
        $('#rotation_template_remove_confirm').dialog('open');
    });

    $('#rotation_template').change(function(){
        var url = base_url +
            $('#steps input[type="radio"][checked="checked"]').val() + "/";
        var selected_tpl = $(this).val();
        if (selected_tpl) url += selected_tpl + "/";
        window.location.replace(url);
    });
});
