function frenchfloatunformatter (cellvalue, options){
    return cellvalue.replace(",", ".");
}

$(document).ready(function(){

    $('#plots_modify').attr('disabled', 'disabled').addClass('ui-state-disabled');
    $('#plots_remove').attr('disabled', 'disabled').addClass('ui-state-disabled');
    function onSelection(){
        var length = $('#plot_list').getGridParam('selarrrow').length;
        if (length == 1){
            $('#plots_modify').removeAttr('disabled').removeClass('ui-state-disabled');
        } else {
            $('#plots_modify').attr('disabled', 'disabled').addClass('ui-state-disabled');
        }
        if (length == 0){
            $('#plots_remove').attr('disabled', 'disabled').addClass('ui-state-disabled');
        } else {
            $('#plots_remove').removeAttr('disabled').removeClass('ui-state-disabled');
        }
    }
    $('#plot_list_div').show();
    $('#plot_form').show();

    var options = {
                   rowNum:10000,
                   autowidth:true,
                   shrinkToFit:false,
                   onSelectRow:onSelection,
                   onSelectAll:onSelection,
                   datatype: 'local'
                   }
    var c_rowid = '';
    var c_col = '';
    if (extra_id == 'full'){
        options['height'] = 350;
        options['beforeSelectRow'] = function(rowid, e) {
            return false;
        };
        saveToCell = function(item){
            var col = jQuery(item).parent().attr('aria-describedby'
                        ).substr('plot_list_'.length);
            var rowId = jQuery(item).parent().parent().attr('id');
            $("#plot_list").jqGrid('setCell', rowId, col,
                                   $('#hidden-div').html());
        };
        loadComplete = function() {
            jQuery(".jqgrow > td > input:text", this).live('change', function(e) {
                var val = jQuery(this).clone();
                $('#hidden-div').html(val);
                $('#hidden-div').find('input').attr('value', $(this).val());
                saveToCell(this);
            });
            jQuery(".jqgrow > td > input:checkbox", this).live('change', function(e) {
                var val = jQuery(this).clone();
                $('#hidden-div').html(val);
                if($(this).is(':checked')){
                    $('#hidden-div').find('input').attr('checked', 'checked');
                } else {
                    $('#hidden-div').find('input').removeAttr('checked');
                }
                saveToCell(this);
            });
            jQuery(".jqgrow > td > select", this).live('change', function(e) {
                var val = jQuery(this).clone();
                $('#hidden-div').html(val);
                $('#hidden-div').find("select option:selected"
                               ).removeAttr('selected');
                $('#hidden-div').find("select option[value="+$(this).val()+"]"
                               ).attr('selected', 'selected');
                saveToCell(this);
            });
        }
        options['loadComplete'] = loadComplete;

        onCellSelect = function(rowid, col, content, e) {
            c_rowid = rowid;
            c_col = col;
        };
        options['onCellSelect'] = onCellSelect;
    } else {
        options['multiselect'] = true;
    }

    tableToGrid("#plot_list", options);

    // numeric sorting
    for (idx=0;idx<numeric_cols.length;idx++){
        var colname = numeric_cols[idx];
        jQuery("#plot_list").setColProp(colname,
                {sorttype:'float', unformat:frenchfloatunformatter});
    }

    sortplot = function(a, b, d){
        if(a === undefined) {
            a = null;
        } else {
            a = a.slice(1);
        }
        if(b === undefined) {
            b = null;
        } else {
            b = b.slice(1);
        }
        return sortnum(a, b, d);
    }

    // plot sorting
    if (plot_col){
        jQuery("#plot_list").setColProp(plot_col, {sortfunc:sortplot})
    }

    sortnum = function(a, b, d){
        if(d === undefined) { d = 1; }
        if(a === undefined) { a = null; }
        if(b === undefined) { b = null; }
        if(a === null && b === null){
            return 0;
        }
        if(a === null && b !== null){
            return 1;
        }
        if(a !== null && b === null){
            return -1;
        }
        a = parseFloat(a.replace(",", "."))*10000;
        b = parseFloat(b.replace(",", "."))*10000;
        if(a<b){return -d;}
        if(a>b){return d;}
        return 0;
    }
    sortselect = function(cell, obj){
        $('#hidden-div').html(cell);
        var val = $('#hidden-div input').val();
        if (val === undefined){
            val = $('#hidden-div select').val();
        }
        if (val == 'on'){
            val = $('#hidden-div input').is(':checked');
        }
        return val;
    }

    if (extra_id == 'full'){
        var currcolModel  = $("#plot_list").jqGrid('getGridParam',
                                                   'colModel');
        for (idx=0;idx<currcolModel.length;idx++){
            var colname = currcolModel[idx].name;
            if (numeric_cols.indexOf(colname) == -1){
                jQuery("#plot_list").setColProp(colname,
                    {sorttype:sortselect});
            } else if (colname == plot_col) {
                jQuery("#plot_list").setColProp(colname,
                                    {sortfunc:sortplot});
            } else {
                jQuery("#plot_list").setColProp(colname,
                    {sorttype:sortselect,
                     sortfunc:sortnum});
            }
        }
    }

    $('#column_chooser').click(function(){
        jQuery("#plot_list").jqGrid('columnChooser', {
            done : function (perm) {
                if (perm) {
                    this.jqGrid("remapColumns", perm, true);
                    var currcolModel  = $("#plot_list").jqGrid('getGridParam',
                                                               'colModel');
                    var hidden_cols = new Array();
                    for (idx=0;idx<currcolModel.length;idx++){
                        if (currcolModel[idx].hidden){
                            hidden_cols.push(currcolModel[idx].name);
                        }
                    }
                    createCookie('plot_list_hidden' + extra_id, hidden_cols);
                    createCookie('plot_list_sort' + extra_id, perm);
                }
                return false;
            }
        });
    });
    var hidden_cols = readCookie('plot_list_hidden' + extra_id);
    var col_sort = readCookie('plot_list_sort' + extra_id);
    if (jQuery("#plot_list").length){
        var col_model = jQuery("#plot_list").jqGrid('getGridParam', 'colModel');
        if (extra_id != 'full'){
            jQuery("#plot_list").jqGrid('hideCol', col_model[0].name);
        }
        if (!hidden_cols){
            for (idx=0;idx<col_model.length;idx++){
                if (idx > default_col_number){
                    jQuery("#plot_list").jqGrid('hideCol', col_model[idx].name);
                }
            }
        } else {
            hidden_cols = hidden_cols.split(',');
            for (idx=0;idx<hidden_cols.length;idx++){
                jQuery("#plot_list").jqGrid('hideCol', hidden_cols[idx]);
            }
        }
        if (col_sort){
            col_sort = col_sort.split(',');
            $("#plot_list").jqGrid('remapColumns', col_sort, true);
        }
    }
    // plot form
    $("#plot_form").dialog({modal:true,
                            autoOpen:dialog_opened,
                            width:500,
                            close: function(event, ui) {
                                window.location.replace(create_url);
                            }
                           });
    $('#plots_add').click(function(){
        $("#plot_form").dialog('open');
    });
    $('#plots_modify').click(function(){
        var url = create_url + 
                  plot_array[$('#plot_list').getGridParam('selarrrow')] +
                  '/modify/';
        window.location.replace(url);
    });
    $('#plots_remove').click(function(){
        var res_array = new Array();
        var sel_items = $('#plot_list').getGridParam('selarrrow');
        for (idx=0 ; idx < sel_items.length ; idx ++){
            res_array.push(plot_array[sel_items[idx]]);
        }
        var url = create_url + res_array.join('_') + '/delete/';
        window.location.replace(url);
    });
    if (import_result_window){
        $("#import_result").dialog({modal:true,
                                autoOpen:true,
                                width:500,
                                close: function(event, ui) {
                                    window.location.replace(create_url);
                                }
                               });
        $('#import_result_ok').click(function (){
                                window.location.replace(create_url);
                            });
    }
});
