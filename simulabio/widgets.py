#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2013  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

import datetime
import locale

from django import forms
from django.conf import settings
from django.forms import widgets
from django.forms.util import flatatt
from django.utils.html import escape, conditional_escape
from django.template import Context, Template
from django.template.loader import render_to_string
from django.utils.encoding import force_text, force_unicode
from django.utils.safestring import mark_safe


class ReadOnlyWidget(forms.Widget):
    def __init__(self, *attrs, **kwargs):
        if 'choices' in kwargs:
            self.choices = kwargs.pop('choices')
        return super(ReadOnlyWidget, self).__init__(*attrs, **kwargs)

    def _get_label(self, value):
        label = value
        if hasattr(self, 'choices'):
            for idx, lbl in self.choices:
                if idx == label:
                    label = lbl
        return label

    def render(self, name, value, attrs):
        final_attrs = self.build_attrs(attrs, name=name)
        if hasattr(self, 'initial'):
            value = self.initial
        value = value if value is not None else ''
        label = self._get_label(value)
        return mark_safe("<p><input %s type='hidden' value=\"%s\"/>%s</p>" % (
                         flatatt(final_attrs), value, label))

    def _has_changed(self, initial, data):
        return False


class ReadOnlyPercentWidget(ReadOnlyWidget):
    def _get_label(self, value):
        label = super(ReadOnlyPercentWidget, self)._get_label(value)
        return int(round(label * 100)) if label else ''


class ReadOnlyIntWidget(ReadOnlyWidget):
    def _get_label(self, value):
        label = super(ReadOnlyIntWidget, self)._get_label(value)
        try:
            locale.setlocale(locale.LC_ALL, settings.LOCALE)
        except:
            pass
        try:
            return '{0:n}'.format(label)
        except ValueError:
            return label


class MultiSelect(widgets.MultiWidget):
    def __init__(self, choices, init_num=5, attrs=None):
        wdgets = [widgets.Select(choices=list(choices))
                  for i in xrange(init_num)]
        self.init_num = init_num
        super(MultiSelect, self).__init__(wdgets, attrs)

    def format_output(self, rendered_widgets):
        return u"</td><td>".join(rendered_widgets)

    def value_from_datadict(self, data, files, name):
        vals = [widget.value_from_datadict(data, files, name + '_%s' % i)
                for i, widget in enumerate(self.widgets)]
        return [v for v in vals if v]

    def decompress(self, value):
        if value:
            return value.split()
        return [None for i in xrange(self.init_num)]

    def _has_changed(self, initial_value, data_value):
        initial_value = [unicode(v) for v in initial_value] if initial_value \
            else []
        return initial_value != data_value
        return False


class RotationSelect(widgets.SelectMultiple):
    class Media:
        js = [
            "%sbsmSelect/js/jquery.bsmselect.js" % settings.STATIC_URL,
            "%sbsmSelect/js/jquery.bsmselect.sortable.js" %
            settings.STATIC_URL,
            "%ssimulabio/js/rotation-widget.js" % settings.STATIC_URL,
        ]
        css = {
            'all':
            ["%sbsmSelect/css/jquery.bsmselect.css" % settings.STATIC_URL, ]
        }

    def render(self, name, value, attrs):
        attrs['class'] = 'bsm'
        render = super(RotationSelect, self).render(name, value, attrs)
        render += "<script type='text/javascript'>\n"\
            "   if (typeof bsm_value == 'undefined'){\n"\
            "       bsm_value = new Array();\n    };\n"
        if value:
            render += "   bsm_value['%s'] = [\"%s\"];\n" % (
                name, u'", "'.join([unicode(val) for val in value]))
        render += "</script>\n"
        return render


class PlotsSelect(widgets.SelectMultiple):
    def __init__(self, *args, **kwargs):
        self.numeric_cols = []
        if 'numeric_cols' in kwargs:
            self.numeric_cols = kwargs.pop('numeric_cols')
        super(PlotsSelect, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs):
        table_name = name + '_table'
        context = Context(
            {'object_list': self.choices.queryset.all(),
             'name': table_name})
        year = datetime.date.today().year
        context['year_2'] = year - 2
        context['year_1'] = year - 1
        context['year'] = year
        context['study'] = None
        if self.choices.queryset.all():
            context['study'] = self.choices.queryset.all()[0].study

        template_string = """
{% load simulabio_tags %}
<div id='{{name}}_div'>
{% plot_table study name year_2 year_1 year object_list %}
</div>
<script type='text/javascript'>
var selected_plot_array = new Array();
{% for plot in object_list %}selected_plot_array['{{forloop.counter}}'] = '{{plot.pk}}';
{%endfor%}
</script>
        """
        render = Template(template_string).render(context)
        numeric_cols = u', '.join(['"%s"' % unicode(numeric_col)
                                   for numeric_col in self.numeric_cols])
        render += """
<input type='hidden' name="%(name)s" id='%(name)s_field'/>
<script type='text/javascript'>
    $(document).ready(function(){
        function onSelection(){
            var res_array = new Array();
            var sel_items = $('#%(table_name)s').getGridParam('selarrrow');
            for (idx=0 ; idx < sel_items.length ; idx ++){
                res_array.push(selected_plot_array[sel_items[idx]]);
            }
            $('#%(name)s_field').val(res_array);
        }
        function onSubmit(){
        }
        var options = {multiselect: true,
                   rowNum:10000,
                   onSelectRow:onSelection,
                   onSelectAll:onSelection,
                   width:400,
                   shrinkToFit:false
                   }
        tableToGrid("#%(table_name)s", options);
        var hidden_cols = readCookie('plot_list_hidden');
        var col_sort = readCookie('plot_list_sort');
        if (jQuery("#%(table_name)s").length){
            var col_model = jQuery("#%(table_name)s").jqGrid('getGridParam', 'colModel');
            jQuery("#%(table_name)s").jqGrid('hideCol', col_model[0].name);
            if (!hidden_cols){
                for (idx=0;idx<col_model.length;idx++){
                    if (idx > default_col_number){
                        jQuery("#%(table_name)s").jqGrid('hideCol', col_model[idx].name);
                    }
                }
            } else {
                hidden_cols = hidden_cols.split(',');
                for (idx=0;idx<hidden_cols.length;idx++){
                    jQuery("#%(table_name)s").jqGrid('hideCol', hidden_cols[idx]);
                }
            }
            if (col_sort){
                col_sort = col_sort.split(',');
                $("#%(table_name)s").jqGrid('remapColumns', col_sort, true);
            }
        }
        // numeric sorting
        var numeric_cols = new Array(
            %(numeric_cols)s
        );

        for (idx=0;idx<numeric_cols.length;idx++){
            var colname = numeric_cols[idx];
            jQuery("#%(table_name)s").setColProp(colname,
                    {sorttype:'float', unformat:frenchfloatunformatter});
        }
    });
</script>
        """ % {"table_name": table_name, 'name': name,
               "numeric_cols": numeric_cols}
        return render


class ColoredSelect(widgets.Select):
    def __init__(self, *args, **kwargs):
        self.colors = {}
        if 'colors' in kwargs:
            self.colors = kwargs['colors']
        super(ColoredSelect, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        if option_value in self.colors:
            selected_html += ' style="background-color:#%s"' % \
                self.colors[option_value]
        return u'<option value="%s"%s>%s</option>' % (
            escape(option_value), selected_html,
            conditional_escape(force_unicode(option_label)))


class JQueryDate(forms.TextInput):
    def __init__(self, *args, **kwargs):
        super(JQueryDate, self).__init__(*args, **kwargs)
        if 'class' not in self.attrs:
            self.attrs['class'] = ''
        self.attrs['class'] = 'date-pickup'

    def render(self, name, value=None, attrs=None):
        rendered = super(JQueryDate, self).render(name, value, attrs)
        rendered += render_to_string(
            'widgets/jquerydate.html',
            {"name": name, "country": settings.LANGUAGE_CODE.split('-')[0]})
        return rendered
