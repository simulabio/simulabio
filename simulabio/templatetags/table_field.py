#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import template
from django.utils.translation import ugettext as _

register = template.Library()

@register.inclusion_tag('simulabio/blocks/table_field.html')
def table_field(field):
    u"""
    Render a specific field as a row in a table.
    """
    return {'field':field}

