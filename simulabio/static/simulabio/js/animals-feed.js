$(document).ready(function(){
    if (feed_template_series){
       $('#id_feed_template').val(feed_template_id);
       var feed_template_plot = $.jqplot('feed_template', feed_template_series,
       {
        stackSeries: true,
        seriesColors: feed_template_colors,
        seriesDefaults: {
            fill:true,
            shadow: false
        },
        axes:{
          xaxis:{
            label:month_label,
            ticks:month_ticks,
            renderer: $.jqplot.CategoryAxisRenderer,
            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
            tickOptions: {
                angle: -30
            }
          },
          yaxis:{
            min:0,
            label:quantity_label
          }
        },
        legend: {
            show: true,
            location: 'ne',
            placement: 'outsideGrid',
            labels:feed_labels
        }
       });
    }
    $('#id_feed_template').change(
        function(){
            var url = current_url;
            var val = $(this).val();
            if (val) url += val + "/#feed_template";
            window.location.replace(url);
        }
    );
    if (feed_series && feed_series.length){
       var feed_plot = $.jqplot('current_feed', feed_series,
       {
        stackSeries: true,
        seriesColors: feed_colors,
        seriesDefaults: {
            fill:true,
            shadow: false
        },
        highlighter: {
            show: true,
            tooltipAxes: 'y'
        },
        axes:{
          xaxis:{
            label:month_label,
            ticks:month_ticks,
            renderer: $.jqplot.CategoryAxisRenderer,
            tickRenderer: $.jqplot.CanvasAxisTickRenderer,
            tickOptions: {
                angle: -30
            }
          },
          yaxis:{
            min:0,
            label:quantity_label
          }
        },
        legend: {
            show: true,
            location: 'ne',
            placement: 'outsideGrid',
            labels:labels
        }
       });
       $('#feed_form').dialog({modal:true,
                               autoOpen:false,
                               width:500
                               });
       $('#current_feed').bind('jqplotDataClick',
            function (ev, seriesIndex, pointIndex, data) {
                var month = pointIndex + 1;
                if (is_season){
                    month = (month - 1) * 3 + 1;
                }
                var url = form_url + month + "/";
                jQuery('#feed_form').load(url
                       ).dialog("option", "title",
                                feed_label + " - " + month_ticks[pointIndex]
                       ).dialog('open');
       });
    }
});
