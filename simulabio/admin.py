#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

from django.contrib import admin
from models import Farm, Study, HarvestCategory, Harvest,\
    GeographicalZone, ReferenceHarvest, RotationDominantTemplate, \
    RotationDominantTemplateHarvest, FeedItem, \
    FeedTemplate, FeedTemplateDetail, HarvestCategoryUsageType, UsageType, \
    ExtraSupply, HarvestExpenseType, Workshop, WorkshopExpenses, \
    ExtraExpenseType, ExtraRevenueType, GenericField, GenericForm, \
    ReportTemplate, GrassGrowth
from forms import RotationDominantHarvestAdminForm


class GeneralTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'available', 'order']
admin.site.register(ExtraSupply, GeneralTypeAdmin)

admin.site.register(GenericForm, GeneralTypeAdmin)
admin.site.register(GenericField, GeneralTypeAdmin)


class UsageTypeAdmin(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display + ['target_type']
admin.site.register(UsageType, UsageTypeAdmin)


class HarvestAdmin(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display + ['category']
admin.site.register(Harvest, HarvestAdmin)

admin.site.register(HarvestExpenseType, GeneralTypeAdmin)
admin.site.register(Workshop, GeneralTypeAdmin)
admin.site.register(WorkshopExpenses, GeneralTypeAdmin)


class FeedItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'harvest_category', 'usage_type')
admin.site.register(FeedItem, FeedItemAdmin)


class ExtraExpenseTypeAdmin(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display + ['full_eco']
admin.site.register(ExtraExpenseType, ExtraExpenseTypeAdmin)
admin.site.register(ExtraRevenueType, ExtraExpenseTypeAdmin)


class ReferenceHarvestInline(admin.TabularInline):
    model = ReferenceHarvest
    extra = 1


class GeographicalZoneAdmin(admin.ModelAdmin):
    '''
    Geographical zone admin: precise fields and add inline fields
    '''
    list_display = ['name', 'available', 'default', 'order']
    inlines = (ReferenceHarvestInline,)

admin.site.register(GeographicalZone, GeographicalZoneAdmin)


class RotationDominantHarvestInline(admin.TabularInline):
    model = RotationDominantTemplateHarvest
    extra = 1
    ordering = ('order',)
    form = RotationDominantHarvestAdminForm


class RotationDominantTemplateAdmin(admin.ModelAdmin):
    '''
    Rotation dominant template: add inline fields
    '''
    inlines = (RotationDominantHarvestInline,)
    list_filter = ('geographical_zone',)

admin.site.register(RotationDominantTemplate,
                    RotationDominantTemplateAdmin)


class HarvestCategoryUsageTypeInline(admin.TabularInline):
    model = HarvestCategoryUsageType
    extra = 1


class HarvestCategoryAdmin(GeneralTypeAdmin):
    '''
    Rotation dominant template: add inline fields
    '''
    inlines = (HarvestCategoryUsageTypeInline,)

admin.site.register(HarvestCategory,
                    HarvestCategoryAdmin)


class FeedTemplateDetailInline(admin.TabularInline):
    model = FeedTemplateDetail
    extra = 1


class FeedTemplateAdmin(admin.ModelAdmin):
    inlines = (FeedTemplateDetailInline,)
    list_display = ['name', 'milk_production', 'animal_default',
                    'default_for_diag']

    class Media:
        css = {
            'all': ('simulabio/css/admin.css',)
        }

admin.site.register(FeedTemplate, FeedTemplateAdmin)


class ReportTemplateAdmin(admin.ModelAdmin):
    list_display = ['name', 'target', 'available']

admin.site.register(ReportTemplate, ReportTemplateAdmin)

for model in (Farm, Study, GrassGrowth):
    admin.site.register(model)
