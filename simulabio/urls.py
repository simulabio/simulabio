#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2014  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

from django.conf import settings
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView, DetailView

from models import Farm
from views import *

urlpatterns = patterns(
    '',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    url(r'^$', RedirectView.as_view(url='welcome/', permanent=False),
        name='index'),
    url(r'^files/parcelles_ref.csv$',
        CSVFile.as_view(template='simulabio/files/parcelles_ref.csv',
                        filename='parcelles_ref.csv', data_class='Harvest'),
        name='plot-ref-csv'),
    url(r'^accounts/profile/$', RedirectView.as_view(url='/')),
    url(r'^active-session/$', login_required(HasActiveSession.as_view()),
        name='has-active-session'),
    url(r'^reinit-session/$', login_required(ReinitializeSession.as_view()),
        name='reinit-session'),
    url(r'^welcome/$', login_required(WelcomeView.as_view()), name='welcome'),
    url(r'^farms/$', login_required(FarmList.as_view()), name='farm-list'),
    url(r'^farm/(?P<pk>\d+)/$', DetailView.as_view(model=Farm),
        name='farm-detail'),
    url(r'^farm/add/$', login_required(FarmCreate.as_view()), name='farm-add'),
    url(r'^farm/(?P<pk>\d+)/update/$', login_required(FarmUpdate.as_view()),
        name='farm-update'),
    url(r'^farm/(?P<pk>\d+)/delete/$', login_required(FarmDelete.as_view()),
        name='farm-delete'),
    url(r'^farm/(?P<farm_pk>\d+)/study/create/$',
        login_required(StudyCreate.as_view()), name='study-add'),
    url(r'^farm/(?P<farm_pk>\d+)/study/(?P<pk>\d+)/update/$',
        StudyUpdate.as_view(), name='study-update'),
    url(r'^study/(?P<study_pk>\d+)/has-lock/(?P<lock>[-A-Z]+)/$',
        HasLockView.as_view(), name='has-lock'),
    url(r'^study/(?P<pk>\d+)/generic/(?P<form>[-A-Za-z]+)/$',
        GenericFormView.as_view(), name='generic-form'),
    url(r'^study/(?P<study_pk>\d+)/report-generation/$',
        ReportGenerationView.as_view(), name='report-generation'),
    url(r'^study/(?P<pk>\d+)/harvest-settings/$',
        HarvestSettingsView.as_view(), name='harvest-settings'),
    url(r'^study/(?P<study_pk>\d+)/harvest-plot/$',
        HarvestPlotsView.as_view(), name='harvest-plots'),
    url(r'^study/(?P<study_pk>\d+)/harvest-plot/(?P<pk>\d+)/modify/$',
        HarvestPlotsView.as_view(), name='harvest-plots-modify'),
    url(r'^study/(?P<study_pk>\d+)/harvest-plot/(?P<pks>[0-9_]*)/delete/$',
        HarvestPlotsDeleteView.as_view(), name='harvest-plots-delete'),
    url(r'^study/(?P<study_pk>\d+)/harvest-rotationdominant/'
        r'(?:(?P<step>([0-9]+|D|P))/)?$',
        HarvestRotationDominantView.as_view(),
        name='harvest-rotationdominant'),
    url(r'^study/(?P<study_pk>\d+)/harvest-rotationdominant/'
        r'(?P<step>([0-9]+|D|P))/(?P<rotation_pk>\d+)/$',
        HarvestRotationDominantView.as_view(),
        name='harvest-rotationdominant-detail'),
    url(r'^study/(?P<study_pk>\d+)/harvest-rotationdominant/'
        r'(?P<step>([0-9]+|D|P))/(?P<rotation_pk>\d+)/delete/?$',
        HarvestRotationDominantDeleteView.as_view(),
        name='harvest-rotationdominant-delete'),
    url(r'^study/(?P<study_pk>\d+)/harvest-rotationdominant/'
        r'(?P<step>([0-9]+|D|P))/(?P<rotation_pk>\d+)/'
        r'(?P<pks>[0-9_]*)/delete/?$',
        HarvestRotationDominantDeleteAttachedView.as_view(),
        name='harvest-rotationdominant-delete-attached'),
    url(r'^study/(?P<pk>\d+)/harvest-plots-full/$',
        HarvestFullPlotsView.as_view(), name='harvest-plots-full'),
    url(r'^study/(?P<study_pk>\d+)/harvest-production/$',
        HarvestProductionView.as_view(), name='harvest-production'),
    url(r'^study/(?P<study_pk>\d+)/harvest-indicators/$',
        HarvestIndicatorsView.as_view(), name='harvest-indicators'),
    url(r'^study/(?P<study_pk>\d+)/animals-herd/(?:(?P<step>([0-9]+|D|P))/)?$',
        AnimalsHerdView.as_view(), name='animals-herd'),
    url(r'^study/(?P<study_pk>\d+)/animals-feed-season/'
        r'(?P<animal>[a-zA-Z][a-zA-Z0-9]*)/(?:(?P<step>([0-9]+|D|P))/)?$',
        AnimalsFeedSeasonView.as_view(), name='animals-feed-season'),
    url(r'^study/(?P<study_pk>\d+)/animals-feed/'
        r'(?P<animal>[a-zA-Z][a-zA-Z0-9]*)/(?:(?P<step>([0-9]+|D|P))/)?$',
        AnimalsFeedView.as_view(), name='animals-feed'),
    url(r'^study/(?P<study_pk>\d+)/animals-feed-custom/'
        r'(?P<animal>[a-zA-Z][a-zA-Z0-9]*)/(?:(?P<step>([0-9]+|D|P))/)?$',
        AnimalsCustomFeedView.as_view(), name='animals-feed-custom'),
    url(r'^study/(?P<study_pk>\d+)/animals-feed/'
        r'(?P<animal>[a-zA-Z][a-zA-Z0-9]*)/'
        r'(?P<step>([0-9]+|D|P))/(?P<template_pk>\d+)/$',
        AnimalsFeedView.as_view(), name='animals-feed-template'),
    url(r'^study/(?P<study_pk>\d+)/animals-feed-update/'
        r'(?P<animal>[a-zA-Z][a-zA-Z0-9]*)/(?P<step>([0-9]+|D|P))/'
        r'(?P<month>\d+)/$',
        AnimalsFeedUpdateView.as_view(), name='animals-feed-update'),
    url(r'^study/(?P<study_pk>\d+)/animals-litter/'
        r'(?P<animal>[a-zA-Z][a-zA-Z0-9]*)/(?:(?P<step>([0-9]+|D|P))/)?$',
        AnimalsLitterView.as_view(), name='animals-litter'),
    url(r'^study/(?P<study_pk>\d+)/animals-primary-balance/'
        r'(?:(?P<step>([0-9]+|D|P))/)?$',
        AnimalsPrimaryBalanceView.as_view(), name='animals-primary-balance'),
    url(r'^study/(?P<study_pk>\d+)/milk-production/$',
        MilkProductionView.as_view(), name='milk-production'),
    url(r'^study/(?P<study_pk>\d+)/balance-fodder/'
        r'(?:(?P<step>([0-9]+|D|P))/'
        r'(?:(?P<harvest_category>[a-zA-Z][-a-zA-Z0-9]*)/)?)?$',
        BalanceFodderView.as_view(), name='balance-fodder'),
    url(r'^study/(?P<pk>\d+)/economy-settings/$',
        EconomySettingsView.as_view(), name='economy-settings'),
    url(r'^study/(?P<pk>\d+)/simple-economy/initial-state/$',
        SimpleEcoInitialView.as_view(), name='economy-simple-initial'),
    url(r'^study/(?P<pk>\d+)/simple-economy/revenues/$',
        SimpleEcoRevenuesView.as_view(), name='economy-simple-revenues'),
    url(r'^study/(?P<pk>\d+)/simple-economy/expenses/$',
        SimpleEcoExpensesView.as_view(), name='economy-simple-expenses'),
    url(r'^study/(?P<pk>\d+)/simple-economy/investments/$',
        SimpleEcoInvestmentsView.as_view(), name='economy-simple-investments'),
    url(r'^study/(?P<pk>\d+)/simple-economy/synthesis/$',
        SimpleEcoSynthesisView.as_view(), name='economy-simple-synthesis'),
    url(r'^study/(?P<pk>\d+)/report/$', ReportView.as_view(),
        name='study-report'),
    url(r'^study/(?P<pk>\d+)/full-economy/workshop/cultures/'
        r'(?:(?P<state>[0-9a-zA-Z][-a-zA-Z0-9]*)/)?$',
        FullEcoHarvestWorkshopView.as_view(),
        name='economy-full-harvest-workshop'),
    url(r'^study/(?P<pk>\d+)/full-economy/harvest-expenses/'
        r'(?:(?P<harvest>[a-zA-Z][-a-zA-Z0-9]*)/)?$',
        FullEcoHarvestSettingsExpenseView.as_view(),
        name='economy-full-harvest-expenses'),
    url(r'^study/(?P<pk>\d+)/full-economy/workshop/new/?$',
        FullEcoNewWorkshopView.as_view(), name='economy-full-newworkshop'),
    url(r'^study/(?P<pk>\d+)/full-economy/workshop/bovin-lait/$',
        FullEcoMilkWorkshopView.as_view(), name='economy-full-milkworkshop'),
    url(r'^study/(?P<pk>\d+)/full-economy/workshop/'
        r'(?:(?P<workshop>[0-9a-zA-Z][-a-zA-Z0-9]*)/'
        r'(?:(?P<state>[0-9a-zA-Z][-a-zA-Z0-9]*)/)?)?$',
        FullEcoWorkshopView.as_view(), name='economy-full-workshop'),
    url(r'^study/(?P<pk>\d+)/full-economy/extra-revenues/$',
        FullEcoRevenuesView.as_view(), name='economy-full-revenues'),
    url(r'^study/(?P<pk>\d+)/full-economy/expenses/$',
        FullEcoExpensesView.as_view(), name='economy-full-expenses'),
    url(r'^study/(?P<pk>\d+)/full-economy/investments/$',
        FullEcoInvestmentsView.as_view(), name='economy-full-investments'),
    url(r'^study/(?P<pk>\d+)/full-economy/synthesis/$',
        FullEcoSynthesisView.as_view(), name='economy-full-synthesis'),
)
