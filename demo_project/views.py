#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks@proxience.com>

from simulabio import models
from simulabio.views import StudyCheckMixin
from django.views.generic import TemplateView

class ReportGenerationView(StudyCheckMixin, TemplateView):
    template_name = 'simulabio/study_report_gen.html'
    _comments = False

    def get_context_data(self, **kwargs):
        context = super(ReportGenerationView, self).get_context_data(**kwargs)
        context['gen_forms'] = list(models.GenericForm.objects.filter(
                                        available=True).order_by('order').all())
        return context

