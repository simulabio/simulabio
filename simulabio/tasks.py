#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

try:
    from celery.task import task
    from celery.task.control import inspect
    from celery.control import revoke
except ImportError:
    pass

import itertools

from django.conf import settings
from django.db import IntegrityError, transaction

from compute import Compute
import models

if not settings.EAT_CELERY:
    task = lambda f: f

DEFAULT_TASK_OPTIONS = {
    'expire': 60 * 4,  # 4 minutes - it is too late$
    'default_retry_delay': 5  # try to retry every 5 seconds
}


@task
def calculate_task(item, task_id=None):
    active_tasks = inspect().active()

    if active_tasks and \
       task_id in itertools.chain(*[
            [task['id'] for task in active_tasks[worker]
             if task['name'] == 'simulabio.tasks.calculate_task']
            for worker in active_tasks]):
        revoke(task_id)
    return item.calculate()


def calculate(item):
    task_id = "_".join(["calculate", item.__class__.__name__,
                        unicode(item.pk)])
    if settings.EAT_CELERY:
        options = DEFAULT_TASK_OPTIONS.copy()
        options['task_id'] = task_id
        return calculate_task.apply_async([item], **options)
    else:
        return calculate_task(item)


@task
def copy_study_task(item, copied_item, task_id=None):
    # active_tasks = inspect().active()
    res = item.copy_from(copied_item)
    item.available = True
    item.save()
    item.regenerate_all_graph()
    return res


def copy_study(item, copied_item):
    task_id = "_".join(["copy_study", item.__class__.__name__,
                        unicode(item.pk), unicode(copied_item.pk)])
    if settings.EAT_CELERY:
        options = DEFAULT_TASK_OPTIONS.copy()
        options['task_id'] = task_id
        return copy_study_task.apply_async([item, copied_item], **options)
    else:
        return copy_study_task(item)


@task
def preload_study_task(study_pk):
    if settings.INTERNAL_CACHE:
        Compute(study_pk)


def preload_study(study_pk):
    if settings.EAT_CELERY:
        options = DEFAULT_TASK_OPTIONS.copy()
        with transaction.commit_on_success():
            try:
                return preload_study_task.apply_async([study_pk], **options)
            except IntegrityError:
                # error on celery load
                pass
    return preload_study_task(study_pk)


@task
def compute_task(study_pk, tasks):
    c = Compute(study_pk, follow_time=True)
    for task_name, args in tasks:
        print task_name
        getattr(c, task_name)(*args)


def compute(study_pk, tasks, locks=[]):
    if locks:
        models.StudyLock.get_locks(study_pk, locks)
    if settings.EAT_CELERY:
        options = DEFAULT_TASK_OPTIONS.copy()
        return compute_task.apply_async([study_pk, tasks], **options)
    else:
        return compute_task(study_pk, tasks)
