#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json, re

from django import template
from django.conf import settings
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext as _, activate, get_language
from django.utils.encoding import force_unicode

register = template.Library()

@register.inclusion_tag('simulabio/blocks/main_menu.html')
def main_menu(grey_bg=True, action=''):
    """
    Display study menu.
    """
    context_data = {'action':action, 'STATIC_URL':settings.STATIC_URL,
                    'grey_bg':grey_bg, 'CONTACT_EMAIL':settings.CONTACT_EMAIL}
    return context_data

@register.inclusion_tag('simulabio/blocks/study_menu.html')
def study_menu(study, category='', action='', gen_forms=[]):
    """
    Display study menu.
    """
    context_data = {'category':category, 'study':study, 'action':action,
                    'gen_forms':gen_forms}
    return context_data

@register.inclusion_tag('simulabio/blocks/switch.html')
def switch(main_id, item_list, selected, url, end_url='', all_active=False):
    """
    Display a switch.
    """
    context_data = {'main_id':main_id,
                    'item_list':item_list,
                    'selected':selected,
                    'url':url,
                    'end_url':end_url,
                    'all_active':all_active}
    return context_data

@register.inclusion_tag('simulabio/blocks/plot_table.html')
def plot_table(study, plot_id, year_2, year_1, year, object_list,
               printing=False):
    """
    Display the plot table.
    """
    context_data = {'plot_id':plot_id,
                    'year_2':year_2,
                    'year_1':year_1,
                    'year':year,
                    'object_list':object_list,
                    'study':study,
                    'printing':printing
    }
    return context_data


def get_year_forms(year_forms, main_label='', first_form_is_parent_form=False,
               display_header=True, parent_form_header='name', has_delete=None,
               field_group_nb=1):
    """
    Render a year forms as an inline table.
    """
    if has_delete == None and first_form_is_parent_form:
        has_delete = True
    hidden_fields = []
    fields = []
    if not year_forms:
        return ""
    start = 0
    if first_form_is_parent_form:
        start = 1
    field_groups = []
    c_group = []
    idx = 0
    for field in year_forms[start].fields:
        if year_forms[start].fields[field].widget.is_hidden:
            cfields = [form[field] for form in year_forms[start:]]
            hidden_fields += cfields
            continue
        if not idx % field_group_nb and c_group:
            field_groups.append(c_group)
            c_group = []
        c_group.append(field)
        idx += 1
    field_groups.append(c_group)
    for field_group in field_groups:
        cfields = []
        for form in year_forms[start:]:
            cfields += [form[field] for field in field_group]
        lbl = year_forms[start].fields[field_group[0]].label
        if first_form_is_parent_form:
            if parent_form_header:
                lbl = year_forms[0][parent_form_header]
            if has_delete:
                cfields.append(year_forms[0]['DELETE'])

        # not very clean way...
        current_lang = get_language()
        activate('en')
        cls = slugify(force_unicode(lbl))
        activate(current_lang)

        fields.append([lbl, cls, cfields])
    return {'year_forms':year_forms[start:], 'fields':fields,
            'hidden_fields':hidden_fields, 'main_label':main_label,
            'has_delete':has_delete,
            'display_header':display_header}

@register.inclusion_tag('simulabio/blocks/year_forms.html')
def year_forms(year_forms, main_label='', first_form_is_parent_form=False,
               display_header=True, parent_form_header='name', has_delete=None,
               field_group_nb=1):
    return get_year_forms(year_forms, main_label, first_form_is_parent_form,
               display_header, parent_form_header, has_delete, field_group_nb)

@register.inclusion_tag('simulabio/blocks/year_forms.json')
def json_year_forms(year_forms, main_label='', first_form_is_parent_form=False,
               display_header=True, parent_form_header='name', has_delete=None,
               field_group_nb=1):
    return get_year_forms(year_forms, main_label, first_form_is_parent_form,
               display_header, parent_form_header, has_delete, field_group_nb)

@register.inclusion_tag('simulabio/blocks/year_header.html')
def year_header(nb_transition_years, headers="", has_diag=True,
                has_project=True, has_delete=True):
    if headers:
        headers = unicode(headers).split(";")
    else:
        headers = None
    nb_years = nb_transition_years
    if has_diag:
        nb_years += 1
    if has_project:
        nb_years += 1
    return {'nb_transition_years':nb_transition_years, 'headers':headers,
            'headers_len':len(headers) or 1 if headers else 1,
            'has_delete':has_delete, 'has_diag':has_diag,
            'has_project':has_project, 'nb_years':nb_years}

RE_TAG = re.compile('<[^>]*>')

def json_escape(value):
    if value == None:
        value = ''
    v = unicode(value)
    v = RE_TAG.sub('', v)
    return json.dumps(v)

register.filter('json_escape', json_escape)

def forms_tilt(forms):
    table = []
    if forms:
        table.append([])
        for k in forms[0].fields:
            if forms[0].fields[k].widget.is_hidden:
                continue
            table[-1].append(forms[0].fields[k].label)
    for form in forms:
        table.append([])
        instance = None
        if hasattr(form, 'instance') and form.instance:
            instance = form.instance
        for k in form.fields:
            field = form.fields[k]
            if field.widget.is_hidden:
                continue
            value = field.initial
            if instance:
                if hasattr(instance, k):
                    value = getattr(instance, k)
            if value == None:
                value = ''
            table[-1].append(unicode(value))
    new_table = []
    for idx_row, row in enumerate(table):
        for idx_col, cell in enumerate(row):
            if len(new_table) <= idx_col:
                new_table.append([])
            new_table[idx_col].append(cell)
    return new_table
register.filter('forms_tilt', forms_tilt)

def percentformat(value):
    try:
        value = float(value)
    except ValueError:
        return 0
    value = int(round(value*100))
    return value
register.filter('percentformat', percentformat)
