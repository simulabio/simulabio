#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

from __future__ import print_function

from calendar import monthrange
import collections
from unicode_csv_reader import unicode_csv_reader
import datetime
from dateutil.relativedelta import relativedelta
import unicodedata
import numpy
import os
import tempfile
import re
os.environ['MPLCONFIGDIR'] = tempfile.mkdtemp()
# force matplotlib to not use any Xwindows backend
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.urlresolvers import reverse
from django.db import models, IntegrityError, transaction
from django.db.models.signals import post_save, pre_delete
from django.db.models.fields.related import RelatedObject, ForeignKey, \
    ManyToManyField
from django.template.defaultfilters import slugify
from django.template.loader import render_to_string
from django.utils import translation
from django.utils.translation import ugettext_lazy as _

from oook_replace.oook_replace import oook_replace, translate_context
from simulabio.data_importer import Importer, ImportFormater

FARMING_TYPES = (('CV', u"Conventionnel"),
                 ('BIO', u"Bio"),
                 ('SFEI', u"Système Fourrager Économe en Intrant"))

YEARS = [('D', _(u'Diag.')),
         ('P', _(u'Project'))]

YEARS += [(unicode(yr), _(u"n+%(year)d") % {'year': yr})
          for yr in xrange(1, 20)]

YIELD_TYPE = (('D', _(u'Diagnostic')),
              ('P', _(u'Project')),
              ('DP', _(u'Diagnostic/Project')),
              )

TARGET_TYPE = (('forage', _('Forage')),
               ('grain', _('Grain')),
               ('straw', _('Straw')),)


def remove_accents(data):
    return ''.join(x for x in unicodedata.normalize('NFKD', data) if
                   unicodedata.category(x)[0] in ('L', 'Z', 'P', 'N'))


class Cached(object):
    @classmethod
    def get_cached(cls, *args, **kwargs):
        cache_key = cls.__name__
        for k in kwargs:
            if k == 'id':
                k = 'pk'
            cache_key += '-' + k + "_" + unicode(kwargs[k])
        cache_key = slugify(cache_key)
        v = cache.get(cache_key)
        if not v:
            v = list(cls.objects.filter(**kwargs).all())
            cache.set(cache_key, v)
        return v


class GeneralType(models.Model, Cached):
    _lbl_field = 'name'
    name = models.CharField(_(u"Name"), max_length=150)
    available = models.BooleanField(_(u"Available"), default=True)
    order = models.IntegerField(_(u"Order"), blank=True, null=True)

    class Meta:
        abstract = True
        ordering = ('order', 'name')

    def __unicode__(self):
        return self.name

    @property
    def unaccented(self):
        return remove_accents(self.name)


class Serialization(models.Model):
    _cached_keys = ['_prefetched_objects_cache']
    _cache_norelated = False
    _cache_saved = False

    class Meta:
        abstract = True

    @classmethod
    def get_all_accessor_names(cls):
        all_related_fields = []
        for k in cls._meta.get_all_field_names():
            field = cls._meta.get_field_by_name(k)[0]
            if isinstance(field, ForeignKey)\
               or isinstance(field, RelatedObject):
                if hasattr(field, 'get_accessor_name'):
                    k = field.get_accessor_name()
                all_related_fields.append(k)
        return all_related_fields

    _study_path = []
    _study_parent = 'study'

    def get_study_pk(self):
        return self.study.pk

    def get_parent_id(self):
        return self.study.pk

    def _browse_tree(self, item, study_path, delete=False,
                     add=None):
        study_path = study_path[:]
        if not study_path:
            if type(item) not in (list, tuple):
                if item['id'] == self.pk:
                    return item
            else:
                for idx, it in enumerate(item):
                    if it['id'] == self.id:
                        return it
            return
        if not item:
            return
        parent = item
        nitem = item[study_path.pop()]
        if not study_path and add:
            parent_id, value = add
            if parent_id and parent['id'] != parent_id:
                return
            if type(nitem) in (list, tuple):
                nitem.append(value)
            else:
                nitem = value
            return
        if type(nitem) in (list, tuple):
            for idx, it in enumerate(nitem):
                if not study_path and delete and it.get('id') == self.pk:
                    nitem.pop(idx)
                    return
                returned = self._browse_tree(it, study_path, delete=delete,
                                             add=add)
                if returned:
                    return returned
        else:
            if not study_path and delete and nitem.get('id') == self.pk:
                nitem = None
                return
            returned = self._browse_tree(nitem, study_path, delete=delete,
                                         add=add)
            if returned:
                return returned

    def get_cached_study(self):
        study_pk = self.get_study_pk()
        cache_key = 'study-%d' % study_pk
        study = cache.get(cache_key)
        if not study:
            try:
                study = Study.objects.get(pk=study_pk
                                          ).get_serialized(full=True)
            except Study.DoesNotExist:
                return {}, cache_key
            cache.set(cache_key, study, settings.CACHE_TIMEOUT)
        return study, cache_key

    def delete_in_cache(self):
        if not self._cache_saved:
            return
        study, cache_key = self.get_cached_study()
        if not study:
            return
        self._browse_tree(study, list(reversed(self._study_path)),
                          delete=True)
        cache.set(cache_key, study)

    def update_in_cache(self):
        if not self._cache_saved:
            return
        study, cache_key = self.get_cached_study()
        if not study:
            return
        item = self._browse_tree(study, list(reversed(self._study_path)))
        if not item:
            return
        for k in self._meta.get_all_field_names():
            field = self._meta.get_field_by_name(k)[0]
            if isinstance(field, ForeignKey) and k not in item.keys():
                v = getattr(self, k)
                if v:
                    item[k + '_id'] = v.pk
            elif isinstance(field, RelatedObject) or\
                    isinstance(field, ManyToManyField) or\
                    k == 'id':
                pass
            else:
                item[k] = getattr(self, k)
        cache.set(cache_key, study)

    def add_in_cache(self):
        if not self._cache_saved:
            return
        study, cache_key = self.get_cached_study()
        if not study:
            return
        new = (self.get_parent_id(), self.get_serialized(full=True))
        self._browse_tree(study, list(reversed(self._study_path)), add=new)
        cache.set(cache_key, study)

    def get_serialized(self, related_fields=[], full=False,
                       already_cached=None, fathers=[]):
        identity = (self.__class__, self.pk)
        if not already_cached:
            already_cached = {}
        elif identity in already_cached:
            return already_cached[identity]
        if not self.pk:
            return {'MODEL': self.__class__.__name__}
        cls = self.__class__
        all_related_fields = self.get_all_accessor_names()

        my_related_fields = all_related_fields if full else \
            [field for field in related_fields if field in all_related_fields]

        obj = self.query_related(query_filter={'pk': self.pk},
                                 related_fields=my_related_fields)[0]
        dct = obj.__dict__
        dct['MODEL'] = self.__class__.__name__
        for cached_key in cls._cached_keys:
            cached = dct[cached_key]
            if isinstance(cached, collections.Hashable):
                key = cached_key[1:-6]
                cached = {key: cached}
            for k in cached:
                value = cached[k]
                if type(value) == models.query.QuerySet:
                    value = list(value.all())
                single = type(value) not in (list, tuple)
                if single:
                    value = [value]
                values = []
                for v in value:
                    videntity = (v.__class__, v.pk if hasattr(v, 'pk')
                                 else None)
                    if videntity in fathers:
                        continue
                    if videntity[1] and videntity in already_cached:
                        values.append(already_cached[videntity])
                    elif hasattr(v, 'get_serialized') and \
                            not self._cache_norelated:
                        f = fathers[:] + [identity]
                        values.append(v.get_serialized(
                            related_fields=related_fields,
                            full=full, already_cached=already_cached,
                            fathers=f))
                    else:
                        values.append(v)
                value = values
                if single:
                    if value:
                        value = values[0]
                    else:
                        continue
                dct[k] = value
        self.clean_dct(dct)
        already_cached[identity] = self
        return dct

    @classmethod
    def clean_duplicates(cls, filters={}):
        # clean duplicates when bad sync occurs
        if not cls._meta.unique_together:
            return
        unique_together = cls._meta.unique_together[0]
        items = cls.objects.filter(**filters)\
                           .order_by('-pk')\
                           .order_by(*unique_together)
        c_tuple, to_delete = None, []
        for item in items.all():
            n_tuple = []
            for k in unique_together:
                v = getattr(item, k)
                if hasattr(v, 'pk'):
                    v = v.pk
                n_tuple.append(v)
            if c_tuple == n_tuple:
                to_delete.append(item.pk)
            c_tuple = n_tuple[:]
        if to_delete:
            print("TO DELETE")
            print(to_delete)
            cls.objects.filter(pk__in=to_delete).delete()

    @classmethod
    def clean_dct(cls, dct):
        for k in dct.keys():
            if k == '_state' or (k.startswith('_') and k.endswith('_cache')):
                dct.pop(k)
            elif type(k) in (list, tuple):
                dct[k] = [cls.clean_dct(item) for item in dct[k]]
        return dct

    @classmethod
    def query_related(cls, query_filter={}, related_fields=[]):
        q = cls.objects
        for related in related_fields:
            q = q.prefetch_related(related)
        return q.filter(**query_filter)

    def save(self, *args, **kwargs):
        if not settings.INTERNAL_CACHE:
            if 'from_cache' in kwargs:
                kwargs.pop('from_cache')
            super(Serialization, self).save(*args, **kwargs)
            return
        update_in_cache = False
        # update_in_cache, add_in_cache = False, False
        if not kwargs.get('from_cache'):
            if self.pk:
                update_in_cache = True
            # else:
            #    add_in_cache = True
        else:
            kwargs.pop('from_cache')
        super(Serialization, self).save(*args, **kwargs)
        if update_in_cache:
            self.update_in_cache()
        else:
            self.add_in_cache()

    def delete(self, *args, **kwargs):
        if not settings.INTERNAL_CACHE:
            if 'from_cache' in kwargs:
                kwargs.pop('from_cache')
            super(Serialization, self).delete(*args, **kwargs)
            return
        if not kwargs.get('from_cache'):
            self.delete_in_cache()
        else:
            kwargs.pop('from_cache')
        super(Serialization, self).delete(*args, **kwargs)


def update_cache_trigger(sender, **kwargs):
    return
    if not kwargs['instance']:
        return
    instance = kwargs['instance']
    if kwargs.get('created'):
        instance.add_in_cache()
    else:
        instance.update_in_cache()


def remove_cache_trigger(sender, **kwargs):
    return
    if not kwargs['instance']:
        return
    instance = kwargs['instance']
    instance.delete_in_cache()


def auto_inc_order_trigger(sender, **kwargs):
    if not kwargs['instance']:
        return
    instance = kwargs['instance']
    if instance.order:
        return
    q = instance.__class__.objects.order_by('-order')
    order = 1
    if q.count():
        item = q.all()[0]
        order = (item.order if item.order else 0) + 1
    instance.order = order
    instance.save()


class SimulabioUser(models.Model):
    user = models.ForeignKey(User, unique=True, verbose_name=_('User'),
                             related_name='simulabio_user')
    current_ip = models.GenericIPAddressField(_(u"Current IP address"),
                                              null=True, blank=True)
    current_agent = models.TextField(_(u"Current agent"),
                                     null=True, blank=True)
    current_session = models.ForeignKey(Session, null=True, blank=True,
                                        on_delete=models.SET_NULL)
    farms = models.ManyToManyField("Farm", null=True, blank=True,
                                   verbose_name=_(u"Farms"),
                                   related_name='simulabio_user')

    class Meta:
        verbose_name = _(u"SimulaBIO user")
        verbose_name_plural = _(u"SimulaBIO users")


class GeographicalZone(GeneralType):
    default = models.BooleanField(_(u"Default"), default=False)

    class Meta:
        verbose_name = _(u"Geographical zone")
        verbose_name_plural = _(u"Geographicals zones")

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        q = self.__class__.objects.filter(default=True)
        if self.pk:
            q = q.exclude(pk=self.pk)
        if self.default and q.count():
            default_item = q.all()[0]  # only one item should be concerned
            default_item.default = False
            default_item.save()
        return super(GeographicalZone, self).save(*args, **kwargs)


class GenericForm(GeneralType):
    slug = models.SlugField()

    class Meta:
        verbose_name = _(u"Generic form")
        verbose_name_plural = _(u"Generics forms")

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(GenericForm, self).save(*args, **kwargs)
        if not self.report_target.count():
            ReportTarget.objects.create(
                class_name='GenericForm', name=self.name, generic_form=self)
        else:
            report_target = self.report_target.all()[0]
            if self.name != report_target.name:
                report_target.name = self.name
                report_target.save()


class GenericField(GeneralType):
    slug = models.SlugField()
    form = models.ForeignKey(GenericForm, related_name='generic_fields')

    class Meta:
        verbose_name = _(u"Generic field")
        verbose_name_plural = _(u"Generics fields")

    def __unicode__(self):
        return self.name


class ReportTarget(GeneralType):
    class_name = models.CharField(_(u"Class name"), max_length=100)
    generic_form = models.ForeignKey(
        GenericForm, null=True, blank=True, verbose_name=_(u"Generic form"),
        related_name='report_target')

    class Meta:
        verbose_name = _(u"Report target")
        ordering = ('order',)

    def __unicode__(self):
        return self.name

    def generate_doc(self, template, context, tempdir="", extra_file_name=''):
        if not tempdir:
            tempdir = tempfile.mkdtemp("-simulabio-reports")
        output_name = tempdir + os.path.sep + \
            slugify(self.name.replace(' ', '_').lower()) + u'-' +\
            datetime.date.today().strftime('%Y-%m-%d') +\
            extra_file_name + u".odt"
        context = translate_context(context,
                                    settings.LANGUAGE_CODE.split('-')[0])
        oook_replace(template, output_name, context)
        return output_name


class ReportTemplate(models.Model):
    name = models.CharField(_(u"Name"), max_length=100)
    template = models.FileField(_(u"Template"), upload_to="upload/templates/")
    target = models.ForeignKey(ReportTarget, verbose_name=_(u"Target"))
    available = models.BooleanField(_(u"Available"), default=True)

    class Meta:
        verbose_name = _(u"Report template")
        verbose_name_plural = _(u"Report templates")

    def __unicode__(self):
        return self.name

    def publish(self, c_object):
        tempdir = tempfile.mkdtemp("-simulabio-reports")
        output_name = tempdir + os.path.sep + \
            slugify(self.name.replace(' ', '_').lower()) + u'-' +\
            datetime.date.today().strftime('%Y-%m-%d') +\
            u"." + self.template.name.split('.')[-1]
        oook_replace(self.template, output_name, c_object.get_values())
        return output_name


class GrassGrowth(models.Model):
    perc_1 = models.FloatField(_(u"Grass growth in january (%)"), default=0)
    perc_2 = models.FloatField(_(u"Grass growth in february (%)"), default=0)
    perc_3 = models.FloatField(_(u"Grass growth in march (%)"), default=0)
    perc_4 = models.FloatField(_(u"Grass growth in april (%)"), default=0)
    perc_5 = models.FloatField(_(u"Grass growth in may (%)"), default=0)
    perc_6 = models.FloatField(_(u"Grass growth in june (%)"), default=0)
    perc_7 = models.FloatField(_(u"Grass growth in july (%)"), default=0)
    perc_8 = models.FloatField(_(u"Grass growth in august (%)"), default=0)
    perc_9 = models.FloatField(_(u"Grass growth in september (%)"), default=0)
    perc_10 = models.FloatField(_(u"Grass growth in october (%)"), default=0)
    perc_11 = models.FloatField(_(u"Grass growth in november (%)"), default=0)
    perc_12 = models.FloatField(_(u"Grass growth in december (%)"), default=0)
    geographical_zone = models.ForeignKey(
        GeographicalZone, null=True, blank=True,
        verbose_name=_(u"Geographical zone"))
    default = models.BooleanField(_(u"Default"), default=False)

    class Meta:
        verbose_name = _(u"Grass growth")
        verbose_name_plural = _(u"Grass growth")

    def save(self, *args, **kwargs):
        super(GrassGrowth, self).save(*args, **kwargs)
        # only one default
        if self.default:
            q = GrassGrowth.objects.filter(default=True).exclude(pk=self.pk)
            if q.count():
                for gg in q.all():
                    gg.default = False
                    gg.save()


class Farm(models.Model):
    corporate_name = models.CharField(_(u"Corporate name"), max_length=150)
    address = models.CharField(_(u"Address"), max_length=200, null=True,
                               blank=True)
    postal_code = models.CharField(_(u"Postal code"), max_length=6, null=True,
                                   blank=True)
    town = models.CharField(_(u"Town"), max_length=50, null=True, blank=True)
    geographical_zone = models.ForeignKey(
        GeographicalZone, null=True, blank=True,
        verbose_name=_(u"Geographical zone"))
    available = models.BooleanField(u"Available", default=True)

    class Meta:
        verbose_name = _(u"Farm")
        verbose_name_plural = _(u"Farms")

    def __unicode__(self):
        return self.corporate_name

    def get_absolute_url(self):
        return reverse('simulabio:farm-detail', kwargs={'pk': self.pk})


class Study(Serialization):
    _cache_saved = True
    name = models.CharField(_(u"Name"), max_length=150)
    farm = models.ForeignKey(Farm, related_name='study')
    year = models.IntegerField(_(u"Year"))
    technician = models.CharField(_(u"Technician name"), max_length=150,
                                  blank=True, null=True)
    transition_years = models.IntegerField(_(u"Number of transition years"),
                                           default=0)
    farming_type = models.CharField(_(u"Farming type"), default='BIO',
                                    choices=FARMING_TYPES, max_length=4)
    available = models.BooleanField(_(u"Available"), default=True)
    # harvest
    forage_yield_total = models.FloatField(_(u"Total forage yield"),
                                           blank=True, null=True)
    forage_yield_project_total = models.FloatField(
        _(u"Total forage yield project"), blank=True, null=True)
    grain_yield_total = models.FloatField(_(u"Total grain yield"), blank=True,
                                          null=True)
    grain_yield_project_total = models.FloatField(
        _(u"Total grain yield project"), blank=True, null=True)
    straw_yield_total = models.FloatField(_(u"Total straw yield"), blank=True,
                                          null=True)
    straw_yield_project_total = models.FloatField(
        _(u"Total straw yield project"), blank=True, null=True)
    # simple economy
    total_product = models.IntegerField(_(u"Total revenue"), default=0)
    operational_charges = models.IntegerField(_(u"Operational charges"),
                                              default=0)
    structural_charges = models.IntegerField(_(u"Structural charges"),
                                             default=0)
    depreciation = models.IntegerField(_(u"Depreciation"), default=0)
    annuities = models.IntegerField(_(u"Annuities"), default=0)
    share_capital = models.IntegerField(_(u"Share capital"), default=0)
    interests = models.IntegerField(_(u"Interests"), default=0)

    class Meta:
        verbose_name = _(u"Study")
        verbose_name_plural = _(u"Studies")
        ordering = ('-year', 'pk')

    def __unicode__(self):
        return u"%s (%d)" % (self.name, self.year)

    def get_absolute_url(self):
        return reverse('simulabio:study-update',
                       kwargs={'farm_pk': self.farm.pk, 'pk': self.pk})

    def delete_in_cache(self):
        pass

    def transition_years_1(self):
        return self.transition_years + 1

    def transition_years_2(self):
        return self.transition_years + 2

    def get_study_pk(self):
        return self.pk

    def update_in_cache(self):
        study, cache_key = self.get_cached_study()
        if not study:
            return
        for k in self._meta.get_all_field_names():
            field = self._meta.get_field_by_name(k)[0]
            if not isinstance(field, ForeignKey) and \
               not isinstance(field, RelatedObject):
                study[k] = getattr(self, k)
        cache.set(cache_key, study)

    def add_in_cache(self):
        pass

    @property
    def year_1(self):
        return self.year - 1

    @property
    def year_2(self):
        return self.year - 2

    @classmethod
    def calculate_annuities(cls, study):
        study['annuities'] = study['interests'] + study['share_capital']

    def calculate_db_annuities(self):
        annuities = self.interests + self.share_capital
        if annuities != self.annuities:
            self.annuities = annuities
            self.save()

    def regenerate_all_graph(self):
        q = RotationDominant.objects.filter(study=self)
        for rd in q.all():
            rd.generate_pie()

    def copy_from(self, study):
        # be carreful: delete all attached data
        for k in self._meta.fields:
            if k.name not in ('id', 'name', 'farm', 'year'):
                setattr(self, k.name, getattr(study, k.name))
        self.save()
        # (model, study_key)
        COPIED_ITEMS = ((StudyField, 'study'),
                        (StudyComment, 'study'),
                        (PastureAvailable, 'study'),
                        (TransitionType, 'study'),
                        (HarvestCategoryStudy, 'study'),
                        (HarvestSettings, 'study'),
                        (HarvestSettingsYear, 'harvest__study'),
                        (HarvestSettingsSold, 'harvest__study'),
                        (HarvestSettingsSoldYear,
                         'harvest_sold__harvest__study'),
                        (HarvestSettingsExpense, 'harvest__study'),
                        (HarvestSettingsExpenseYear,
                         'harvest_expense__harvest__study'),
                        (RotationDominant, 'study'),
                        (RotationDominantHarvest, 'rotation_dominant__study'),
                        (HarvestPlots, 'study'),
                        (HarvestTransition, 'plot__study'),
                        (Herd, 'study'),
                        (HerdDetail, 'herd__study'),
                        (MilkProduction, 'study'),
                        (Feed, 'study'),
                        (FeedDetail, 'feed__study'),
                        (StrawUsage, 'herd_detail__herd__study'),
                        (FeedNeed, 'study'),
                        (FeedNeedSupply, 'feed_need__study'),
                        (FeedNeedExtraSupply, 'feed_need__study'),
                        (SimpleEcoInitialExpenseDetail, 'study'),
                        (Revenues, 'study'),
                        (ExtraRevenue, 'study'),
                        (ExtraRevenueYear, 'extra_revenue__study'),
                        (Expenses, 'study'),
                        (ExtraExpenses, 'study'),
                        (ExtraExpenseYear, 'extra_expense__study'),
                        (ExtraSupplyPrice, 'study'),
                        (ExtraSupplyPriceYear, 'extra_supply_price__study'),
                        (Investment, 'study'),
                        (EconomicSynthesis, 'study'),
                        (Workshop, 'study'),
                        (WorkshopExpenses, 'workshop__study'),
                        (WorkshopExpensesYear, 'workshop__workshop__study'),
                        (WorkshopRevenues, 'workshop__study'),
                        (WorkshopRevenuesYear, 'workshop__workshop__study'),
                        (EconomicSynthesis, 'study'),
                        )
        value_connections = {}
        for model, study_key in COPIED_ITEMS:
            excluded_fields = ['study', 'id']
            field_names = [field.name for field in model._meta.fields]
            # clean current study
            if 'study' in field_names:
                try:
                    with transaction.commit_on_success():
                        model.objects.filter(study=self).delete()
                except IntegrityError:  # parrallel task may cause duplicates
                    pass
            value_connections[model] = {}
            for item in model.objects.filter(**{study_key: study}).all():
                values = {field_name: getattr(item, field_name)
                          for field_name in field_names
                          if field_name not in excluded_fields}
                # management of foreign keys
                for field in model._meta.fields:
                    if field.name in values and hasattr(field, 'related') and \
                       field.related.parent_model in value_connections and \
                       values[field.name] in \
                       value_connections[field.related.parent_model]:
                        values[field.name] = value_connections[
                            field.related.parent_model][values[field.name]]
                if 'study' in field_names:
                    values['study'] = self
                # clean already initialized data
                if model._meta.unique_together and \
                   'study' in model._meta.unique_together:
                    query_values = {}
                    for k in model._meta.unique_together:
                        query_values[k] = values[k]
                    models.objects.filter(**query_values).delete()
                try:
                    with transaction.commit_on_success():
                        new_item = model.objects.create(**values)
                except IntegrityError:  # parrallel task may cause duplicates
                    q = model.objects.filter(**values)
                    if not q.count():
                        continue
                    new_item = q.all()[0]
                value_connections[model][item] = new_item

    def get_years(self):
        years = ['D'] + [unicode(yr) for yr in
                         xrange(1, (self.transition_years + 1))] + ['P']
        return years

    @property
    def harvest_lbls(self):
        labels = [
            unicode(_(u"Harvest %(year)d (n-2)")) % {"year": self.year - 2},
            unicode(_(u"Harvest %(year)d (n-1)")) % {"year": self.year - 1},
            unicode(_(u"Harvest %(year)d (n)")) % {"year": self.year}]
        for idx in xrange(1, self.transition_years + 1):
            labels.append(_(u"Harvest %(year)d (n+%(idx)d)") %
                          {'year': self.year + idx, 'idx': idx})
        return labels

    def total_surface(self, year):
        surface = 0
        q = HarvestSettingsYear.objects.filter(year=year, harvest__study=self)
        for harvest_settings in q:
            surface += harvest_settings.surface_total or 0
        return surface

post_save.connect(update_cache_trigger, sender=Study)


class StudyField(models.Model):
    study = models.ForeignKey(Study, related_name='generic_fields')
    field = models.ForeignKey(GenericField)
    value = models.TextField(_(u"Value"), blank=True, null=True)

    class Meta:
        verbose_name = _(u"Study - generic field")

    def __unicode__(self):
        return self.value

LOCK_TYPES = (
    ('SIT', _(u"Study - Initialize")),
    ('HHA', _(u"Harvest - Harvests")),
    ('AFN', _(u"Animals - Feed needs")),
    ('AMK', _(u"Animals - Milk")),
    ('ESY', _(u"Economic - Synthesis")),
    ('ERV', _(u"Economic - Revenues")),
    ('EEX', _(u"Economic - Expenses")),
)


class StudyLock(Serialization):
    study = models.ForeignKey(Study, related_name='locks')
    lock = models.CharField(max_length=3, choices=LOCK_TYPES)
    datetime = models.DateTimeField(_(u"Datetime"), auto_now=True)

    @classmethod
    def get_lock(cls, study_pk, lock):
        lock, created = cls.objects.get_or_create(study_id=study_pk, lock=lock)
        return created

    @classmethod
    def get_locks(cls, study_pk, locks):
        for lock in locks:
            cls.get_lock(study_pk, lock)

    @classmethod
    def remove_lock(cls, study_pk, lock):
        q = cls.objects.filter(study__pk=study_pk, lock=lock)
        for lock in q.all():
            lock.delete()

    @classmethod
    def clean(cls, study_pk):
        """Clean locks if they are older than the timeout"""
        old = datetime.datetime.now() - \
            datetime.timedelta(seconds=settings.LOCK_TIMEOUT)
        for lock in cls.objects.filter(study__pk=study_pk, datetime__lt=old):
            lock.delete()

    @classmethod
    def has_lock(cls, study_pk, locks):
        cls.clean(study_pk)
        if not locks:
            return False
        if type(locks) not in (list, tuple):
            locks = locks.split('-')
        return bool(cls.objects.filter(study__pk=study_pk, lock__in=locks
                                       ).count())


class StudyComment(Serialization):
    study = models.ForeignKey(Study, related_name='comment')
    form = models.CharField(_(u"Form name"), max_length=40)
    comment = models.TextField(_(u"Comment"), blank=True, null=True)
    context = models.CharField(_(u"Context"), max_length=50, blank=True,
                               null=True)
    attached_file = models.FileField(upload_to='attached', blank=True,
                                     null=True)

    class Meta:
        unique_together = ('study', 'form', 'context')


class PastureAvailable(Serialization):
    _cache_saved = False
    study = models.ForeignKey(Study, related_name='pasture_available')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    area = models.FloatField(_(u"Area"), default=0)
    production = models.FloatField(_(u"Production"), default=0)
    prod_1 = models.FloatField(_(u"Prod. available by day by cow in january"),
                               default=0)
    prod_2 = models.FloatField(_(u"Prod. available by day by cow in february"),
                               default=0)
    prod_3 = models.FloatField(_(u"Prod. available by day by cow in march"),
                               default=0)
    prod_4 = models.FloatField(_(u"Prod. available by day by cow in april"),
                               default=0)
    prod_5 = models.FloatField(_(u"Prod. available by day by cow in may"),
                               default=0)
    prod_6 = models.FloatField(_(u"Prod. available by day by cow in june"),
                               default=0)
    prod_7 = models.FloatField(_(u"Prod. available by day by cow in july"),
                               default=0)
    prod_8 = models.FloatField(_(u"Prod. available by day by cow in august"),
                               default=0)
    prod_9 = models.FloatField(
        _(u"Prod. available by day by cow in september"), default=0)
    prod_11 = models.FloatField(_(u"Prod. available by day by cow in october"),
                                default=0)
    prod_10 = models.FloatField(
        _(u"Prod. available by day by cow in november"), default=0)
    prod_12 = models.FloatField(
        _(u"Prod. available by day by cow in december"), default=0)
    balance_1 = models.FloatField(_(u"Balance for january"), default=0)
    balance_2 = models.FloatField(_(u"Balance for february"), default=0)
    balance_3 = models.FloatField(_(u"Balance for march"), default=0)
    balance_4 = models.FloatField(_(u"Balance for april"), default=0)
    balance_5 = models.FloatField(_(u"Balance for may"), default=0)
    balance_6 = models.FloatField(_(u"Balance for june"), default=0)
    balance_7 = models.FloatField(_(u"Balance for july"), default=0)
    balance_8 = models.FloatField(_(u"Balance for august"), default=0)
    balance_9 = models.FloatField(_(u"Balance for september"), default=0)
    balance_11 = models.FloatField(_(u"Balance for october"), default=0)
    balance_10 = models.FloatField(_(u"Balance for november"), default=0)
    balance_12 = models.FloatField(_(u"Balance for december"), default=0)

    class Meta:
        unique_together = ('study', 'year',)
        verbose_name = _(u'Pasture available')
        verbose_name_plural = _(u'Pastures available')

    def reinit(self):
        keys = ['area', 'production']
        keys += ['prod_%d' % month for month in range(1, 13)]
        for k in keys:
            setattr(self, k, 0)
        self.save()

    def calculate(self):
        dct = {}
        dct['area'] = 0
        dct['production'] = 0
        # filter plots
        if self.year == 'D':
            q = {'study': self.study, 'diagnostic': True,
                 'harvest__harvest__is_pasturable': True, 'cow_access': True}
            for plot in HarvestPlots.objects.filter(**q).distinct():
                dct['area'] += plot.area
                dct['production'] += plot.harvest.forage_yield * plot.area
        elif self.year == 'P':
            q = {'study': self.study, 'project': True, 'cow_access': True}
            harvest_settings = [
                hs for hs in HarvestSettings.objects.filter(
                    study=self.study, harvest__is_pasturable=True)]
            for plot in HarvestPlots.objects.filter(**q):
                for hs in harvest_settings:
                    area = plot.get_db_weighted_area(hs.harvest, 'P')
                    dct['area'] += area
                    dct['production'] += hs.forage_yield_project * area
        else:
            q = {'plot__study': self.study, 'year': self.year,
                 'plot__cow_access': True,
                 'harvest_setting__harvest__is_pasturable': True}
            try:
                tt = self.study.transition_type.get(year=self.year)
            except ObjectDoesNotExist:
                return self.reinit()  # no calculation
            for trans_plot in HarvestTransition.objects.filter(**q):
                dct['area'] += trans_plot.plot.area
                forage_yield = trans_plot.harvest_setting.forage_yield
                if tt.yield_type == 'P':
                    forage_yield = \
                        trans_plot.harvest_setting.forage_yield_project
                elif tt.yield_type == 'DP':
                    forage_yield = (
                        trans_plot.harvest_setting.forage_yield
                        + trans_plot.harvest_setting.forage_yield_project)\
                        / 2
                dct['production'] += forage_yield * trans_plot.plot.area
        grass_growth = None
        if self.study.farm.geographical_zone:
            q_keys = {'geographical_zone': self.study.farm.geographical_zone}
            q = GrassGrowth.objects.filter(**q_keys)
            if q.count():
                grass_growth = q.all()[0]
        if not grass_growth:
            q_keys = {'default': True}
            q = GrassGrowth.objects.filter(**q_keys)
            if not q.count():
                return self.reinit()  # no calculation
            grass_growth = q.all()[0]
        q = HerdDetail.objects.filter(herd__study=self.study, year=self.year)
        if not q.count() or not q.all()[0].dairy_cow:
            return self.reinit()  # no calculation
        cow_nb = q.all()[0].dairy_cow
        pasturable_feed_details = list(FeedDetail.objects.filter(
            feed__year=self.year, feed__study=self.study, feed__animal='dc',
            feed_item__usage_type__is_pasturable=True))
        for month in range(1, 13):
            current_prod = getattr(grass_growth, 'perc_%d' % month) \
                * dct['production'] * 1000 / 100.0
            # 2013 is a non-bisextile year
            current_prod_by_day = current_prod / monthrange(2013, month)[1]
            dct['prod_%d' % month] = current_prod_by_day / cow_nb
            dct['balance_%d' % month] = dct['prod_%d' % month]
            for fd in pasturable_feed_details:
                dct['balance_%d' % month] -= getattr(fd, 'amount_%d' % month)
            dct['balance_%d' % month] = round(dct['balance_%d' % month], 1)
            dct['prod_%d' % month] = round(dct['prod_%d' % month], 1)

        changed = False
        dct['production'] = int(round(dct['production']))
        for k in dct:
            if dct[k] != getattr(self, k):
                changed = True
                setattr(self, k, dct[k])
        if changed:
            self.save()


class TransitionType(Serialization):
    _cache_saved = True
    study = models.ForeignKey(Study, related_name='transition_type')
    year = models.IntegerField(_(u"Year number"))
    yield_type = models.CharField(max_length=2, choices=YIELD_TYPE)
    _study_path = ('transition_type',)

post_save.connect(update_cache_trigger, sender=TransitionType)
pre_delete.connect(remove_cache_trigger, sender=TransitionType)

RE_COLOR = re.compile(r'^[A-Fa-f0-9]{6}')


class HarvestCategory(GeneralType, Serialization):
    _cache_saved = False
    color = models.CharField(_(u"Color"), max_length=6)
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)

    class Meta:
        verbose_name = _(u"Harvest category")
        verbose_name_plural = _(u"Harvest categories")

    def clean(self):
        if not RE_COLOR.match(self.color):
            raise ValidationError(
                _(u"Color must be set with 6 hex character and no #"))

post_save.connect(auto_inc_order_trigger, sender=HarvestCategory)


class Harvest(GeneralType, Serialization):
    _cache_saved = False
    _cache_norelated = True
    category = models.ForeignKey(HarvestCategory, related_name='harvests')
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    is_pasturable = models.BooleanField(_(u"Is pasturable"), default=False)

    class Meta:
        verbose_name = _(u"Harvest")


class UsageType(GeneralType, Serialization):
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    target_type = models.CharField(_(u"Target type"), choices=TARGET_TYPE,
                                   max_length=15)
    is_pasturable = models.BooleanField(_(u"Is pasturable"), default=False)

    class Meta:
        verbose_name = _(u"Usage type")
        verbose_name_plural = _(u"Usage types")
post_save.connect(auto_inc_order_trigger, sender=UsageType)


class HarvestCategoryUsageType(Serialization, Cached):
    harvest_category = models.ForeignKey(HarvestCategory,
                                         verbose_name=_(u"Harvest category"))
    usage_type = models.ForeignKey(UsageType, verbose_name=_(u"Usage type"))
    diag_yield = models.FloatField(_(u"Diagnostic yield (%)"), default=100)
    project_yield = models.FloatField(_(u"Project yield (%)"), default=100)

    class Meta:
        verbose_name = _(u"Harvest category - Usage type")

    def __unicode__(self):
        return u" - ".join([unicode(self.harvest_category),
                            unicode(self.usage_type)])


class ReferenceHarvest(models.Model):
    harvest = models.ForeignKey(Harvest, verbose_name=_(u"Harvest"))
    geographical_zone = models.ForeignKey(GeographicalZone,
                                          verbose_name=_(u"Geographical zone"),
                                          related_name='reference_harvest')
    forage_yield = models.FloatField(_(u"Reference forage yield"), default=0)
    grain_yield = models.FloatField(_(u"Reference grain yield"), default=0)
    straw_yield = models.FloatField(_(u"Reference straw yield"), default=0)

    class Meta:
        verbose_name = _(u"Reference harvest")

    def __unicode__(self):
        return unicode(self.harvest)


class HarvestCategoryStudy(Serialization):
    _cache_saved = True
    study = models.ForeignKey(Study, related_name='harvest_categories')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    harvest_category = models.ForeignKey(HarvestCategory,
                                         verbose_name=_(u"Harvest category"))
    area = models.FloatField(_(u"Area"), default=0)
    average_forage_yield = models.FloatField(_(u"Average forage yield"),
                                             default=0)
    average_grain_yield = models.FloatField(_(u"Average grain yield"),
                                            default=0)
    average_straw_yield = models.FloatField(_(u"Average straw yield"),
                                            default=0)
    area_needed = models.FloatField(_(u"Area needed"), default=0,
                                    blank=True, null=True)
    harvest_category_usage_error = models.ForeignKey(
        HarvestCategoryUsageType, verbose_name=_(u"Harvest category error"),
        blank=True, null=True)

    class Meta:
        verbose_name = _(u"Study - Harvest category")
        unique_together = ('study', 'year', 'harvest_category')

    _study_path = ('harvest_categories',)

    @property
    def project(self):
        return self.year == 'P'

    @property
    def balance(self):
        return self.area - self.area_needed

    @property
    def autonomy_pourc(self):
        if not self.area_needed:
            return
        return self.area / self.area_needed * 100

    @classmethod
    def calculate(cls, study, hc):
        # for harvest_category in study['harvest_categories']:
        #     if harvest_category == hc_id:
        #         hc = harvest_category
        #         break
        # else:
        #     return
        #  available surface, yields
        surface, forage_yield, grain_yield, straw_yield = 0, 0, 0, 0

        for hs in study['harvest_settings']:
            for hsy in hs['year_details']:
                cat_id = None
                if hasattr(hs['harvest'], 'category'):
                    cat_id = hs['harvest'].category.pk
                else:
                    cat_id = hs['harvest']['category_id']
                if unicode(hsy['year']) == unicode(hc['year']) and\
                   cat_id == hc['harvest_category_id']:
                    surface += hsy['surface_total']
                    forage_yield += hsy['forage_yield_total']
                    grain_yield += hsy['grain_yield_total']
                    straw_yield += hsy['straw_yield_total']
        if surface:
            forage_yield = float(forage_yield) / surface
            grain_yield = float(grain_yield) / surface
            straw_yield = float(straw_yield) / surface
        values = {'area': surface}
        values['average_forage_yield'] = forage_yield
        values['average_grain_yield'] = grain_yield
        values['average_straw_yield'] = straw_yield

        # surface needed
        surface_needed = 0
        values['harvest_category_usage_error_id'] = None
        for feed_need in study['feed_need']:
            if not feed_need:
                continue
            category_usage = HarvestCategoryUsageType.get_cached(
                pk=feed_need['harvest_category_usage_id'])[0]
            if unicode(feed_need['year']) == unicode(hc['year']) and\
               category_usage.harvest_category.pk == hc['harvest_category_id']:
                if feed_need['associated_surface'] is not None:
                    surface_needed += feed_need["associated_surface"]
                else:
                    values['harvest_category_usage_error_id'] = \
                        feed_need['harvest_category_usage_id']
                    surface_needed = None
                    break
        values['area_needed'] = surface_needed

        # save
        changed = False
        for k in values:
            if hc.get(k) != values[k]:
                changed = True
                hc[k] = values[k]
        return changed

    def calculate_db(self):
        # available surface, yields
        surface, forage_yield, grain_yield, straw_yield = 0, 0, 0, 0

        for harvest_settings in HarvestSettingsYear.objects.filter(
                year=self.year, harvest__study=self.study,
                harvest__harvest__category=self.harvest_category):
            surface += harvest_settings.surface_total
            forage_yield += getattr(harvest_settings,
                                    'forage_yield_total')
            grain_yield += getattr(harvest_settings,
                                   'grain_yield_total')
            straw_yield += getattr(harvest_settings,
                                   'straw_yield_total')
        if surface:
            forage_yield = float(forage_yield) / surface
            grain_yield = float(grain_yield) / surface
            straw_yield = float(straw_yield) / surface
        values = {'area': surface}
        values['average_forage_yield'] = forage_yield
        values['average_grain_yield'] = grain_yield
        values['average_straw_yield'] = straw_yield

        # surface needed
        surface_needed = 0
        values['harvest_category_usage_error'] = None
        q = FeedNeed.objects.filter(
            study=self.study, year=self.year,
            harvest_category_usage__harvest_category=self.harvest_category)
        for feed_need in q:
            if feed_need.associated_surface is not None:
                surface_needed += feed_need.associated_surface
            else:
                values['harvest_category_usage_error'] = \
                    feed_need.harvest_category_usage
                surface_needed = None
                break
        values['area_needed'] = surface_needed

        # save
        changed = False
        for k in values:
            if getattr(self, k) != values[k]:
                changed = True
                setattr(self, k, values[k])
        if changed:
            self.save()
        return changed

post_save.connect(update_cache_trigger, sender=HarvestCategoryStudy)
pre_delete.connect(remove_cache_trigger, sender=HarvestCategoryStudy)


class HarvestSettings(Serialization):
    _cache_saved = True
    _cached_keys = ['_prefetched_objects_cache', '_harvest_cache']
    _study_path = ('harvest_settings',)
    study = models.ForeignKey(Study, related_name='harvest_settings')
    harvest = models.ForeignKey(Harvest, related_name='harvest_settings')
    forage_yield = models.FloatField(_(u"Forage yield"), default=0)
    forage_yield_project = models.FloatField(_(u"Forage yield project"),
                                             default=0)
    grain_yield = models.FloatField(_(u"Grain yield"), default=0)
    grain_yield_project = models.FloatField(_(u"Grain yield project"),
                                            default=0)
    straw_yield = models.FloatField(_(u"Straw yield"), default=0)
    straw_yield_project = models.FloatField(_(u"Straw yield project"),
                                            default=0)
    order = models.IntegerField(_(u"Order"), default=100)

    class Meta:
        verbose_name = _(u"Harvest - Settings")
        verbose_name_plural = _(u"Harvest - Settings")
        ordering = ('harvest__order',)
        unique_together = ('study', 'harvest')

    def __unicode__(self):
        return unicode(self.harvest)

    def _salable(self, usagetype):
        for yd in self.year_details.all():
            if getattr(yd, usagetype + '_yield_total'):
                return True

    @property
    def salable(self):
        return self.forage_salable or self.grain_salable or self.straw_salable

    @property
    def forage_salable(self):
        return self._salable('forage')

    @property
    def grain_salable(self):
        return self._salable('grain')

    @property
    def straw_salable(self):
        return self._salable('straw')

    @property
    def harvestsetting_project(self):
        q = HarvestSettingsYear.objects\
                               .filter(harvest=self, year='P')\
                               .order_by('-pk')
        count = q.count()
        if not count:
            return
        elif q.count() > 1:
            for hst in q.all()[1:]:
                hst.delete()
        return q.all()[0]

    @property
    def harvestsetting_diag(self):
        q = HarvestSettingsYear.objects\
                               .filter(harvest=self, year='D')\
                               .order_by('-pk')
        count = q.count()
        if not count:
            return
        elif q.count() > 1:
            for hst in q.all()[1:]:
                hst.delete()
        return q.all()[0]

    @property
    def surface_delta(self):
        return self.harvestsetting_diag.surface_total - \
            self.harvestsetting_project.surface_total

    def transition_years(self):
        years = [unicode(yr) for yr in xrange(1,
                                              self.study.transition_years + 1)]
        return self.year_details\
                   .exclude(year__in=['D', 'P'])\
                   .filter(year__in=years).order_by('year')

    @classmethod
    def calculate(cls, study, hs):
        years = ['D', 'P'] + range(1, study["transition_years"] + 1)
        for year_idx in years:
            surface_total = 0
            year_idx = unicode(year_idx)
            for plot in study['harvest_plots']:
                surface_total += HarvestPlots.get_weighted_area(
                    study, plot, hs, year_idx)
            values = {'surface_total': surface_total}
            hst = None
            for h in hs['year_details']:
                if h['year'] == unicode(year_idx):
                    hst = h
                    break
            HS_KEYS = ['surface_total', 'forage_yield_total',
                       'grain_yield_total', 'straw_yield_total', 'forage_used',
                       'grain_used', 'straw_used']
            if not hst:
                hst = {'harvest_id': hs['id'], 'year': year_idx,
                       'MODEL': 'HarvestSettingsYear'}
                for k in HS_KEYS:
                    hst[k] = 0
                hs['year_details'].append(hst)
            yield_type = ''
            if year_idx in ('D', 'P'):
                yield_type = year_idx
            else:
                yield_type = 'DP'
                for tt in study['transition_type']:
                    if unicode(tt['year']) == year_idx:
                        yield_type = tt['yield_type']
            if yield_type == 'DP':
                forage_yield = (hs['forage_yield_project'] +
                                hs['forage_yield']) / 2
                grain_yield = (hs['grain_yield_project'] +
                               hs['grain_yield']) / 2
                straw_yield = (hs['straw_yield_project'] +
                               hs['straw_yield']) / 2
            elif yield_type == 'P':
                forage_yield = hs['forage_yield_project']
                grain_yield = hs['grain_yield_project']
                straw_yield = hs['straw_yield_project']
            else:
                forage_yield = hs['forage_yield']
                grain_yield = hs['grain_yield']
                straw_yield = hs['straw_yield']
            values['forage_yield_total'] = \
                surface_total * forage_yield if surface_total else 0
            values['grain_yield_total'] = \
                surface_total * grain_yield if surface_total else 0
            values['straw_yield_total'] = \
                surface_total * straw_yield if surface_total else 0

            values['straw_used'], values['forage_used'] = 0, 0
            values['grain_used'] = 0
            appropriate_feedneeds = dict(
                [(fn['id'], fn) for fn in study['feed_need']
                 if unicode(fn['year']) == unicode(year_idx)])
            for supply in hs['supply']:
                if supply['feed_need_id'] in appropriate_feedneeds:
                    fn = appropriate_feedneeds[supply['feed_need_id']]
                    target_type = HarvestCategoryUsageType\
                        .get_cached(pk=fn['harvest_category_usage_id'])[0]\
                        .usage_type.target_type
                    values[target_type + '_used'] += supply['used']
            for solded in values['selling']:
                usage_type = \
                    models.UsageType.get_cached(pk=solded['usage_type_id'])
                for solded_year in solded['year_details']:
                    values[usage_type.target_type + '_used'] += \
                        solded_year['sold']
            changed = False
            for k in HS_KEYS:
                value = values[k]
                if round(float(hst[k] or 0) - float(value), 2):
                    hst[k] = value
                    changed = True
        return changed

    def clean(self):
        years = ['D', 'P'] + range(1, self.study.transition_years + 1)
        for year_idx in years:
            q = HarvestSettingsYear.objects\
                                   .filter(harvest=self, year=year_idx)\
                                   .order_by("-surface_total")
            if q.count() > 1:
                for item in q.all()[1:]:
                    item.delete()
                return True

    def calculate_db(self, cyear=None):
        self.clean()
        for het in HarvestExpenseType.objects.filter(available=True).all():
            het.init_harvest(self)
        years = ['D', 'P'] + range(1, self.study.transition_years + 1)
        for year_idx in years:
            if cyear and unicode(cyear) != unicode(year_idx):
                continue
            surface_total = 0
            for plot in HarvestPlots.objects.filter(study=self.study):
                surface_total += plot.get_db_weighted_area(self.harvest,
                                                           year_idx)
            values = {'surface_total': surface_total}
            q = HarvestSettingsYear.objects.filter(harvest=self, year=year_idx)
            if q.count():
                hst = q.all()[0]
            else:
                hst = HarvestSettingsYear.objects.create(harvest=self,
                                                         year=year_idx)
            yield_type = ''
            if year_idx in ('D', 'P'):
                yield_type = year_idx
            else:
                try:
                    yield_type = self.study.transition_type\
                                     .get(year=year_idx).yield_type
                except ObjectDoesNotExist:
                    yield_type = 'DP'

            if yield_type == 'DP':
                forage_yield = (self.forage_yield_project
                                + self.forage_yield) / 2
                grain_yield = (self.grain_yield_project + self.grain_yield) / 2
                straw_yield = (self.straw_yield_project + self.straw_yield) / 2
            elif yield_type == 'P':
                forage_yield = self.forage_yield_project
                grain_yield = self.grain_yield_project
                straw_yield = self.straw_yield_project
            else:
                forage_yield = self.forage_yield
                grain_yield = self.grain_yield
                straw_yield = self.straw_yield
            values['forage_yield_total'] = surface_total * forage_yield \
                if surface_total else 0
            values['grain_yield_total'] = surface_total * grain_yield \
                if surface_total else 0
            values['straw_yield_total'] = surface_total * straw_yield \
                if surface_total else 0

            values['straw_used'], values['forage_used'] = 0, 0
            values['grain_used'] = 0
            for supply in \
                    self.supply.filter(feed_need__year=year_idx).all():
                values[supply.feed_need.target_type + '_used'] += supply.used
            q = HarvestSettingsSoldYear.objects\
                                       .filter(year=year_idx,
                                               harvest_sold__harvest=self)
            for sold in q.all():
                values[sold.harvest_sold.usage_type.target_type + '_used'] \
                    += sold.sold
            changed = False
            surface_changed = False
            for k in ['surface_total', 'forage_yield_total',
                      'grain_yield_total', 'straw_yield_total', 'forage_used',
                      'grain_used', 'straw_used']:
                value = values[k]
                if round(float(getattr(hst, k) or 0) - float(value), 2):
                    setattr(hst, k, value)
                    changed = True
                    if k == 'surface':
                        surface_changed = True
            if changed:
                hst.save()
            if surface_changed:
                qhs = HarvestSettingsExpense.objects.filter(harvest=self).all()
                for hse in qhs:
                    expense, created = \
                        HarvestSettingsExpenseYear.objects.get_or_create(
                            harvest_expense=hse, year=hst.year)
                    expense.calculate()

post_save.connect(update_cache_trigger, sender=HarvestSettings)
pre_delete.connect(remove_cache_trigger, sender=HarvestSettings)


class HarvestSettingsYear(Serialization):
    _cache_saved = True
    _study_path = ('harvest_settings', 'year_details')
    harvest = models.ForeignKey(HarvestSettings, related_name='year_details')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    surface_total = models.FloatField(_(u"Surface total"), default=0)
    forage_yield_total = models.FloatField(_(u"Forage yield total"), default=0)
    forage_used = models.FloatField(_(u"Forage used"), default=0)
    grain_yield_total = models.FloatField(_(u"Grain yield total"), default=0)
    grain_used = models.FloatField(_(u"Grain used"), default=0)
    straw_yield_total = models.FloatField(_(u"Straw yield total"), default=0)
    straw_used = models.FloatField(_(u"Straw used"), default=0)

    # class Meta:
    #     unique_together = ('harvest', 'year')

    def get_parent_id(self):
        return self.harvest.pk

    def get_study_pk(self):
        return self.harvest.study.pk

    @property
    def straw_left(self):
        return round(self.straw_yield_total - self.straw_used, 3)

    @property
    def grain_left(self):
        return round(self.grain_yield_total - self.grain_used, 3)

    @property
    def forage_left(self):
        return round(self.forage_yield_total - self.forage_used, 3)

post_save.connect(update_cache_trigger, sender=HarvestSettingsYear)
pre_delete.connect(remove_cache_trigger, sender=HarvestSettingsYear)


class HarvestSettingsSold(Serialization):
    _cache_saved = True
    _study_path = ('harvest_settings', 'selling')
    harvest = models.ForeignKey(HarvestSettings, related_name='selling')
    usage_type = models.ForeignKey(UsageType, verbose_name=_(u"Usage type"))

    def get_parent_id(self):
        return self.harvest.pk

    def get_study_pk(self):
        return self.harvest.study.pk

    @property
    def salable(self):
        target = self.usage_type.target_type + '_salable'
        return getattr(self.harvest, target)


class HarvestSettingsSoldYear(Serialization):
    _cache_saved = True
    _study_path = ('harvest_settings', 'selling', 'year_details')
    harvest_sold = models.ForeignKey(HarvestSettingsSold,
                                     related_name='year_details')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    sold = models.FloatField(_(u"Sold"), default=0)
    price = models.IntegerField(_(u"Price"), default=0)

    def get_parent_id(self):
        return self.harvest_sold.harvest.pk

    def get_study_pk(self):
        return self.harvest_sold.harvest.study.pk

    @property
    def total(self):
        return self.sold * self.price

post_save.connect(update_cache_trigger, sender=HarvestSettingsSoldYear)
pre_delete.connect(remove_cache_trigger, sender=HarvestSettingsSoldYear)


class HarvestExpenseType(GeneralType, Serialization):
    _cache_saved = False
    _cache_norelated = True

    class Meta:
        verbose_name = _(u"Harvest expense type")
        verbose_name_plural = _(u"Harvest expense types")

    def init_harvest(self, harvest):
        filters = {'harvest': harvest, 'expense_type': self}
        q = HarvestSettingsExpense.objects.filter(**filters)
        if not q.count():
            hse, created = HarvestSettingsExpense.objects\
                                                 .get_or_create(**filters)
        else:
            hse = q.all()[0]
            for hs in q.all()[1:]:
                hs.delete()
        return hse


class HarvestSettingsExpense(Serialization):
    _cache_saved = True
    _study_path = ('harvest_settings', 'expenses')
    harvest = models.ForeignKey(HarvestSettings, related_name='expenses')
    expense_type = models.ForeignKey(HarvestExpenseType,
                                     verbose_name=_(u"Expense"))

    def get_parent_id(self):
        return self.harvest.pk

    def get_study_pk(self):
        return self.harvest.study.pk


class HarvestSettingsExpenseYear(Serialization):
    _cache_saved = True
    _study_path = ('harvest_settings', 'expenses', 'year_details')
    harvest_expense = models.ForeignKey(HarvestSettingsExpense,
                                        related_name='year_details')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    price = models.IntegerField(_(u"Price by ha"), default=0)
    total = models.IntegerField(_(u"Total"), default=0)

    def get_parent_id(self):
        return self.harvest_expense.harvest.pk

    def get_study_pk(self):
        return self.harvest_expense.harvest.study.pk

    def calculate(self):
        q = HarvestSettingsYear.objects.filter(
            harvest=self.harvest_expense.harvest, year=self.year)
        total = 0
        if q.count():
            total = self.price * q.all()[0].surface_total
        if total != self.total:
            self.total = total
            self.save()

post_save.connect(update_cache_trigger, sender=HarvestSettingsSoldYear)
pre_delete.connect(remove_cache_trigger, sender=HarvestSettingsSoldYear)


SOIL_TYPE = (('S', 'S'), ('N', 'N'), ('H', 'H'))


class HarvestPlots(Serialization):
    _cache_saved = True
    _study_path = ('harvest_plots',)
    study = models.ForeignKey(Study, related_name='harvest_plots')
    diagnostic = models.BooleanField(_(u"Diagnostic"), default=True)
    project = models.BooleanField(_(u"Project"), default=True)
    # TODO: check if diag or project is selected
    geocode = models.CharField(_(u"Geocode"), max_length=5, null=True,
                               blank=True)
    plot_group_number = models.CharField(_(u"Plot group number"), max_length=3,
                                         null=True, blank=True)
    name = models.CharField(_(u"Name"), max_length=100)
    area = models.FloatField(_(u"Area"))
    geographical_area = models.CharField(
        _(u"Geographical area"), max_length=150, null=True, blank=True)
    soil_type = models.CharField(_(u"Soil type"), choices=SOIL_TYPE,
                                 max_length=2, null=True, blank=True)
    cow_access = models.BooleanField(_(u"Cow access"), default=False)
    remark = models.CharField(_(u"Plot caracteristic"), max_length=1000,
                              null=True, blank=True)
    rotation_dominant_diag = models.ForeignKey(
        'RotationDominant',
        verbose_name=_(u"Rotation dominant diag"), null=True, blank=True,
        related_name='harvest_plots_diag', on_delete=models.SET_NULL)
    rotation_dominant_project = models.ForeignKey(
        'RotationDominant',
        verbose_name=_(u"Rotation dominant project"), null=True, blank=True,
        related_name='harvest_plots_project', on_delete=models.SET_NULL)
    harvest_n2 = models.ForeignKey(HarvestSettings, null=True, blank=True,
                                   related_name='harvest_plot_n2')
    harvest_n1 = models.ForeignKey(HarvestSettings, null=True, blank=True,
                                   related_name='harvest_plot_n1')
    harvest = models.ForeignKey(HarvestSettings, related_name='harvest_plot_n')
    harvest_transition = models.ManyToManyField(
        HarvestSettings, null=True, blank=True, through='HarvestTransition',
        verbose_name=_(u"Harvest transition"),
        related_name='harvest_plot_transition')

    class Meta:
        verbose_name = _(u"Harvest - Plot")
        verbose_name_plural = _(u"Harvest - Plots")

    DIAG_HARVEST_FIELDS = ('harvest_n2', 'harvest_n1', 'harvest')

    @property
    def ilot_pac(self):
        if not self.geocode:
            return
        return int(self.study.split('-')[0])

    def __unicode__(self):
        return self.name + ((' - ' + self.geocode) if self.geocode else '')

    @property
    def length_diag(self):
        return len([k for k in self.DIAG_HARVEST_FIELDS
                    if getattr(self, k)])

    @property
    def length_db_project(self):
        if not self.rotation_dominant_project:
            return 0
        return self.rotation_dominant_project.length

    @classmethod
    def length_project(cls, study, plot):
        if not plot['rotation_dominant_project_id']:
            return 0
        for rd in study['rotation_dominant']:
            if rd['id'] == plot['rotation_dominant_project_id']:
                return rd['length']
        return 0

    @classmethod
    def get_weighted_area(cls, study, plot, harvest_setting, year):
        weighted_nb = 0
        if year == 'P':
            length = cls.length_project(study, plot)
            if not length:
                return 0
            rd = dict([(rd['id'], rd) for rd in study['rotation_dominant']])
            nb = len([h for h in rd[plot['rotation_dominant_project_id']]
                     ['rotation_dominant_harvest']
                      if h['harvest_id'] == harvest_setting['harvest_id']])
            weighted_nb = nb / float(length)
        elif year == 'D':
            if plot['harvest_id'] == harvest_setting['id']:
                return plot['area']
            return 0
        else:
            available_hs = [hs['id'] for hs in study['harvest_settings']
                            if hs['id'] == harvest_setting['id']]
            for ht in plot['harvesttransition']:
                if unicode(ht['year']) == unicode(year) and \
                   ht['harvest_setting_id'] in available_hs:
                    return plot['area']
            return 0
        return weighted_nb * plot['area']

    def get_db_weighted_area(self, harvest, year):
        weighted_nb = 0
        if year == 'P':
            if not self.length_db_project:
                return 0
            nb = self.rotation_dominant_project\
                     .rotation_dominant_harvest\
                     .filter(harvest=harvest).distinct().count()
            weighted_nb = nb / float(self.length_db_project)
        elif year == 'D':
            if self.harvest.harvest == harvest:
                return self.area
            return 0
        else:
            if HarvestTransition.objects\
                    .filter(plot=self, year=year,
                            harvest_setting__harvest=harvest).count():
                return self.area
            return 0
        return weighted_nb * self.area

    def harvest_transitions(self):
        ht = []
        for year in xrange(1, self.study.transition_years + 1):
            try:
                ht.append(HarvestTransition.objects.get(plot=self,
                                                        year=year))
            except HarvestTransition.DoesNotExist:
                ht.append('')
        return ht

post_save.connect(update_cache_trigger, sender=HarvestPlots)
pre_delete.connect(remove_cache_trigger, sender=HarvestPlots)


class HarvestTransition(Serialization):
    _cache_saved = True
    _study_path = ('harvest_plots', 'harvesttransition')
    plot = models.ForeignKey(HarvestPlots)
    harvest_setting = models.ForeignKey(HarvestSettings,
                                        related_name='harvest_settings')
    year = models.IntegerField(_(u"Year number"))

    def __unicode__(self):
        return unicode(self.harvest_setting.harvest)

    def get_parent_id(self):
        return self.plot.pk

    def get_study_pk(self):
        return self.plot.study.pk

    @property
    def study(self):
        return self.plot.study

post_save.connect(update_cache_trigger, sender=HarvestTransition)
pre_delete.connect(remove_cache_trigger, sender=HarvestTransition)


class HarvestPlotImporter(Importer):
    LINE_FORMAT = [
        ImportFormater('name', Importer.get_unicode_formater(100)),
        ImportFormater('plot_group_number',
                       Importer.get_unicode_formater(3), required=False),
        ImportFormater('geographical_area', unicode, required=False),
        ImportFormater('soil_type', Importer.choices_check(SOIL_TYPE),
                       required=False),
        ImportFormater('cow_access', Importer.boolean_formater),
        ImportFormater('area', Importer.float_formater),
        ImportFormater('remark', unicode, required=False),
        ImportFormater('diagnostic', Importer.boolean_formater),
        ImportFormater('project', Importer.boolean_formater),
        ImportFormater('harvest_n2', 'harvest_formater', required=False),
        ImportFormater('harvest_n1', 'harvest_formater', required=False),
        ImportFormater('harvest', 'harvest_formater'),
        ImportFormater('harvest_setting', 'harvest_formater',
                       through=HarvestTransition,
                       through_key='plot',
                       through_dict={'year': 1},
                       through_unicity_keys=['plot', 'year'],
                       required=False),
        ImportFormater('harvest_setting', 'harvest_formater',
                       through=HarvestTransition,
                       through_key='plot',
                       through_dict={'year': 2},
                       through_unicity_keys=['plot', 'year'],
                       required=False),
        ImportFormater('harvest_setting', 'harvest_formater',
                       through=HarvestTransition,
                       through_key='plot',
                       through_dict={'year': 3},
                       through_unicity_keys=['plot', 'year'],
                       required=False),
        ImportFormater('harvest_setting', 'harvest_formater',
                       through=HarvestTransition,
                       through_key='plot',
                       through_dict={'year': 4},
                       through_unicity_keys=['plot', 'year'],
                       required=False),
        ImportFormater('harvest_setting', 'harvest_formater',
                       through=HarvestTransition,
                       through_key='plot',
                       through_dict={'year': 5},
                       through_unicity_keys=['plot', 'year'],
                       required=False),
        ImportFormater('harvest_setting', 'harvest_formater',
                       through=HarvestTransition,
                       through_key='plot',
                       through_dict={'year': 6},
                       through_unicity_keys=['plot', 'year'],
                       required=False),
    ]
    OBJECT_CLS = HarvestPlots
    UNICITY_KEYS = []

    def __init__(self, study, skip_first_line=None):
        # get the reference header
        dct = {'separator': settings.CSV_DELIMITER}
        dct['data'] = Harvest.objects.filter(available=True).all()
        reference_file = render_to_string('simulabio/files/parcelles_ref.csv',
                                          dct)
        reference_header = unicode_csv_reader(
            [reference_file.split('\n')[0]]).next()
        super(HarvestPlotImporter, self).__init__(
            skip_first_line=skip_first_line, reference_header=reference_header)
        self.study = study
        self.default_vals = {'study': self.study}
        self.harvest_dct = dict(
            [(slugify(h.unaccented), h)
             for h in Harvest.objects.filter(available=True).all()])

    def harvest_formater(self, value):
        value = value.strip()
        if not value:
            return
        slug = slugify(value)
        if slug not in self.harvest_dct:
            raise ValueError(_(u"\"%(value)s\" not in %(values)s") % {
                'value': value,
                'values': u", ".join([val.name for val in
                                      Harvest.objects.filter(available=True)])
            })
        harvest = self.harvest_dct[slug]
        hs, created = HarvestSettings.objects.get_or_create(study=self.study,
                                                            harvest=harvest)
        if created:
            self.message = _(
                u"\"%(harvest)s\" has been added in your settings "
                u"don't forget to fill yields for this harvest.") % \
                {'harvest': harvest.name}
        return hs

    def importation(self, *args, **kwargs):
        super(HarvestPlotImporter, self).importation(*args, **kwargs)


class RotationDominantTemplate(Serialization):
    name = models.CharField(_(u"Name"), max_length=100)
    remark = models.CharField(_(u"Remark"), max_length=1000, null=True,
                              blank=True)
    geographical_zone = models.ManyToManyField(
        GeographicalZone, null=True, blank=True,
        verbose_name=_(u"Geographical zone"))

    class Meta:
        verbose_name = _(u"Rotation dominant template")
        verbose_name_plural = _(u"Rotation dominant templates")

    def __unicode__(self):
        return self.name


class RotationDominantTemplateHarvest(models.Model):
    harvest = models.ForeignKey(Harvest)
    rotation_dominant_template = models.ForeignKey(
        RotationDominantTemplate, related_name='rotation_dominant_harvest')
    order = models.IntegerField(_(u"Order"), default=1)

    class Meta:
        ordering = ('order',)


class RotationDominantHarvest(Serialization):
    _cache_saved = True
    _study_path = ('rotation_dominant', 'rotation_dominant_harvest')
    harvest = models.ForeignKey(Harvest)
    rotation_dominant = models.ForeignKey(
        'RotationDominant', related_name='rotation_dominant_harvest')
    order = models.IntegerField(_(u"Order"), default=1)

    class Meta:
        ordering = ('order',)

    def get_parent_id(self):
        return self.rotation_dominant.pk

    def get_study_pk(self):
        return self.rotation_dominant.study.pk

post_save.connect(update_cache_trigger, sender=RotationDominantHarvest)
pre_delete.connect(remove_cache_trigger, sender=RotationDominantHarvest)


class RotationDominant(Serialization):
    _cache_saved = True
    _study_path = ('rotation_dominant',)
    name = models.CharField(_(u"Name"), max_length=100)
    study = models.ForeignKey(Study, related_name='rotation_dominant')
    diagnostic = models.BooleanField(_(u"Diagnostic"), default=True)
    rotation_dominant_template = models.ForeignKey(
        RotationDominantTemplate, related_name='rotation_dominant')
    surface = models.FloatField(_(u"Surface (ha)"), null=True, blank=True)
    length = models.FloatField(_(u"Length (year)"), null=True, blank=True)
    elementary_surface = models.FloatField(_(u"Elementary surface"), null=True,
                                           blank=True)
    parcel_number = models.IntegerField(_(u'Parcel number'), null=True,
                                        blank=True)
    min_surface = models.FloatField(_(u'Minimum surface'), null=True,
                                    blank=True)
    avg_surface = models.FloatField(_(u'Average surface'), null=True,
                                    blank=True)
    max_surface = models.FloatField(_(u'Maximum surface'), null=True,
                                    blank=True)
    cow_access = models.NullBooleanField(_(u"Cow access"), null=True,
                                         blank=True)

    @property
    def project(self):
        return not self.diagnostic

    def __unicode__(self):
        return self.name

    @property
    def areas_by_harvest(self):
        areas = {}
        total = self.rotation_dominant_harvest.count()
        for rd_harvest in self.rotation_dominant_harvest.all():
            if rd_harvest.harvest not in areas:
                areas[rd_harvest.harvest] = 0
            areas[rd_harvest.harvest] += round(1 / float(total) * self.surface,
                                               2)
        return [(harvest, areas[harvest]) for harvest in areas]

    def calculate_db(self):
        plots_key = 'harvest_plots_diag' if self.diagnostic \
                    else 'harvest_plots_project'
        plots = getattr(self, plots_key).all()
        areas = [plot.area for plot in plots]
        values = {}
        # surface
        values['surface'] = sum(areas) if areas else None
        # length
        values['length'] = int(self.rotation_dominant_harvest.count())
        values['elementary_surface'] = \
            values['surface'] / values['length'] \
            if values['length'] and values['surface'] else None
        values['parcel_number'] = len(plots)
        values['min_surface'] = min(areas) if areas else None
        values['max_surface'] = max(areas) if areas else None
        values['avg_surface'] = values['surface'] / values['parcel_number'] \
            if values['parcel_number'] and values['surface'] else None
        changed = False
        for k in values:
            if round(float(getattr(self, k) or 0) - float(values[k] or 0), 3):
                setattr(self, k, values[k])
                changed = True
        if changed:
            # only save on change
            self.save()
        return changed

    @classmethod
    def calculate(cls, study, rd):
        plots_key = 'harvest_plots_diag' if rd['diagnostic'] \
                    else 'harvest_plots_project'
        plots = []
        plots_ids = [pts.get('id') for pts in rd[plots_key]]
        for pts in study['harvest_plots']:
            if pts['id'] in plots_ids:
                plots.append(pts)
        areas = [plot['area'] for plot in plots]
        values = {}
        # surface
        values['surface'] = sum(areas) if areas else None
        # length
        values['length'] = len(rd['rotation_dominant_harvest'])

        values['elementary_surface'] = \
            rd['surface'] / rd['length'] \
            if rd['length'] and rd['surface'] else None
        values['parcel_number'] = len(plots)
        values['min_surface'] = min(areas) if areas else None
        values['max_surface'] = max(areas) if areas else None
        values['avg_surface'] = \
            rd['surface'] / rd['parcel_number'] \
            if rd['parcel_number'] and rd['surface'] else None
        changed = False
        for k in values:
            if round(float(rd[k] or 0) - float(values[k] or 0), 3):
                rd[k] = values[k]
                changed = True
        return changed

    @property
    def image_path(self):
        return '/rotations/%d-%d.png' % (self.study.pk, self.pk)

    def generate_pie(self):
        harvests = []
        for rd_harvest in self.rotation_dominant_harvest.order_by('order'):
            if harvests and harvests[-1][0] == rd_harvest.harvest:
                harvests[-1][1] += 1
            else:
                harvests.append([rd_harvest.harvest, 1])

        labels, sizes, colors = [], [], []
        for harvest, size in harvests:
            labels.append(unicode(harvest))
            sizes.append(size)
            colors.append('#' + harvest.category.color)

        pyplot.figure('rotation-%d' % self.pk)
        pyplot.clf()

        translation.activate(settings.LOCALE.split('.')[0])
        # cur_language = translation.get_language()

        pyplot.pie(
            sizes, labels=labels, colors=colors,
            autopct=lambda pct: _('%d years') % int(round(pct * sum(sizes)
                                                          / 100.0))
        )

        # Set aspect ratio to be equal so that pie is drawn as a circle.
        pyplot.axis('equal')

        pyplot.show()
        path = settings.MEDIA_ROOT + self.image_path
        pyplot.savefig(path)
        return path

post_save.connect(update_cache_trigger, sender=RotationDominant)
pre_delete.connect(remove_cache_trigger, sender=RotationDominant)

ANIMALS = (('dc', _(u'Dairy cow')),
           ('h2', _(u"Heifer 2")),
           ('h1', _(u"Heifer 1")),
           ('h0', _(u"Heifer 0")),
           ('o', _(u"Other")),)

ANIMALS_KEY = {'dc': u"dairy_cow",
               'h2': u"heifer_2",
               'h1': u"heifer_1",
               'h0': u"heifer_0",
               'o': u"other_animal"}


class Herd(Serialization):
    _cache_saved = True
    study = models.ForeignKey(Study, related_name='herd')


class HerdDetail(Serialization):
    _cache_saved = True
    _study_path = ('herd', 'herd_detail')
    herd = models.ForeignKey(Herd, related_name="herd_detail")
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    dairy_cow = models.IntegerField(_(u"Dairy cows"), blank=True, null=True)
    heifer_2 = models.IntegerField(_(u"Heifers 2"), blank=True, null=True)
    heifer_1 = models.IntegerField(_(u"Heifers 1"), blank=True, null=True)
    heifer_0 = models.IntegerField(_(u"Heifers 0"), blank=True, null=True)
    other_animal = models.IntegerField(_(u"Other animals"), blank=True,
                                       null=True)
    calving_age = models.IntegerField(_(u"Calving age"), blank=True, null=True)
    reform_rate = models.IntegerField(_(u"Reform rate (%)"), blank=True,
                                      null=True)
    turnover = models.FloatField(_(u"Turnover (%)"), blank=True, null=True)
    reformed_cow_sold = models.IntegerField(_(u"Reformed cows sold"),
                                            blank=True, null=True)
    heifer_2_sold = models.IntegerField(_(u"Heifers 2 sold"), blank=True,
                                        null=True)
    heifer_1_sold = models.IntegerField(_(u"Heifers 1 sold"), blank=True,
                                        null=True)
    calves_sold = models.IntegerField(_(u"Calves sold"), blank=True,
                                      null=True)
    cow_heifer_2_purchased = models.IntegerField(
        _(u"Cows/heifers 2 purchased"), blank=True, null=True)
    heifer_1_purchased = models.IntegerField(_(u"Heifers 1 purchased"),
                                             blank=True, null=True)
    calves_purchased = models.IntegerField(_(u"Calves purchased"), blank=True,
                                           null=True)

    def get_study_pk(self):
        return self.herd.study.pk

    def get_parent_id(self):
        return self.herd.pk

    def has_values(self):
        # only check if has animals
        for att in ('dairy_cow', 'heifer_2', 'heifer_1', 'heifer_0',
                    'other_animal'):
            if getattr(self, att):
                return True

    @classmethod
    def calculate(cls, study, herd_detail):
        herd_detail['turnover'] = None
        if herd_detail['dairy_cow']:
            herd_detail['turnover'] = float(
                (herd_detail['heifer_2'] or 0) +
                (herd_detail['cow_heifer_2_purchased'] or 0)
                - (herd_detail['heifer_2_sold'] or 0)) \
                / float(herd_detail['dairy_cow'])
        herd_detail['reformed_cow_sold'] = int(
            herd_detail['dairy_cow'] *
            float(herd_detail['reform_rate'] or 0) / 100.0)
        herd_detail['calves_sold'] = (herd_detail['dairy_cow'] or 0) \
            - (herd_detail['heifer_0'] or 0)

    def calculate_db(self):
        self.turnover = None
        if self.dairy_cow:
            self.turnover = float(
                (self.heifer_2 or 0) + (self.cow_heifer_2_purchased or 0)
                - (self.heifer_2_sold or 0)) / float(self.dairy_cow)
        self.reformed_cow_sold = int(round((self.dairy_cow or 0) *
                                     float(self.reform_rate or 0) / 100.0))
        self.calves_sold = (self.dairy_cow or 0) - (self.heifer_0 or 0)
        self.save()

post_save.connect(update_cache_trigger, sender=HerdDetail)
pre_delete.connect(remove_cache_trigger, sender=HerdDetail)


class MilkProduction(Serialization):
    _cache_saved = True
    _study_path = ('milk_production',)
    study = models.ForeignKey(Study, related_name='milk_production')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    quota = models.IntegerField(_(u"Quota"), blank=True, null=True)
    ref_mg = models.FloatField(_(u"Ref. MG"), blank=True, null=True)
    delivered_tb = models.FloatField(_(u"Delivered TB"), blank=True,
                                     null=True)
    available_litrage = models.IntegerField(_(u"Available litrage"),
                                            blank=True, null=True)
    production_per_cow_permited = models.IntegerField(_(u"Milk/cow permited"),
                                                      blank=True, null=True)
    total_milk_production = models.IntegerField(_(u"Total milk production"),
                                                blank=True, null=True)
    milk_used_per_calve_sold = models.IntegerField(
        _(u"Milk used/calve sold"), blank=True, null=True, default=25)
    milk_used_per_calve_raised = models.IntegerField(
        _(u"Milk used/calve raised"), blank=True, null=True, default=550)
    self_consumption_calves = models.IntegerField(
        _(u"Self consumption calves"), blank=True, null=True)
    self_consumption_private = models.IntegerField(
        _(u"Self consumption private"), blank=True, null=True)
    delivery = models.IntegerField(_(u"Delivery"), blank=True, null=True)
    quota_result = models.IntegerField(_(u"Quota result (+/-)"), blank=True,
                                       null=True)

    class Meta:
        unique_together = ('study', 'year',)

    @property
    def feed_milk_production(self):
        q = Feed.objects.filter(study=self.study, year=self.year, animal='dc')
        if not q.count():
            return
        f = q.all()[0]
        if not f.feed_template:
            return
        return f.feed_template.milk_production

    @classmethod
    def get_feed_milk_production(cls, study, milk_production):
        feed = None
        for fd in study['feed']:
            if unicode(fd['year']) == unicode(milk_production['year']) \
               and fd['animal'] == 'dc':
                feed = fd
        if not feed:
            return
        if not feed['feed_template_id']:
            return
        return FeedTemplate.get_cached(
            pk=feed['feed_template_id'])[0].milk_production

    @classmethod
    def calculate(cls, study, milk_production):
        hd = None
        for herd in study['herd']:
            for herd_detail in herd['herd_detail']:
                if herd_detail['year'] == milk_production['year']:
                    hd = herd_detail
                    break
            if hd:
                break
        yr = milk_production['year']
        available_litrage = None
        if milk_production['quota']:
            available_litrage = \
                milk_production['quota'] * (
                    ((milk_production['ref_mg'] or 0)
                     - (milk_production['delivered_tb'] or 0)) * 0.0175 + 1)
            available_litrage = round(available_litrage)

        production_per_cow_permited = None
        feed_milk_production = MilkProduction.get_feed_milk_production(
            study, milk_production)
        if feed_milk_production:
            production_per_cow_permited = feed_milk_production

        total_milk_production = None
        if production_per_cow_permited and hd and hd['dairy_cow']:
            total_milk_production = \
                hd['dairy_cow'] * production_per_cow_permited

        self_consumption_calves = None
        if hd:
            self_consumption_calves = \
                (hd['calves_sold'] or 0) * \
                (milk_production['milk_used_per_calve_sold'] or 0) + \
                (hd['heifer_0'] or 0) * \
                (milk_production['milk_used_per_calve_raised'] or 0) \
                or None

        delivery = None
        if total_milk_production:
            delivery = total_milk_production - (self_consumption_calves or 0) \
                - (milk_production['self_consumption_private'] or 0)

        calculated = ['self_consumption_calves', 'total_milk_production',
                      'production_per_cow_permited', 'available_litrage',
                      'delivery']
        changed = False
        for cal in calculated:
            if locals()[cal] != milk_production[cal]:
                changed = True
                milk_production[cal] = locals()[cal]
        return changed

    def calculate_db(self):
        q = HerdDetail.objects.filter(herd__study=self.study,
                                      year=self.year)
        hd = None
        if q.count():
            hd = q.all()[0]

        available_litrage = None
        if self.quota:
            available_litrage = self.quota * (
                ((self.ref_mg or 0) - (self.delivered_tb or 0)) * 0.0175 + 1)
            available_litrage = round(available_litrage)

        production_per_cow_permited = self.production_per_cow_permited
        if not production_per_cow_permited:
            production_per_cow_permited = self.feed_milk_production

        total_milk_production = None
        if production_per_cow_permited and hd and hd.dairy_cow:
            total_milk_production = hd.dairy_cow * production_per_cow_permited

        self_consumption_calves = None
        if hd:
            self_consumption_calves = \
                (hd.calves_sold or 0) * (self.milk_used_per_calve_sold or 0) + \
                (hd.heifer_0 or 0) * (self.milk_used_per_calve_raised or 0) \
                or None

        delivery = None
        if total_milk_production:
            delivery = total_milk_production - (self_consumption_calves or 0) \
                - (self.self_consumption_private or 0)

        calculated = ['self_consumption_calves', 'total_milk_production',
                      'production_per_cow_permited', 'available_litrage',
                      'delivery']
        changed = False
        for cal in calculated:
            if locals()[cal] != getattr(self, cal):
                changed = True
                setattr(self, cal, locals()[cal])
        if changed:
            self.save()


def simplecalculation_trigger(sender, **kwargs):
    instance = kwargs.get('instance')
    if not instance:
        return
    instance.calculate()

# post_save.connect(simplecalculation_trigger, sender=MilkProduction)
post_save.connect(update_cache_trigger, sender=MilkProduction)
pre_delete.connect(remove_cache_trigger, sender=MilkProduction)


class FeedItem(Serialization, Cached):
    name = models.CharField(_(u"Name"), max_length=150)
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    harvest_category = models.ForeignKey(
        HarvestCategory, verbose_name=_(u"Harvest category"), blank=True,
        null=True)
    usage_type = models.ForeignKey(UsageType, verbose_name=_(u"Usage type"),
                                   blank=True, null=True)
    color = models.CharField(_(u"Color"), max_length=20, blank=True, null=True)

    class Meta:
        verbose_name = _(u"Feed item")
        verbose_name_plural = _(u"Feed items")

    def __unicode__(self):
        return self.name


class BaseFeed(object):
    def graph_series(self, queryset, season=False):
        series, colors, feed_labels = [], [], []
        details = queryset.distinct().all()
        for detail in details:
            amounts = [getattr(detail, 'amount_%d' % month_nb) or 0
                       for month_nb in xrange(1, 13)
                       if not season or not (month_nb - 1) % 3]
            # dont't render empty feed items
            if not sum(amounts):
                continue
            if detail.feed_item.color:
                colors.append(detail.feed_item.color)
            else:
                colors.append('aaa')
            feed_labels.append(detail.feed_item.name)
            series.append(amounts)
        return series, colors, feed_labels


class FeedTemplate(Serialization, BaseFeed, Cached):
    _cache_saved = False
    name = models.CharField(_(u"Name"), max_length=150)
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    color = models.CharField(_(u"Color code"), max_length=10,
                             blank=True, null=True)
    diagnostic = models.BooleanField(_(u"Diagnostic"), default=False)
    farming_type = models.CharField(_(u"Farming type"), choices=FARMING_TYPES,
                                    max_length=4)
    default_for_diag = models.BooleanField(_(u"Default for diagnostic"),
                                           default=False)
    milk_production = models.IntegerField(_(u"Milk production"), blank=True,
                                          null=True)
    animal_default = models.CharField(
        _(u"Default for animal"), choices=ANIMALS, max_length=2, null=True,
        blank=True)
    # saved custom feed template
    associated_user = models.ForeignKey(
        User, verbose_name=_('Associated user'), related_name='feed_templates',
        blank=True, null=True)

    class Meta:
        verbose_name = _(u"Feed template")
        verbose_name_plural = _(u"Feed templates")

    def __unicode__(self):
        return self.name

    def copy_from_feed(self, feed):
        FeedTemplateDetail.objects.filter(feed_template=self).delete()
        for detail in feed.feed_details.all():
            ftd, created = FeedTemplateDetail.objects.get_or_create(
                feed_item=detail.feed_item, feed_template=self)
            for month_idx in xrange(1, 13):
                amount_key = 'amount_%d' % month_idx
                setattr(ftd, amount_key, getattr(detail, amount_key))
            ftd.save()

    @classmethod
    def create_default_for_diag(cls):
        ft, created = cls.objects.get_or_create(
            name=u"Default for diagnostic", diagnostic=True,
            default_for_diag=True)
        values = {'feed_template': ft}
        for month in xrange(1, 13):
            values['amount_%d' % month] = 1
        for feed_item in FeedItem.objects.all():
            values['feed_item'] = feed_item
            FeedTemplateDetail.objects.create(**values)
        return ft

    def graph_series(self):
        q = FeedTemplateDetail.objects\
                              .filter(feed_template=self)\
                              .order_by('feed_item')
        return super(FeedTemplate, self).graph_series(q)


def default_feed_trigger(sender, **kwargs):
    instance = kwargs.get('instance')
    if not instance:
        return
    if instance.default_for_diag:
        for ft in FeedTemplate.objects\
                              .exclude(pk=instance.pk)\
                              .filter(default_for_diag=True).all():
            ft.default_for_diag = False
            ft.save()
    if instance.animal_default:
        for ft in FeedTemplate.objects\
                              .exclude(pk=instance.pk)\
                              .filter(animal_default=instance.animal_default)\
                              .all():
            ft.animal_default = None
            ft.save()

post_save.connect(default_feed_trigger, sender=FeedTemplate)


class FeedTemplateDetail(models.Model):
    feed_template = models.ForeignKey(FeedTemplate,
                                      related_name='feed_template_details')
    feed_item = models.ForeignKey(FeedItem)
    amount_1 = models.FloatField(_(u"January"), default=0)
    amount_2 = models.FloatField(_(u"February"), default=0)
    amount_3 = models.FloatField(_(u"March"), default=0)
    amount_4 = models.FloatField(_(u"April"), default=0)
    amount_5 = models.FloatField(_(u"May"), default=0)
    amount_6 = models.FloatField(_(u"June"), default=0)
    amount_7 = models.FloatField(_(u"July"), default=0)
    amount_8 = models.FloatField(_(u"August"), default=0)
    amount_9 = models.FloatField(_(u"September"), default=0)
    amount_10 = models.FloatField(_(u"October"), default=0)
    amount_11 = models.FloatField(_(u"November"), default=0)
    amount_12 = models.FloatField(_(u"December"), default=0)


class Feed(Serialization, BaseFeed):
    _cache_saved = True
    _study_path = ('feed',)
    study = models.ForeignKey(Study, related_name='feed')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    feed_template = models.ForeignKey(
        FeedTemplate, blank=True, null=True,
        verbose_name=_(u"Associated feed template"))
    animal = models.CharField(_(u"Animal"), choices=ANIMALS, max_length=2,
                              null=True, blank=True)

    def get_feed_items(self):
        return FeedItem.objects.filter(feed_details__feed=self)\
                               .distinct().order_by('pk').all()

    def has_values(self):
        return True

    def graph_series(self, season=False):
        q = FeedDetail.objects.filter(feed=self).order_by('feed_item')
        return super(Feed, self).graph_series(q, season=season)

    @property
    def image_path(self):
        return '/feeds/%d-%s-%s.png' % (self.study.pk, self.year, self.animal)

    def numpy_graph(self):
        series, colors, feed_labels = self.graph_series()
        if not series:
            return
        y = numpy.row_stack(series)
        x = numpy.arange(1, 13)
        y_stack = numpy.cumsum(y, axis=0)
        fig = pyplot.figure()
        ax = fig.add_subplot(111)
        legends, boxes = [], []
        for idx, color in enumerate(colors):
            start = 0
            if idx:
                start = y_stack[idx - 1, :]
            ax.fill_between(x, start, y_stack[idx, :], color='#' + color,
                            alpha=0.7)
            legends.append(unicode(feed_labels[idx]))
            boxes.append(pyplot.Rectangle((0, 0), 1, 1, fc="#" + color))

        ax.set_xticks(range(12))
        ax.set_xticklabels(['', unicode(_('Jan.')), unicode(_('Fev.')),
                            unicode(_('Mar.')), unicode(_('Avr.')),
                            unicode(_('Mai')), unicode(_('Juin')),
                            unicode(_('Juil.')), unicode(_('Aout')),
                            unicode(_('Sept.')), unicode(_('Oct.')),
                            unicode(_('Nov.')), unicode(_('Dec.'))])

        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.3,
                         box.width, box.height * 0.7])

        ax.legend(
            reversed(boxes), reversed(legends),
            loc='upper center', bbox_to_anchor=(0.5, -0.05),
            fancybox=True, shadow=True, ncol=2)

        # pyplot.show()
        path = settings.MEDIA_ROOT + self.image_path
        pyplot.savefig(path)
        return path


# save old templates values to verify if they have changed
pre_save_feed_values = {}


def feed_presave(sender, **kwargs):
    if not kwargs['instance'] or not kwargs['instance'].pk:
        return
    instance = kwargs['instance']
    try:
        instance = Feed.objects.get(pk=instance.pk)
        pre_save_feed_values[instance.pk] = instance.feed_template
    except ObjectDoesNotExist:
        pass
# pre_save.connect(feed_presave, sender=Feed)


def feed_changed(sender, **kwargs):
    feed = kwargs.get('instance')
    if feed.year == 'D' and not feed.feed_template:
        q = FeedTemplate.objects.filter(default_for_diag=True)
        ft = None
        if q.count():
            ft = q.all()[0]
        else:
            ft = FeedTemplate.create_default_for_diag()
        feed.feed_template = ft
        feed.save()
        return
    if not feed or feed.pk not in pre_save_feed_values \
       or pre_save_feed_values[feed.pk] == feed.feed_template:
        return
    FeedDetail.objects.filter(feed=feed).delete()
    if not feed.feed_template \
       or not feed.feed_template.feed_template_details.count():
        return
    for detail in feed.feed_template.feed_template_details.all():
        values = {'feed': feed, 'feed_item': detail.feed_item, 'defaults': {}}
        for month_nb in xrange(1, 13):
            amount_key = 'amount_%d' % month_nb
            values['defaults'][amount_key] = getattr(detail, amount_key)
        fd, created = FeedDetail.objects.get_or_create(**values)
        if not created:
            for k in values['defaults']:
                setattr(fd, k, values['defaults'][k])
            fd.save()

# post_save.connect(feed_changed, sender=Feed)
post_save.connect(update_cache_trigger, sender=Feed)
pre_delete.connect(remove_cache_trigger, sender=Feed)


class FeedDetail(Serialization):
    _cache_saved = True
    _study_path = ('feed', 'feed_details')
    feed = models.ForeignKey(Feed, related_name='feed_details')
    amount_1 = models.FloatField(_(u"January"), default=0)
    amount_2 = models.FloatField(_(u"February"), default=0)
    amount_3 = models.FloatField(_(u"March"), default=0)
    amount_4 = models.FloatField(_(u"April"), default=0)
    amount_5 = models.FloatField(_(u"May"), default=0)
    amount_6 = models.FloatField(_(u"June"), default=0)
    amount_7 = models.FloatField(_(u"July"), default=0)
    amount_8 = models.FloatField(_(u"August"), default=0)
    amount_9 = models.FloatField(_(u"September"), default=0)
    amount_10 = models.FloatField(_(u"October"), default=0)
    amount_11 = models.FloatField(_(u"November"), default=0)
    amount_12 = models.FloatField(_(u"December"), default=0)
    feed_item = models.ForeignKey(FeedItem, related_name='feed_details')

    def get_study_pk(self):
        return self.feed.study.pk

    def get_parent_id(self):
        return self.feed.pk


post_save.connect(update_cache_trigger, sender=FeedDetail)
pre_delete.connect(remove_cache_trigger, sender=FeedDetail)


def update_season(detail):
    changed = False
    for season_number in xrange(0, 4):
        season_mt_number = season_number * 3 + 1
        value = getattr(detail, 'amount_%d' % season_mt_number)
        for delta in xrange(1, 3):
            mt_number = season_mt_number + delta
            if value != getattr(detail, 'amount_%d' % mt_number):
                setattr(detail, 'amount_%d' % mt_number, value)
                changed = True
    if changed:
        detail.save()


class StrawUsage(Serialization):
    _cache_saved = True
    _study_path = ('herd', 'herd_detail', 'straw_usage')
    herd_detail = models.ForeignKey(HerdDetail, related_name="straw_usage")
    animal = models.CharField(_(u"Animal"), choices=ANIMALS, max_length=2)
    daily_amount = models.FloatField(_(u"Daily amount"), default=0)
    stall_days_1 = models.IntegerField(_(u"Jan."), default=31)
    stall_days_2 = models.IntegerField(_(u"Feb."), default=28)
    stall_days_3 = models.IntegerField(_(u"Mar."), default=31)
    stall_days_4 = models.IntegerField(_(u"Apr."), default=30)
    stall_days_5 = models.IntegerField(_(u"May"), default=31)
    stall_days_6 = models.IntegerField(_(u"Jun."), default=30)
    stall_days_7 = models.IntegerField(_(u"Jul."), default=31)
    stall_days_8 = models.IntegerField(_(u"Aug."), default=31)
    stall_days_9 = models.IntegerField(_(u"Sep."), default=30)
    stall_days_10 = models.IntegerField(_(u"Oct."), default=31)
    stall_days_11 = models.IntegerField(_(u"Nov."), default=30)
    stall_days_12 = models.IntegerField(_(u"Dec."), default=31)

    def get_study_pk(self):
        return self.herd_detail.herd.study.pk

    def get_parent_id(self):
        return self.herd_detail.pk

    def has_values(self):
        if not self.daily_amount:
            return False
        for att in ('stall_days_1', 'stall_days_2', 'stall_days_3',
                    'stall_days_4', 'stall_days_5', 'stall_days_6',
                    'stall_days_7', 'stall_days_8', 'stall_days_9',
                    'stall_days_10', 'stall_days_11', 'stall_days_12',):
            if getattr(self, att):
                return True

    def save(self, *args, **kwargs):
        if not self.pk and not self.daily_amount and \
           self.animal in settings.DEFAULT_STRAW_USAGE:
            self.daily_amount, self.stall_days_1, self.stall_days_2, \
                self.stall_days_3, self.stall_days_4, self.stall_days_5, \
                self.stall_days_6, self.stall_days_7, self.stall_days_8, \
                self.stall_days_9, self.stall_days_10, self.stall_days_11, \
                self.stall_days_12 = settings.DEFAULT_STRAW_USAGE[self.animal]
        return super(StrawUsage, self).save(*args, **kwargs)

post_save.connect(update_cache_trigger, sender=StrawUsage)
pre_delete.connect(remove_cache_trigger, sender=StrawUsage)


class FeedNeed(Serialization):
    _cache_saved = True
    _study_path = ('feed_need',)
    study = models.ForeignKey(Study, related_name='feed_need')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    harvest_category_usage = models.ForeignKey(
        HarvestCategoryUsageType, verbose_name=_(u"Harvest category"))
    amount = models.FloatField(_(u"Amount"), default=0)
    litter_amount = models.FloatField(_(u"Litter amount"), default=0)
    associated_surface = models.FloatField(_(u"Associated surface"), default=0,
                                           blank=True, null=True)
    balance = models.FloatField(_(u"Balance"), default=0, blank=True,
                                null=True)

    @property
    def target_type(self):
        return self.harvest_category_usage.usage_type.target_type

    @property
    def balance_pourc(self):
        if not self.amount:
            return '-'
        balance_pourc = 100 + self.balance / self.amount * 100
        return round(balance_pourc, 1)

    @classmethod
    def calculate(self, study, feed_need_id):
        for fn in study['feed_need']:
            if fn and fn.get('id') == feed_need_id:
                feed_need = fn
                break
        else:
            return
        herd = study['herd'][0]
        herd_detail = None
        for hd in herd['herd_detail']:
            if unicode(hd['year']) == unicode(feed_need['year']):
                herd_detail = hd
        if not herd_detail:
            # TODO: set to 0 values?
            return
        amount = 0
        harvest_category_usage = HarvestCategoryUsageType.get_cached(
            pk=feed_need['harvest_category_usage_id'])[0]
        for feed in study['feed']:
            for fd in feed['feed_details']:
                feed_item = FeedItem.get_cached(pk=fd['feed_item_id'])[0]
                if unicode(feed['year']) == unicode(feed_need['year']) \
                   and feed_item.harvest_category == \
                   harvest_category_usage.harvest_category \
                   and feed_item.usage_type == \
                   harvest_category_usage.usage_type:
                    nb_animal = herd_detail[ANIMALS_KEY[feed['animal']]]
                    if not nb_animal:
                        continue
                    # 2013 is a non-bisextile year
                    amount += nb_animal * sum(
                        [monthrange(2013, month_nb)[1] / float(1000)
                         * (fd['amount_%d' % month_nb] or 0)
                         for month_nb in xrange(1, 13)])

        # litter management
        if harvest_category_usage.harvest_category.txt_idx == 'cereale' \
                and harvest_category_usage.usage_type.txt_idx == 'paille':
            for animal in ANIMALS_KEY:
                nb_animals = herd_detail[ANIMALS_KEY[animal]]
                if not nb_animals:
                    continue
                straw_usage = None
                for su in herd_detail['straw_usage']:
                    if animal == su['animal']:
                        straw_usage = su
                if not straw_usage:
                    herd_detail['straw_usage'].append(
                        {'herd_detail_id': herd_detail['id'],
                         'animal': animal, 'MODEL': 'StrawUsage'})
                    straw_usage = herd_detail['straw_usage'][-1]
                if not straw_usage.get('daily_amount'):
                    continue
                stall_days = sum([straw_usage.get('stall_days_%d' % mt_nb)
                                  for mt_nb in xrange(1, 13)])
                amount += nb_animals * straw_usage.get('daily_amount') \
                    * stall_days / 1000.0

        # not used in simulabio LT
        # if self.diagnostic:
        #     amount = amount/(self.harvest_category_usage.diag_yield/100.0)
        # else:
        #     amount = amount/(self.harvest_category_usage.project_yield/100.0)
        # associated_surface

        harvest_yield_attr = 'average_%s_yield' % \
                             harvest_category_usage.usage_type.target_type
        hcs = None
        for harvest_category in study['harvest_categories']:
            if unicode(harvest_category['year']) == unicode(feed_need['year'])\
               and unicode(harvest_category_usage.harvest_category.pk) \
               == unicode(harvest_category['harvest_category_id']):
                hcs = harvest_category
                break
        if not hcs:
            study['harvest_categories'].append({
                'harvest_category_id':
                harvest_category_usage.harvest_category.pk,
                'study_id': study['id'], 'year': feed_need['year'],
                'MODEL': 'HarvestCategoryStudy'})
            hcs = study['harvest_categories'][-1]
        harvest_yield = hcs.get(harvest_yield_attr)
        associated_surface = 0
        if harvest_yield:
            associated_surface = amount / harvest_yield
        else:
            associated_surface = None
        # TODO: warning when no harvest_yield is defined

        balance = -amount
        for supply in feed_need['supply']:
            balance += supply['used']
        for extra_supply in feed_need['extra_supply']:
            balance += extra_supply['used']
        feed_need['amount'] = amount
        feed_need['balance'] = balance
        feed_need['associated_surface'] = associated_surface
        return feed_need

    def calculate_db(self):
        herd = Herd.objects.get(study=self.study)
        herd_detail = herd.herd_detail.filter(year=self.year)
        if not herd_detail.count():
            # TODO: set to 0 values?
            return
        herd_detail = herd_detail.all()[0]
        amount = 0
        # check all the feeds to see if the associated category have a value
        c_hc = self.harvest_category_usage.harvest_category
        q_fd = FeedDetail.objects.filter(
            feed__study=self.study, feed__year=self.year,
            feed_item__harvest_category=c_hc,
            feed_item__usage_type=self.harvest_category_usage.usage_type).all()
        for fd in q_fd:
            nb_animal = getattr(herd_detail, ANIMALS_KEY[fd.feed.animal])
            if not nb_animal:
                continue
            # add the amount of feed needed
            # 2013 is a non-bisextile year - amount is in tons
            amount += nb_animal * sum(
                [monthrange(2013, month_nb)[1] / float(1000) *
                 (getattr(fd, 'amount_%d' % month_nb) or 0)
                 for month_nb in xrange(1, 13)])

        litter_amount = 0
        # litter management
        if self.harvest_category_usage.harvest_category.txt_idx == 'paille' \
                and self.harvest_category_usage.usage_type.txt_idx == 'paille':
            for animal in ANIMALS_KEY:
                nb_animals = getattr(herd_detail, ANIMALS_KEY[animal])
                if not nb_animals:
                    continue
                straw_usage, created = StrawUsage.objects\
                    .get_or_create(herd_detail=herd_detail, animal=animal)
                if not straw_usage.daily_amount:
                    continue
                stall_days = sum([getattr(straw_usage, 'stall_days_%d' % mt_nb)
                                  for mt_nb in xrange(1, 13)])
                # amount is in tons
                litter_amount += nb_animals * straw_usage.daily_amount \
                    * stall_days / 1000.0
        amount += litter_amount

        # not used in simulabio LT
        # if self.diagnostic:
        #     amount = amount/(self.harvest_category_usage.diag_yield/100.0)
        # else:
        #     amount = amount/(self.harvest_category_usage.project_yield/100.0)
        #  associated_surface
        harvest_yield_attr = 'average_%s_yield' % \
                             self.harvest_category_usage.usage_type.target_type
        hcs, created = HarvestCategoryStudy.objects.get_or_create(
            study=self.study, year=self.year,
            harvest_category=self.harvest_category_usage.harvest_category)
        harvest_yield = getattr(hcs, harvest_yield_attr)
        associated_surface = 0
        if harvest_yield:
            associated_surface = amount / harvest_yield
        else:
            associated_surface = None
        # TODO: warning when no harvest_yield is defined

        balance = -amount
        for supply in self.supply.all():
            balance += supply.used
        for extra_supply in self.extra_supply.all():
            balance += extra_supply.used

        if amount != self.amount or \
           litter_amount != self.litter_amount or \
           associated_surface != self.associated_surface or \
           balance != self.balance:
            self.amount = amount
            self.balance = balance
            self.associated_surface = associated_surface
            self.litter_amount = litter_amount
            self.save()
            return True

post_save.connect(update_cache_trigger, sender=FeedNeed)
pre_delete.connect(remove_cache_trigger, sender=FeedNeed)


class FeedNeedSupply(Serialization):
    _cache_saved = True
    _study_path = ('feed_need', 'supply')
    feed_need = models.ForeignKey(FeedNeed, related_name='supply')
    harvest_settings = models.ForeignKey(HarvestSettings,
                                         related_name='supply')
    used = models.FloatField(_(u"Used"), default=0)

    def get_study_pk(self):
        return self.feed_need.study.pk

    def get_parent_id(self):
        return self.feed_need.pk

"""
def supply_changed(sender, **kwargs):
    supply = kwargs.get('instance')
    if not supply:
        return
    try:
        if supply.harvest_settings and supply.feed_need:
            #supply.harvest_settings.calculate(not supply.feed_need.diagnostic)
            calculate(supply.harvest_settings)
            calculate(supply.feed_need)
    except ObjectDoesNotExist:
        # occured in some deletions
        pass
"""

# post_save.connect(supply_changed, sender=FeedNeedSupply)
# post_delete.connect(supply_changed, sender=FeedNeedSupply)


class ExtraSupply(GeneralType):
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)

    class Meta:
        verbose_name = _(u"Extra supply")
        verbose_name_plural = _(u"Extra supplies")
        ordering = ('order',)
post_save.connect(auto_inc_order_trigger, sender=ExtraSupply)


class FeedNeedExtraSupply(Serialization):
    _cache_saved = True
    _study_path = ('feed_need', 'supply')
    feed_need = models.ForeignKey(FeedNeed, related_name='extra_supply')
    extra_supply = models.ForeignKey(ExtraSupply, related_name='extra_supply')
    used = models.FloatField(_(u"Used"), default=0)

    def get_study_pk(self):
        return self.feed_need.study.pk

    def get_parent_id(self):
        return self.feed_need.pk

post_save.connect(update_cache_trigger, sender=FeedNeedExtraSupply)
pre_delete.connect(remove_cache_trigger, sender=FeedNeedExtraSupply)

# simple economy

EXPENSE_TYPES = (('VET', _(u"Frais véto")),
                 ('PHY', _(u"Frais phyto")),
                 ('ENG', _(u"Frais engrais")),
                 ('FEED', _(u"Frais aliments achetés")),
                 ('STRA', _(u"Frais paille achetée")),
                 )


class SimpleEcoInitialExpenseDetail(Serialization):
    _cache_saved = True
    _study_path = ('charge_details',)
    study = models.ForeignKey(Study, related_name='charge_details')
    type = models.CharField(_(u"Type"), max_length='4', choices=EXPENSE_TYPES)
    value = models.IntegerField(_(u"Value"), default=0)
    ASSOCIATED_COST = {'VET': 'veterinary_cost',
                       'PHY': 'phytosanitary_cost',
                       'ENG': 'fertilizer_cost'}

    class Meta:
        verbose_name = _(u"Simple eco - initial charge detail")
        verbose_name_plural = _(u"Simple eco - initial charge details")
        unique_together = ('study', 'type',)

    def save(self, *args, **kwargs):
        if self.type in self.ASSOCIATED_COST.keys():
            associated_expense, created = Expenses.objects.get_or_create(
                study=self.study, year='D')
            if self.type == 'VET':
                q = HerdDetail.objects.filter(herd__study=self.study,
                                              year='D')
                new_val = 0
                if q.count():
                    herd = q.all()[0]
                    new_val = self.value / herd.dairy_cow if herd.dairy_cow \
                        else 0
                if getattr(associated_expense,
                           self.ASSOCIATED_COST[self.type]) != new_val:
                    setattr(associated_expense,
                            self.ASSOCIATED_COST[self.type], new_val)
                    associated_expense.save()
            else:
                new_val = self.value / self.study.total_surface('D') \
                    if self.study.total_surface('D') else 0
                if getattr(associated_expense,
                           self.ASSOCIATED_COST[self.type]) != new_val:
                    setattr(associated_expense,
                            self.ASSOCIATED_COST[self.type], new_val)
                    associated_expense.save()
        return super(SimpleEcoInitialExpenseDetail, self).save(*args, **kwargs)

post_save.connect(update_cache_trigger, sender=SimpleEcoInitialExpenseDetail)
pre_delete.connect(remove_cache_trigger, sender=SimpleEcoInitialExpenseDetail)


class Revenues(Serialization):
    _cache_saved = True
    _study_path = ('revenues',)
    study = models.ForeignKey(Study, related_name='revenues')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    milk_price = models.IntegerField(_(u"Milk price (€/t)"), blank=True,
                                     null=True)
    milk_revenue = models.IntegerField(_(u"Milk revenue"), blank=True,
                                       null=True)
    reformed_cow_price = models.IntegerField(_(u"Reformed cow price"),
                                             blank=True, null=True)
    reformed_cow_revenue = models.IntegerField(_(u"Reformed cow revenue"),
                                               blank=True, null=True)
    calve_price = models.IntegerField(_(u"Calve price"), blank=True, null=True)
    calve_revenue = models.IntegerField(_(u"Calve revenue"), blank=True,
                                        null=True)
    heifer_price = models.IntegerField(_(u"Heifer price"), blank=True,
                                       null=True)
    heifer_revenue = models.IntegerField(_(u"Heifer revenue"), blank=True,
                                         null=True)
    total_revenue = models.IntegerField(_(u"Total revenue"), default=0)

    class Meta:
        unique_together = ('study', 'year',)

    def __unicode__(self):
        return u'%s - %s - %s' % (unicode(self.study), unicode(self.year),
                                  unicode(self.total_revenue))

    @classmethod
    def calculate(cls, study, revenue):
        milk_revenue = None
        if revenue['milk_price']:
            for milk_production in study['milk_production']:
                if unicode(milk_production['year'])\
                   == unicode(revenue['year']) and \
                   milk_production['delivery']:
                    milk_revenue = milk_production['delivery'] / 1000.0 \
                        * revenue['milk_price']

        reformed_cow_revenue, heifer_revenue, calve_revenue = None, None, None
        for herd_detail in study['herd'][0]['herd_detail']:
            if herd_detail['year'] == revenue['year']:
                if revenue['reformed_cow_price']:
                    reformed_cow_revenue = (
                        herd_detail['reformed_cow_sold'] or 0) * \
                        revenue['reformed_cow_price']
                if revenue['calve_price']:
                    calve_revenue = (herd_detail['calves_sold'] or 0
                                     ) * revenue['calve_price']
                if revenue['heifer_price']:
                    heifer_price = (herd_detail['heifer_2_sold'] or 0
                                    ) * revenue['heifer_price']
        total_revenue = (milk_revenue or 0) + (reformed_cow_revenue or 0) + \
                        (heifer_revenue or 0) + (calve_revenue or 0)
        for k in ('milk_revenue', 'heifer_revenue', 'calve_revenue',
                  'total_revenue'):
            revenue[k] = locals()[k]

    def calculate_db(self):
        milk_revenue = None
        if self.milk_price:
            q = MilkProduction.objects.filter(study=self.study, year=self.year)
            if q.count():
                milk = q.all()[0]
                if milk.delivery:
                    milk_revenue = milk.delivery / 1000.0 * self.milk_price

        reformed_cow_revenue, heifer_revenue, calve_revenue = None, None, None
        q = HerdDetail.objects.filter(herd__study=self.study, year=self.year)
        if q.count():
            herd = q.all()[0]
            if self.reformed_cow_price:
                reformed_cow_revenue = \
                    (herd.reformed_cow_sold or 0) * self.reformed_cow_price
            if self.calve_price:
                calve_revenue = \
                    (herd.calves_sold or 0) * self.calve_price
            if self.heifer_price:
                heifer_revenue = \
                    (herd.heifer_2_sold or 0) * self.heifer_price

        total_revenue = (milk_revenue or 0) + (reformed_cow_revenue or 0) + \
                        (heifer_revenue or 0) + (calve_revenue or 0)

        if milk_revenue != self.milk_revenue or \
           reformed_cow_revenue != self.reformed_cow_revenue or\
           heifer_revenue != self.heifer_revenue or\
           calve_revenue != self.calve_revenue or\
           total_revenue != self.total_revenue:
            self.milk_revenue = milk_revenue
            self.reformed_cow_revenue = reformed_cow_revenue
            self.heifer_revenue = heifer_revenue
            self.calve_revenue = calve_revenue
            self.total_revenue = total_revenue
            self.save()

post_save.connect(update_cache_trigger, sender=Revenues)
pre_delete.connect(remove_cache_trigger, sender=Revenues)

# post_save.connect(simplecalculation_trigger, sender=Revenues)


class ExtraRevenueType(GeneralType):
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    full_eco = models.BooleanField(_(u"Only available on full eco"),
                                   default=True)

    class Meta:
        verbose_name = _(u"Extra revenue type")
        verbose_name_plural = _(u"Extra revenue types")
        ordering = ('order',)
post_save.connect(auto_inc_order_trigger, sender=ExtraRevenueType)


class ExtraRevenue(Serialization):
    _cache_saved = True
    _lbl_field = 'name'
    _study_path = ('extra_revenues',)
    study = models.ForeignKey(Study, related_name='extra_revenues')
    name = models.CharField(_(u"Name"), max_length=100)
    full_eco = models.BooleanField(_(u"Available on full eco"), default=True)
    associated_type = models.ForeignKey(
        ExtraRevenueType, related_name='extra_revenues', blank=True, null=True)


class ExtraRevenueYear(Serialization):
    _cache_saved = True
    _study_path = ('extra_revenues', 'year')
    extra_revenue = models.ForeignKey(ExtraRevenue, related_name='year')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    price = models.IntegerField(_(u"Price"), default=0)
    quantity = models.IntegerField(_(u"Quantity"), default=0)

    class Meta:
        unique_together = ('extra_revenue', 'year',)

    def get_study_pk(self):
        return self.extra_revenue.study.pk

    def get_parent_id(self):
        return self.extra_revenue.pk

    @property
    def revenue(self):
        return self.price * self.quantity


class Expenses(Serialization):
    _cache_saved = True
    _study_path = ('expenses',)
    study = models.ForeignKey(Study, related_name='expenses')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    veterinary_cost = models.IntegerField(_(u"Veterinary cost €/cow"),
                                          null=True, blank=True)
    veterinary_expenses = models.IntegerField(_(u"Veterinary expenses"),
                                              default=0)
    phytosanitary_cost = models.IntegerField(_(u"Phytosanitary cost €/ha"),
                                             null=True, blank=True)
    phytosanitary_expenses = models.IntegerField(_(u"Phytosanitary expenses"),
                                                 default=0)
    fertilizer_cost = models.IntegerField(_(u"Fertilizer cost €/ha"),
                                          null=True, blank=True)
    fertilizer_expenses = models.IntegerField(_(u"Fertilizer expenses"),
                                              default=0)
    calve_cost = models.IntegerField(_(u"Calve cost"), null=True, blank=True)
    calve_expenses = models.IntegerField(_(u"Calve expenses"), default=0)
    heifer_cost = models.IntegerField(_(u"Heifer cost"), null=True, blank=True)
    heifer_expenses = models.IntegerField(_(u"Heifer expenses"), default=0)
    total_expenses = models.IntegerField(_(u"Total expenses"), default=0)
    total_expenses_without_extra = models.IntegerField(
        _(u"Total expenses without extra"), default=0)

    class Meta:
        unique_together = ('study', 'year',)

    def __unicode__(self):
        return u'%s - %s - %s' % (unicode(self.study), unicode(self.year),
                                  unicode(self.total_expenses))

    @classmethod
    def calculate(cls, study, expenses):
        # TODO: revoir les couts phyto, ... pour le diag
        values = {'phytosanitary_expenses': 0,
                  'veterinary_expenses': 0,
                  'fertilizer_expenses': 0,
                  'calve_expenses': 0,
                  'heifer_expenses': 0,
                  'total_expenses': 0}
        herd = None
        for herd_detail in study['herd'][0]['herd_detail']:
            if unicode(herd_detail['year']) == unicode(expenses['year']):
                herd = herd_detail
                break
        if expenses['year'] == 'D':
            for value_key, type in (('phytosanitary_expenses', 'PHY'),
                                    ('veterinary_expenses', 'VET'),
                                    ('fertilizer_expenses', 'ENG'),
                                    ):
                for charge_detail in study['charge_details']:
                    if charge_detail['type'] == type:
                        values[value_key] = charge_detail['value']
        else:
            if herd:
                values['veterinary_expenses'] = \
                    (expenses['veterinary_cost'] or 0) * \
                    (herd['dairy_cow'] or 0)
            surface = 0
            for harvest_settings in study['harvest_settings']:
                for harvest_settings_year in harvest_settings['year_details']:
                    if unicode(harvest_settings_year['year']) == \
                       unicode(expenses['year']):
                        surface += harvest_settings_year['surface_total'] or 0
            values['fertilizer_expenses'] = (surface or 0) *\
                                            (expenses['fertilizer_cost'] or 0)
            values['phytosanitary_expenses'] = \
                (surface or 0) * (expenses['phytosanitary_cost'] or 0)
        if herd:
            if herd["calves_purchased"]:
                values['calve_expenses'] = (expenses['calve_cost'] or 0) * \
                                           (herd['calves_purchased'] or 0)
            if herd['cow_heifer_2_purchased']:
                values['heifer_expenses'] = (expenses["heifer_cost"] or 0) * \
                                            (herd['heifer_1_purchased'] or 0)
            # TODO: cow_heifer_2_purchased ???
        # straw = 0
        values['total_expenses'] = values['heifer_expenses'] + \
            values['calve_expenses'] + \
            values['veterinary_expenses'] + values['fertilizer_expenses'] + \
            values['phytosanitary_expenses']
        changed = False
        for k in values:
            if values[k] != expenses[k]:
                expenses[k] = values[k]
                changed = True
        return changed

    def calculate_db(self):
        values = {'phytosanitary_expenses': 0,
                  'veterinary_expenses': 0,
                  'fertilizer_expenses': 0,
                  'calve_expenses': 0,
                  'heifer_expenses': 0,
                  'total_expenses': 0}
        herd = None
        q = HerdDetail.objects.filter(herd__study=self.study, year=self.year)
        if q.count():
            herd = q.all()[0]
        extra_expense = 0
        if self.year == 'D':
            # straw
            q = SimpleEcoInitialExpenseDetail.objects.filter(
                study=self.study, type='STRA')
            if q.count():
                extra_expense = q.all()[0].value or 0
            surface = self.study.total_surface(self.year)
            associated_expense, created = Expenses.objects.get_or_create(
                study=self.study, year='D')
            for value_key, type in (('phytosanitary_expenses', 'PHY'),
                                    ('veterinary_expenses', 'VET'),
                                    ('fertilizer_expenses', 'ENG'),
                                    ):
                q = SimpleEcoInitialExpenseDetail.objects.filter(
                    study=self.study, type=type)
                if q.count():
                    xp = q.all()[0]
                    values[value_key] = xp.value
                    # force update of cost by surface and cost by cow
                    xp.save()
        else:
            if herd:
                values['veterinary_expenses'] = (self.veterinary_cost or 0) *\
                                                (herd.dairy_cow or 0)
            surface = self.study.total_surface(self.year)
            values['fertilizer_expenses'] = surface *\
                (self.fertilizer_cost or 0)
            values['phytosanitary_expenses'] = (surface or 0) *\
                                               (self.phytosanitary_cost or 0)
        if herd:
            if herd.calves_purchased:
                values['calve_expenses'] = (self.calve_cost or 0) * \
                                           (herd.calves_purchased or 0)
            values['heifer_expenses'] = 0
            if herd.heifer_1_purchased:
                values['heifer_expenses'] += (self.heifer_cost or 0) * \
                    (herd.heifer_1_purchased or 0)
            if herd.cow_heifer_2_purchased:
                values['heifer_expenses'] += (self.heifer_cost or 0) * \
                    (herd.cow_heifer_2_purchased or 0)
        values['total_expenses'] = values['heifer_expenses'] + \
            values['calve_expenses'] + \
            values['veterinary_expenses'] + values['fertilizer_expenses'] + \
            values['phytosanitary_expenses'] + extra_expense
        extra_supply_prices = {}
        """
        if self.year == "D":
            q = SimpleEcoInitialExpenseDetail.objects.filter(
                                            study=self.study, type='FEED')
            if q.count():
                values['total_expenses'] += q.all()[0].value
        else:
        """
        q = ExtraSupplyPriceYear.objects.filter(
            extra_supply_price__study=self.study,
            year=self.year)
        for extra_supply_price in q.distinct().all():
            extra_supply_prices[extra_supply_price.pk] = \
                [extra_supply_price, 0]
        q = FeedNeedExtraSupply.objects.filter(
            feed_need__study=self.study, feed_need__year=self.year).all()
        for extra_supply in q:
            q = ExtraSupplyPriceYear.objects.filter(
                extra_supply_price__study=self.study,
                extra_supply_price__extra_supply=extra_supply.extra_supply,
                year=self.year)
            if q.count():
                extra_supply_price = q.all()[0]
                val = (extra_supply_price.price or 0) \
                    * (extra_supply.used or 0)
                extra_supply_prices[extra_supply_price.pk][1] += val
                values['total_expenses'] += val
        for k in extra_supply_prices:
            extra_supply_price, val = extra_supply_prices[k]
            extra_supply_price.total = val
            extra_supply_price.save()
        # end else
        values['total_expenses_without_extra'] = values['total_expenses']
        q = ExtraExpenseYear.objects\
            .filter(year=self.year,
                    extra_expense__study=self.study,
                    extra_expense__full_eco=False)\
            .exclude(extra_expense__associated_type__full_eco=True).all()
        for expense in q:
            values['total_expenses'] += expense.expense

        changed = bool([setattr(self, k, values[k]) or k
                        for k in values if values[k] != getattr(self, k)])
        if changed:
            self.save()

# post_save.connect(simplecalculation_trigger, sender=Expenses)


class ExtraExpenseType(GeneralType):
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    full_eco = models.BooleanField(_(u"Only available on full eco"),
                                   default=True)

    class Meta:
        verbose_name = _(u"Extra expense type")
        verbose_name_plural = _(u"Extra expenses types")
        ordering = ('order',)
post_save.connect(auto_inc_order_trigger, sender=ExtraExpenseType)


class ExtraExpenses(Serialization):
    _cache_saved = True
    _lbl_field = 'name'
    _study_path = ('extra_expenses',)
    study = models.ForeignKey(Study, related_name='extra_expenses')
    name = models.CharField(_(u"Name"), max_length=100)
    full_eco = models.BooleanField(_(u"Available on full eco"), default=True)
    associated_type = models.ForeignKey(
        ExtraExpenseType, related_name='extra_expenses', blank=True, null=True)


class ExtraExpenseYear(Serialization):
    _cache_saved = True
    _study_path = ('extra_expenses', 'year')
    extra_expense = models.ForeignKey(ExtraExpenses, related_name='year')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    expense = models.IntegerField(_(u"Extra expense"), default=0)

    class Meta:
        unique_together = ('extra_expense', 'year',)

    def get_study_pk(self):
        return self.extra_expense.study.pk

    def get_parent_id(self):
        return self.extra_expense.pk


class ExtraSupplyPrice(Serialization):
    _cache_saved = True
    _lbl_field = 'extra_supply'
    _study_path = ('extra_supply_prices',)
    study = models.ForeignKey(Study, related_name='extra_supply_prices')
    extra_supply = models.ForeignKey(ExtraSupply, related_name='price')

    class Meta:
        verbose_name = _(u"Crop selling price")
        verbose_name_plural = _(u"Crop selling prices")


class ExtraSupplyPriceYear(Serialization):
    _cache_saved = True
    _study_path = ('extra_supply_prices', 'years')
    extra_supply_price = models.ForeignKey(ExtraSupplyPrice,
                                           related_name='years')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    price = models.FloatField(_(u"Price"), default=0)
    total = models.FloatField(_(u"Price"), default=0)

    class Meta:
        verbose_name = _(u"Extra supply price - year")
        verbose_name_plural = _(u"Extra supply prices - years")

    def get_study_pk(self):
        return self.extra_supply_price.study.pk

    def get_parent_id(self):
        return self.extra_supply_price.pk


class Investment(Serialization):
    _cache_saved = True
    _study_path = ('investments',)
    study = models.ForeignKey(Study, related_name='investments')
    name = models.CharField(_(u"Name"), max_length=50)
    date = models.DateField(_(u"Date"))
    amount = models.IntegerField(_(u"Amount"))
    duration = models.IntegerField(_(u"Duration (years)"))
    residual_value = models.IntegerField(
        _(u"Residual value after depreciation"))
    rate = models.FloatField(_(u"Loan rate"))
    loan_duration = models.IntegerField(_(u"Loan duration (years)"))

    @property
    def annual_depreciation(self):
        if not self.duration:
            return 0
        return int(round(
            (self.amount - self.residual_value) / float(self.duration)))

    def get_depreciation_by_year(self, year):
        if year > (self.date.year + self.duration):
            return 0
        nb_day = 365
        if year == self.date.year:
            nb_day = self.date.timetuple().tm_yday - 1
        if year == (self.date.year + self.duration):
            nb_day = 366 - self.date.timetuple().tm_yday
        return int(round(self.annual_depreciation * nb_day / 365.0))

    @property
    def monthly_payment(self):
        t = self.rate / 100.0
        if not self.rate:
            res = self.amount * (t + 1) / (12.0 * self.loan_duration)
        else:
            nm = self.loan_duration * 12
            res = self.amount * t / 12.0
            res = res / (1 - (1 + t / 12) ** -nm)
        return res

    def get_month_cost(self, monthly_payment, date, end, payed=0,
                       current_year_payed=0, cost=0, current_year_cost=0):
        if date == datetime.date(end.year - 1, 1, 1):
            current_year_cost = 0
            current_year_payed = 0
        to_be_payed = self.amount - payed
        monthly_interest = to_be_payed * (self.rate / 100.0) / 12
        cost += monthly_interest
        current_year_cost += monthly_payment
        payed += monthly_payment - monthly_interest
        current_year_payed += monthly_payment - monthly_interest
        if monthly_payment <= monthly_interest:
            return 0, 0  # can't be payed
        date = datetime.date(date.year, date.month, 1) + \
            relativedelta(months=1)
        if payed >= self.amount or date >= end:
            return current_year_cost, current_year_payed
        return self.get_month_cost(
            monthly_payment, date, end, payed,
            current_year_payed, cost, current_year_cost)

    def yearly_financial_cost(self, year):
        if year > (self.date.year + self.loan_duration):
            return 0, 0
        monthly_payment = self.monthly_payment
        return self.get_month_cost(monthly_payment, self.date,
                                   datetime.date(year + 1, 1, 1))


class Workshop(GeneralType):
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)
    study = models.ForeignKey(Study, related_name='workshop', blank=True,
                              null=True)

    class Meta:
        verbose_name = _(u"Workshop")
        verbose_name_plural = _(u"Workshops")

    def get_absolute_url(self):
        return reverse('simulabio:economy-full-workshop',
                       kwargs={'pk': self.study.pk, 'workshop': self.txt_idx})

    def save(self, *args, **kwargs):
        super(Workshop, self).save(*args, **kwargs)
        # fix recopy from another study
        if not self.txt_idx.startswith(unicode(self.study.pk)):
            idx = 0
            txt_idx = unicode(self.study.pk) + u"-" + \
                slugify(self.name) + u"-" + unicode(idx)
            while Workshop.objects.filter(txt_idx=txt_idx).count():
                idx += 1
                txt_idx = u"-".join(txt_idx.split('-')[:-1]) + u"-" \
                          + unicode(idx)
            if txt_idx != self.txt_idx:
                self.txt_idx = txt_idx
                self.save()


class WorkshopExpenses(GeneralType):
    __classname__ = "WorkshopExpenses"
    workshop = models.ForeignKey(Workshop, related_name='expenses')
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)

    class Meta:
        verbose_name = _(u"Workshop expense")
        verbose_name_plural = _(u"Workshop expenses")


class WorkshopExpensesYear(Serialization):
    _cache_saved = True
    _study_path = ('economic_synthesis',)
    study = models.ForeignKey(Study, related_name='workshop_expenses')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    workshop = models.ForeignKey(WorkshopExpenses,
                                 related_name='expenses_details')
    quantity = models.FloatField(_(u"Quantity"), default=0)
    price = models.FloatField(_(u"Price"), default=0)

    class Meta:
        unique_together = ('study', 'year', 'workshop')

    @property
    def total(self):
        return self.quantity * self.price


class WorkshopRevenues(GeneralType):
    __classname__ = "WorkshopRevenues"
    workshop = models.ForeignKey(Workshop, related_name='revenues')
    txt_idx = models.CharField(_(u"Textual id"), max_length=150)


class WorkshopRevenuesYear(Serialization):
    _cache_saved = True
    _study_path = ('economic_synthesis',)
    study = models.ForeignKey(Study, related_name='workshop_revenues')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    workshop = models.ForeignKey(WorkshopRevenues,
                                 related_name='revenues_details')
    quantity = models.FloatField(_(u"Quantity"), default=0)
    price = models.FloatField(_(u"Price"), default=0)

    class Meta:
        unique_together = ('study', 'year', 'workshop')

    @property
    def total(self):
        return self.quantity * self.price


class EconomicSynthesis(Serialization):
    _cache_saved = True
    _study_path = ('economic_synthesis',)
    study = models.ForeignKey(Study, related_name='economic_synthesis')
    year = models.CharField(_(u"Year (D, 1, 2, 3, ..., P)"), max_length='2',
                            choices=YEARS)
    man_work_unit = models.IntegerField(_(u"Man work unit"), default=1)
    # simple
    total_revenue = models.IntegerField(_(u"Total revenue"), default=0)
    total_expenses = models.IntegerField(_(u"Total expenses"), default=0)
    gop = models.IntegerField(_(u"GOP"), default=0)
    # full
    full_total_revenue = models.IntegerField(_(u"Total revenues"), default=0)
    vegetal_revenues = models.IntegerField(_(u"Vegetal revenues"), default=0)
    bovine_workshop_revenue = models.IntegerField(
        _(u"Bovine workshop revenues"), default=0)
    extra_revenues = models.IntegerField(_(u"Extra revenues"), default=0)

    operational_expenses = models.IntegerField(_(u"Operational expenses"),
                                               default=0)
    vegetal_expenses = models.IntegerField(_(u"Vegetal expenses"), default=0)
    bovine_workshop_expenses = models.IntegerField(
        _(u"Bovine workshop expenses"), default=0)
    extra_expenses = models.IntegerField(_(u"Fixed expenses"), default=0)
    full_gop = models.IntegerField(_(u"GOP"), default=0)

    # simple/full
    depreciation = models.IntegerField(_(u"Depreciation"), default=0)
    financial_expenses = models.IntegerField(_(u"Financial expenses"),
                                             default=0)
    # simple
    current_result = models.IntegerField(_(u"Current result"), default=0)
    # full
    full_current_result = models.IntegerField(_(u"Current result"), default=0)

    # simple/full
    capital_loans = models.IntegerField(_(u"Loans: capital"), default=0)
    interest_loans = models.IntegerField(_(u"Loans: interest"), default=0)
    total_annuity = models.IntegerField(_(u"Loans: total annuity"), default=0)

    # simple
    final_disposable_income = models.IntegerField(
        _(u"Total disposable income"), default=0)
    final_disposable_income_by_man_work_unit = models.IntegerField(
        _(u"Total disposable income by man work unit"), default=0)
    gop_by_revenue = models.IntegerField(_(u"GOP by revenue (%)"), default=0)

    # full
    full_final_disposable_income = models.IntegerField(
        _(u"Total disposable income"), default=0)
    full_final_disposable_income_by_man_work_unit = models.IntegerField(
        _(u"Total disposable income by man work unit"), default=0)
    full_gop_by_revenue = models.IntegerField(_(u"GOP by revenue (%)"),
                                              default=0)

    @classmethod
    def calculate(cls, study, eco_synth):
        values = {}
        if eco_synth['year'] == 'D':
            values['total_revenue'] = study['total_product']
            values['total_expenses'] = study['operational_charges'] + \
                study['structural_charges']
        else:
            year_n_1 = ''
            year = unicode(eco_synth['year'])
            if year == '1':
                year_n_1 = 'D'
            if year == 'P':
                year_n_1 = study['transition_years']
                if study['transition_years'] == 0:
                    year_n_1 = 'D'
            else:
                year_n_1 = int(year) - 1
                if year_n_1 < 1:
                    return
            year_n_1 = str(year_n_1)
            last_synth = None
            for es in study['economic_synthesis']:
                if unicode(es['year']) == year_n_1:
                    last_synth = es
                    break
            else:
                last_synth = {
                    'study_id': study['id'], 'year': year_n_1,
                    'man_work_unit': 1, 'total_revenue': 0,
                    'total_expenses': 0, 'gop': 0, 'depreciation': 0,
                    'financial_expenses': 0, 'current_result': 0,
                    'capital_loans': 0, 'interest_loans': 0,
                    'total_annuity': 0, 'final_disposable_income': 0,
                    'final_disposable_income_by_man_work_unit': 0,
                    'gop_by_revenue': 0, 'MODEL': 'EconomicSynthesis'}
                study['economic_synthesis'].append(last_synth)
            last_expenses, expenses = 0, 0
            for expense in study['expenses']:
                if unicode(expense['year']) == year:
                    expenses = expense['total_expenses']
                if unicode(expense['year']) == year_n_1:
                    last_expenses = expense['total_expenses']
            values['total_expenses'] = \
                last_synth['total_expenses'] - last_expenses + expenses
            last_revenues, revenues = 0, 0
            for revenue in study['revenues']:
                if unicode(revenue['year']) == year:
                    revenues = revenue['total_revenue']
                if unicode(revenue['year']) == year_n_1:
                    last_revenues = revenue['total_revenue']
            values['total_revenue'] = \
                last_synth['total_revenue'] - last_revenues + revenues
        values['gop'] = values['total_revenue'] - values['total_expenses']
        # values['final_disposable_income_by_man_work_unit'] = 0
        # if eco_synth['man_work_unit']:
        #     values['final_disposable_income_by_man_work_unit'] = int(
        #             float(final_disposable_income)/self.man_work_unit)
        # values['final_disposable_income_by_man_work_unit'] = int(
        #             float(final_disposable_income)/self.man_work_unit)
        # values['gop_by_revenue'] = int(float(values['gop'])/
        #    values['total_revenue']) if values['total_revenue'] else 0
        changed = False
        for k in values:
            if values[k] != eco_synth[k]:
                eco_synth[k] = values[k]
                changed = True
        return changed

    def calculate_db(self):
        values = {}
        year = unicode(self.year)
        values['financial_expenses'] = self.study.interests
        values['depreciation'] = self.study.depreciation
        values['capital_loans'] = self.study.share_capital
        if year == 'D':
            values['total_revenue'] = self.study.total_product
            values['total_expenses'] = self.study.operational_charges +\
                self.study.structural_charges
        else:
            year_n_1 = ''
            if year == '1':
                year_n_1 = 'D'
            elif year == 'P':
                year_n_1 = self.study.transition_years
                if self.study.transition_years == 0:
                    year_n_1 = 'D'
            else:
                year_n_1 = int(year) - 1
                if year_n_1 < 1:
                    return
                year_n_1 = str(year_n_1)
            last_synth, created = EconomicSynthesis.objects.get_or_create(
                study=self.study, year=year_n_1)
            last_expenses = 0
            q = Expenses.objects.filter(year=year_n_1, study=self.study)
            if q.count():
                last_expenses = q.all()[0].total_expenses_without_extra
            expenses = 0
            q = Expenses.objects.filter(year=self.year, study=self.study)
            if q.count():
                expenses = q.all()[0].total_expenses
            values['total_expenses'] = last_synth.total_expenses \
                - last_expenses + expenses

            last_revenue = 0
            q = Revenues.objects.filter(year=year_n_1, study=self.study)
            if q.count():
                last_revenue = q.all()[0].total_revenue

            revenue = 0
            q = Revenues.objects.filter(year=self.year, study=self.study)
            if q.count():
                revenue = q.all()[0].total_revenue
            q_ery = ExtraRevenueYear.objects\
                .filter(year=self.year, extra_revenue__study=self.study)\
                .exclude(extra_revenue__full_eco=True).all()
            for er in q_ery:
                revenue += er.revenue
            values['total_revenue'] = last_synth.total_revenue \
                - last_revenue + revenue
            c_year = self.study.year
            if year == 'P':
                c_year += self.study.transition_years + 1
            else:
                c_year += int(year)
            for investment in Investment.objects.filter(
                    study=self.study).all():
                values['depreciation'] += \
                    investment.get_depreciation_by_year(c_year)
                financial_total_expenses, payed = \
                    investment.yearly_financial_cost(c_year)
                values['financial_expenses'] += financial_total_expenses \
                    - payed
                values['capital_loans'] += payed

        values['gop'] = values['total_revenue'] - values['total_expenses']
        values['current_result'] = \
            values['gop'] - values['depreciation'] \
            - values['financial_expenses']
        values['interest_loans'] = values['financial_expenses']
        values['total_annuity'] = \
            values['interest_loans'] + values['capital_loans']
        values['final_disposable_income'] = \
            values['gop'] - values['financial_expenses'] \
            - values['capital_loans']
        values['final_disposable_income_by_man_work_unit'] = 0
        if self.man_work_unit:
            values['final_disposable_income_by_man_work_unit'] = \
                int(round(float(values['final_disposable_income'])
                    / self.man_work_unit))
        values['gop_by_revenue'] = 0
        if values['total_revenue']:
            values['gop_by_revenue'] = \
                int(round(100 * float(values['gop'])
                    / values['total_revenue']))
        changed = bool([setattr(self, k, values[k]) or k
                        for k in values if values[k] != getattr(self, k)])
        if changed:
            self.save()
        self.calculate_full_db()

    def calculate_full_db(self):
        values = {}
        values['vegetal_revenues'] = 0
        for hss in HarvestSettingsSoldYear.objects.filter(
                year=self.year, harvest_sold__harvest__study=self.study).all():
            values['vegetal_revenues'] += hss.total

        values['extra_revenues'] = 0
        for er in ExtraRevenueYear.objects\
                                  .filter(year=self.year,
                                          extra_revenue__full_eco=True,
                                          extra_revenue__study=self.study)\
                                  .all():
            values['extra_revenues'] += er.revenue

        values['bovine_workshop_revenue'] = 0
        try:
            values['bovine_workshop_revenue'] += Revenues.objects.get(
                study=self.study, year=self.year).total_revenue
        except Revenues.DoesNotExist:
            pass

        q = WorkshopRevenuesYear.objects\
                                .filter(year=self.year, study=self.study)\
                                .exclude(workshop__txt_idx='bovin-lait')
        other_workshop_revenues = sum([revenue.total for revenue in q])

        values['full_total_revenue'] = values['vegetal_revenues'] + \
            values['extra_revenues'] + values['bovine_workshop_revenue'] +\
            other_workshop_revenues

        values['vegetal_expenses'] = 0
        q_hse = HarvestSettingsExpenseYear.objects.filter(
            year=self.year, harvest_expense__harvest__study=self.study).all()
        for hse in q_hse:
            hse.calculate()
            values['vegetal_expenses'] += hse.total

        values['bovine_workshop_expenses'] = 0
        try:
            expense = Expenses.objects.get(study=self.study, year=self.year)
            values['bovine_workshop_expenses'] = expense.heifer_expenses +\
                expense.calve_expenses
            q_es = ExtraSupplyPriceYear.objects.filter(
                year=self.year, extra_supply_price__study=self.study).all()
            for extra_supply in q_es:
                values['bovine_workshop_expenses'] += extra_supply.total
            q_wey = WorkshopExpensesYear.objects.filter(
                year=self.year, study=self.study,
                workshop__workshop__txt_idx='bovin-lait').all()
            for expense in q_wey:
                values['bovine_workshop_expenses'] += expense.total
        except Expenses.DoesNotExist:
            pass

        q = WorkshopExpensesYear.objects\
            .filter(year=self.year, study=self.study)\
            .exclude(workshop__workshop__txt_idx='bovin-lait')
        other_workshop_expenses = sum([expense.total for expense in q])

        values['operational_expenses'] = values['bovine_workshop_expenses'] +\
            values['vegetal_expenses'] + other_workshop_expenses
        values['extra_expenses'] = 0
        q_eey = ExtraExpenseYear.objects.filter(
            extra_expense__full_eco=True, year=self.year,
            extra_expense__study=self.study).all()
        for expense in q_eey:
            values['extra_expenses'] += expense.expense

        values['full_gop'] = values['full_total_revenue'] \
            - values['operational_expenses'] - values['extra_expenses']

        values['full_current_result'] = values['full_gop'] \
            - self.depreciation - self.financial_expenses

        values['full_final_disposable_income'] = values['full_gop'] \
            - self.financial_expenses - self.capital_loans
        values['full_final_disposable_income_by_man_work_unit'] = 0
        if self.man_work_unit:
            values['full_final_disposable_income_by_man_work_unit'] = \
                int(round(float(values['full_final_disposable_income'])
                          / self.man_work_unit))
        values['full_gop_by_revenue'] = 0
        if values['full_total_revenue']:
            values['full_gop_by_revenue'] = int(round(
                100 * float(values['full_gop'])
                / values['full_total_revenue']))
        changed = bool([setattr(self, k, values[k]) or k
                        for k in values if values[k] != getattr(self, k)])
        if changed:
            self.save()
