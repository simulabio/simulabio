jQuery(document).ready(function(){
    if (typeof bsmRemoveLabel == 'undefined'){
        bsmRemoveLabel = 'X';
        bsmAddLabel = 'Add';
    }
    jQuery(".bsm").bsmSelect({
        addItemTarget: 'bottom',
        animate: true,
        highlight: true,
        cloneOption: true,
        removeLabel: bsmRemoveLabel,
        title: bsmAddLabel,
        plugins: [jQuery.bsmSelect.plugins.sortable()]
      });
    for(var key in bsm_value){
        var div_parent = jQuery("select[name='"+key+"']").filter('.bsm').parent();
        div_parent.find('.bsmList .bsmListItemRemove').click();
        for (var idx=0;idx<bsm_value[key].length;idx++){
            var opt = div_parent.find('.bsmSelect option[value="'+bsm_value[key][idx]+'"]');
            opt.attr('selected', 'selected');
            div_parent.find('.bsmSelect').change();
        }
    }
});
