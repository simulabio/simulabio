# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FeedTemplateDetail'
        db.create_table('simulabio_feedtemplatedetail', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('month', self.gf('django.db.models.fields.IntegerField')()),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('feed_template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.FeedTemplate'])),
            ('feed_item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.FeedItem'])),
        ))
        db.send_create_signal('simulabio', ['FeedTemplateDetail'])

        # Adding model 'FeedTemplate'
        db.create_table('simulabio_feedtemplate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('simulabio', ['FeedTemplate'])

        # Adding model 'FeedItem'
        db.create_table('simulabio_feeditem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
        ))
        db.send_create_signal('simulabio', ['FeedItem'])

        # Adding model 'Herd'
        db.create_table('simulabio_herd', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('study', self.gf('django.db.models.fields.related.ForeignKey')(related_name='herd', to=orm['simulabio.Study'])),
            ('calving_age', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('simulabio', ['Herd'])

        # Adding model 'HerdDetail'
        db.create_table('simulabio_herddetail', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('herd', self.gf('django.db.models.fields.related.ForeignKey')(related_name='herd_detail', to=orm['simulabio.Herd'])),
            ('diagnostic', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('reform_rate', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('turnover', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('dairy_cow', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('heifer_2', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('heifer_1', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('heifer_0', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('other_animal', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('reformed_cow_sold', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('heifer_2_sold', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('heifer_1_sold', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('calves_sold', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('cow_heifer_2_purchased', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('heifer_1_purchased', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('calves_purchased', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('simulabio', ['HerdDetail'])


    def backwards(self, orm):
        # Deleting model 'FeedTemplateDetail'
        db.delete_table('simulabio_feedtemplatedetail')

        # Deleting model 'FeedTemplate'
        db.delete_table('simulabio_feedtemplate')

        # Deleting model 'FeedItem'
        db.delete_table('simulabio_feeditem')

        # Deleting model 'Herd'
        db.delete_table('simulabio_herd')

        # Deleting model 'HerdDetail'
        db.delete_table('simulabio_herddetail')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'simulabio.farm': {
            'Meta': {'object_name': 'Farm'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'corporate_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'geographical_zone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.GeographicalZone']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'simulabio.feeditem': {
            'Meta': {'object_name': 'FeedItem'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.feedtemplate': {
            'Meta': {'object_name': 'FeedTemplate'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.feedtemplatedetail': {
            'Meta': {'object_name': 'FeedTemplateDetail'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'feed_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.FeedItem']"}),
            'feed_template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.FeedTemplate']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month': ('django.db.models.fields.IntegerField', [], {})
        },
        'simulabio.geographicalzone': {
            'Meta': {'object_name': 'GeographicalZone'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvest': {
            'Meta': {'object_name': 'Harvest'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvestcategory': {
            'Meta': {'object_name': 'HarvestCategory'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvestplots': {
            'Meta': {'object_name': 'HarvestPlots'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'cow_access': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'geocode': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'geographical_area': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plot_n'", 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n1'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n2'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'project': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'remark': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'rotation_dominant_diag': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plots_diag'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['simulabio.RotationDominant']"}),
            'rotation_dominant_project': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plots_project'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['simulabio.RotationDominant']"}),
            'soil_type': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plots'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.harvestsettings': {
            'Meta': {'ordering': "('order',)", 'object_name': 'HarvestSettings'},
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_settings'", 'to': "orm['simulabio.Study']"}),
            'surface_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'surface_total': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.herd': {
            'Meta': {'object_name': 'Herd'},
            'calving_age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.herddetail': {
            'Meta': {'object_name': 'HerdDetail'},
            'calves_purchased': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'calves_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cow_heifer_2_purchased': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'dairy_cow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'heifer_0': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_1_purchased': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_1_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_2_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'herd': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd_detail'", 'to': "orm['simulabio.Herd']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'other_animal': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'reform_rate': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'reformed_cow_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'turnover': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.referenceharvest': {
            'Meta': {'object_name': 'ReferenceHarvest'},
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'geographical_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reference_harvest'", 'to': "orm['simulabio.GeographicalZone']"}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.rotationdominant': {
            'Meta': {'object_name': 'RotationDominant'},
            'avg_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'cow_access': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'elementary_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'max_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'min_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'parcel_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rotation_dominant_template': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant'", 'to': "orm['simulabio.RotationDominantTemplate']"}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant'", 'to': "orm['simulabio.Study']"}),
            'surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.rotationdominantharvest': {
            'Meta': {'ordering': "('order',)", 'object_name': 'RotationDominantHarvest'},
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'rotation_dominant_template': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant_harvest'", 'to': "orm['simulabio.RotationDominantTemplate']"})
        },
        'simulabio.rotationdominanttemplate': {
            'Meta': {'object_name': 'RotationDominantTemplate'},
            'geographical_zone': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['simulabio.GeographicalZone']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'remark': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'})
        },
        'simulabio.simulabiouser': {
            'Meta': {'object_name': 'SimulabioUser'},
            'farms': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'simulabio_user'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['simulabio.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simulabio_user'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'simulabio.study': {
            'Meta': {'object_name': 'Study'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'study'", 'to': "orm['simulabio.Farm']"}),
            'forage_yield_project_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'forage_yield_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'grain_yield_project_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'grain_yield_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'straw_yield_project_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'straw_yield_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['simulabio']