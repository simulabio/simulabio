#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2013 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

from django.conf import settings


def simulabio(request):
    dct = {}
    for k in (
            'JQUERY_URL', 'JQUERY_UI_URL', 'JQUERY_UI_CSS', 'STATIC_URL',
            'APP_NAME', 'LANGUAGE', 'EXCANVAS_JS', 'JQGRID_JS', 'JQGRID_CSS',
            'JQPLOT_JS', 'JQPLOT_CSS', 'JQUERY_SPINNER_URL',
            'JQUERY_SPINNER_CSS', 'CONTACT_EMAIL'):
        dct[k] = getattr(settings, k)
    return dct
