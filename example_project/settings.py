# Django settings for simulabio project.

import os

DEBUG = False
INTERNAL_CACHE = False

ROOT_PATH = os.path.dirname(os.path.realpath(__file__ )) + os.path.sep

ENCODING = 'windows-1252'
CSV_DELIMITER = ";"

EAT_CELERY = True
# only useful when Celery is active
BROKER_URL = "django://"
#BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_IMPORTS = ("simulabio.tasks", )
CELERY_RESULT_BACKEND = "database"
CELERY_RESULT_DBURI = "sqlite:///" + ROOT_PATH + "celery.db"

CACHES = {
    'default': {
    'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
    'LOCATION': '127.0.0.1:11211',
    }
}

CACHE_TIMEOUT = 3600

LOCK_TIMEOUT = 4*60 # 4 minuts

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ROOT_PATH + 'media'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ROOT_PATH + 'static'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'
LOCALE = 'fr_FR.utf8'
LANGUAGES = ['fr']

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = []

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'simulabio.middleware.RequestCacheMiddleware',
    'simulabio.middleware.RestrictUserSessionMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "simulabio.context_processors.simulabio",
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
)

INTERNAL_IPS = ('127.0.0.1', 'localhost')

ROOT_URLCONF = 'urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'simulabio.wsgi.application'

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'extra_views',
    'registration',
    'south',
    'oook_replace',
    'simulabio'
]

CSV_DELIMITER = ","
CSV_QUOTECHAR = '"'

OOO_DATE_FORMAT = u"%-d %B %Y"
PAGE_DIM = (21, 29.7) # A4

CONTACT_EMAIL = 'simulabio.info@agrobioconseil.com'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        # Log to a text file that can be rotated by logrotate
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/django/simulabio.log'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

DEFAULT_STRAW_USAGE = {
    #animal : daily amount, stall days jan., stall days feb., etc.
    'dc':(7, 31, 28, 15, 0, 0, 0, 0, 0, 15, 20, 30, 31),
    'h2':(6, 31, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 31),
    'h1':(4, 31, 28, 31, 30, 0, 0, 0, 0, 0, 15, 30, 31),
    'h0':(3, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31),
}

EXTRA_INSTALLED_APPS = []

try:
    from local_settings import *
except ImportError, e:
    print 'Unable to load local_settings.py:', e

for idx, app in EXTRA_INSTALLED_APPS:
    INSTALLED_APPS.insert(idx, app)

ROOT_PATH = ROOT_PATH if ROOT_PATH.endswith('/') else ROOT_PATH + '/'

LANGUAGE = LANGUAGE_CODE.split('-')[0]

if EAT_CELERY:
    import djcelery
    djcelery.setup_loader()
    INSTALLED_APPS.insert(INSTALLED_APPS.index('simulabio')-1,
                          'djcelery')
    INSTALLED_APPS.insert(INSTALLED_APPS.index('simulabio')-1,
                          'kombu.transport.django')

if 'LOGGING_FILENAME' in dir():
    LOGGING['handlers']['logfile']['filename'] = LOGGING_FILENAME

if 'JQUERY_URL' not in dir():
    JQUERY_URL = STATIC_URL + "javascript/jquery/jquery.js"
if 'JQUERY_UI_URL' not in dir():
    JQUERY_UI_URL = STATIC_URL + "javascript/jquery-ui/"
if 'JQUERY_UI_CSS' not in dir():
    JQUERY_UI_CSS = STATIC_URL + "javascript/jquery-ui/css/smoothness/jquery-ui.css"
if 'JQUERY_SPINNER_URL' not in dir():
    JQUERY_SPINNER_URL = STATIC_URL + "jquery-ui-spinner/ui.spinner.min.js"
if 'JQUERY_SPINNER_CSS' not in dir():
    JQUERY_SPINNER_CSS = STATIC_URL + "jquery-ui-spinner/ui.spinner.css"
if 'JQGRID_JS' not in dir():
    JQGRID_JS = [STATIC_URL + "jquery-localisation/js/jquery.localisation.min.js",
                 STATIC_URL + "jqgrid/js/i18n/grid.locale-fr.js",
                 STATIC_URL + "jqgrid/plugins/ui.multiselect.js",
                 STATIC_URL + "jqgrid/js/jquery.jqGrid.min.js"]
if 'JQGRID_CSS' not in dir():
    JQGRID_CSS = [STATIC_URL + "jqgrid/css/ui.jqgrid.css",
                  STATIC_URL + "jqgrid/plugins/ui.multiselect.css"]
if 'JQPLOT_JS' not in dir():
    JQPLOT_JS = [STATIC_URL + "jqplot/jquery.jqplot.min.js",
                 STATIC_URL + "jqplot/plugins/jqplot.canvasTextRenderer.min.js",
                 STATIC_URL + "jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js",
                 STATIC_URL + "jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js",
                 STATIC_URL + "jqplot/plugins/jqplot.categoryAxisRenderer.min.js",
                 STATIC_URL + "jqplot/plugins/jqplot.highlighter.min.js",
                ]
if 'JQPLOT_CSS' not in dir():
    JQPLOT_CSS = [STATIC_URL + "jqplot/jquery.jqplot.min.css"]
if 'EXCANVAS_JS' not in dir():
    EXCANVAS_JS = STATIC_URL + "jqplot/excanvas.min.js"
