#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import template
from django.utils.translation import ugettext as _

register = template.Library()

@register.inclusion_tag('simulabio/blocks/inline_formset.html')
def inline_formset(caption, formset, header=True, skip=False,
                   nb_col_hasheader=False, table_hidden=False):
    """
    Render a formset as an inline table.
    For i18n of the caption be carreful to add manualy the caption label to
    the translated fields
    """
    return {'caption':caption, 'formset':formset, 'header':header, 'skip':skip,
            'nb_col_hasheader':nb_col_hasheader, 'table_hidden':table_hidden}


@register.inclusion_tag('simulabio/blocks/inline_formset.json')
def json_inline_formset(caption, formset, header=True, skip=False,
                   nb_col_hasheader=False, table_hidden=False):
    return {'caption':caption, 'formset':formset, 'header':header, 'skip':skip,
            'nb_col_hasheader':nb_col_hasheader, 'table_hidden':table_hidden}

