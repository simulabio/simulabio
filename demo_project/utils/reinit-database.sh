#!/bin/bash

DEMO_DB=simulabiodemo
DEMO_INSTANCE=demo-nginx.conf
TMP_INSTANCE=demo-tmp-nginx.conf
SUPERVISOR_JOB=celery-demo
DEMO_UWSGI=demo-uwsgi.ini

DEMO_BACKUP=/srv/simulabiodemo/reference.sql

NGINX_SITE_ENABLED=/etc/nginx/sites-enabled/
NGINX_SITE_AVAILABLE=/etc/nginx/sites-available/
UWSGI_ENABLED=/etc/uwsgi/apps-enabled/
UWSGI_AVAILABLE=/etc/uwsgi/apps-available/

rm $NGINX_SITE_ENABLED$DEMO_INSTANCE
ln -s $NGINX_SITE_AVAILABLE$TMP_INSTANCE $NGINX_SITE_ENABLED
service nginx reload

rm $UWSGI_ENABLED$DEMO_UWSGI
service uwsgi reload

supervisorctl stop $SUPERVISOR_JOB
su postgres -c 'dropdb '$DEMO_DB 2> /dev/null > /dev/null
su postgres -c 'createdb --echo --owner '$DEMO_DB' --encoding UNICODE '$DEMO_DB 2> /dev/null > /dev/null
su postgres -c 'psql -f '$DEMO_BACKUP' '$DEMO_DB 2> /dev/null > /dev/null
supervisorctl start $SUPERVISOR_JOB

ln -s $UWSGI_AVAILABLE$DEMO_UWSGI $UWSGI_ENABLED
service uwsgi reload

rm $NGINX_SITE_ENABLED$TMP_INSTANCE
ln -s $NGINX_SITE_AVAILABLE$DEMO_INSTANCE $NGINX_SITE_ENABLED
service nginx reload
