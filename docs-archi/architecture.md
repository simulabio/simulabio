# Architecture de l'application Simulabio #

- version 2017-01-22


## Bibliothèques / technologies utilisées ##

Le programme est développé essentiellement en Python / Django / Javascript.

Tous les fichiers sources suivent les recommandations de la PEP8.

L'application est compatible Python 2.7.8.

La version utilisée de Django est la 1.4.

Les dépendances Python sont listées dans le fichier requirements.txt.

Sous forme d'arborescence, cela se présente ainsi :

```
├── Django
│   ├── psycopg2 # connecteur PostgreSQL
│   └── south # gestion des migrations
├── django-extra-views # jeu supplémentaire de vues fondées sur les classes
├── django-registration # application de gestion de l'authentification
├── Celery # démon de gestion des tâches
│   └── django-celery # extension django
├── python-memcached # bibliothèque d'interfaçage avec le serveur de cache memcached
├── numpy # bibliothèque scientifique
├── matplotlib # bibliothèque de génération de graphes
├── reportlab / pisa # génération de pdf
└── OOoPy # bibliothèque de manipulation de fichier OpenDocument
```

Une bibliothèque externe a été développée pour pouvoir générer des fichiers OpenDocument depuis des templates OpenDocument : oook_replace.
Celle-ci a besoin des dépendances suivantes :

```
├── Pillow # bibliothèque de manipulation d'images
├── lxml # bibliothèque de traitement XML
└── beautifulsoup4 # bibliothèque de manipulation de fichier HTML / XML
```

Un projet simulabio-data a été créé pour séparer le code source (disponible sous licence libre) des données (données non-libres, propriété d'Agrobio Conseil).
Ce projet contient des « fixtures » en json contenant les données d'initialisation de la base de données ainsi que des templates OpenDocument pour la génération des rapports.


## Détail de l'arborescence du code source ##

```
├── confs # exemples de fichiers de configuration serveur
├── demo_project  # projet spécifique permettant de mettre à disposition un mode
│                 # de démonstration bridé
├── example_project  # projet par défaut servant de patron pour les instances
├── extra  # patch de reportlab pour gérer les tables trop grandes
├── Makefile.example
├── simulabio
│   ├── admin.py
│   ├── compute.py  # gestion des grandes étapes des calculs
│   ├── context_processors.py  # dictionnaire de base pour alimenter les
│   │                          # templates django
│   ├── data_importer.py  # bibliothèque d'import de données CSV -
│   │                     # utilisé en particulier pour l'import des parcelles
│   ├── forms.py  # formulaires de l'application
│   ├── imports.py  # fonction d'import des rations
│   ├── management
│   │   └── commands
│   │       ├── clean_db.py  # ancien script de maintenance
│   │       └── import_feedtemplates.py  # import des rations
│   ├── middleware.py  # middleware de gestion de cache « thread safe » - restriction
│   │                  # à une seule session par utilisateur
│   ├── migrations
│   ├── migration_tools.py  # utilitaires pour les migrations
│   ├── models.py  # modèles en base de données mais aussi gestion de cache
│   │              # et portion de calculs
│   ├── oook_translation.py  # chaines de traduction pour les clés des patrons
│   │                        # libreoffice
│   ├── static
│   │   ├── bsmSelect  # sélecteur multiple
│   │   ├── javascript
│   │   │   ├── jquery
│   │   │   └── jquery-ui
│   │   ├── jqgrid  # génération de tableaux en javascript
│   │   ├── jqplot  # génération de graphes, de camemberts, ... en javascript
│   │   ├── jquery-ui-spinner  # champ numérique
│   │   └── simulabio  # les fichiers et répertoires "*-new" concernent le thème
│   │       │          # créé par Mathieu
│   │       ├── css
│   │       ├── fonts
│   │       ├── img
│   │       ├── img-new
│   │       └── js
│   │           ├── animals-feed.js  # gestion de l'interface rations
│   │           ├── harvest-plots.js  # gestion de l'interface cultures
│   │           ├── harvest-rotations.js  # gestion de l'interface rotations
│   │           ├── interface.js  # interface générale et utilitaires
│   │           ├── menu-sort.js  # composant pour réagencer les menus
│   │           ├── rotation-widget.js  # composant pour la gestion des rotations
│   │           └── table-autocomplete.js  # gestion de la complétion automatique
│   │                                      # des cases dans les formulaires type table
│   ├── tasks.py  # gestionnaire de tâches
│   ├── templates
│   │   ├── 404.html
│   │   ├── 500.html
│   │   ├── base.html
│   │   ├── blocks
│   │   │   └── sql_debug.html  # block de debug SQL
│   │   ├── has_active_session.html
│   │   ├── registration  # patrons pour la gestion des comptes
│   │   ├── simulabio  # au vu de la spécificité de chaque page, un template
│   │   │   │          # dédié a été créé
│   │   │   ├── blocks  # composants
│   │   │   ├── files  # patrons pour export CSV
│   │   │   ├── print  # patrons pour les données d'impression (format JSON)
│   │   └── widgets
│   │       └── jquerydate.html  # template pour le sélecteur de date
│   ├── templatetags
│   │   ├── inline_formset.py  # rendu d'un "formset" en table
│   │   ├── range.py  # filtres pour obtenir des listes
│   │   ├── simulabio_tags.py  # filtres et balises spécifiques à Simulabio
│   │   └── table_field.py  # rendu d'un champ de formulaire en ligne de tableau
│   ├── tests  # jeux de données pour les tests
│   ├── tests.py
│   ├── unicode_csv_reader.py  # lecteur de CSV en unicode
│   ├── urls.py
│   ├── views.py
│   ├── widgets.py
│   └── wsgi.py
└── simulabio-data  # jeu de données Agrobio Conseil (non-libre)
    ├── fixtures
    └── templates  # patrons pour la génération de documents
```


## Table de correspondance : Adresse / Classe / Patron ##

* id ferme, id étude, id parcelle, id rotation, id dom. rot. (dominante rotationnelle), id patron, id mois : numérique
* id verrou : lettres majuscules et « - »
* id animal, état, id culture, id atelier: lettres minuscules, chiffres et « - »
* id étape : « D » (diagnostic) ou numérique (année de transition « n ») ou « P » (projet)

|  Adresse                                                    | Classe                                    | Patron
| ----------------------------------------------------------- | ----------------------------------------- | -----------------------------------
| `welcome/`                                                  | `WelcomeView`                               | `simulabio/welcome.html`
| `farms/`                                                    | `FarmList`                                  | `simulabio/farm_list.html`
| `farm/add/`                                                 | `FarmCreate`                                | `simulabio/farm_form.html`
| `farm/{id ferme}/`                                          | `DetailView`                                | `simulabio/farm_detail.html`
| `farm/{id ferme}/update/`                                   | `FarmUpdate`                                | `simulabio/farm_form.html`
| `farm/{id ferme}/delete/`                                   | `FarmDelete`                                | `simulabio/farm_delete.html`
| `farm/{id ferme}/study/create/`                             | `StudyCreate`                               | `simulabio/study_form.html`
| `farm/{id ferme}/study/update/`                             | `StudyUpdate`                               | `simulabio/study_form.html`
| `study/{id étude}/has-lock/{id verrou}/`                    | `HasLockView`                               | `- (renvoie « true » ou « false »)`
| `study/{id étude}/generic/{id verrou}/`                     | `GenericFormView`                           | `simulabio/generic_form_view.html`
| `study/{id étude}/report-generation/`                       | `ReportGenerationView`                      | `simulabio/study_report_gen.html`
| `study/{id étude}/harvest-settings/`                        | `HarvestSettingsView`                       | `simulabio/harvest_settings.html`
| `study/{id étude}/harvest-plot/`                            | `HarvestPlotsView`                          | `simulabio/harvest_plots.html`
| `.../harvest-plot/{id parcelle}/modify/`                    | `HarvestPlotsView`                          | `simulabio/harvest_plots.html`
| `.../harvest-plot/{id parcelle}/delete/`                    | `HarvestPlotsDeleteView`                    | `simulabio/harvest_plots_delete.html`
| `study/{id étude}/harvest-rotationdominant/`                | `HarvestRotationDominantView`               | `simulabio/harvest_rotationdominant.html`
| `.../harvest-rotationdominant/{id étape}/`                  | `HarvestRotationDominantView`               | `simulabio/harvest_rotationdominant.html`
| `.../{id rotation}/`                                        | `HarvestRotationDominantView`               | `simulabio/harvest_rotationdominant.html`
| `.../{id rotation}/delete/`                                 | `HarvestRotationDominantDeleteView`         | `- (suppression puis redirection)`
| `.../{id rotation}/{id dom. rot.}/delete/`                  | `HarvestRotationDominantDeleteAttachedView` | `- (suppression puis redirection)`
| `study/{id étude}/harvest-plots-full/`                      | `HarvestFullPlotsView`                      | `simulabio/harvest_full_plots.html`
| `study/{id étude}/harvest-production/`                      | `HarvestProductionView`                     | `simulabio/harvest_prod_list.html`
| `study/{id étude}/harvest-indicators/`                      | `HarvestIndicatorsView`                     | `simulabio/harvest_indicators.html`
| `study/{id étude}/animals-herd/`                            | `AnimalsHerdView`                           | `simulabio/animals_herd_detail.html`
| `.../animals-herd/{id étape}/`                              | `AnimalsHerdView`                           | `simulabio/animals_herd_detail.html`
| `study/{id étude}/animals-feed-season/{id animal}/`         | `AnimalsFeedSeasonView`                     | `simulabio/animals_feed_season.html`
| `.../animals-feed-season/{id animal}/{id étape}/`           | `AnimalsFeedSeasonView`                     | `simulabio/animals_feed_season.html`
| `study/{id étude}/animals-feed/{id animal}/{id étape}/`     | `AnimalsFeedView`                           | `simulabio/animals_feed.html`
| `.../animals-feed/{id animal}/{id étape}/{id patron}/`      | `AnimalsFeedView`                           | `simulabio/animals_feed.html`
| `study/{id étude}/animals-feed-custom/{id animal}/`         | `AnimalsCustomFeedView`                     | `simulabio/animals_feed.html`
| `.../animals-feed-custom/{id animal}/{id étape}/`           | `AnimalsCustomFeedView`                     | `simulabio/animals_feed.html`
| `.../animals-feed-update/{id animal}/{id étape}/{id mois}/` | `AnimalsFeedUpdateView`                     | `simulabio/animals_feed_update.html`
| `study/{id étude}/animals-litter/{id animal}/`              | `AnimalsLitterView`                         | `simulabio/animals_litter.html`
| `.../animals-litter/{id animal}/{id étape}/`                | `AnimalsLitterView`                         | `simulabio/animals_litter.html`
| `study/{id étude}/animals-primary-balance/`                 | `AnimalsPrimaryBalanceView`                 | `simulabio/animals_primary_balance_list.html`
| `.../animals-primary-balance/{id étape}/`                   | `AnimalsPrimaryBalanceView`                 | `simulabio/animals_primary_balance_list.html`
| `study/{id étude}/milk-production/`                         | `MilkProductionView`                        | `simulabio/animals_milk_production.html`
| `study/{id étude}/balance-fodder/`                          | `BalanceFodderView`                         | `simulabio/balance_fodder.html`
| `.../balance-fodder/{id étape}/`                            | `BalanceFodderView`                         | `simulabio/balance_fodder.html`
| `.../balance-fodder/{id étape}/{id catégorie culture}/`     | `BalanceFodderView`                         | `simulabio/balance_fodder.html`
| `study/{id étude}/economy-settings/`                        | `EconomySettingsView`                       | `simulabio/economy_settings.html`
| `study/{id étude}/simple-economy/initial-state/`            | `SimpleEcoInitialView`                      | `simulabio/economy_simple_initial.html`
| `.../simple-economy/revenues/`                              | `SimpleEcoRevenuesView`                     | `simulabio/economy_simple_revenues.html`
| `.../simple-economy/expenses/`                              | `SimpleEcoExpensesView`                     | `simulabio/economy_simple_expenses.html`
| `.../simple-economy/investments/`                           | `SimpleEcoInvestmentsView`                  | `simulabio/economy_simple_investments.html`
| `.../simple-economy/synthesis/`                             | `SimpleEcoSynthesisView`                    | `simulabio/economy_simple_synthesis.html`
| `study/{id étude}/full-economy/workshop/cultures/`          | `FullEcoHarvestWorkshopView`                | `simulabio/economy_full_harvest_workshop.html`
| `.../full-economy/workshop/cultures/{état}/`                | `FullEcoHarvestWorkshopView`                | `simulabio/economy_full_harvest_workshop.html`
| `.../full-economy/harvest-expenses/`                        | `FullEcoHarvestSettingsExpenseView`         | `simulabio/economy_full_simple_harvestexpense.html`
| `.../full-economy/harvest-expenses/{id culture}/`           | `FullEcoHarvestSettingsExpenseView`         | `simulabio/economy_full_simple_harvestexpense.html`
| `.../full-economy/workshop/new/`                            | `FullEcoNewWorkshopView`                    | `simulabio/economy_full_newworkshop.html`
| `.../full-economy/workshop/bovin-lait/`                     | `FullEcoMilkWorkshopView`                   | `simulabio/economy_full_milkworkshop.html`
| `.../full-economy/workshop/{id atelier}/`                   | `FullEcoWorkshopView`                       | `simulabio/economy_full_workshop.html`
| `.../full-economy/workshop/{id atelier}/{état}/`            | `FullEcoWorkshopView`                       | `simulabio/economy_full_workshop.html`
| `.../full-economy/extra-revenues/`                          | `FullEcoRevenuesView`                       | `simulabio/economy_full_revenues.html`
| `.../full-economy/expenses/`                                | `FullEcoExpensesView`                       | `simulabio/economy_full_expenses.html`
| `.../full-economy/investments/`                             | `FullEcoInvestmentsView`                    | `simulabio/economy_full_investments.html`
| `.../full-economy/synthesis/`                               | `FullEcoSynthesisView`                      | `simulabio/economy_full_synthesis.html`
| `study/{id étude}/report/`                                  | `ReportView`                                | `simulabio/print/main.html`


## Modèles ##

Les modèles sont définis dans le fichier `models.py`.

Une classe mère `Cached` a été définie pour faciliter la gestion du cache avec memcached. Elle est essentiellement utilisées pour les types génériques.
Une classe mère `Serialization` a été définie pour stocker des éléments de calcul intermédiaire dans le cache. Cette mécanique s'avérant à la fois complexe et trop fragile et ne donnant pas de bénéfice sensible en terme de performance par rapport à une gestion en mémoire Python, elle est maintenant dépréciée. Elle n'est plus utilisée que marginalement.

Des modèles génériques sont définis afin de permettre pas mal de configuration en base de données. Cette configuration est accessible depuis l'interface d'administration de Django.

Les différents modèles disponibles sont :

* GeographicalZone - zone géographique d'une instance de Simulabio ;
* GenericForm - paramétrage de formulaire générique (ces formulaires sont des informations génériques surtout utilisées pour la génération du document final) ;
* GenericField - paramétrage des champs du formulaire ;
* ReportTarget - paramétrage de rapport : tables à intégrer au rendu avec ordre et titre ;
* ReportTemplate - association de patrons OpenDocument à des cibles de rendu ;
* GrassGrowth - paramétrage de la pousse de l'herbe ;
* HarvestCategory - catégories de récoltes (maïs, herbe, ...) ;
* Harvest - récoltes disponibles (maïs, sorgho, pp, ...) ;
* ReferenceHarvest - données de rendement de référence pour les cultures ;
* UsageType - types d'utilisation des récoltes (pâture, ensilage, ...) ;
* HarvestCategoryUsageType - liens entre catégories de cultures et types d'usage ;
* HarvestExpenseType - types de dépenses liées aux cultures (semences, travail du sol, récolte, ...) ;
* RotationDominantTemplate - patrons de dominante rotationnelle ;
* RotationDominantTemplateHarvest - association de culture / dominante rotationnelle ;
* FeedItem - types de rations ;
* FeedTemplate, FeedTemplateDetail - patrons de rations ;
* ExtraSupply - types d'achats supplémentaires pour l'alimentation (foin, déshydraté, paille, ...) ;
* ExtraRevenueType, ExtraRevenue - type d'autres revenus (subventions, ...) ;
* ExtraExpenseType, ExtraExpenses - type d'autres dépenses (carburant, foncier, main d'œuvre, ...) ;
* WorkshopExpenses - type de dépenses pour les ateliers ;
* WorkshopRevenues - type de ressources pour les ateliers.


Les autres tables concernent le cœur de l'application Simulabio (fermes, études, cultures, ...) :

* SimulabioUser - utilisateur de Simulabio ;
* Farm - fermes ;
* Study - étude ;
* StudyField - valeurs des champs des formulaires génériques ;
* StudyLock - verrous pour les calculs en cours ;
* StudyComment - commentaires attachés à l'étude ;
* PastureAvailable - pâturages disponibles ;
* TransitionType - paramétrage des types de paramètres à prendre en compte pour les années de transition (diagnostic, projet ou moyenne diagnostic-projet) ;
* HarvestSettings - rendements cultures pour une étude ;
* HarvestSettingsYear - rendements annuels des cultures ;
* HarvestCategoryStudy - rendements moyens en fourrage, grain, paille pour l'étude ;
* HarvestSettingsSold, HarvestSettingsSoldYear - ventes de cultures ;
* HarvestSettingsExpense, HarvestSettingsExpenseYear - dépenses liées aux cultures ;
* HarvestPlots - parcelles ;
* HarvestTransition - association cultures / parcelles pour les années de transition ;
* RotationDominant - dominantes rotationnelles ;
* Herd, HerdDetail - troupeaux ;
* MilkProduction - production laitière ;
* Feed, FeedDetail - rations ;
* StrawUsage - utilisation de la paille ;
* FeedNeed - besoins en ration ;
* FeedNeedSupply - apport aux rations ;
* FeedNeedExtraSupply - apport en rations supplémentaires ;
* SimpleEcoInitialExpenseDetail - dépenses - économie simplifiée ;
* Revenues - revenus de l'atelier bovin lait ;
* Expenses - dépenses de l'atelier bovin lait ;
* ExtraExpenseYear - détail des dépenses supplémentaires par année ;
* ExtraSupplyPrice, ExtraSupplyPriceYear - détail des coûts des apports aux rations ;
* Investment - investissements ;
* Workshop - ateliers supplémentaires ;
* WorkshopExpensesYear - détails des dépenses sur les ateliers supplémentaires ;
* WorkshopRevenuesYear - détails des revenus sur les ateliers supplémentaires ;
* EconomicSynthesis - synthèse économique.

## Vues ##

Outre les classiques vues de présentation, édition et suppression (cf. table de correspondance Adresse / Classes), les vues suivantes sont exposées :

* une vue d'export en CSV (CSVFile) ;
* des vues, *mixins* permettant la gestion de formulaires multiples imbriqués (ExtraYearFormConf, ExtraYearFormsView, ExtraFormsMixin, ExtraYearsExtraFormsMixin, ExtraInlineFormSetView, InlineFormSetView) ;
* un *mixin* de gestion des formulaires de commentaire sur chaque page (CommentMixin) ;
* des *mixins* de gestion des « impressions » de chaque page (PrintableMixin, YearPrintableMixin, AnimalYearPrintableMixin, HarvestYearPrintableMixin) - ces « impressions » sont ensuite utilisées pour la génération de PDF ;
* un *mixin* pour la gestion génériques des vues de ferme (FarmCheckMixin) ;
* un *mixin* pour la gestion des verrous (HasLockView) ;
* un *mixin* pour les formulaires de cultures (HarvestModificationMixin) ;
* un *mixin* pour les formulaires de rotations de cultures (HarvestRotationDominantMixin) ;
* un *mixin* pour les formulaires d'économie simplifiée (SimpleEcoMixin) ;
* un *mixin* pour les formulaires d'économie complète (FullEcoMixin) ;
* un *mixin* pour les formulaires concernant les ateliers (WorkshopMixin).


## Calculs ##

Les calculs sont déportés sur le serveur de tâches Celery. Lorsqu'un calcul est en cours, des verrous sont mis en place pour éviter d'afficher des calculs erronés et éventuellement différer de nouveaux calculs dépendants de calculs non finalisés.

Les calculs sont lancés au moment de la soumission de formulaires (cf. le détail d'implémentation des formulaires et des vues) ou au moment de l'enregistrement des objets.
À terme, en vue d'uniformisation, il pourrait être pertinent de verser les calculs des formulaires vers les vues (ou inversement selon la pertinence).
Le déclenchement des calculs au moment de l'enregistrement des objets permet de gérer facilement les calculs en cascade mais peut nuire à la lisibilité et nécessite une certaine vigilance pour éviter des calculs cycliques.

Le détail des calculs apparaît au niveau des modèles (`models.py`) si ces calculs ont un périmètre se limitant essentiellement à un objet précis (par exemple : le calcul du coût d'un prêt est calculé en ne prenant en compte que l'objet « Investissement » concerné). Dans le cas de calculs plus globaux (exemple : calcul du besoin fourrager), le détail du calcul apparaît au niveau du fichier `compute.py` quitte à ce que ces calculs plus globaux fassent appel aux algorithmes présents dans les modèles.

En terme d'implémentation, il existe actuellement deux types d'appels pour certains calculs des méthodes utilisant la sérialisation (par exemple `calculate_economic_synthesis`) et des méthodes écrivant directement les résultats en base (`calculate_db_economic_synthesis`). Les implémentations utilisant la sérialisation sont les implémentations historiques et ont été laissées là dans un but documentaire. Pour éviter toute ambiguïté, il serait nécessaire que ces implémentations soient retirées.



## Déploiement ##

En dehors des bibliothèques Python, les services suivants sont nécessaires au bon fonctionnement du logiciel :

- un serveur de cache memcached ;
- un serveur web (apache ou nginx) ;
- un serveur python (mod_python ou uwsgi) ;
- un serveur de base de données PostgreSQL ;
- un serveur de courriel pour l'envoi de notifications ;
- un serveur de tâche Celery.


Un script d'installation (`install.sh`) ainsi que des patrons de fichiers de configuration sont disponibles dans le répertoire `confs`.
