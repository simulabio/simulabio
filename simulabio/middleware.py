#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks@proxience.com>

from threading import currentThread

from django.core.cache.backends.locmem import LocMemCache
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

_request_cache = {}
_installed_middleware = False


def get_request_cache():
    assert _installed_middleware, 'RequestCacheMiddleware not loaded'
    return _request_cache[currentThread()]

# LocMemCache is a threadsafe local memory cache


class RequestCache(LocMemCache):
    def __init__(self):
        name = 'locmemcache@%i' % hash(currentThread())
        params = dict()
        super(RequestCache, self).__init__(name, params)


class RequestCacheMiddleware(object):
    def __init__(self):
        global _installed_middleware
        _installed_middleware = True

    def process_request(self, request):
        cache = _request_cache.get(currentThread()) or RequestCache()
        _request_cache[currentThread()] = cache
        cache.clear()


class RestrictUserSessionMiddleware(object):
    """
    Restrict a user to one active session
    """
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.user or not hasattr(request.user, 'simulabio_user') \
           or not request.user.simulabio_user.count():
            return
        simulabio_user = request.user.simulabio_user.all()[0]
        remote_addr = '0.0.0.0'
        if "REMOTE_ADDR" in request.META:
            remote_addr = request.META['REMOTE_ADDR']
        user_agent = 'empty-user-agent'
        if "HTTP_USER_AGENT" in request.META:
            # 4ko is enough
            user_agent = request.META['HTTP_USER_AGENT'][:4096]
        if not getattr(view_func, 'restricted_page', True) \
           or view_func.__name__ == 'logout':
            return
        if not simulabio_user.current_ip or not simulabio_user.current_agent:
            simulabio_user.current_ip = remote_addr
            simulabio_user.current_agent = user_agent
            simulabio_user.current_session_id = request.session.session_key
            simulabio_user.save()
            return
        if simulabio_user.current_ip != remote_addr or \
           simulabio_user.current_agent != user_agent:
            return HttpResponseRedirect(
                reverse('simulabio:has-active-session'))
