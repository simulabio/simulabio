# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SimulabioUser'
        db.create_table('simulabio_simulabiouser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='simulabio_user', unique=True, to=orm['auth.User'])),
        ))
        db.send_create_signal('simulabio', ['SimulabioUser'])

        # Adding M2M table for field farms on 'SimulabioUser'
        db.create_table('simulabio_simulabiouser_farms', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('simulabiouser', models.ForeignKey(orm['simulabio.simulabiouser'], null=False)),
            ('farm', models.ForeignKey(orm['simulabio.farm'], null=False))
        ))
        db.create_unique('simulabio_simulabiouser_farms', ['simulabiouser_id', 'farm_id'])

        # Adding model 'Farm'
        db.create_table('simulabio_farm', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('corporate_name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('postal_code', self.gf('django.db.models.fields.CharField')(max_length=6, null=True, blank=True)),
            ('town', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal('simulabio', ['Farm'])

        # Adding model 'Study'
        db.create_table('simulabio_study', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('farm', self.gf('django.db.models.fields.related.ForeignKey')(related_name='study', to=orm['simulabio.Farm'])),
            ('year', self.gf('django.db.models.fields.IntegerField')()),
            ('is_diagnostic', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('simulabio', ['Study'])

        # Adding model 'HarvestCategory'
        db.create_table('simulabio_harvestcategory', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('color', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('simulabio', ['HarvestCategory'])

        # Adding model 'Harvest'
        db.create_table('simulabio_harvest', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.HarvestCategory'])),
            ('forage_yield', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('grain_yield', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('straw_yield', self.gf('django.db.models.fields.FloatField')(default=0)),
        ))
        db.send_create_signal('simulabio', ['Harvest'])

        # Adding model 'HarvestSettings'
        db.create_table('simulabio_harvestsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('study', self.gf('django.db.models.fields.related.ForeignKey')(related_name='harvest_settings', to=orm['simulabio.Study'])),
            ('harvest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.Harvest'])),
            ('forage_yield', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('forage_yield_project', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('grain_yield', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('grain_yield_project', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('straw_yield', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('straw_yield_project', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal('simulabio', ['HarvestSettings'])

        # Adding model 'HarvestPlots'
        db.create_table('simulabio_harvestplots', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('study', self.gf('django.db.models.fields.related.ForeignKey')(related_name='harvest_plots', to=orm['simulabio.Study'])),
            ('geocode', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('area', self.gf('django.db.models.fields.FloatField')()),
            ('geographical_area', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('soil_type', self.gf('django.db.models.fields.CharField')(max_length=2, null=True, blank=True)),
            ('vehicule_access', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('remark', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('rotation_block', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('harvest_n2', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='harvest_plot_n2', null=True, to=orm['simulabio.HarvestSettings'])),
            ('harvest_n1', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='harvest_plot_n1', null=True, to=orm['simulabio.HarvestSettings'])),
            ('harvest', self.gf('django.db.models.fields.related.ForeignKey')(related_name='harvest_plot_n', to=orm['simulabio.HarvestSettings'])),
        ))
        db.send_create_signal('simulabio', ['HarvestPlots'])


    def backwards(self, orm):
        # Deleting model 'SimulabioUser'
        db.delete_table('simulabio_simulabiouser')

        # Removing M2M table for field farms on 'SimulabioUser'
        db.delete_table('simulabio_simulabiouser_farms')

        # Deleting model 'Farm'
        db.delete_table('simulabio_farm')

        # Deleting model 'Study'
        db.delete_table('simulabio_study')

        # Deleting model 'HarvestCategory'
        db.delete_table('simulabio_harvestcategory')

        # Deleting model 'Harvest'
        db.delete_table('simulabio_harvest')

        # Deleting model 'HarvestSettings'
        db.delete_table('simulabio_harvestsettings')

        # Deleting model 'HarvestPlots'
        db.delete_table('simulabio_harvestplots')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'simulabio.farm': {
            'Meta': {'object_name': 'Farm'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'corporate_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'simulabio.harvest': {
            'Meta': {'object_name': 'Harvest'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategory']"}),
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.harvestcategory': {
            'Meta': {'object_name': 'HarvestCategory'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvestplots': {
            'Meta': {'object_name': 'HarvestPlots'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'geocode': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'geographical_area': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plot_n'", 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n1'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n2'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'remark': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'rotation_block': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'soil_type': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plots'", 'to': "orm['simulabio.Study']"}),
            'vehicule_access': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'simulabio.harvestsettings': {
            'Meta': {'ordering': "('order',)", 'object_name': 'HarvestSettings'},
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_settings'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.simulabiouser': {
            'Meta': {'object_name': 'SimulabioUser'},
            'farms': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'simulabio_user'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['simulabio.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simulabio_user'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'simulabio.study': {
            'Meta': {'object_name': 'Study'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'study'", 'to': "orm['simulabio.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['simulabio']