#!/bin/sh

# Récupérer le dossier parent en suivant le lien symbolique si c’en est un
CUR_DIR="$(readlink -f $(dirname "$0"))"
cd $CUR_DIR
echo $CUR_DIR

######################################################################
# Minimum configuration                                              #
######################################################################

# only lower alpha-numeric char and '_' and must start with a letter
[ $APP_NAME ] || ( echo 'APP_NAME empty. Default to: "default".' ; );
APP_NAME=${APP_NAME-default}

[ $URL ] || ( echo 'URL empty. Default to: "localhost".' ; );
URL=${URL-localhost}

PROJECT_NAME="Simulabio - $APP_NAME"

[ $ADMIN_NAME ] || ( echo 'ADMIN_NAME empty. Default to: "default".' ; );
ADMIN_NAME=${ADMIN_NAME-default}

[ $ADMIN_EMAIL ] || ( echo 'ADMIN_EMAIL empty. Default to: "webmaster@localhost".' ; );
ADMIN_EMAIL=${ADMIN_EMAIL-webmaster@localhost}

######################################################################
# Advanced configuration                                             #
######################################################################

LOG_PATH="/var/log/django/"

# default to www-data - Debian default
WWW_USER=www-data

PROJECT_TEMPLATE='example_project'

# webserver port - default "[::]:80"
NGINX_PORT="80"
NGINX_PORT_V6="[::]:80"
PROTOCOL="http"

# replace by 'https' for secured version
# TODO: create nginx template for https only http is managed

DB_PASSWORD='' # if not set automatically generated
DB_HOST='127.0.0.1' # put 127.0.0.1 for localhost and automatic db creation
DB_PORT='5432'
DB_NAME='simulabio'$APP_NAME

# default for debian
UWSGI_AVAILABLE_PATH='/etc/uwsgi/apps-available/'
UWSGI_ENABLE_PATH='/etc/uwsgi/apps-enabled/'
NGINX_AVAILABLE_PATH='/etc/nginx/sites-available/'
NGINX_ENABLE_PATH='/etc/nginx/sites-enabled/'
SUPERVISOR_PATH='/etc/supervisor/conf.d/'

# don't edit bellow this line
######################################################################

MAIN_PATH=`readlink -f ..`'/'
APP_PATH=$MAIN_PATH$APP_NAME'/'
CONF_PATH=$MAIN_PATH"confs/"$APP_NAME"/"
FULL_URL=$PROTOCOL'://'$URL
DATE=`date +%F-%H%M%S`
SECRET_KEY=`apg -a 0 -M ncl -n 6 -x 10 -m 40 |head -n 1`

mkdir $CONF_PATH

echo "* create database and user database"
if [ -z "$DB_PASSWORD" ]
then
    DB_PASSWORD=`apg -a 0 -M ncl -n 6 -x 10 -m 10 |head -n 1`
fi

if [ $DB_HOST = '127.0.0.1' ]
then

    DB_PASSWORD=$DB_PASSWORD DB_NAME=$DB_NAME PROJECT_NAME=$PROJECT_NAME su postgres <<'EOF'
    cd

    if [ `psql -l | grep $DB_NAME | wc -l` -ne 1 ]; then
        createuser --echo --adduser --createdb --encrypted $DB_NAME 2> /dev/null > /dev/null
        psql --command "ALTER USER "$DB_NAME" with password '"$DB_PASSWORD"';" 2> /dev/null > /dev/null
        createdb --echo --owner $DB_NAME --encoding UNICODE $DB_NAME "$PROJECT_NAME" 2> /dev/null > /dev/null

    fi
EOF

fi


echo '* load configuration'

sed -s "s|#APP#|$APP_NAME|g;\
        s|#ADMIN_NAME#|$ADMIN_NAME|g;\
        s|#ADMIN_EMAIL#|$ADMIN_EMAIL|g;\
        s|#DATE#|$DATE|g;\
        s|#URL#|$FULL_URL|g;\
        s|#PROJECT_NAME#|$PROJECT_NAME|g;\
        s|#LOG_PATH#|$LOG_PATH|g;\
        s|#APP_DIR#|$APP_PATH|g;\
        s|#SECRET_KEY#|$SECRET_KEY|g;\
        s|#DB_HOST#|$DB_HOST|g;\
        s|#DB_NAME#|$DB_NAME|g;\
        s|#DB_PORT#|$DB_PORT|g;\
        s|#DB_PASSWORD#|$DB_PASSWORD|g;" 'local_settings.py.template' > \
                                         $CONF_PATH'local_settings.py'


cp -ra $MAIN_PATH$PROJECT_TEMPLATE $APP_PATH
rm $APP_PATH'settings.py'
ln -s $MAIN_PATH$PROJECT_TEMPLATE'/settings.py' $APP_PATH
ln -s $CONF_PATH'local_settings.py' $APP_PATH'local_settings.py'

mkdir -p $APP_PATH'/media'
mkdir $APP_PATH'/media/attached' $APP_PATH'/media/feed' $APP_PATH'/media/feeds'\
      $APP_PATH'/media/rotations' $APP_PATH'/media/upload'

if [ ! -d $LOG_PATH ] ; then
    mkdir -p $LOG_PATH
fi

sed -s "s|#APP#|$APP_NAME|g;" 'Makefile.template' > \
                                $CONF_PATH'Makefile'

echo '* supervisor configuration'
cd $CUR_DIR

sed -s "s|#APP#|$APP_NAME|g;\
        s|#APP_PATH#|$APP_PATH|g;\
        s|#WWW_USER#|$WWW_USER|g;\
        s|#LOG_PATH#|$LOG_PATH|g;" 'supervisor.conf.template' > \
                                   $CONF_PATH'supervisor.conf'
ln -s $CONF_PATH'supervisor.conf' $SUPERVISOR_PATH$APP_NAME'.conf'

echo '* uwsgi configuration'
cd $CUR_DIR

sed -s "s|#APP_PATH#|$APP_PATH|g;\
        s|#APP#|$APP_NAME|g;\
        s|#MAIN_PATH#|$MAIN_PATH|g;" 'wsgi.py.template' > \
                                     $CONF_PATH'wsgi.py'

ln -s $CONF_PATH'wsgi.py' $APP_PATH

sed -s "s|#APP#|$APP_NAME|g;\
        s|#MAIN_PATH#|$MAIN_PATH|g;\
        s|#LOG_PATH#|$LOG_PATH|g;\
        s|#UWSGI_PORT#|$UWSGI_PORT|g;\
        s|#URL#|$URL|g;" 'uwsgi-app.ini.template' > \
                                $CONF_PATH'uwsgi.ini'

ln -s "${CONF_PATH}uwsgi.ini" \
      "$UWSGI_AVAILABLE_PATH$APP_NAME.ini"
ln -s "$UWSGI_AVAILABLE_PATH$APP_NAME.ini" \
      "$UWSGI_ENABLE_PATH$APP_NAME.ini"


echo '* nginx configuration'
cd $CUR_DIR

sed -s "s|#APP#|$APP_NAME|g;\
        s|#NGINX_PORT#|$NGINX_PORT|g;\
        s|#NGINX_PORT_V6#|$NGINX_PORT_V6|g;\
        s|#MAIN_PATH#|$MAIN_PATH|g;\
        s|#LOG_PATH#|$LOG_PATH|g;\
        s|#DATE#|$DATE|g;\
        s|#UWSGI_PORT#|$UWSGI_PORT|g;\
        s|#URL#|$URL|g;" 'nginx-'$PROTOCOL'-app.conf.template' > \
                                $CONF_PATH'nginx.conf'

ln -s $CONF_PATH'nginx.conf' \
      $NGINX_AVAILABLE_PATH$APP_NAME'-nginx.conf'
ln -s $NGINX_AVAILABLE_PATH$APP_NAME'-nginx.conf' \
      $NGINX_ENABLE_PATH$APP_NAME'-nginx.conf'

echo "* sync database"
cd $MAIN_PATH
make --makefile=$CONF_PATH'Makefile' fresh_syncdb

echo "* collectstatic"
cd $MAIN_PATH
make --makefile=$CONF_PATH'Makefile' collectstatic

# reinit rights
chown -R $WWW_USER:$WWW_USER $LOG_PATH
chown -R $WWW_USER:$WWW_USER $APP_PATH'/media'

service supervisor restart
service uwsgi restart
service nginx restart
