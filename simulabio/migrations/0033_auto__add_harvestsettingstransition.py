# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HarvestSettingsTransition'
        db.create_table('simulabio_harvestsettingstransition', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('harvest', self.gf('django.db.models.fields.related.ForeignKey')(related_name='transition', to=orm['simulabio.HarvestSettings'])),
            ('year', self.gf('django.db.models.fields.IntegerField')()),
            ('surface_total', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('forage_yield_total', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('forage_used', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('grain_yield_total', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('grain_used', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('straw_yield_total', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('straw_used', self.gf('django.db.models.fields.FloatField')(default=0)),
        ))
        db.send_create_signal('simulabio', ['HarvestSettingsTransition'])


    def backwards(self, orm):
        # Deleting model 'HarvestSettingsTransition'
        db.delete_table('simulabio_harvestsettingstransition')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'simulabio.extrasupply': {
            'Meta': {'ordering': "('order',)", 'object_name': 'ExtraSupply'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.farm': {
            'Meta': {'object_name': 'Farm'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'corporate_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'geographical_zone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.GeographicalZone']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'simulabio.feed': {
            'Meta': {'object_name': 'Feed'},
            'animal': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'feed_template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.FeedTemplate']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feed'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.feeddetail': {
            'Meta': {'object_name': 'FeedDetail'},
            'amount_1': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_10': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_11': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_12': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_2': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_3': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_4': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_5': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_6': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_7': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_8': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_9': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'feed': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feed_details'", 'to': "orm['simulabio.Feed']"}),
            'feed_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feed_details'", 'to': "orm['simulabio.FeedItem']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'simulabio.feeditem': {
            'Meta': {'object_name': 'FeedItem'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'harvest_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategory']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'usage_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.UsageType']", 'null': 'True', 'blank': 'True'})
        },
        'simulabio.feedneed': {
            'Meta': {'object_name': 'FeedNeed'},
            'amount': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'associated_surface': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'balance': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'harvest_category_usage': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategoryUsageType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feed_need'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.feedneedextrasupply': {
            'Meta': {'object_name': 'FeedNeedExtraSupply'},
            'extra_supply': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra_supply'", 'to': "orm['simulabio.ExtraSupply']"}),
            'feed_need': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra_supply'", 'to': "orm['simulabio.FeedNeed']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'used': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.feedneedsupply': {
            'Meta': {'object_name': 'FeedNeedSupply'},
            'feed_need': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'supply'", 'to': "orm['simulabio.FeedNeed']"}),
            'harvest_settings': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'supply'", 'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'used': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.feedtemplate': {
            'Meta': {'object_name': 'FeedTemplate'},
            'default_for_diag': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'farming_type': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.feedtemplatedetail': {
            'Meta': {'object_name': 'FeedTemplateDetail'},
            'amount_1': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_10': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_11': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_12': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_2': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_3': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_4': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_5': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_6': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_7': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_8': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'amount_9': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'feed_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.FeedItem']"}),
            'feed_template': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feed_template_details'", 'to': "orm['simulabio.FeedTemplate']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'simulabio.geographicalzone': {
            'Meta': {'object_name': 'GeographicalZone'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvest': {
            'Meta': {'object_name': 'Harvest'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvests'", 'to': "orm['simulabio.HarvestCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.harvestcategory': {
            'Meta': {'object_name': 'HarvestCategory'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvestcategorystudy': {
            'Meta': {'object_name': 'HarvestCategoryStudy'},
            'area': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'area_needed': ('django.db.models.fields.FloatField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'average_forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'average_grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'average_straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'harvest_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategory']"}),
            'harvest_category_usage_error': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategoryUsageType']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_categories'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.harvestcategoryusagetype': {
            'Meta': {'object_name': 'HarvestCategoryUsageType'},
            'diag_yield': ('django.db.models.fields.FloatField', [], {'default': '100'}),
            'harvest_category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'project_yield': ('django.db.models.fields.FloatField', [], {'default': '100'}),
            'usage_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.UsageType']"})
        },
        'simulabio.harvestplots': {
            'Meta': {'object_name': 'HarvestPlots'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'cow_access': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'geocode': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'geographical_area': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plot_n'", 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n1'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n2'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_transition': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'harvest_plot_transition'", 'to': "orm['simulabio.HarvestSettings']", 'through': "orm['simulabio.HarvestTransition']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'plot_group_number': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'project': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'remark': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'}),
            'rotation_dominant_diag': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plots_diag'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['simulabio.RotationDominant']"}),
            'rotation_dominant_project': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plots_project'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['simulabio.RotationDominant']"}),
            'soil_type': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plots'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.harvestsettings': {
            'Meta': {'ordering': "('harvest__order',)", 'object_name': 'HarvestSettings'},
            'forage_project_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_project_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_settings'", 'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'straw_project_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_settings'", 'to': "orm['simulabio.Study']"}),
            'surface_project_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'surface_total': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.harvestsettingstransition': {
            'Meta': {'object_name': 'HarvestSettingsTransition'},
            'forage_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transition'", 'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'straw_used': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'surface_total': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'simulabio.harvesttransition': {
            'Meta': {'object_name': 'HarvestTransition'},
            'harvest_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'plot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestPlots']"}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'simulabio.herd': {
            'Meta': {'object_name': 'Herd'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.herddetail': {
            'Meta': {'object_name': 'HerdDetail'},
            'calves_purchased': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'calves_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'calving_age': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cow_heifer_2_purchased': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'dairy_cow': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'heifer_0': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_1': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_1_purchased': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_1_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_2': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'heifer_2_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'herd': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'herd_detail'", 'to': "orm['simulabio.Herd']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'other_animal': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'reform_rate': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'reformed_cow_sold': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'turnover': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.referenceharvest': {
            'Meta': {'object_name': 'ReferenceHarvest'},
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'geographical_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reference_harvest'", 'to': "orm['simulabio.GeographicalZone']"}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.rotationdominant': {
            'Meta': {'object_name': 'RotationDominant'},
            'avg_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'cow_access': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'elementary_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'max_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'min_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parcel_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rotation_dominant_template': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant'", 'to': "orm['simulabio.RotationDominantTemplate']"}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant'", 'to': "orm['simulabio.Study']"}),
            'surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.rotationdominantharvest': {
            'Meta': {'ordering': "('order',)", 'object_name': 'RotationDominantHarvest'},
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'rotation_dominant': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant_harvest'", 'to': "orm['simulabio.RotationDominant']"})
        },
        'simulabio.rotationdominanttemplate': {
            'Meta': {'object_name': 'RotationDominantTemplate'},
            'geographical_zone': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['simulabio.GeographicalZone']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'remark': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'blank': 'True'})
        },
        'simulabio.rotationdominanttemplateharvest': {
            'Meta': {'ordering': "('order',)", 'object_name': 'RotationDominantTemplateHarvest'},
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'rotation_dominant_template': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant_harvest'", 'to': "orm['simulabio.RotationDominantTemplate']"})
        },
        'simulabio.simulabiouser': {
            'Meta': {'object_name': 'SimulabioUser'},
            'farms': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'simulabio_user'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['simulabio.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simulabio_user'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'simulabio.strawusage': {
            'Meta': {'object_name': 'StrawUsage'},
            'animal': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'daily_amount': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'herd_detail': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'straw_usage'", 'to': "orm['simulabio.HerdDetail']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'stall_days_1': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_10': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_11': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'stall_days_12': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_2': ('django.db.models.fields.IntegerField', [], {'default': '28'}),
            'stall_days_3': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_4': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'stall_days_5': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_6': ('django.db.models.fields.IntegerField', [], {'default': '30'}),
            'stall_days_7': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_8': ('django.db.models.fields.IntegerField', [], {'default': '31'}),
            'stall_days_9': ('django.db.models.fields.IntegerField', [], {'default': '30'})
        },
        'simulabio.study': {
            'Meta': {'object_name': 'Study'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'study'", 'to': "orm['simulabio.Farm']"}),
            'forage_yield_project_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'forage_yield_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'grain_yield_project_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'grain_yield_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'straw_yield_project_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'straw_yield_total': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'transition_years': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        },
        'simulabio.transitiontype': {
            'Meta': {'object_name': 'TransitionType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transition_type'", 'to': "orm['simulabio.Study']"}),
            'year': ('django.db.models.fields.IntegerField', [], {}),
            'yield_type': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        'simulabio.usagetype': {
            'Meta': {'object_name': 'UsageType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'order': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'target_type': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        }
    }

    complete_apps = ['simulabio']