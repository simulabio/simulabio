#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import random

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.test import TestCase

import simulabio
from simulabio import imports
from simulabio.compute import Compute
from simulabio.forms import StudyForm
from simulabio.models import Farm, Study, SimulabioUser, Harvest, \
    HarvestCategory, GeographicalZone, ReferenceHarvest, HarvestSettings, \
    HarvestPlots, RotationDominantTemplate, RotationDominantTemplateHarvest, \
    RotationDominant, RotationDominantHarvest
from simulabio import models


class SimulabioTestCase(TestCase):
    def identification(self, client=None):
        if not client:
            client = self.client
        self.user = User.objects.create_user('user', 'admin@test.com',
                                             'pass')
        SimulabioUser.objects.create(user=self.user)
        client.login(username='user', password='pass')
        return self.user

    def harvestcategories_create(self):
        self.harvest_categories = []
        self.harvest_categories.append(
            HarvestCategory.objects.create(name='cat 1', color='#f00'))
        self.harvest_categories.append(HarvestCategory.objects.create(
                                                  name='cat 2', color='#0f0'))
        return self.harvest_categories

    def harvests_create(self, harvest_categories=None):
        if not harvest_categories and (not hasattr(self, 'harvest_categories') \
                                       or not self.harvest_categories):
            harvest_categories = self.harvestcategories_create()
        self.harvests = []
        h, created = Harvest.objects.get_or_create(name='Harvest 1',
                                           category=harvest_categories[0])
        self.harvests.append(h)
        h, created = Harvest.objects.get_or_create(name='Harvest 2',
                                           category=harvest_categories[1])
        self.harvests.append(h)
        return self.harvests

    def farms_create(self, user=None, harvests=None):
        if not harvests and (not hasattr(self, 'harvests') \
                             or not self.harvests):
            harvests = self.harvests_create()
        if not user:
            if (not hasattr(self, 'user') or not self.user):
                self.identification()
            user = self.user
        geographical_zone = GeographicalZone.objects.create(
                                                name=u"Geographical zone n°1")
        reference_harvest = ReferenceHarvest.objects.create(harvest=harvests[0],
                             geographical_zone=geographical_zone,
                             forage_yield=3.5, grain_yield=42, straw_yield=421)
        self.farms = []
        values = {'corporate_name':u"Ferme n°1",
                  'address':u'3 rue des Rossignols perchés',
                  'postal_code':'35000',
                  'town':'Rennes',
                  'geographical_zone':geographical_zone
                  }
        self.farms.append(Farm.objects.create(**values))
        self.farms.append(Farm.objects.create(**values))
        sb_user = user.simulabio_user.all()[0]
        # don't attach the first farm to test associated farm permissions
        for farm in self.farms[1:]:
            sb_user.farms.add(farm)
        return self.farms

    def study_create(self, farms=None):
        if not farms and (not hasattr(self, 'farms') \
                             or not self.farms):
            farms = self.farms_create()
        self.studies = []
        for farm in farms:
            values = {'name':u'Study n°1', 'year':2012, 'farm':farm}
            self.studies.append(Study.objects.create(**values))
        return self.studies

    def referenceharvest_create(self):
        geographical_zone = models.GeographicalZone.objects.create(
                                            name='default', default=True)
        if not hasattr(self, 'harvests') or not self.harvests:
            harvests = self.harvests_create()
        for harvest in self.harvests:
            models.ReferenceHarvest.objects.create(harvest=harvest,
                geographical_zone=geographical_zone,
                forage_yield=random.randint(1, 5),
                grain_yield=random.randint(1, 5),
                straw_yield=random.randint(1, 5))

    def economic_create(self):
        if not hasattr(self, 'studies') or not self.studies:
            self.study_create()
        self.extra_expenses_types = []
        for expense in ('expense1', 'expense2', 'expense3'):
            e, c = models.ExtraExpenseType.objects.get_or_create(name=expense,
                                                              txt_idx=expense)
            self.extra_expenses_types.append(e)
        self.extra_revenues_types = []
        for revenue in ('revenue1', 'revenue2', 'revenue3'):
            e, c = models.ExtraRevenueType.objects.get_or_create(name=revenue,
                                                              txt_idx=revenue)
            self.extra_revenues_types.append(e)

    def harvestsettings_create(self, studies=None):
        if not studies and (not hasattr(self, 'studies') \
                             or not self.studies):
            studies = self.study_create()
        elif not studies:
            studies = self.studies
        self.harvest_settings = []

        for study in self.studies:
            for idx_harvest in xrange(len(self.harvests)):
                hs, created = HarvestSettings.objects.get_or_create(
                        study=study,
                        harvest=self.harvests[idx_harvest],
                        forage_yield=(idx_harvest)+1,
                        forage_yield_project=(idx_harvest)+2,
                        grain_yield=2*(idx_harvest)+1,
                        grain_yield_project=2*(idx_harvest)+2,
                        straw_yield=3*(idx_harvest)+1,
                        straw_yield_project=3*(idx_harvest)+2,
                        )
                if created:
                    self.harvest_settings.append(hs)
        return self.harvest_settings

    def harvestplots_create(self):
        if (not hasattr(self, 'harvest_settings') or not self.harvest_settings):
            self.harvestsettings_create()
        self.harvest_plots = []
        for study in self.studies:
            for idx_harvest in xrange(len(self.harvest_settings)):
                hp = HarvestPlots.objects.create(study=study, project=True,
                            name='Plot %d' % idx_harvest,
                            area=(idx_harvest+1)*idx_harvest,
                            harvest=self.harvest_settings[idx_harvest])
                self.harvest_plots.append(hp)

    def harvestrotationdominant_create(self):
        if (not hasattr(self, 'harvest_plots') or not self.harvest_plots):
            self.harvestplots_create()
        self.rotation_dominant_templates = []
        rdt, c = RotationDominantTemplate.objects.get_or_create(
                                        name='Rotation dominant 1')
        self.rotation_dominant_templates.append(rdt)
        rdt, c = RotationDominantTemplate.objects.get_or_create(
                                        name='Rotation dominant 2')
        self.rotation_dominant_templates.append(rdt)
        len_rdt = len(self.rotation_dominant_templates)
        for idx_harvest in xrange(len(self.harvests)):
            RotationDominantTemplateHarvest.objects.get_or_create(
                harvest=self.harvests[idx_harvest],
                order=idx_harvest/len_rdt,
                rotation_dominant_template=\
                        self.rotation_dominant_templates[idx_harvest % len_rdt])
        self.rotations_dominant = []
        for study in self.studies:
            old_len = len(self.rotations_dominant)
            rd, c = RotationDominant.objects.get_or_create(
                    name='Rotation dominant 1',
                    study=study, diagnostic=False,
                    rotation_dominant_template=\
                                        self.rotation_dominant_templates[0])
            self.rotations_dominant.append(rd)
            rd, c = RotationDominant.objects.get_or_create(
                    name='Rotation dominant 2',
                    study=study, diagnostic=False,
                    rotation_dominant_template=\
                                        self.rotation_dominant_templates[1])
            self.rotations_dominant.append(rd)
            len_rd = len(self.rotations_dominant) - old_len
            for idx_harvest in xrange(len(self.harvests)):
                RotationDominantHarvest.objects.get_or_create(
                    harvest=self.harvests[idx_harvest],
                    order=idx_harvest/len_rdt,
                    rotation_dominant=\
                        self.rotations_dominant[idx_harvest % len_rd])

    def harvestrotationdominant_associate(self):
        if (not hasattr(self, 'rotations_dominant')
           or not self.rotations_dominant):
            self.harvestrotationdominant_create()
        for idx, hp in enumerate(self.harvest_plots):
            if not hp.rotation_dominant_project:
                rds = RotationDominant.objects.filter(study=hp.study
                                                  ).order_by('pk').all()
                hp.rotation_dominant_project = rds[idx%len(rds)]
                hp.save()

    def workshop_create(self):
        if not hasattr(self, 'studies') or not self.studies:
            self.study_create()
        for study in self.studies:
            wks = models.Workshop.objects.create(name=u'Poulpes',
                                               txt_idx='poulpes', study=study)
            exp = models.WorkshopExpenses.objects.create(workshop=wks,
                                            name=u'Piscine', txt_idx='piscine')
            models.WorkshopExpensesYear.objects.create(workshop=exp, year='D',
                                              quantity=5, price=10, study=study)
            models.WorkshopExpensesYear.objects.create(workshop=exp, year='P',
                                              quantity=4, price=2, study=study)
            for idx in xrange(study.transition_years):
                models.WorkshopExpensesYear.objects.create(workshop=exp,
                          year=unicode(idx+1), quantity=2, price=2, study=study)

            rev = models.WorkshopRevenues.objects.create(workshop=wks,
                                            name=u'Encre', txt_idx='encre')
            models.WorkshopRevenuesYear.objects.create(workshop=rev, year='D',
                                             quantity=10, price=42, study=study)
            models.WorkshopRevenuesYear.objects.create(workshop=rev, year='P',
                                             quantity=1, price=4, study=study)
            for idx in xrange(study.transition_years):
                models.WorkshopRevenuesYear.objects.create(workshop=rev,
                          year=unicode(idx+1), quantity=2, price=2, study=study)


class RestrictUserSessionTest(SimulabioTestCase):
    def setUp(self):
        self.identification()
        self.BASE_META = {'HTTP_USER_AGENT':'real-human',
                          'REMOTE_ADDR':'127.0.0.1'}
        self.ALT_META = {'HTTP_USER_AGENT':u'Äkta människor',
                         'REMOTE_ADDR':'127.0.0.1'}
        self.ALT_META_2 = {'HTTP_USER_AGENT':'real-human',
                           'REMOTE_ADDR':'0.0.0.0'}

    def test_access_restriction(self):
        response = self.client.get(reverse('simulabio:welcome'),
                                    **self.BASE_META)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('simulabio:farm-list'),
                                   **self.BASE_META)
        self.assertEqual(response.status_code, 200)
        # other HTTP_USER_AGENT
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.ALT_META)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('simulabio:has-active-session'))
        # other IP
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.ALT_META_2)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('simulabio:has-active-session'))
        # no IP - no HTTP_USER_AGENT
        response = self.client.get(reverse('simulabio:welcome'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('simulabio:has-active-session'))

    def test_session_reinit(self):
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.BASE_META)
        response = self.client.get(reverse('simulabio:reinit-session'),
                                   **self.ALT_META)
        # reinit-session disconnect client
        self.client.login(username='user', password='pass')
        # BASE cannot access pages anymore
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.BASE_META)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('simulabio:has-active-session'))
        # ALT can now access to pages
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.ALT_META)
        self.assertEqual(response.status_code, 200)

    def test_session_logout_clear(self):
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.BASE_META)
        self.client.logout()
        self.client.login(username='user', password='pass')
        response = self.client.get(reverse('simulabio:welcome'),
                                   **self.ALT_META)
        self.assertEqual(response.status_code, 200)


class FarmFormTest(SimulabioTestCase):
    def test_farm_creation(self):
        values = {'corporate_name':u"Ferme n°1",
                  'address':u'3 rue des Rossignols perchés',
                  'postal_code':'35000',
                  'town':'Rennes'}
        response = self.client.post(reverse('simulabio:farm-add'), values)
        # login required
        self.assertRedirects(response, reverse('auth_login') +\
                                       '?next='+reverse('simulabio:farm-add'))
        self.identification()
        # creation
        response = self.client.post(reverse('simulabio:farm-add'), values)
        self.assertRedirects(response, reverse('simulabio:farm-list'))
        self.assertEqual(Farm.objects.count(), 1)


class StudyFormTest(SimulabioTestCase):
    def setUp(self):
        self.identification()
        self.farms_create()

    def test_study_creation(self):
        values = {'name':u'Study n°1', 'year':2012, 'farm':self.farms[0].pk,
                  'is_diagnostic':True, 'transition_years':4}
        # via form object
        form = StudyForm(values)
        # the farm permission is not tested here
        self.assertEqual(form.is_valid(), True)

        # via views
        # login required
        self.client.logout()
        response = self.client.post(reverse('simulabio:study-add',
                                            args=[self.farms[0].pk]), values)
        self.assertRedirects(response, reverse('auth_login') +\
                                       '?next='+reverse('simulabio:study-add',
                                                       args=[self.farms[0].pk]))
        self.client.login(username=self.user.username,
                          password='pass')
        # creation

        # the association is currently disabled
        #response = self.client.post(reverse('simulabio:study-add',
        #                                    args=[self.farms[0].pk]), values)
        ## the associated farm is not owned by the user
        #self.assertEqual(Study.objects.count(), 0)

        values['farm'] = self.farms[1].pk
        response = self.client.post(reverse('simulabio:study-add',
                                            args=[self.farms[1].pk]), values)
        self.assertEqual(Study.objects.count(), 1)


class HarvestSettingsFormTest(SimulabioTestCase):
    def setUp(self):
        self.identification()
        self.study_create()

    def test_harvestsetting_creation(self):
        # harvestsettings are now automatically created with plots
        pass


class HarvestPlotsFormTest(SimulabioTestCase):
    def setUp(self):
        self.identification()
        self.harvestsettings_create()

    def test_plots_creation(self):
        values = {'study':self.studies[1].pk,
                  'project':1,
                  'geocode':32,
                  'name':'Test',
                  'area':5.8,
                  'harvest_n2':self.harvest_settings[0].pk,
                  'harvest_n1':self.harvest_settings[1].pk,
                  'harvest':self.harvest_settings[2].pk,
                  }
        response = self.client.post(reverse('simulabio:harvest-plots',
                                            args=[self.studies[1].pk]), values)
        self.assertEqual(HarvestPlots.objects.count(), 1)


class HarvestQuantitiesTest(SimulabioTestCase):
    def setUp(self):
        self.harvestrotationdominant_associate()

    def test_calculate(self):
        parcel_number = len(self.harvest_plots)/len(self.rotations_dominant)
        for rd in RotationDominant.objects.all():
            self.assertEqual(rd.parcel_number, parcel_number)


class CalculationInitTest(SimulabioTestCase):
    def setUp(self):
        self.study_create()
        self.referenceharvest_create()
        self.economic_create()
        self.compute = Compute(self.studies[0].pk, follow_time=True)

    def test_initialize_db_transitiontypes(self):
        study = self.compute._study
        study.transition_years = 3
        study.save()
        self.assertEqual(study.transition_type.count(), 0)
        self.compute.initialize_db_transitiontypes()
        self.assertEqual(study.transition_type.count(), 3)

        study.transition_years = 4
        study.save()
        self.compute.initialize_db_transitiontypes()
        self.assertEqual(study.transition_type.count(), 4)

        study.transition_years = 2
        study.save()
        self.compute.initialize_db_transitiontypes()
        # don't remove transition type when decreasing
        self.assertEqual(study.transition_type.count(), 4)

    def test_initialize_harvestsettings(self):
        study = self.compute._study
        self.assertEqual(study.harvest_settings.count(), 0)
        self.compute.initialize_harvestsettings()
        self.assertEqual(study.harvest_settings.count(), len(self.harvests))
        self.compute.initialize_harvestsettings()
        self.assertEqual(study.harvest_settings.count(), len(self.harvests))

    def test_initialize_economic(self):
        study = self.compute._study
        self.assertEqual(study.extra_expenses.count(), 0)
        self.assertEqual(study.extra_revenues.count(), 0)
        self.compute.initialize_economic()
        self.assertEqual(study.extra_expenses.count(),
                         len(self.extra_expenses_types))
        self.assertEqual(study.extra_revenues.count(),
                         len(self.extra_revenues_types))


class CalculationTest(SimulabioTestCase):
    def setUp(self):
        self.study_create()
        self.referenceharvest_create()
        self.economic_create()
        self.harvestrotationdominant_associate()
        self.compute = Compute(self.studies[0].pk, follow_time=True)

    def test_harvest_calculate(self):
        study = self.studies[0]
        for rd in models.RotationDominant.objects.filter(study=study):
            rd.calculate_db()

        for hs in HarvestSettings.objects.filter(study=study):
            self.assertEqual(hs.year_details.count(), 0)
            hs.calculate_db()
            self.assertEqual(hs.year_details.count(), len(self.harvests))
        for hc in models.HarvestCategoryStudy.objects.filter(study=study).all():
            hc.calculate_db()


class StudyCopyTest(SimulabioTestCase):
    def setUp(self):
        self.study_create()
        self.referenceharvest_create()
        self.economic_create()
        self.harvestrotationdominant_associate()
        self.workshop_create()
        self.study = self.studies[0]
        self.compute = Compute(self.study.pk, follow_time=True)
        values = {'name':u'Study n°2', 'year':2015, 'farm':self.farms[0]}
        self.study_2 = Study.objects.create(**values)

    def test_copy(self):
        self.study_2.copy_from(self.study)
        self.assertEqual(self.study.harvest_settings.count(),
                         self.study_2.harvest_settings.count())
        self.assertEqual(self.study.rotation_dominant.count(),
                         self.study_2.rotation_dominant.count())
        self.assertEqual(self.study.harvest_plots.count(),
                         self.study_2.harvest_plots.count())
        self.assertEqual(self.study.workshop.count(),
                         self.study_2.workshop.count())
        self.assertEqual(
            models.WorkshopExpenses.objects.filter(workshop__study=self.study
                                                  ).count(),
            models.WorkshopExpenses.objects.filter(workshop__study=self.study_2
                                                  ).count())
        self.assertEqual(
            models.WorkshopRevenues.objects.filter(workshop__study=self.study
                                                  ).count(),
            models.WorkshopRevenues.objects.filter(workshop__study=self.study_2
                                                  ).count())
        self.client.get(reverse('simulabio:economy-full-workshop',
                            args=[self.study_2.pk, 'poulpes']))
        self.assertEqual(
            models.WorkshopExpensesYear.objects.filter(study=self.study
                                                      ).count(),
            models.WorkshopExpensesYear.objects.filter(study=self.study_2
                                                      ).count())
        self.assertEqual(
            models.WorkshopRevenuesYear.objects.filter(study=self.study
                                                      ).count(),
            models.WorkshopRevenuesYear.objects.filter(study=self.study_2
                                                      ).count())

        self.assertEqual(
            models.WorkshopRevenuesYear.objects.filter(
                workshop__workshop__study=self.study_2).count(),
            models.WorkshopRevenuesYear.objects.filter(study=self.study_2
                                                      ).count())


class ImportCSV(SimulabioTestCase):
    def setUp(self):
        self.identification()
        self.harvestsettings_create()

    def test_import_feed(self):
        csv_file = os.path.abspath(simulabio.__path__[0]
                                   + '/tests/feed_sample.csv')
        errors = imports.import_feed(csv_file)
        self.assertEqual(errors, None)

    def test_import_plot(self):
        plot_number = HarvestPlots.objects.count()
        csv_file = os.path.abspath(simulabio.__path__[0]
                                   + '/tests/plot_sample.csv')
        values = {'csv_file':open(csv_file)
                  }
        response = self.client.post(reverse('simulabio:harvest-plots',
                                            args=[self.studies[1].pk]), values)
        self.assertEqual(HarvestPlots.objects.count(), plot_number + 1)
        plot_number = HarvestPlots.objects.count()
        csv_file = os.path.abspath(simulabio.__path__[0]
                                   + '/tests/plot_sample-2.csv')
        values = {'csv_file':open(csv_file)
                  }
        response = self.client.post(reverse('simulabio:harvest-plots',
                                            args=[self.studies[1].pk]), values)
