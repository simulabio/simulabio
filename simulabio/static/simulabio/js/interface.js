function toggleHelp(){
    $('#help').fadeToggle();
}

function toggleStudyList(){
    $('#farm-studies').fadeToggle();
}

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0,
             (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0,
             (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

function register_display_validate(){
    $('input, select, textarea').click(function(){
        var cid = $(this).attr('id');
        if (cid == 'step_switch_revenues' || cid == 'step_switch_expenses' ){
            return true;
        }
        $('.validate-bar').show();
    });
    $('.cancel-form').click(function(){ window.location='.'; return false });
}

$(document).ready(function(){
    $('.menu li').live('click', function(){
        window.location = $(this).children('a').attr('href');
    });
    $("input[type=submit], button, .validate a").button();
    $('#balance-fodder-table input, select, textarea').click(function(){
        var displayed = false;
        $(this).parent().parent().nextAll().each(function(){
            if (displayed) return;
            if ($(this).find('input').filter('.displayed').length){
                displayed = true;
            } else if ($(this).find('input').filter('.hidden').length){
                displayed = true;
                $(this).find('input').filter('.hidden').removeClass('hidden'
                                                ).addClass('displayed');
            }
        });
    });
    register_display_validate();

    $('.check-all').click(function(){
        var checkboxs = $(this).parent().parent().parent(
                            ).find("input[type='checkbox']").not(this);
        if ($(this).attr('checked') == 'checked' ||
            $(this).attr('checked') == true){
            checkboxs.attr('checked', true);
        } else {
            checkboxs.attr('checked', false);
        }
    });

    $('#help-button').click(toggleHelp);
    $('#help-close').click(toggleHelp);

    $('#farm-button').click(toggleStudyList);
    $('#farm-studies').click(toggleStudyList);

    $('#main-study-menu li').mouseenter(
        function(){
            var tooltip = $(this).children('.menu-tooltip');
            if (!tooltip.is(":visible")) tooltip.fadeIn();
        }
    );
    $('#main-study-menu li').mouseleave(
        function(){
            var tooltip = $(this).children('.menu-tooltip');
            if (tooltip.is(":visible")) tooltip.fadeOut();
        }
    );

    $('#menu li').mouseenter(
        function(){
            var tooltip = $(this).children('.vmenu-tooltip');
            if (!tooltip.is(":visible")) tooltip.fadeIn();
        }
    );

    $('#menu li').mouseleave(
        function(){
            var tooltip = $(this).children('.vmenu-tooltip');
            if (tooltip.is(":visible")) tooltip.fadeOut();
        }
    );

    $('#main-menu-close').live('click', function(){
        $("#main-study-menu").hide("slide", {
                                direction: "left",
                                complete:function(){
                                    $('#main-menu-open').show();
                                }
                              });
    });
    $('#main-menu-open').live('click', function(){
        $('#main-menu-open').hide();
        $("#main-study-menu").show("slide", { direction: "left" });
    });
    $("#message-close").live('click', function(){
        $("#message").hide();
        return false;
    });
});

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = escape(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return unescape(c.substring(nameEQ.length,
                                                                c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
