#!/bin/sh

gen_doc(){
    pandoc -c buttondown.css -s -S --toc -f markdown -t html $CFILE'md' > $CFILE'html'
}

CFILE='architecture.'
gen_doc
