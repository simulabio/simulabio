#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright (C) 2015 Étienne Loks  <etienne.loks@proxience.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.utils.translation import pgettext_lazy

TRANSLATION_STRINGS = [
    ('study', pgettext_lazy('ooo key', u'study')),
    ('farm', pgettext_lazy('ooo key', u"farm")),
    ('comment', pgettext_lazy('ooo key', u"comment")),
    ('comment_file', pgettext_lazy('ooo key', u"comment_file")),
    ('year', pgettext_lazy('ooo key', u"year")),
    ('technician', pgettext_lazy('ooo key', u"technician")),
    ('address', pgettext_lazy('ooo key', u"address")),
    ('postal_code', pgettext_lazy('ooo key', u"postal_code")),
    ('town', pgettext_lazy('ooo key', u"town")),
    ('transition_years', pgettext_lazy('ooo key', u"transition_years")),
    ('rotation_name', pgettext_lazy('ooo key', u"rotation_name")),
    ('rotation_image', pgettext_lazy('ooo key', u"rotation_image")),
    ('rotation_remark', pgettext_lazy('ooo key', u"rotation_remark")),
    ('surface', pgettext_lazy('ooo key', u"surface")),
    ('elementary_surface', pgettext_lazy('ooo key', u"elementary_surface")),
    ('parcel_number', pgettext_lazy('ooo key', u"parcel_number")),
    ('parcel_average_size', pgettext_lazy('ooo key', u"parcel_average_size")),
    ('surface_by_harvest', pgettext_lazy('ooo key', u"surface_by_harvest")),
    ('table_plots', pgettext_lazy('ooo key', u"table_plots")),
    ('rotation_years', pgettext_lazy('ooo key', u"rotation_years")),
    ('table_settings', pgettext_lazy('ooo key', u"table_settings")),
    ('table_herd', pgettext_lazy('ooo key', u"table_herd")),
    ('table_milk', pgettext_lazy('ooo key', u"table_milk")),
    ('table_production', pgettext_lazy('ooo key', u"table_production")),
    ('table_balance', pgettext_lazy('ooo key', u"table_balance")),
    ('animal', pgettext_lazy('ooo key', u"animal")),
    ('harvest', pgettext_lazy('ooo key', u"harvest")),
    ('feed', pgettext_lazy('ooo key', u"feed")),
    ("total_product", pgettext_lazy('ooo key', u"total_product")),
    ("operational_charges", pgettext_lazy('ooo key', u"operational_charges")),
    ("VET", pgettext_lazy('ooo key', u"VET")),
    ("PHY", pgettext_lazy('ooo key', u"PHY")),
    ("ENG", pgettext_lazy('ooo key', u"ENG")),
    ("FEED", pgettext_lazy('ooo key', u"FEED")),
    ("STRA", pgettext_lazy('ooo key', u"STRA")),
    ("structural_charges", pgettext_lazy('ooo key', u"structural_charges")),
    ("depreciation", pgettext_lazy('ooo key', u"depreciation")),
    ("annuities", pgettext_lazy('ooo key', u"annuities")),
    ("share_capital", pgettext_lazy('ooo key', u"share_capital")),
    ("interests", pgettext_lazy('ooo key', u"interests")),
    ("workshop", pgettext_lazy('ooo key', u"workshop")),
    ("table_revenues", pgettext_lazy('ooo key', u"table_revenues")),
    ("table_help", pgettext_lazy('ooo key', u"table_help")),
    ("table_expenses", pgettext_lazy('ooo key', u"table_expenses")),
    ("table_extraexpenses", pgettext_lazy('ooo key', u"table_extraexpenses")),
    ("table_animalpurchase",
     pgettext_lazy('ooo key', u"table_animalpurchase")),
    ("table_feedexpenses", pgettext_lazy('ooo key', u"table_feedexpenses")),
    ("table_investments", pgettext_lazy('ooo key', u"table_investments")),
    ("table_synthesis", pgettext_lazy('ooo key', u"table_synthesis")),
    ("table_harvestrevenues",
     pgettext_lazy('ooo key', u"table_harvestrevenues")),
    ("table_harvestexpenses",
     pgettext_lazy('ooo key', u"table_harvestexpenses")),
]
