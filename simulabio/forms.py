#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

import re
from unicode_csv_reader import unicode_csv_reader

from django import forms
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import IntegrityError, transaction
from django.db.models import Q
from django.forms.fields import FloatField, IntegerField
from django.forms.models import BaseModelFormSet
from django.forms.formsets import DELETION_FIELD_NAME
from django.template.defaultfilters import slugify
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from simulabio.middleware import get_request_cache

from data_importer import ImporterError, Importer

import models
from tasks import copy_study, compute
from compute import Compute
import widgets


class FrenchIntField(IntegerField):
    def to_python(self, value):
        if value:
            value = unicode(value)
            value = value.split(',')[0]
            value = value.split('.')[0]
        elif self.required:
            value = 0
        return super(FrenchIntField, self).to_python(value)


class FrenchFloatField(FloatField):
    def to_python(self, value):
        if value:
            value = value.replace(',', '.')
        elif self.required:
            value = 0
        return super(FrenchFloatField, self).to_python(value)


class FrenchModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        for field in self.base_fields:
            if type(self.base_fields[field]) == FloatField:
                self.base_fields[field] = FrenchFloatField(
                    required=self.base_fields[field].required,
                    label=self.base_fields[field].label)
            if type(self.base_fields[field]) == IntegerField:
                self.base_fields[field] = FrenchIntField(
                    required=self.base_fields[field].required,
                    label=self.base_fields[field].label)
        super(FrenchModelForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if type(self.fields[field]) == FloatField:
                self.fields[field].to_python = FrenchFloatField.to_python


class BaseForm(FrenchModelForm):
    """
    A model form with simplify ability to hide or readonly fields
    """
    _hidden_fields = []
    # list of (readonly field name, choices)
    _readonly_fields = []

    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        for hidden_field in self._hidden_fields:
            self.fields[hidden_field].label = u" "
            self.fields[hidden_field].widget = forms.HiddenInput()
        readonly_fields = self._readonly_fields
        if readonly_fields and \
           type(self._readonly_fields[0]) not in (list, tuple):
            readonly_fields = [(readonly_field, [])
                               for readonly_field in self._readonly_fields]
        for readonly_field, choices in readonly_fields:
            try:
                assert readonly_field in self.fields.keys()
            except AssertionError:
                raise AssertionError(
                    u"Provided readonly field '%s' not in fields" %
                    readonly_field)
            if choices:
                self.fields[readonly_field].widget = widgets.ReadOnlyWidget(
                    choices=choices)
            else:
                self.fields[readonly_field].widget = widgets.ReadOnlyWidget()

    def clean(self):
        # prevent forged form
        super(BaseForm, self).clean()
        instance = getattr(self, 'instance', None)
        if not instance or not self.instance.pk:
            return self.cleaned_data
        readonly_fields = self._readonly_fields
        if readonly_fields and \
           type(self._readonly_fields[0]) not in (list, tuple):
            readonly_fields = [(readonly_field, [])
                               for readonly_field in self._readonly_fields]
        unchanged_field = [readonly_field
                           for readonly_field, choices in readonly_fields]
        unchanged_field += self._hidden_fields
        for field in unchanged_field:
            self.cleaned_data[field] = getattr(instance, field)
        return self.cleaned_data


class BaseStudyForm(BaseForm):
    """
    A model form that need an hidden study field
    """
    study = forms.ModelChoiceField(label=u" ",
                                   queryset=models.Study.objects.all(),
                                   widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        assert 'study' in kwargs
        self.study = kwargs.pop('study')
        super(BaseStudyForm, self).__init__(*args, **kwargs)
        self.fields['study'].initial = self.study.pk

    def clean_study(self):
        study = self.cleaned_data['study']
        if study != self.study:
            raise forms.ValidationError(_(u"Incorrect study id."))
        return study


class BaseStudyFormSet(BaseModelFormSet):
    """
    A formset that need study context
    """
    def __init__(self, *args, **kwargs):
        self.study = kwargs.pop('instance')
        if 'year' in kwargs:
            self.year = kwargs.pop('year')
        if 'harvest' in kwargs:
            self.harvest = kwargs.pop('harvest')
        if 'workshop' in kwargs:
            self.workshop = kwargs.pop('workshop')
        super(BaseStudyFormSet, self).__init__(*args, **kwargs)

    def get_queryset(self, *args, **kwargs):
        q = self.model.objects.filter(study=self.study).order_by('pk')
        return q

    def _construct_forms(self):
        self.forms = []
        kwargs = {'study': self.study}
        if hasattr(self, 'year'):
            kwargs['year'] = self.year
        if hasattr(self, 'harvest'):
            kwargs['harvest'] = self.harvest
        if hasattr(self, 'workshop'):
            kwargs['workshop'] = self.workshop
        for i in xrange(self.total_form_count()):
            self.forms.append(self._construct_form(i, **kwargs))

    def add_fields(self, form, index):
        super(BaseStudyFormSet, self).add_fields(form, index)
        if (not form.instance or not form.instance.pk) \
           and (DELETION_FIELD_NAME in form.fields):
            form.fields[DELETION_FIELD_NAME].widget = forms.HiddenInput()


class FarmForm(FrenchModelForm):
    class Meta:
        model = models.Farm
        exclude = ('available',)

# studies


class StudyForm(FrenchModelForm):
    farm = forms.ModelChoiceField(
        label=u" ", queryset=models.Farm.objects.all(),
        widget=forms.HiddenInput())
    copy_from = forms.ChoiceField(label=_(u"Copy data from"), required=False,
                                  choices=[])

    class Meta:
        model = models.Study
        exclude = ('forage_yield_total', 'forage_yield_project_total',
                   'grain_yield_total', 'grain_yield_project_total',
                   'straw_yield_total', 'straw_yield_project_total',
                   'total_product', 'operational_charges',
                   'structural_charges', 'depreciation', 'annuities',
                   'share_capital', 'interests', 'farming_type',
                   'available'
                   )

    def __init__(self, *args, **kwargs):
        self.farm = None
        if 'farm' in kwargs:
            self.farm = kwargs.pop('farm')
        super(StudyForm, self).__init__(*args, **kwargs)
        self.fields['transition_years'].validators = [MinValueValidator(0),
                                                      MaxValueValidator(10)]

        # manage other studies to copy from (if relevant)
        if not self.farm:  # farm is only precised at creation
            self.fields.pop('copy_from')
            return
        other_studies = models.Study.objects.filter(farm=self.farm)
        if not other_studies.count():
            self.fields.pop('copy_from')
        else:
            self.fields["copy_from"].choices = [('', '-' * 9)] +\
                [(std.pk, std.name) for std in other_studies.all()]

    def clean_year(self):
        year = self.cleaned_data['year']
        if year < 2000 or year > 2100:
            raise forms.ValidationError(_(u"Incorrect year."))
        return year

    def clean_name(self):
        name = self.cleaned_data['name'].strip()
        if not name:
            raise forms.ValidationError(_(u"You should provide a name."))
        return name

    def clean(self):
        year = self.cleaned_data.get('year')
        name = self.cleaned_data.get('name')
        farm = self.cleaned_data.get('farm')
        if not year or not name or not farm:
            return self.cleaned_data
        if models.Study.objects.filter(
                farm=farm, name__iexact=name, year=year).count():
            raise forms.ValidationError(_(u"A study with the same name already"
                                          u" exist."))
        return self.cleaned_data

    def save(self):
        study = super(StudyForm, self).save()
        copied_study = self.cleaned_data.get('copy_from')
        if copied_study:
            study.available = False
            study.save()
            try:
                copy_study(study, models.Study.objects.get(pk=copied_study))
            except models.Study.DoesNotExist:
                study.available = True
                study.save()
        compute(study.pk, [
            ['initialize_study', [True]],
        ], ['SIT'])
        return study


class StudyFormUpdate(StudyForm):
    def clean(self):
        return self.cleaned_data


class TransitionYearForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.study = None
        if 'study' in kwargs:
            self.study = kwargs.pop('study')
        super(TransitionYearForm, self).__init__(*args, **kwargs)
        for idx in xrange(1, self.study.transition_years + 1):
            initial = ''
            try:
                initial = models.TransitionType.objects.get(
                    study=self.study, year=idx).yield_type
            except ObjectDoesNotExist:
                pass
            except MultipleObjectsReturned:
                q = models.TransitionType.objects.filter(study=self.study,
                                                         year=idx)
                for tt in q.all()[1:]:
                    tt.delete()
                initial = q.all()[0]
            choices = [('', '---------')] + list(models.YIELD_TYPE)
            if initial:
                # if initialized don't allow to set to Null
                choices.pop(0)
            self.fields['yield_type_%d' % idx] = forms.ChoiceField(
                label=_(u"%(year)d (n+%(idx)d)") % {
                    'year': self.study.year + idx, 'idx': idx},
                choices=choices, initial=initial, required=False)

    def clean(self):
        return self.cleaned_data

    def save(self):
        for tt in models.TransitionType.objects.filter(study=self.study).all():
            tt.delete()
        for k in self.cleaned_data:
            if not k.startswith('yield_type_') or not self.cleaned_data[k]:
                continue
            try:
                current_idx = int(k.split('_')[-1])
            except KeyError:
                continue
            models.TransitionType.objects.get_or_create(
                study=self.study, yield_type=self.cleaned_data[k],
                year=current_idx)
        compute(
            self.study.pk, [
                ['initialize_db_transitiontypes', []],
                ['calculate_db_harvest', []],
                ['calculate_db_economic_synthesis', []],
            ], ['HHA', 'ESY'])


class StudyCommentForm(forms.ModelForm):
    class Meta:
        model = models.StudyComment
        exclude = ('study', 'form', 'context')


class ReportGenerationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ReportGenerationForm, self).__init__(*args, **kwargs)
        self.report_target_list = models.ReportTarget.objects\
            .filter(available=True)\
            .exclude(class_name='Default').order_by('order')
        # default_report_target, c = models.ReportTarget.objects.get_or_create(
        #                        class_name='Default', defaults={'order':1})
        for report_target in self.report_target_list:
            cls_name, lbl = report_target.class_name, report_target.name
            q = models.ReportTemplate.objects\
                .filter(available=True, target=report_target)
            if not q.count():
                continue
            self.fields['check_%s' % cls_name] = forms.BooleanField(
                initial=True, required=False)
            self.fields['template_%s' % cls_name] = forms.ModelChoiceField(
                queryset=q, empty_label=None, required=True, label=lbl,)

    def clean(self):
        # verify that at least one item has been checked
        cleaned_data = super(ReportGenerationForm, self).clean()
        for k in self.cleaned_data:
            if k.startswith('check_') and self.cleaned_data[k]:
                return cleaned_data
        raise forms.ValidationError(_(u"At least one item must be checked."))

    def save(self):
        submited = []
        for report_target in self.report_target_list:
            if self.cleaned_data.get('check_%s' % report_target.class_name):
                submited.append(
                    (report_target,
                     self.cleaned_data.get('template_%s' %
                                           report_target.class_name)))
        return submited

# generic form


def get_generic_form(generic_form):
    class GenericForm(forms.Form):
        def __init__(self, *args, **kwargs):
            self.study = None
            if 'instance' in kwargs:
                self.study = kwargs.pop('instance')
            if 'study' in kwargs:
                self.study = kwargs.pop('study')
            super(GenericForm, self).__init__(*args, **kwargs)
            for field in generic_form.generic_fields\
                    .filter(available=True).order_by('order').all():
                initial = ''
                q = models.StudyField.objects.filter(field=field,
                                                     study=self.study)
                if q.count():
                    initial = q.all()[0].value
                self.fields[field.slug] = forms.CharField(
                    label=field.name, widget=forms.Textarea(), initial=initial,
                    required=False)

        def save(self, *args, **kwargs):
            for field in generic_form.generic_fields\
                    .filter(available=True).order_by('order').all():
                value = self.cleaned_data.get(field.slug)
                cfield, created = models.StudyField.objects.get_or_create(
                    field=field, study=self.study, defaults={'value': value})
                if not created:
                    cfield.value = value
                    cfield.save()
    return GenericForm

# harvests


class HarvestSettingsForm(FrenchModelForm):
    class Meta:
        model = models.HarvestSettings
        exclude = ('surface_total', 'surface_project_total',
                   'forage_yield_total', 'forage_yield_project_total',
                   'grain_yield_total', 'grain_yield_project_total',
                   'straw_yield_total', 'straw_yield_project_total',
                   'forage_project_used', 'forage_used',
                   'grain_project_used', 'grain_used',
                   'straw_project_used', 'straw_used')

    def __init__(self, *args, **kwargs):
        super(HarvestSettingsForm, self).__init__(*args, **kwargs)
        for k in ('study', 'order'):
            self.fields[k].widget = forms.HiddenInput()
        self.fields['harvest'].widget = widgets.ReadOnlyWidget(
            choices=self.fields['harvest'].choices)


class BaseHarvestSettingsForm(forms.Form):
    harvest = forms.ModelChoiceField(
        label=_(u"Add a new harvest"),
        queryset=models.Harvest.objects.filter(available=True))

    def __init__(self, *args, **kwargs):
        self.study = None
        if 'study' in kwargs:
            self.study = kwargs.pop('study')
        super(BaseHarvestSettingsForm, self).__init__(*args, **kwargs)
        if not self.study:
            return
        self.fields['harvest'].queryset = models.Harvest.objects\
            .filter(available=True).exclude(harvest_settings__study=self.study)

    def save(self):
        harvest = self.cleaned_data['harvest']
        forage_yield, grain_yield, straw_yield = 0, 0, 0
        has_ref_harvest = False
        if self.study.farm.geographical_zone:
            ref_harvest = self.study.farm\
                .geographical_zone.reference_harvest.filter(harvest=harvest)
            if ref_harvest.count():
                has_ref_harvest = True
        if not has_ref_harvest:
            ref_harvest = models.GeographicalZone.objects.filter(default=True,
                                                                 harvest=True)
        if ref_harvest.count():
            ref_harvest = ref_harvest.all()[0]
            forage_yield = ref_harvest.forage_yield
            grain_yield = ref_harvest.grain_yield
            straw_yield = ref_harvest.straw_yield
        models.HarvestSettings.objects.create(
            study=self.study, harvest=harvest,
            forage_yield_project=forage_yield, grain_yield_project=grain_yield,
            straw_yield_project=straw_yield)


class HarvestPlotsImportForm(forms.Form):
    csv_file = forms.FileField(label=_(u"Plot list file (CSV)"))

    def save(self, study):
        csv_file = self.cleaned_data['csv_file']
        importer = models.HarvestPlotImporter(study, skip_first_line=True)
        # some softwares (at least Gnumeric) convert CSV file to utf-8 no
        # matter what the CSV source encoding is
        encodings = [settings.ENCODING, 'utf-8']
        for encoding in encodings:
            try:
                importer.importation(
                    unicode_csv_reader([line.decode(encoding)
                                        for line in csv_file.readlines()]))
            except ImporterError, e:
                if e.type == ImporterError.HEADER \
                        and encoding != encodings[-1]:
                    csv_file.seek(0)
                    continue
                return 0, [[0, 0, e.msg]], []
            except UnicodeDecodeError, e:
                return 0, [[0, 0, Importer.ERRORS['header_check']]], []
            break
        tasks = [['calculate_db_harvest', []]]
        tasks.append(['calculate_db_economic_synthesis', []])
        compute(study.pk, tasks, ['HHA', 'ESY'])
        return importer.number_imported, importer.errors, importer.messages


class HarvestPlotsForm(FrenchModelForm):
    _hidden_fields = ('study', 'rotation_dominant_project',
                      'rotation_dominant_diag')
    remark = forms.CharField(label=_(u"Plot caracteristic"),
                             widget=forms.Textarea(), required=False)

    class Meta:
        model = models.HarvestPlots
        exclude = ('geocode', 'harvest_transition')

    def __init__(self, *args, **kwargs):
        self.study = kwargs.pop('study')
        self.instance = None
        if 'instance' in kwargs:
            self.instance = kwargs['instance']

        # limit displaying of float value to 2 decimal
        self.area_backup = 0
        if 'decimal_2' in kwargs:
            kwargs.pop('decimal_2')
            if self.instance and self.instance.area:
                self.area_backup = self.instance.area
                self.instance.area = int(self.instance.area * 100) / 100.0

        super(HarvestPlotsForm, self).__init__(*args, **kwargs)
        self.fields['study'].initial = self.study.pk
        cache = get_request_cache()
        if 'harvest_settings_choices' not in cache:
            choices = [('', '---------')] + \
                [(hs.pk, unicode(hs.harvest))
                 for hs in
                 models.HarvestSettings.objects.filter(study=self.study)]
            cache.set('harvest_settings_choices', choices)
        choices = cache.get('harvest_settings_choices')
        self.hs_choices = choices
        for k in ('harvest_n2', 'harvest_n1', 'harvest'):
            self.fields[k].choices = choices
        year = self.study.year
        # transition years
        for idx in xrange(1, self.study.transition_years + 1):
            initial = ''
            if self.instance:
                try:
                    initial = models.HarvestTransition.objects.get(
                        plot=self.instance, year=idx).harvest_setting.pk
                except ObjectDoesNotExist:
                    pass
            self.fields['transition_year_%d' % idx] = forms.ChoiceField(
                label=_(u"Harvest %(year)d (n+%(idx)d)") % {'year': year + idx,
                                                            'idx': idx},
                choices=choices, required=False, initial=initial)

        for k in self._hidden_fields:
            self.fields[k].widget = forms.HiddenInput()
        self.fields['harvest_n2'].label = unicode(
            _(u"Harvest %(year)d (n-2)")) % {"year": year - 2}
        self.fields['harvest_n1'].label = unicode(
            _(u"Harvest %(year)d (n-1)")) % {"year": year - 1}
        self.fields['harvest'].label = unicode(
            _(u"Harvest %(year)d (n)")) % {"year": year}

    def save(self, *args, **kwargs):
        # if no changes in the rounded value we assume that the value
        # has not changed
        if self.instance and self.area_backup and \
           self.cleaned_data['area'] == int(self.area_backup * 100) / 100.0:
            self.cleaned_data['area'] = self.area_backup

        plot = super(HarvestPlotsForm, self).save(*args, **kwargs)
        models.HarvestTransition.objects.filter(plot=plot).delete()
        for k in self.cleaned_data:
            if not k.startswith('transition_year_') \
                    or not self.cleaned_data[k]:
                continue
            try:
                current_idx = int(k.split('_')[-1])
            except KeyError:
                continue
            try:
                models.HarvestTransition.objects.create(
                    plot=plot,
                    harvest_setting=models.HarvestSettings.objects
                    .get(pk=self.cleaned_data[k]),
                    year=current_idx)
            except ObjectDoesNotExist:
                continue
        return plot


class HarvestPlotsFullForm(HarvestPlotsForm):
    _hidden_fields = ('study',)
    remark = forms.CharField(label=_(u"Plot caracteristic"),
                             widget=forms.TextInput(), required=False)

    def __init__(self, *args, **kwargs):
        kwargs['decimal_2'] = True
        super(HarvestPlotsFullForm, self).__init__(*args, **kwargs)
        cache = get_request_cache()
        if 'rotation_dominant_diag_choices' not in cache:
            choices = [('', '---------')] + \
                [(rd.pk, unicode(rd))
                 for rd in models.RotationDominant.objects
                 .filter(study=self.study, diagnostic=True)]
            cache.set('rotation_dominant_diag_choices', choices)
        self.fields['rotation_dominant_diag'].choices = cache.get(
            'rotation_dominant_diag_choices')
        if 'rotation_dominant_project_choices' not in cache:
            choices = [('', '---------')] + \
                [(rd.pk, unicode(rd))
                 for rd in models.RotationDominant.objects
                 .filter(study=self.study, diagnostic=False)]
            cache.set('rotation_dominant_project_choices', choices)
        self.fields['rotation_dominant_project'].choices = cache.get(
            'rotation_dominant_project_choices')


class HarvestPlotsFormSet(BaseStudyFormSet):
    model = models.HarvestPlots


class RotationDominantHarvestAdminForm(FrenchModelForm):
    class Meta:
        model = models.RotationDominantHarvest

    class Media:
        js = [settings.JQUERY_URL,
              settings.JQUERY_UI_URL + 'jquery-ui.js',
              '%ssimulabio/js/menu-sort.js' % settings.STATIC_URL,
              ]

    def __init__(self, *args, **kwargs):
        super(RotationDominantHarvestAdminForm, self).__init__(*args, **kwargs)
        if not self.initial.get('order'):
            self.initial['order'] = 100


class HarvestRotationTemplateAddForm(forms.Form):
    rotation_dominant_tpl = forms.ModelChoiceField(
        label=_(u"Rotation"), queryset=None)

    def __init__(self, *args, **kwargs):
        geographical_zone = kwargs.pop('geographical_zone')
        kwargs.pop('study')
        kwargs.pop('diagnostic')
        super(HarvestRotationTemplateAddForm, self).__init__(*args, **kwargs)
        query = Q(geographical_zone=geographical_zone) | \
            Q(geographical_zone__isnull=True)
        self.fields['rotation_dominant_tpl'].queryset = \
            models.RotationDominantTemplate.objects.filter(query)

    def save(self, study, is_diag):
        rotation_dominant_tpl = self.cleaned_data['rotation_dominant_tpl']
        rd = models.RotationDominant.objects.create(
            study=study, diagnostic=is_diag,
            rotation_dominant_template=rotation_dominant_tpl,
            name=rotation_dominant_tpl.name)
        q = rotation_dominant_tpl.rotation_dominant_harvest.all()
        for rd_harvest in q:
            models.RotationDominantHarvest.objects.create(
                harvest=rd_harvest.harvest, rotation_dominant=rd,
                order=rd_harvest.order)

        return rd


class PlotModelField(forms.ModelMultipleChoiceField):
    def clean(self, value):
        values = []
        if value:
            for v in value:
                values += v.split(',')
        value = values
        return super(PlotModelField, self).clean(value)


class HarvestRotationAddForm(forms.Form):
    harvest_plot = PlotModelField(
        label=_(u"Plot"), queryset=None, widget=widgets.PlotsSelect)

    def __init__(self, *args, **kwargs):
        self.study = kwargs.pop('study')
        diagnostic = kwargs.pop('diagnostic')
        super(HarvestRotationAddForm, self).__init__(*args, **kwargs)
        q = {'study': self.study}
        if diagnostic:
            q['diagnostic'] = True
            q['rotation_dominant_diag'] = None
        else:
            q['project'] = True
            q['rotation_dominant_project'] = None
        self.fields['harvest_plot'].queryset = \
            models.HarvestPlots.objects.filter(**q).order_by('name')
        self.fields['harvest_plot'].widget.numeric_cols = [
            mark_safe(unicode(col).replace(' ', '_'))
            for col in (_(u'Area'), _(u'Plot group number'))]

    def save(self, rotation, is_diag):
        for harvest_plot in self.cleaned_data['harvest_plot']:
            if is_diag:
                harvest_plot.rotation_dominant_diag = rotation
            else:
                harvest_plot.rotation_dominant_project = rotation
            harvest_plot.save()
        tasks = [['calculate_db_harvest', []]]
        tasks.append(['calculate_db_economic_synthesis', []])
        compute(self.study.pk, tasks, ['HHA', 'ESY'])


class HarvestRotationModifyForm(forms.Form):
    name = forms.CharField(_(u"Name"))
    harvests = forms.MultipleChoiceField(
        label=_(u"Rotation"), required=False, widget=widgets.RotationSelect,
        choices=[])

    def __init__(self, *args, **kwargs):
        self.rotation = kwargs.pop('rotation')
        super(HarvestRotationModifyForm, self).__init__(*args, **kwargs)
        available_harvests = [('', '--')] + \
            [(h.pk, unicode(h))
             for h in models.Harvest.objects.
             filter(available=True).order_by('name').all()]
        self.fields['harvests'].choices = available_harvests

    def save(self, rotation):
        rotation.name = self.cleaned_data['name']
        rotation.save()
        rotation.rotation_dominant_harvest.all().delete()
        for idx, harvest_pk in enumerate(self.cleaned_data['harvests']):
            models.RotationDominantHarvest.objects.create(
                rotation_dominant=rotation,
                harvest=models.Harvest.objects.get(pk=harvest_pk), order=idx)
        tasks = [['calculate_db_harvest', []]]
        tasks.append(['calculate_db_economic_synthesis', []])
        compute(rotation.study.pk, tasks, ['HHA', 'ESY'])

# animals


class AnimalsHerdForm(BaseForm):
    class Meta:
        model = models.HerdDetail
        exclude = ('year', 'herd',)

    def __init__(self, *args, **kwargs):
        super(AnimalsHerdForm, self).__init__(*args, **kwargs)
        self.fields['turnover'].widget = widgets.ReadOnlyPercentWidget()
        self.fields['reformed_cow_sold'].widget = widgets.ReadOnlyWidget()
        self.fields['calves_sold'].widget = widgets.ReadOnlyWidget()


class AnimalsFeedForm(FrenchModelForm):
    class Meta:
        model = models.Feed
        exclude = ('study', 'year', 'animal')

    feed_template = forms.ModelChoiceField(
        queryset=models.FeedTemplate.objects, widget=widgets.ColoredSelect)

    def __init__(self, *args, **kwargs):
        self.feed = kwargs.get('instance')
        self.study = None
        if 'study' in kwargs:
            self.study = kwargs.pop('study')
        super(AnimalsFeedForm, self).__init__(*args, **kwargs)
        if not self.feed:
            return
        q = Q(associated_user__isnull=True) | \
            Q(associated_user__simulabio_user__farms__study__feed=self.feed) | \
            Q(associated_user__in=[user
              for user in self.study.farm.simulabio_user.all()])
        fts = list(models.FeedTemplate.objects.filter(q).distinct())
        self.fields['feed_template'].choices = [('', '---------')] + \
            [(ft.pk, ft.name) for ft in fts]
        self.fields['feed_template'].widget.colors = dict(
            [(unicode(ft.pk), ft.color) for ft in fts if ft.color])

    def save(self):
        m = super(AnimalsFeedForm, self).save()
        c = Compute(m.study.pk)
        c.update_db_feeddetails(m)
        compute(m.study.pk,
                [['update_db_feedneeds', [m.year]], ], ['AFN'])


class AnimalsFeedCustomFeedTemplateForm(FrenchModelForm):
    class Meta:
        model = models.FeedTemplate
        exclude = ('txt_idx', 'diagnostic', 'farming_type',
                   'default_for_diag', 'associated_user', 'animal_default')

    def __init__(self, *args, **kwargs):
        self.user = None
        if 'user' in kwargs:
            self.user = kwargs.pop('user')
        self.current_feed = kwargs.pop('current_feed') \
            if 'current_feed' in kwargs else None
        super(AnimalsFeedCustomFeedTemplateForm, self).__init__(*args,
                                                                **kwargs)

    def save(self):
        vals = {
            'name': self.cleaned_data['name'],
            'milk_production': self.cleaned_data['milk_production'],
            'txt_idx': slugify(self.cleaned_data['name']),
            'diagnostic': self.current_feed.year == 'D',
            'txt_idx': slugify(self.cleaned_data['name']),
            'farming_type': self.current_feed.study.farming_type
            if self.current_feed.year != 'D' else 'CV',
            'default_for_diag': False,
            'associated_user': self.user
        }
        obj = self._meta.model.objects.create(**vals)
        obj.copy_from_feed(self.current_feed)
        self.current_feed.feed_template = obj
        self.current_feed.save()


class AnimalsFeedUpdateForm(forms.Form):
    def __init__(self, **kwargs):
        self.feed = kwargs.pop('feed')
        self.month = kwargs.pop('month')
        self.items = self.feed.get_feed_items()
        super(AnimalsFeedUpdateForm, self).__init__(**kwargs)
        for item in self.items:
            default = 0
            fd = models.FeedDetail.objects.filter(
                feed=self.feed, feed_item=item)
            if fd.count():
                default = getattr(fd.all()[0], 'amount_' + self.month)
            field = FrenchFloatField(label=item.name, required=False,
                                     initial=default)
            self.fields['feed_%s_%d' % (self.month, item.pk)] = field

    def save(self, season=False):
        for item in self.items:
            c_id = 'feed_%s_%d' % (self.month, item.pk)
            amount = self.cleaned_data.get(c_id) or 0
            fd, created = models.FeedDetail.objects.get_or_create(
                feed=self.feed, feed_item=item)
            setattr(fd, 'amount_' + self.month, float(amount))
            fd.save()
            if season:
                models.update_season(fd)
        compute(
            self.feed.study.pk,
            [['update_db_feedneeds', [self.feed.year]], ], ['AFN'])


class BaseFeedNeedFormSet(BaseModelFormSet):
    def __init__(self, **kwargs):
        self.feed_need = kwargs.pop('feed_need')
        self.round = 2
        if 'round' in kwargs:
            self.round = kwargs.pop('round')
        super(BaseFeedNeedFormSet, self).__init__(**kwargs)
        for form in self.forms:
            form.fields.pop('feed_need')

    def save(self):
        for data in self.cleaned_data:
            if data:
                self.save_form(data)

    def save_form(self, data):
        data['feed_need'] = self.feed_need
        base_data = data.copy()
        if 'id' in base_data:
            base_data.pop('id')
        base_data['defaults'] = {'used': base_data.pop('used')}
        obj, created = self.model.objects.get_or_create(**base_data)
        calculate = None
        if obj.__class__ == models.FeedNeedExtraSupply:
            calculate = Compute(obj.feed_need.study.pk)\
                .calculate_db_extrasupply
        elif obj.__class__ == models.FeedNeedSupply:
            calculate = Compute(obj.feed_need.study.pk).calculate_db_supply
        if not created and data['used']:
            obj.used = data['used']
            obj.save()
        if not data['used']:
            obj.delete()
        if calculate:
            calculate(obj.pk)


class FeedNeedFormSet(BaseFeedNeedFormSet):
    def __init__(self, **kwargs):
        super(FeedNeedFormSet, self).__init__(**kwargs)
        target_type = self.feed_need.target_type
        yield_name = target_type + '_yield_total'
        hs_filter = {'harvest__study': self.feed_need.study,
                     'year': self.feed_need.year,
                     (yield_name + '__gt'): 0,
                     ('surface_total__gt'): 0}
        harvest_settings_list = [('', '--')]
        for hs in models.HarvestSettingsYear.objects.filter(**hs_filter).all():
            used = round(getattr(hs, target_type + '_used'), self.round)
            if not self.round:
                used = int(used)
            total = round(getattr(hs, yield_name), self.round)
            if not self.round:
                total = int(total)
            left = total - used
            if not self.round:
                left = int(left)
            if left < 0:
                left = u"<span class='negative'>%d</span>" % left
            lbl = u'%s (%s/%s)' % (unicode(hs.harvest), unicode(left),
                                   unicode(total))
            lbl = mark_safe(lbl)
            harvest_settings_list += [(hs.harvest.pk, lbl)]
        for form in self.forms:
            harvest_setting = form.initial.get('harvest_settings')
            if harvest_setting:
                form.fields['harvest_settings'].widget = \
                    widgets.ReadOnlyWidget()
            form.fields['harvest_settings'].choices = harvest_settings_list
            if harvest_setting:
                for idx, hs in enumerate(harvest_settings_list[:]):
                    pk, lbl = hs
                    if pk == harvest_setting:
                        harvest_settings_list.pop(idx)
                        break


class FeedNeedExtraFormSet(BaseFeedNeedFormSet):
    def __init__(self, **kwargs):
        super(FeedNeedExtraFormSet, self).__init__(**kwargs)
        if not self.forms:
            return
        base_choices = list(self.forms[0].fields['extra_supply'].choices)[:]
        for form in self.forms:
            extra_supply = form.initial.get('extra_supply')
            if extra_supply:
                form.fields['extra_supply'].widget = widgets.ReadOnlyWidget()
            form.fields['extra_supply'].choices = base_choices
            if extra_supply:
                for idx, hs in enumerate(base_choices[:]):
                    pk, lbl = hs
                    if pk == extra_supply:
                        base_choices.pop(idx)
                        break


class AnimalsLitterForm(FrenchModelForm):
    class Meta:
        model = models.StrawUsage
        exclude = ('herd_detail', 'animal',)

    def save(self):
        m = super(AnimalsLitterForm, self).save()
        compute(m.herd_detail.herd.study.pk,
                [['update_db_feedneeds', [m.herd_detail.year]]], ['AFN'])


class MilkProductionForm(BaseForm):
    class Meta:
        model = models.MilkProduction
        exclude = ('production_per_cow_keep',)
    _hidden_fields = ('study', 'year',)
    _readonly_fields = (('available_litrage', None),
                        ('total_milk_production', None),
                        ('self_consumption_calves', None),
                        ('delivery', None),
                        ('quota_result', None))

    def save(self):
        m = super(MilkProductionForm, self).save()
        c = Compute(m.study.pk)
        c.calculate_db_milkproduction(m.pk)
        tasks = (('calculate_db_economic_synthesis', []), )
        compute(m.study.pk, tasks, ['ESY'])

# economy


class CropPriceForm(BaseStudyForm):
    class Meta:
        model = models.HarvestSettingsSold
    RE_PRICE = re.compile('^price_[1-9P]$')
    harvest_list = forms.ModelChoiceField(
        label=_(u"Harvest"),
        queryset=models.Harvest.objects.filter(available=True))

    def __init__(self, *args, **kwargs):
        super(CropPriceForm, self).__init__(*args, **kwargs)
        self.fields['harvest'].widget = forms.HiddenInput()
        self.fields['harvest'].required = False
        if self.instance and self.instance.pk:
            self.fields['harvest_list'].initial = \
                self.instance.harvest.harvest.pk
            self.fields['harvest_list'].widget = widgets.ReadOnlyWidget(
                choices=self.fields['harvest_list'].choices)
            self.fields['usage_type'].widget = widgets.ReadOnlyWidget(
                choices=self.fields['usage_type'].choices)
        self.fields.keyOrder = ['harvest_list', 'usage_type', 'harvest',
                                'study']
        for year in xrange(1, self.study.transition_years + 2):
            initial = None
            if year > self.study.transition_years:
                year = 'P'
            if self.instance and self.instance.pk:
                try:
                    initial = models.HarvestSettingsSoldYear.objects.get(
                        year=year, harvest_sold=self.instance).price
                except ObjectDoesNotExist:
                    pass
            # initial_usage = ''
            self.fields['price_%s' % year] = \
                FrenchFloatField(initial=initial, required=False)

    def clean(self):
        cleaned_data = super(CropPriceForm, self).clean()
        if not cleaned_data.get('harvest'):
            hs, created = models.HarvestSettings.objects.get_or_create(
                study=self.study, harvest=cleaned_data.get('harvest_list'))
            cleaned_data['harvest'] = hs
        return cleaned_data

    def save(self, commit=True):
        if not self.cleaned_data:
            return
        hss = self.cleaned_data.get('id')
        if not hss:
            hss = models.HarvestSettingsSold.objects.create(
                harvest=self.cleaned_data.get('harvest'),
                usage_type=self.cleaned_data.get('usage_type'))
        for year in xrange(1, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            hsy, created = \
                models.HarvestSettingsSoldYear.objects.get_or_create(
                    year=year, harvest_sold=hss)
            value = self.cleaned_data.get('price_%s' % year) or 0
            hsy.price = value
            hsy.save()
        hss.harvest.calculate_db()


class CropPriceFormSet(BaseStudyFormSet):
    model = models.HarvestSettingsSold
    extra = 0

    def get_queryset(self, *args, **kwargs):
        q = self.model.objects.filter(harvest__study=self.study).order_by('pk')
        return q


class SimpleEcoInitialForm(FrenchModelForm):
    class Meta:
        model = models.Study
        exclude = (
            'name', 'farm', 'year', 'transition_years', 'farming_type',
            'forage_yield_total', 'forage_yield_project_total',
            'grain_yield_total', 'grain_yield_project_total',
            'straw_yield_total', 'straw_yield_project_total',
            'farming_type'
        )

    def __init__(self, *args, **kwargs):
        assert 'study' in kwargs
        self.study = kwargs.pop('study')
        kwargs['instance'] = self.study
        super(SimpleEcoInitialForm, self).__init__(*args, **kwargs)
        self.fields['annuities'].widget = widgets.ReadOnlyWidget()


class InitialExpenseDetailForm(BaseStudyForm):
    class Meta:
        model = models.SimpleEcoInitialExpenseDetail

    def __init__(self, *args, **kwargs):
        super(InitialExpenseDetailForm, self).__init__(*args, **kwargs)
        self.fields['type'].widget = widgets.ReadOnlyWidget(
            choices=models.EXPENSE_TYPES)


class InitialExpenseDetailFormSet(BaseStudyFormSet):
    model = models.SimpleEcoInitialExpenseDetail


class RevenuesForm(BaseForm):
    class Meta:
        model = models.Revenues
        exclude = ('reformed_cow_revenue', 'milk_revenue',
                   'calve_revenue', 'heifer_revenue')
    _hidden_fields = ('study', 'year', 'total_revenue')


class RevenuesFullEcoForm(BaseForm):
    class Meta:
        model = models.Revenues
    _hidden_fields = ('study', 'year', 'total_revenue')
    _readonly_fields = (('reformed_cow_revenue', None),
                        ('milk_revenue', None),
                        ('calve_revenue', None),
                        ('heifer_revenue', None))


class ExtraRevenueForm(BaseForm):
    class Meta:
        model = models.ExtraRevenue
    _hidden_fields = ('study', 'associated_type', 'full_eco')

    def __init__(self, *args, **kwargs):
        self.study = None
        if 'study' in kwargs:
            self.study = kwargs.pop('study')
        super(ExtraRevenueForm, self).__init__(*args, **kwargs)
        self.fields['study'].initial = self.study.pk


class SimpleEcoExtraRevenueForm(ExtraRevenueForm):
    def __init__(self, *args, **kwargs):
        super(SimpleEcoExtraRevenueForm, self).__init__(*args, **kwargs)
        self.fields['full_eco'].initial = False


class ExtraRevenueYearForm(BaseForm):
    class Meta:
        model = models.ExtraRevenueYear
    _hidden_fields = ('extra_revenue', 'year',)

    def __init__(self, *args, **kwargs):
        super(ExtraRevenueYearForm, self).__init__(*args, **kwargs)
        self.fields['total'] = forms.IntegerField(
            initial=self.instance.revenue, required=False,
            widget=widgets.ReadOnlyIntWidget())


class ExpensesForm(BaseForm):
    class Meta:
        model = models.Expenses
    _hidden_fields = ('study', 'year', 'veterinary_expenses',
                      'phytosanitary_expenses', 'fertilizer_expenses',
                      'calve_expenses', 'heifer_expenses', 'total_expenses',
                      'total_expenses_without_extra')

    def __init__(self, *args, **kwargs):
        super(ExpensesForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.year == 'D':
            self.fields['veterinary_cost'].widget = widgets.ReadOnlyWidget()
            self.fields['phytosanitary_cost'].widget = widgets.ReadOnlyWidget()
            self.fields['fertilizer_cost'].widget = widgets.ReadOnlyWidget()


class ExpensesFullEcoForm(BaseForm):
    class Meta:
        model = models.Expenses
    _hidden_fields = ('study', 'year', 'veterinary_expenses',
                      'phytosanitary_expenses', 'fertilizer_expenses',
                      'phytosanitary_cost', 'fertilizer_cost',
                      'veterinary_cost', 'total_expenses',
                      'total_expenses_without_extra')
    _readonly_fields = ('calve_expenses', 'heifer_expenses')


class ExtraExpenseForm(BaseForm):
    class Meta:
        model = models.ExtraExpenses
    _hidden_fields = ('study', 'associated_type', 'full_eco')

    def __init__(self, *args, **kwargs):
        self.study = None
        if 'study' in kwargs:
            self.study = kwargs.pop('study')
        super(ExtraExpenseForm, self).__init__(*args, **kwargs)
        self.fields['study'].initial = self.study.pk


class SimpleEcoExtraExpenseForm(ExtraExpenseForm):
    def __init__(self, *args, **kwargs):
        super(SimpleEcoExtraExpenseForm, self).__init__(*args, **kwargs)
        self.fields['full_eco'].initial = False


class ExtraExpenseYearForm(BaseForm):
    class Meta:
        model = models.ExtraExpenseYear
    _hidden_fields = ('extra_expense', 'year')

    def __init__(self, *args, **kwargs):
        # don't display unecessary 0
        if 'instance' in kwargs and kwargs['instance'].expense == 0:
            kwargs['instance'].expense = ''
        super(ExtraExpenseYearForm, self).__init__(*args, **kwargs)


class ExtraSupplyPriceYearSimpleForm(BaseForm):
    class Meta:
        model = models.ExtraSupplyPriceYear
    _hidden_fields = ('extra_supply_price', 'year', 'total')


class ExtraSupplyPriceYearForm(BaseForm):
    class Meta:
        model = models.ExtraSupplyPriceYear
    _hidden_fields = ('extra_supply_price', 'year')
    _readonly_fields = ('total',)


class InvestmentForm(BaseStudyForm):
    date = forms.DateField(widget=widgets.JQueryDate)

    class Meta:
        model = models.Investment

    def clean(self):
        study = self.cleaned_data['study']
        date = self.cleaned_data['date']
        if date.year < study.year:
            raise forms.ValidationError(
                _(u"Investment date cannot be before the date of the study."))
        return self.cleaned_data

    def save(self, *args, **kwargs):
        item = super(InvestmentForm, self).save(*args, **kwargs)
        c = Compute(item.study.pk)
        c.calculate_db_economic_synthesis()


class InvestmentFormSet(BaseStudyFormSet):
    model = models.Investment


class EconomicSynthesisForm(BaseForm):
    class Meta:
        model = models.EconomicSynthesis
    _hidden_fields = (
        'study', 'year', 'vegetal_revenues',
        'bovine_workshop_revenue', 'extra_revenues', 'full_total_revenue',
        'vegetal_expenses', 'bovine_workshop_expenses', 'operational_expenses',
        'full_gop', 'full_current_result', 'full_final_disposable_income',
        'full_final_disposable_income_by_man_work_unit', 'full_gop_by_revenue',
        'extra_expenses')

    def __init__(self, *args, **kwargs):
        super(EconomicSynthesisForm, self).__init__(*args, **kwargs)
        for field_key in self.fields:
            if field_key not in self._hidden_fields \
                    and field_key != 'man_work_unit':
                self.fields[field_key].widget = widgets.ReadOnlyIntWidget()


class CropSellingForm(BaseStudyForm):
    class Meta:
        model = models.HarvestSettingsSold
    RE_PRICE = re.compile('^price_[1-9P]$')
    harvest_list = forms.ModelChoiceField(
        label=_(u"Harvest"),
        queryset=models.Harvest.objects.filter(available=True))

    def __init__(self, *args, **kwargs):
        super(CropSellingForm, self).__init__(*args, **kwargs)
        self.hidden = False
        if not self.instance or not self.instance.pk:
            for k in self.fields.keys():
                self.fields[k].widget = forms.HiddenInput()
            self.hidden = True
            return
        self.fields['harvest_list'].initial = self.instance.harvest.harvest
        self.fields['harvest_list'].widget = widgets.ReadOnlyWidget(
            choices=self.fields['harvest_list'].choices)
        if not self.instance.salable:
            for k in self.fields.keys():
                self.fields[k].widget = forms.HiddenInput()
            self.hidden = True
            return
        self.fields['usage_type'].widget = widgets.ReadOnlyWidget(
            choices=self.fields['usage_type'].choices)
        self.fields['harvest'].widget = forms.HiddenInput()
        self.fields['harvest'].required = False
        self.fields.keyOrder = ['harvest_list', 'usage_type', 'harvest',
                                'study']
        target_type = self.instance.usage_type.target_type
        for year in xrange(1, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            year_detail, initial = None, ''
            q = self.instance.harvest.year_details.filter(year=year)
            year_detail = None
            if q.count():
                year_detail = q.all()[0]
                if q.count() > 1:
                    for yd in q.all()[1:]:
                        yd.delete()
            if year_detail:
                initial = getattr(year_detail, target_type + '_left')
            self.fields['available_%s' % year] = \
                FrenchFloatField(initial=initial, required=False)
            self.fields['available_%s' % year].widget = \
                widgets.ReadOnlyWidget()
            initial, hssy = '', None
            try:
                hssy = models.HarvestSettingsSoldYear.objects.get(
                    year=year, harvest_sold=self.instance)
                initial = hssy.sold
            except ObjectDoesNotExist:
                pass
            # initial_usage = ''
            self.fields['sold_%s' % year] = \
                FrenchFloatField(initial=initial, required=False)
            if not year_detail or not getattr(year_detail,
                                              target_type + '_yield_total'):
                self.fields['sold_%s' % year].widget = widgets.ReadOnlyWidget()
            initial = ''
            if hssy:
                initial = hssy.total
            self.fields['total_%s' % year] = \
                FrenchFloatField(initial=initial, required=False,
                                 widget=widgets.ReadOnlyWidget())
        return

    def save(self, commit=True):
        if not self.cleaned_data:
            return
        hss = self.cleaned_data.get('id')
        if not hss:
            return
        for year in xrange(1, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            hsy, created = \
                models.HarvestSettingsSoldYear.objects.get_or_create(
                    year=year, harvest_sold=hss)
            value = self.cleaned_data.get('sold_%s' % year) or 0
            hsy.sold = value
            hsy.save()
        hss.harvest.calculate_db()


class CropSellingFormSet(BaseStudyFormSet):
    model = models.HarvestSettingsSold
    extra = 0

    def get_queryset(self, *args, **kwargs):
        q = self.model.objects.filter(harvest__study=self.study).order_by('pk')
        return q

    def _construct_forms(self):
        returned = super(CropSellingFormSet, self)._construct_forms()
        self.hidden = not bool([1 for form in self.forms
                                if not form.hidden])
        return returned


class HarvestExpenseForm(BaseStudyForm):
    class Meta:
        model = models.HarvestSettingsExpense

    def __init__(self, *args, **kwargs):
        self.harvest = kwargs.pop('harvest')
        super(HarvestExpenseForm, self).__init__(*args, **kwargs)
        if not self.instance or not self.instance.pk:
            for k in self.fields.keys():
                self.fields[k].widget = forms.HiddenInput()
            return
        q = self.fields['harvest'].queryset
        self.fields['harvest'].queryset = q.filter(
            harvest_id=self.harvest.pk, study=self.study)
        self.fields['harvest'].widget = forms.HiddenInput()
        q = self.fields['expense_type'].queryset
        self.fields['expense_type'].widget = widgets.ReadOnlyWidget(
            choices=q.values_list('id', 'name'))
        for year in xrange(0, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            if year == 0:
                year = 'D'
            year_detail, created = self.instance.year_details.get_or_create(
                year=year)
            self.fields['price_%s' % year] = FrenchIntField(
                initial=year_detail.price or '', required=False)
            self.fields['total_%s' % year] = FrenchIntField(
                initial=year_detail.total, required=False,
                widget=widgets.ReadOnlyWidget())
        return

    def save(self, commit=True):
        if not self.cleaned_data:
            return
        hse = self.cleaned_data.get('id')
        if not hse:
            return
        for year in xrange(0, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            if year == 0:
                year = 'D'
            hsy, created = \
                models.HarvestSettingsExpenseYear.objects.get_or_create(
                    year=year, harvest_expense=hse)
            value = self.cleaned_data.get('price_%s' % year) or 0
            hsy.price = value
            hsy.save()
            hsy.calculate()
        hse.harvest.calculate_db()


class HarvestExpenseFormSet(BaseStudyFormSet):
    model = models.HarvestSettingsExpense
    extra = 0

    def get_queryset(self, *args, **kwargs):
        q = self.model.objects.filter(
            harvest__study=self.study,
            harvest__harvest=self.harvest).order_by('pk')
        return q


class NewWorkshopForm(BaseStudyForm):
    class Meta:
        model = models.Workshop

    _hidden_fields = ('study', 'available', 'order', 'txt_idx',)

    def __init__(self, *args, **kwargs):
        super(NewWorkshopForm, self).__init__(*args, **kwargs)
        self.fields['txt_idx'].initial = 'default'

    def save(self, commit=True):
        if not self.cleaned_data:
            return
        idx = 0
        txt_idx = unicode(self.study.pk) + u"-" + \
            slugify(self.cleaned_data['name']) + u"-" + unicode(idx)
        while models.Workshop.objects.filter(txt_idx=txt_idx).count():
            idx += 1
            txt_idx = u"-".join(txt_idx.split('-')[:-1]) + u"-" + unicode(idx)
        return models.Workshop.objects.create(
            study=self.cleaned_data['study'], txt_idx=txt_idx, available=False,
            order=idx, name=self.cleaned_data['name'])


class WorkshopForm(BaseStudyForm):
    class Meta:
        model = models.WorkshopExpenses

    def __init__(self, *args, **kwargs):
        self.workshop = kwargs.pop('workshop')
        super(WorkshopForm, self).__init__(*args, **kwargs)
        if not self.instance or not self.instance.pk:
            for k in self.fields.keys():
                self.fields[k].widget = forms.HiddenInput()
            return
        for k in ('available', 'order', 'workshop', 'txt_idx'):
            self.fields.pop(k)
        self.fields['name'].widget = widgets.ReadOnlyWidget()
        for year in xrange(0, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            if year == 0:
                year = 'D'
            year_detail, created = \
                self.instance.expenses_details.get_or_create(
                    study=self.study, year=year)
            self.fields['price_%s' % year] = FrenchFloatField(
                initial=year_detail.price or '', required=False)
            self.fields['quantity_%s' % year] = FrenchFloatField(
                initial=year_detail.quantity or '', required=False)
            self.fields['total_%s' % year] = FrenchFloatField(
                initial=year_detail.total, required=False,
                widget=widgets.ReadOnlyWidget())
        return

    def save(self, commit=True):
        if not self.cleaned_data:
            return
        wrk_exp = self.cleaned_data.get('id')
        if not wrk_exp:
            return
        for year in xrange(0, self.study.transition_years + 2):
            if year > self.study.transition_years:
                year = 'P'
            if year == 0:
                year = 'D'
            try:
                with transaction.commit_on_success():
                    wey = models.WorkshopExpensesYear.objects.create(
                        year=year, workshop=wrk_exp, study=self.study)
            except IntegrityError:
                wey = models.WorkshopExpensesYear.objects.get(
                    year=year, workshop=wrk_exp, study=self.study)
            price = self.cleaned_data.get('price_%s' % year)
            wey.price = int(price) if price else 0
            quantity = self.cleaned_data.get('quantity_%s' % year)
            wey.quantity = int(quantity) if quantity else 0
            wey.save()


class WorkshopFormSet(BaseStudyFormSet):
    model = models.WorkshopExpenses
    extra = 0

    def get_queryset(self, *args, **kwargs):
        q = self.model.objects.filter(workshop=self.workshop).order_by('order')
        return q


class WorkshopExpensesYearForm(BaseForm):
    class Meta:
        model = models.WorkshopExpensesYear
    _hidden_fields = ('workshop', 'year', 'study')

    def __init__(self, *args, **kwargs):
        super(WorkshopExpensesYearForm, self).__init__(*args, **kwargs)
        self.fields['total'] = forms.IntegerField(
            initial=self.instance.total, required=False,
            widget=widgets.ReadOnlyIntWidget())


class WorkshopExpensesForm(BaseForm):
    class Meta:
        model = models.WorkshopExpenses
    _hidden_fields = ("workshop", "available", "order", "txt_idx")

    def __init__(self, *args, **kwargs):
        self.workshop = None
        if 'workshop' in kwargs:
            self.workshop = kwargs.pop('workshop')
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'expenses'
        super(WorkshopExpensesForm, self).__init__(*args, **kwargs)
        self.fields['workshop'].initial = self.workshop.pk
        self.fields['txt_idx'].initial = 'default'

    def save(self, *args, **kwargs):
        if not self.cleaned_data:
            return
        model = models.WorkshopExpenses
        data = self.cleaned_data.copy()
        data['available'] = False
        idx = 0
        items = [unicode(data['workshop'].pk),
                 slugify(data["name"]),
                 unicode(idx)]
        txt_idx = u"-".join(items)
        while model.objects.filter(txt_idx=txt_idx).count():
            idx += 1
            items[-1] = unicode(idx)
            txt_idx = u"-".join(items)
        data['txt_idx'] = txt_idx
        return model.objects.create(**data)


class WorkshopRevenuesYearForm(BaseForm):
    class Meta:
        model = models.WorkshopRevenuesYear
    _hidden_fields = ('workshop', 'year', 'study')

    def __init__(self, *args, **kwargs):
        super(WorkshopRevenuesYearForm, self).__init__(*args, **kwargs)
        self.fields['total'] = forms.IntegerField(
            initial=self.instance.total, required=False,
            widget=widgets.ReadOnlyIntWidget())


class WorkshopRevenuesForm(BaseForm):
    class Meta:
        model = models.WorkshopRevenues
    _hidden_fields = ("workshop", "available", "order", "txt_idx")

    def __init__(self, *args, **kwargs):
        self.workshop = None
        if 'workshop' in kwargs:
            self.workshop = kwargs.pop('workshop')
        if 'prefix' not in kwargs:
            kwargs['prefix'] = 'revenues'
        super(WorkshopRevenuesForm, self).__init__(*args, **kwargs)
        self.fields['workshop'].initial = self.workshop.pk
        self.fields['txt_idx'].initial = 'default'

    def save(self, *args, **kwargs):
        if not self.cleaned_data:
            return
        model = models.WorkshopRevenues
        data = self.cleaned_data.copy()
        data['available'] = False
        idx = 0
        items = [unicode(data['workshop'].pk),
                 slugify(data["name"]),
                 unicode(idx)]
        txt_idx = u"-".join(items)
        while model.objects.filter(txt_idx=txt_idx).count():
            idx += 1
            items[-1] = unicode(idx)
            txt_idx = u"-".join(items)
        data['txt_idx'] = txt_idx
        return model.objects.create(**data)


class FullEconomicSynthesisForm(EconomicSynthesisForm):
    class Meta:
        model = models.EconomicSynthesis
    _hidden_fields = ('study', 'year', 'total_revenue', 'total_expenses',
                      'gop', 'current_result', 'final_disposable_income',
                      'final_disposable_income_by_man_work_unit',
                      'gop_by_revenue')

    def __init__(self, *args, **kwargs):
        super(FullEconomicSynthesisForm, self).__init__(*args, **kwargs)
        if not kwargs['instance']:
            return
        instance = kwargs['instance']
        ws_revenues, ws_expenses = [], []
        q = models.Workshop.objects.filter(study=instance.study)
        if not q.count():
            return
        for idx_ws, workshop in enumerate(q):
            lbl = _(u"Revenues") + u" " + workshop.name
            value = sum(
                [revenue.total
                 for revenue in models.WorkshopRevenuesYear.objects.filter(
                     year=instance.year, workshop__workshop=workshop)])
            field_idx = 'workshop-revenues-%d' % idx_ws
            ws_revenues.append(field_idx)
            self.fields[field_idx] = FrenchFloatField(
                label=lbl, initial=value, required=False,
                widget=widgets.ReadOnlyIntWidget())
            self.fields.keyOrder.pop(self.fields.keyOrder.index(field_idx))
            lbl = _(u"Expenses") + u" " + workshop.name
            value = sum(
                [expense.total
                 for expense in models.WorkshopExpensesYear.objects.filter(
                     year=instance.year, workshop__workshop=workshop)])
            field_idx = 'workshop-expenses-%d' % idx_ws
            ws_expenses.append(field_idx)
            self.fields[field_idx] = FrenchFloatField(
                label=lbl, initial=value, required=False,
                widget=widgets.ReadOnlyIntWidget())
            self.fields.keyOrder.pop(self.fields.keyOrder.index(field_idx))
        idx = self.fields.keyOrder.index('extra_revenues')
        self.fields.keyOrder = self.fields.keyOrder[:idx] + ws_revenues + \
            self.fields.keyOrder[idx:]
        idx = self.fields.keyOrder.index('extra_expenses')
        self.fields.keyOrder = self.fields.keyOrder[:idx] + ws_expenses + \
            self.fields.keyOrder[idx:]
