# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RotationDominant'
        db.create_table('simulabio_rotationdominant', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('study', self.gf('django.db.models.fields.related.ForeignKey')(related_name='rotation_dominant', to=orm['simulabio.Study'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('surface', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('length', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('elementary_surface', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('parcel_number', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('min_surface', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('avg_surface', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('max_surface', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('vehicule_access', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal('simulabio', ['RotationDominant'])

        # Adding model 'HarvestRotation'
        db.create_table('simulabio_harvestrotation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rotation_dominant', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.RotationDominant'])),
            ('harvest_setting', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.HarvestSettings'])),
            ('year_number', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('simulabio', ['HarvestRotation'])

        # Deleting field 'HarvestPlots.rotation_block'
        db.delete_column('simulabio_harvestplots', 'rotation_block')

        # Adding field 'HarvestPlots.rotation_dominant'
        db.add_column('simulabio_harvestplots', 'rotation_dominant',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simulabio.RotationDominant'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'RotationDominant'
        db.delete_table('simulabio_rotationdominant')

        # Deleting model 'HarvestRotation'
        db.delete_table('simulabio_harvestrotation')

        # Adding field 'HarvestPlots.rotation_block'
        db.add_column('simulabio_harvestplots', 'rotation_block',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)

        # Deleting field 'HarvestPlots.rotation_dominant'
        db.delete_column('simulabio_harvestplots', 'rotation_dominant_id')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'simulabio.farm': {
            'Meta': {'object_name': 'Farm'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'corporate_name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'geographical_zone': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.GeographicalZone']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '6', 'null': 'True', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'})
        },
        'simulabio.geographicalzone': {
            'Meta': {'object_name': 'GeographicalZone'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvest': {
            'Meta': {'object_name': 'Harvest'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestCategory']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvestcategory': {
            'Meta': {'object_name': 'HarvestCategory'},
            'color': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'simulabio.harvestplots': {
            'Meta': {'object_name': 'HarvestPlots'},
            'area': ('django.db.models.fields.FloatField', [], {}),
            'geocode': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'geographical_area': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plot_n'", 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n1': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n1'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'harvest_n2': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'harvest_plot_n2'", 'null': 'True', 'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'remark': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'rotation_dominant': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.RotationDominant']", 'null': 'True', 'blank': 'True'}),
            'soil_type': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_plots'", 'to': "orm['simulabio.Study']"}),
            'vehicule_access': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'simulabio.harvestrotation': {
            'Meta': {'object_name': 'HarvestRotation'},
            'harvest_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.HarvestSettings']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rotation_dominant': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.RotationDominant']"}),
            'year_number': ('django.db.models.fields.IntegerField', [], {})
        },
        'simulabio.harvestsettings': {
            'Meta': {'ordering': "('order',)", 'object_name': 'HarvestSettings'},
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'forage_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'grain_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'straw_yield_project': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'harvest_settings'", 'to': "orm['simulabio.Study']"})
        },
        'simulabio.referenceharvest': {
            'Meta': {'object_name': 'ReferenceHarvest'},
            'forage_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'geographical_zone': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reference_harvest'", 'to': "orm['simulabio.GeographicalZone']"}),
            'grain_yield': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'harvest': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simulabio.Harvest']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'straw_yield': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        'simulabio.rotationdominant': {
            'Meta': {'object_name': 'RotationDominant'},
            'avg_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'elementary_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'harvest_setting_years': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['simulabio.HarvestSettings']", 'through': "orm['simulabio.HarvestRotation']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'length': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'max_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'min_surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'parcel_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'study': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'rotation_dominant'", 'to': "orm['simulabio.Study']"}),
            'surface': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'vehicule_access': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        'simulabio.simulabiouser': {
            'Meta': {'object_name': 'SimulabioUser'},
            'farms': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'simulabio_user'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['simulabio.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simulabio_user'", 'unique': 'True', 'to': "orm['auth.User']"})
        },
        'simulabio.study': {
            'Meta': {'object_name': 'Study'},
            'farm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'study'", 'to': "orm['simulabio.Farm']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_diagnostic': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'year': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['simulabio']