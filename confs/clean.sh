#!/bin/bash

CUR_DIR="$(dirname "$0")"
cd $CUR_DIR

# only lower alpha-numeric char and '_' and must start with a letter
[ $APP_NAME ] || ( echo 'APP_NAME empty. Default to: "default".' ; );
APP_NAME=${APP_NAME-default}


read -r -p "Are you sure to delete "$APP_NAME" app? [y/N] " response
response=${response,,}    # tolower
if [[ $response =~ ^(yes|y)$ ]]
then
    echo '* deleting '$APP_NAME;
else
    echo "canceled";
    exit;
fi

MAIN_PATH=`readlink -f ..`'/'
APP_PATH=$MAIN_PATH$APP_NAME'/'
CONF_PATH=$MAIN_PATH"confs/"$APP_NAME"/"

# default for debian
UWSGI_AVAILABLE_PATH='/etc/uwsgi/apps-available/'
UWSGI_ENABLE_PATH='/etc/uwsgi/apps-enabled/'
NGINX_AVAILABLE_PATH='/etc/nginx/sites-available/'
NGINX_ENABLE_PATH='/etc/nginx/sites-enabled/'
SUPERVISOR_PATH='/etc/supervisor/conf.d/'

rm $SUPERVISOR_PATH$APP_NAME'.conf'
rm $UWSGI_AVAILABLE_PATH$APP_NAME'-uwsgi.ini'
rm $UWSGI_ENABLE_PATH$APP_NAME'-uwsgi.ini'
rm $NGINX_AVAILABLE_PATH$APP_NAME'-nginx.conf'
rm $NGINX_ENABLE_PATH$APP_NAME'-nginx.conf'

rm -rf $APP_PATH
rm -rf $CONF_PATH

service supervisor restart
service uwsgi restart
service nginx restart
