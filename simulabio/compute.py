#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2014 Étienne Loks <etienne.loks_AT_peacefrogsDOTnet>

import datetime
import copy

from django.conf import settings
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

import models


class Compute(object):
    def __init__(self, study_pk, follow_time=False):
        self.follow_time = follow_time
        if follow_time:
            self.now = datetime.datetime.now()
        self._start_timer()
        self.cache_key = 'study-%s' % unicode(study_pk)
        self.study = cache.get(self.cache_key)
        self.ref_study = copy.deepcopy(self.study)
        self._study = models.Study.objects.get(pk=study_pk)
        if settings.INTERNAL_CACHE:
            if not self.study:
                self.refresh_cache(study_pk)
            else:
                self.ref_study = copy.deepcopy(self.study)

    def refresh_cache(self, study_pk=None):
        if not study_pk and self.study and 'id' in self.study:
            study_pk = self._study.pk
        try:
            self.study = models.Study.objects.get(pk=study_pk)\
                                             .get_serialized(full=True)
        except ObjectDoesNotExist:
            return
        cache.set(self.cache_key, self.study, settings.CACHE_TIMEOUT)
        self.ref_study = copy.deepcopy(self.study)
        self._timer_step(u"Study set to cache")

    def _start_timer(self):
        if not self.follow_time:
            return
        self._now = datetime.datetime.now()

    def _timer_step(self, msg=''):
        if not self.follow_time:
            return
        now = datetime.datetime.now()
        print(now - self._now, msg)
        self._now = now

    def _browse_saving(self, study, ref_study):
        c_object = None
        model = getattr(models, study['MODEL'])
        to_be_created, model_list = {}, None
        for k in study:
            if k == 'MODEL':
                continue
            value = study[k]
            if type(value) == dict:
                self._browse_saving(value, ref_study[k])
            elif not model._cache_saved:
                continue
            elif value is None or type(value) in (int, bool, float,
                                                  str, unicode,):
                if value != ref_study.get(k):
                    if not c_object:
                        try:
                            c_object = model.objects.get(pk=self._study.pk)
                        except ObjectDoesNotExist:
                            study = None
                            return  # manual action on the db may cause this
                    setattr(c_object, k, value)
            elif type(value) == list:
                if value:
                    model_list = getattr(models, value[0]['MODEL'])
                elif ref_study[k]:
                    model_list = getattr(models, ref_study[k][0]['MODEL'])
                else:
                    continue
                ref_study_items = dict(
                    [(item['id'], item) for item in ref_study[k]
                     if 'id' in item])
                n_items = [item['id'] for item in value if item.get('id')][:]
                for item in value:
                    if not item.get('id'):
                        vals = item.copy()
                        vals.pop('MODEL')
                        if model_list not in to_be_created:
                            to_be_created[model_list] = []
                        to_be_created[model_list].append(vals)
                    elif item['id'] in ref_study_items:
                        self._browse_saving(item, ref_study_items[item['id']])
                for ref_id in ref_study_items.keys():
                    if ref_id not in n_items:
                        try:
                            model_list.objects.get(pk=ref_id)\
                                              .delete(from_cache=True)
                        except ObjectDoesNotExist:
                            pass
        if to_be_created:
            for md in to_be_created:
                # print "CREATED", md
                try:
                    md.objects.bulk_create(
                        [md(**vls) for vls in to_be_created[md]])
                except ObjectDoesNotExist:
                    # manual action on the db may cause this try one by one
                    try:
                        for vals in to_be_created[md]:
                            md.objects.bulk_create([md(**vals)])
                    except ObjectDoesNotExist:
                        pass
        elif c_object:
            k = (c_object.__class__.__name__, c_object.pk)
            if k in self.saved_objects:
                return
            self.saved_objects.add(k)
            # print 'UPDATE', c_object.__class__.__name__
            try:
                c_object.save(from_cache=True)
            except ObjectDoesNotExist:
                pass  # manual action on the db may cause this

    def initialize_study(self, save=False):
        self.initialize_db_transitiontypes()
        self.initialize_harvestsettings()
        self.initialize_economic()
        self.initialize_herd()
        models.StudyLock.remove_lock(self._study.pk, 'SIT')

    def initialize_transitiontypes(self):
        if self.study['transition_years'] < 1:
            return
        current_years = [
            transition_type['year']
            for transition_type in self.study['transition_type']]
        default_transition_type = {'MODEL': 'TransitionType',
                                   'study_id': self._study.pk,
                                   'yield_type': 'DP'}
        for year in xrange(1, self.study['transition_years'] + 1):
            if year not in current_years:
                tt = default_transition_type.copy()
                tt['year'] = year
                self.study['transition_type'].append(tt)

    def initialize_db_transitiontypes(self):
        if self._study.transition_years < 1:
            return
        vals = {'study': self._study}
        for year in xrange(1, self._study.transition_years + 1):
            vals['year'] = year
            for tt in models.TransitionType.objects\
                                           .filter(**vals).all()[1:]:
                tt.delete()
            defaults = vals.copy()
            defaults['defaults'] = {'yield_type': 'DP'}
            models.TransitionType.objects.get_or_create(**defaults)

    def initialize_harvestsettings(self):
        q = models.Harvest.objects\
                          .filter(available=True)\
                          .exclude(harvest_settings__study=self._study)
        for harvest in q.all():
            forage_yield, grain_yield, straw_yield = 0, 0, 0
            farm = self._study.farm
            ref_harvest = None
            if farm.geographical_zone:
                ref_harvest = farm.geographical_zone.reference_harvest.filter(
                    harvest=harvest)
            if not ref_harvest or not ref_harvest.count():
                gz = models.GeographicalZone.objects.filter(default=True)
                if gz.count():
                    ref_harvest = models.ReferenceHarvest.objects.filter(
                        geographical_zone=gz, harvest=harvest)
            if ref_harvest and ref_harvest.count():
                ref_harvest = ref_harvest.all()[0]
                forage_yield = ref_harvest.forage_yield
                grain_yield = ref_harvest.grain_yield
                straw_yield = ref_harvest.straw_yield
            models.HarvestSettings.objects.get_or_create(
                study=self._study,
                harvest=harvest,
                defaults={
                    'forage_yield_project': forage_yield,
                    'grain_yield_project': grain_yield,
                    'straw_yield_project': straw_yield})

    def initialize_economic(self):
        for exp_type in models.ExtraExpenseType\
                              .objects.filter(available=True)\
                              .exclude(extra_expenses__study=self._study)\
                              .all():
            models.ExtraExpenses.objects.create(study=self._study,
                                                name=exp_type.name,
                                                associated_type=exp_type)
        for rev_type in models.ExtraRevenueType\
                              .objects.filter(available=True)\
                              .exclude(extra_revenues__study=self._study)\
                              .all():
            models.ExtraRevenue.objects.create(study=self._study,
                                               name=rev_type.name,
                                               associated_type=rev_type)
        for expense_type, lbl in models.EXPENSE_TYPES:
            q = models.SimpleEcoInitialExpenseDetail.objects.filter(
                type=expense_type, study=self._study)
            if not q.count():
                models.SimpleEcoInitialExpenseDetail.objects.create(
                    type=expense_type, study=self._study)

    def initialize_herd(self):
        herd, created = models.Herd.objects.get_or_create(study=self._study)
        feed_updated = []
        for year in self._study.get_years():
            herd_detail, created = models.HerdDetail.objects.get_or_create(
                herd=herd, year=year)
            for animal, lbl in models.ANIMALS:
                straw_usage, created = models.StrawUsage.objects.get_or_create(
                    herd_detail=herd_detail, animal=animal)
                feed_dct = {'study': self._study, 'year': year,
                            'animal': animal}
                if models.Feed.objects.filter(**feed_dct).count():
                    continue
                q = models.FeedTemplate.objects.filter(animal_default=animal)
                for extra in q.all()[1:]:
                    extra.delete()  # clean db messup
                if not q.count():
                    continue
                feed_dct['defaults'] = {'feed_template': q.all()[0]}
                feed, created = models.Feed.objects.get_or_create(**feed_dct)
                if created:
                    feed_updated.append(feed)
            c = Compute(self._study.pk)
        if feed_updated:
            for feed in feed_updated:
                c.update_db_feeddetails(feed)

    def clean_harvests(self):
        self.clean_data(self.study['harvest_categories'],
                        ('year', 'harvest_category_id'))
        self.clean_data(self.study['harvest_settings'], ('harvest',))

    def save(self):
        if not settings.INTERNAL_CACHE:
            return
        self.clean_harvests()
        self.saved_objects = set()
        self._browse_saving(self.study, self.ref_study)
        self.ref_study = copy.deepcopy(self.study)
        self._start_timer()
        models.HarvestCategoryStudy.clean_duplicates(
            {'study__pk': self._study.pk})
        models.HarvestSettings.clean_duplicates(
            {'study__pk': self._study.pk})
        models.HarvestSettingsYear.clean_duplicates(
            {'harvest__study__pk': self._study.pk})
        self._timer_step()

    def calculate_harvestsettings(self, harvest_settings=None):
        changed = False
        if harvest_settings:
            changed = models.HarvestSettings.calculate(self.study,
                                                       harvest_settings)
        else:
            for hs in self.study['harvest_settings']:
                changed = models.HarvestSettings.calculate(self.study, hs) or\
                    changed
        return changed

    def calculate_db_harvestsettings(self, harvest_settings=None):
        changed = False
        if harvest_settings:
            changed = harvest_settings.calculate_db()
        else:
            for hs in models.HarvestSettings.objects.filter(
                    study=self._study):
                changed = hs.calculate_db() or changed
        return changed

    def calculate_db_harvest(self, harvest_settings=None):
        self._start_timer()
        changed = False
        study_cond = {'study': self._study}
        q = models.RotationDominant.objects.filter(**study_cond)
        if harvest_settings:
            q = q.filter(
                rotation_dominant_harvest__harvest=harvest_settings.harvest)
        for rd in q.all():
            rd.generate_pie()
            changed = rd.calculate_db() or changed
        changed = self.calculate_db_harvestsettings(harvest_settings) \
            or changed
        years = ['D', 'P'] + range(1, self._study.transition_years + 1)
        values, harvest_categories = [], []
        if not harvest_settings:
            harvest_categories = models.HarvestCategory.get_cached()
        else:
            harvest_categories = models.HarvestCategory.get_cached(
                harvests__id=harvest_settings.harvest.pk)
        for category in harvest_categories:
            values += [(category.pk, unicode(year_idx)) for year_idx in years]
        for hc in models.HarvestCategoryStudy.objects.filter(**study_cond)\
                                                     .all():
            key = (hc.harvest_category.pk, unicode(hc.year))
            if key in values:
                values.pop(values.index(key))
            hc.calculate_db()
        for harvest_category_id, year in values:
            vals = {'study': self._study, 'year': year,
                    'harvest_category_id': harvest_category_id}
            hc, created = models.HarvestCategoryStudy.objects.get_or_create(
                **vals)
            if created:
                hc.calculate_db()

        models.StudyLock.remove_lock(self._study.pk, 'HHA')
        self._timer_step(u"Harvest %s calculated" % (
            unicode(harvest_settings or "All")))

    def calculate_harvest(self, harvest_settings=None, save=False):
        for rd in self.study['rotation_dominant']:
            if not harvest_settings or \
                    [1 for harvest in rd['rotation_dominant_harvest']
                     if harvest['harvest_id'] ==
                     harvest_settings['harvest_id']]:
                models.RotationDominant.calculate(self.study, rd)
        self.calculate_harvestsettings(harvest_settings)
        years = ['D', 'P'] + range(1, self.study['transition_years'] + 1)
        values, harvest_categories = [], []
        if not harvest_settings:
            harvest_categories = models.HarvestCategory.get_cached()
        else:
            harvest_categories = models.HarvestCategory.get_cached(
                harvests__id=harvest_settings['harvest_id'])
        for category in harvest_categories:
            values += [(category.pk, unicode(year_idx)) for year_idx in years]
        for hc in self.study['harvest_categories']:
            key = (hc['harvest_category_id'], unicode(hc['year']))
            if key in values:
                values.pop(values.index(key))
            models.HarvestCategoryStudy.calculate(self.study, hc)
        for harvest_category_id, year in values:
            hc = {'study_id': self._study.pk, 'year': year,
                  'harvest_category_id': harvest_category_id,
                  'MODEL': 'HarvestCategoryStudy'}
            self.study['harvest_categories'].append(hc)
            models.HarvestCategoryStudy.calculate(self.study, hc)
        if save:
            self.save()
        """
        q =  models.HarvestCategoryStudy.objects.filter(**vals)
        if q.count() > 1:
            for obj in q.all()[1:]:
                obj.delete()"""

    # try to find and remove duplicates in the cache
    def clean_data(self, lst, keys):
        items = []
        to_delete = []
        for idx, data in enumerate(lst):
            key = tuple([data[k] for k in keys])
            if key in items:  # keep the first one
                to_delete.append(idx)
        for idx in reversed(to_delete):
            lst.pop(idx)

    def calculate_milkproduction(self, milk_production_id=None, save=False):
        self.clean_data(self.study['milk_production'], ('year',))
        for milk_production in self.study['milk_production']:
            if not milk_production_id or \
               milk_production_id == milk_production['id']:
                models.MilkProduction.calculate(self.study, milk_production)
        self.calculate_revenues()
        if save:
            self.save()

    def calculate_db_milkproduction(self, milk_production_id=None, save=False):
        q = models.MilkProduction.objects.filter(study=self._study)
        if milk_production_id:
            q = q.filter(pk=milk_production_id)
        for milk_production in q.all():
            milk_production.calculate_db()
        self.calculate_db_revenues()
        if save:
            self.save()
        models.StudyLock.remove_lock(self._study.pk, 'AMK')

    def calculate_db_pastureavailable(self, year=None):
        years = []
        if not year:
            years = ['D', 'P'] + range(1, self._study.transition_years + 1)
        else:
            years = [year]
        for year in years:
            pa, created = models.PastureAvailable.objects.get_or_create(
                study=self._study, year=year)
            pa.calculate()
        models.StudyLock.remove_lock(self._study.pk, 'HPA')

    def calculate_herddetail(self, herddetail_id, save=False):
        herddetail = None
        for hd in self.study['herd'][0]['herd_detail']:
            if hd['id'] == herddetail_id:
                herddetail = hd
                break
        else:
            return
        models.HerdDetail.calculate(self.study, herddetail)
        if save:
            self.save()

    def calculate_db_herddetail(self, herddetail):
        herddetail.calculate_db()

    def update_db_feeddetails(self, feed):
        # update/create feed details when changing template
        if feed.year == 'D' and not feed.feed_template:
            ft = models.FeedTemplate.get_cached(default_for_diag=True)
            if ft:
                ft = ft[0]
            else:
                ft = models.FeedTemplate.create_default_for_diag()
            feed.feed_template = ft.pk
        if not feed.id or not feed.feed_template:
            return
        ft = models.FeedTemplate.get_cached(pk=feed.feed_template.id)[0]
        models.FeedDetail.objects.filter(feed=feed).delete()

        template_details_dct = dict(
            [(detail.feed_item.pk, detail)
             for detail in ft.feed_template_details.all()])
        feed_details = []
        for feed_item_id in template_details_dct:
            tpl = template_details_dct[feed_item_id]
            feed_detail = {'feed': feed,
                           'feed_item_id': feed_item_id}
            for month_nb in xrange(1, 13):
                amount_key = 'amount_%d' % month_nb
                feed_detail[amount_key] = getattr(tpl, amount_key)
            feed_details.append(models.FeedDetail(**feed_detail))
        models.FeedDetail.objects.bulk_create(feed_details)

    def update_feeddetails(self, year, animal, template_id, save=False):
        # doesn't work as expected: use update_db_feeddetails
        feed = None
        to_clean = []
        for idx, f in enumerate(self.study['feed']):
            if f['year'] != year or f['animal'] != animal:
                continue
            if feed or f['feed_template_id'] != template_id:
                to_clean.append(idx)
            else:
                feed = f
        for idx in reversed(to_clean):  # TODO: purge shouldn't be needed but..
            self.study['feed'].pop(idx)

        # update/create feed details when changing template
        if feed['year'] == 'D' and not feed['feed_template_id']:
            ft = models.FeedTemplate.get_cached(default_for_diag=True)
            if ft:
                ft = ft[0]
            else:
                ft = models.FeedTemplate.create_default_for_diag()
            feed["feed_template_id"] = ft.pk
        if not feed['id'] or not feed['feed_template_id']:
            return
        ft = models.FeedTemplate.get_cached(pk=feed['feed_template_id'])[0]
        template_details_dct = dict(
            [(detail.feed_item.pk, detail)
             for detail in ft.feed_template_details.all()])
        feed_details = []
        for feed_item_id in template_details_dct:
                # if feed_item_id not in feed_item_present:
                tpl = template_details_dct[feed_item_id]
                feed_detail = {'feed_id': feed['id'],
                               'feed_item_id': feed_item_id,
                               'MODEL': 'FeedDetail'}
                for month_nb in xrange(1, 13):
                    amount_key = 'amount_%d' % month_nb
                    feed_detail[amount_key] = getattr(tpl, amount_key)
                feed_details.append(feed_detail)
        feed['feed_details'] = feed_details
        self.save()

    def update_db_feedneeds(self, year):
        self._start_timer()
        harvest_cat_tuples = [
            (fd.feed_item.harvest_category, fd.feed_item.usage_type)
            for fd in models.FeedDetail.objects
                                       .filter(feed__study=self._study)
                                       .all()
            if fd.feed_item.harvest_category]
        # TODO:if feed_item.harvest_category why?

        # force litter management
        cereal = models.HarvestCategory.get_cached(txt_idx='paille')
        if not cereal:
            cereal = models.HarvestCategory.objects.create(txt_idx='paille',
                                                           name=_(u"Straw"),
                                                           color='ffff00')
        else:
            cereal = cereal[0]
        straw = models.UsageType.get_cached(txt_idx='paille')
        if not straw:
            straw = models.UsageType.objects.create(txt_idx='paille',
                                                    name=_(u"Straw"),
                                                    target_type='straw')
        else:
            straw = straw[0]
        if (cereal.pk, straw.pk) not in [
           (hct[0].pk, hct[1].pk)for hct in harvest_cat_tuples]:
            harvest_cat_tuples.append((cereal, straw))
        current_harvest_cat_usages = []

        for harvest_cat, usage_type in harvest_cat_tuples:
            harvest_category_usage = models.HarvestCategoryUsageType\
                .get_cached(
                    harvest_category=harvest_cat, usage_type=usage_type)
            if harvest_category_usage:
                harvest_category_usage = harvest_category_usage[0]
            else:
                harvest_category_usage = \
                    models.HarvestCategoryUsageType.objects.create(
                        harvest_category=harvest_cat,
                        usage_type=usage_type)
            query_args = {'study': self._study,
                          'year': year,
                          'harvest_category_usage': harvest_category_usage}
            current_harvest_cat_usages.append(harvest_category_usage)
            # TODO: this cannot be possible but...
            q = models.FeedNeed.objects.filter(**query_args)
            count = q.count()
            fn = None
            if count > 1:
                for f in models.FeedNeed.objects\
                                        .filter(**query_args)\
                                        .order_by('-pk').all()[1:]:
                    f.delete()
                fn = q.all()[0]
            elif not count:
                fn, created = models.FeedNeed.objects.get_or_create(
                    **query_args)
            else:
                fn = q.all()[0]
            fn.calculate_db()
            if not fn.amount:
                fn.delete()

        # remove feed needs which are no more needed
        query_args = {'study': self._study,
                      'year': year, }
        q = models.FeedNeed.objects\
            .filter(**query_args)\
            .exclude(harvest_category_usage__in=current_harvest_cat_usages)
        for fn in q.all():
            fn.delete()

        # TODO: only calculate on changed
        for hcs in models.HarvestCategoryStudy.objects.filter(
                study=self._study, year=year).all():
            hcs.calculate_db()
        # calculate pasture available
        p, c = models.PastureAvailable.objects.get_or_create(study=self._study,
                                                             year=year)
        p.calculate()
        models.StudyLock.remove_lock(self._study.pk, 'AFN')
        self._timer_step('Feed need (db) - year: %s' % unicode(year))

    def calculate_supply(self, supply_id, save=False):
        for fn in self.study['feed_need']:
            for sp in fn['supply']:
                if sp['id'] == supply_id:
                    supply = sp
        else:
            return
        if not supply['harvest_settings_id'] or not supply['feed_need_id']:
            return
        for harvest_settings in self.study['harvest_settings']:
            if harvest_settings['id'] == supply['harvest_settings_id']:
                self.calculate_harvest(harvest_settings)
                break
        for feed_need in self.study['feed_need']:
            if feed_need['id'] == supply['feed_need_id']:
                models.FeedNeed.calculate(self.study, feed_need)
        if save:
            self.save()

    def calculate_db_supply(self, supply_id):
        q = models.FeedNeedSupply.objects\
                                 .filter(pk=supply_id,
                                         feed_need__study=self._study)
        if not q.count():
            return
        supply = q.all()[0]
        if not supply.harvest_settings or not supply.feed_need:
            return
        self.calculate_db_harvest(supply.harvest_settings)
        supply.feed_need.calculate_db()

    def initialize_extrasupplyprices(self):
        to_create = []
        q = models.ExtraSupply\
                  .objects\
                  .filter(extra_supply__feed_need__study=self._study)\
                  .exclude(price__study=self._study)
        for extra_supply in q.distinct().all():
            to_create.append(models.ExtraSupplyPrice(extra_supply=extra_supply,
                                                     study=self._study))
        if to_create:
            models.ExtraSupplyPrice.objects.bulk_create(to_create)
            return True

    def calculate_extrasupply(self, extra_supply_id, save=False):
        for fn in self.study['feed_need']:
            for sp in fn['extra_supply']:
                if sp['id'] == extra_supply_id:
                    extra_supply = sp
        else:
            return
        for feed_need in self.study['feed_need']:
            if feed_need['id'] == extra_supply['feed_need_id']:
                models.FeedNeed.calculate(self.study, feed_need)
        if save:
            self.save()

    def calculate_db_extrasupply(self, extra_supply_id):
        for fn in models.FeedNeed.objects.filter(
                extra_supply__pk=extra_supply_id):
            fn.calculate_db()

    def calculate_revenues(self, revenue_pk=None, save=False):
        revenues = []
        if revenue_pk:
            for rv in self.study['revenues']:
                if rv['id'] == revenue_pk:
                    revenues = [rv]
                    break
            else:
                return
        else:
            revenues = self.study['revenues']
        for revenue in revenues:
            models.Revenues.calculate(self.study, revenue)
        if save:
            self.save()

    def calculate_db_revenues(self, revenue_pk=None):
        q = models.Revenues.objects.filter(study__pk=self._study.pk)
        if revenue_pk:
            q = q.filter(pk=revenue_pk)
        for revenue in q.all():
            revenue.calculate_db()
        models.StudyLock.remove_lock(self._study.pk, 'ERV')

    def calculate_expenses(self, expense_pk=None, save=False):
        expenses = []
        if expense_pk:
            for xp in self.study['expenses']:
                if xp['id'] == expense_pk:
                    expenses = [xp]
                    break
            else:
                return
        else:
            expenses = self.study['expenses']
        for expense in expenses:
            models.Expenses.calculate(self.study, expense)
        if save:
            self.save()

    def calculate_db_expenses(self, expense_pk=None):
        q = models.Expenses.objects.filter(study__pk=self._study.pk)
        if expense_pk:
            q = q.filter(pk=expense_pk)
        for expense in q.all():
            expense.calculate_db()
        models.StudyLock.remove_lock(self._study.pk, 'EEX')

    def calculate_db_annuities(self):
        models.Study.objects.get(id=self._study.pk).calculate_db_annuities()

    def calculate_economic_synthesis(self, eco_synth_id=None, save=False):
        for eco_synth in self.study['economic_synthesis']:
            if not eco_synth_id or eco_synth_id == eco_synth['id']:
                models.EconomicSynthesis.calculate(self.study, eco_synth)
                if eco_synth_id:
                    break
        if save:
            self.save()

    def calculate_db_economic_synthesis(self, eco_synth_id=None):
        self.calculate_db_expenses()
        self.calculate_db_revenues()
        q = models.EconomicSynthesis.objects.filter(study__pk=self._study.pk)
        if eco_synth_id:
            q = q.filter(pk=eco_synth_id)
        # the order to proceed is important
        years = ['D'] + \
            [unicode(yr) for yr in xrange(1,
                                          (self._study.transition_years + 1))]\
            + ['P']
        ess = [(years.index(es.year), es)
               for es in q.all() if es.year in years]
        for idx, eco_synth in sorted(ess, key=lambda x: x[0]):
            eco_synth.calculate_db()
        models.StudyLock.remove_lock(self._study.pk, 'ESY')
