#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013  Étienne Loks  <etienne.loks_AT_proxienceDOTcom>

from django.core.management.base import BaseCommand, CommandError

from simulabio import imports

class Command(BaseCommand):
    args = '<filename>'
    help = "Import feed templates from a csv file"

    def handle(self, *args, **options):
        if not args or not args[0]:
            raise CommandError("No file provided.")
        filename = args[0]
        errors = imports.import_feed(filename)
