#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015  Étienne Loks  <etienne.loks_AT_proxienceDOTcom>

import sys

from django.core.management.base import BaseCommand, CommandError

from simulabio import models

class Command(BaseCommand):
    help = "Clean the database"

    def handle(self, *args, **options):
        for model in [models.HarvestSettings]:
            idx = 0
            for item in model.objects.all():
                cleaned = item.clean()
                if cleaned:
                    idx += 1
            sys.stdout.write('* %d %s cleaned\n' % (idx, str(model)))
